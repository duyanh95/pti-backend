#!/bin/bash
set -a
  . .env.dev
set +a

cd bhty
if [ $1 = 'run' ]
then
  gradle test bootRun
elif [ $1 = 'test' ]
then
  gradle test
else
  exit 1
fi

