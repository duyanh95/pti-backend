package vn.anvui.api;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import vn.anvui.bsn.auth.AnvuiAuthenticationToken;
import vn.anvui.bsn.auth.CredentialService;
import vn.anvui.bsn.beans.notification.NotificationService;
import vn.anvui.bsn.dto.account.LoginRequest;
import vn.anvui.bsn.dto.notification.NotificationDto;
import vn.anvui.dt.entity.Notification;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.DeviceType;
import vn.anvui.dt.enumeration.NotificationTypeEnum;
import vn.anvui.dt.enumeration.UserNotificationType;
import vn.anvui.dt.repos.fcmNotification.DeviceRepository;
import vn.anvui.dt.repos.notification.NotificationTypeRepository;
import vn.anvui.dt.repos.user.UserRepository;

@Ignore("token changed")
public class FCMServiceTest extends SpringTest {
Logger log = Logger.getLogger("FCM");
    
    @Autowired
    UserRepository userRepo;
    
    @Autowired
    DeviceRepository deviceRepo;
    
    @Autowired
    NotificationService notiService;
    
    @Autowired 
    CredentialService credentialSv;
    
    @Autowired
    NotificationTypeRepository notiTypeRepo; 
    
    public static final String token = "eqvBmHFIVDk:APA91bHxVTcWQ3h1o7SUcOgR4As2yeCW37fJFImmB_tNjmEDMLRuNXQslU0nlzscDEkrf0-ooEWQ_W6CfIfob0qlLfVDAu6PI5Y-n6V1TgU28T3SZlPvhR-DTlB_B_QK3Vc88VmRI_qY";
    
    @Test
    @Transactional
    public void testUserTopic() {
        User user = userRepo.findFirstByUserName("admin");
        
        notiService.addUserToken(token, user.getId(), DeviceType.ANDROID);
        assertEquals(deviceRepo.findFirstByToken(token).getToken(), token);
        
        notiService.subscribeSystemTopic(Arrays.asList(token));
    }
    
    NotificationDto notiDto;
    
    @Before
    public void setupNotificaion() {
        Map<String, String> fields = new HashMap<>();
        
        notiDto = new NotificationDto(NotificationTypeEnum.REGISTER_FAIL.getValue(), 1,
                UserNotificationType.CUSTOMER.getValue(), fields);
        
        LoginRequest req = new LoginRequest();
        AnvuiAuthenticationToken authentication = credentialSv.store(userRepo.findOne(1), req);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    
    @Test
    @Transactional
    public void testNotification() {
        notiService.createAndSendNotification(notiDto);
        
        Notification noti = notiService.listNotification(0, 1, false).get(0);
        assertEquals(noti.getContent(), 
                notiTypeRepo.findOne(NotificationTypeEnum.REGISTER_FAIL.getValue()).getContent());
    }
}
