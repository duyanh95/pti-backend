package vn.anvui.dt.repos.history;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.History;

public interface HistoryRepository extends CrudRepository<History, Integer>, JpaRepository<History, Integer> {
    public List<History> findAllByUserId(Integer userId, Pageable page);
}
