package vn.anvui.dt.repos.contactAssignment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.ContactAssignment;

public interface ContactAssignmentRepository extends CrudRepository<ContactAssignment, Integer>,
    JpaRepository<ContactAssignment, Integer> {

}
