package vn.anvui.dt.repos.Province;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.Province;

public interface ProvinceRepository extends JpaRepository<Province, Integer>, 
        CrudRepository<Province, Integer> {
}
