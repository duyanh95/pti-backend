package vn.anvui.dt.repos.contract.bike;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.contract.BikeInsuranceInfo;

public interface BikeInsuranceInfoRepository extends CrudRepository<BikeInsuranceInfo, Integer>,
        JpaRepository<BikeInsuranceInfo, Integer> {
    
    @Query(value = "select count(*) from bike_insurance_info info\n"
            + "inner join contract c on c.id = info.contract_id\n"
            + "where info.reg_number = ?1 and c.end_date > ?2 and c.contract_status in ?3", nativeQuery = true)
    Integer checkContractExistedByRegNumAndDateAndStatus(String regNum, Date date, Collection<Integer> status);

}
