package vn.anvui.dt.repos.report;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.DailyIncomeReport;

public interface DailyReportRepository extends JpaRepository<DailyIncomeReport, Integer>,
        CrudRepository<DailyIncomeReport, Integer> {
    
}
