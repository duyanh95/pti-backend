package vn.anvui.dt.repos.fcmNotification;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.Device;

public interface DeviceRepository extends JpaRepository<Device, Integer>, CrudRepository<Device, Integer> {

    List<Device> findAllByTokenIn(List<String> tokens);

    Device findFirstByToken(String token);
}
