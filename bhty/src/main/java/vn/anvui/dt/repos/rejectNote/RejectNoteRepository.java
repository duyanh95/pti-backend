package vn.anvui.dt.repos.rejectNote;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.anvui.dt.entity.RejectNote;

public interface RejectNoteRepository extends JpaRepository<RejectNote, Integer> {
    Set<RejectNote> findAllByActive(boolean active);
}
