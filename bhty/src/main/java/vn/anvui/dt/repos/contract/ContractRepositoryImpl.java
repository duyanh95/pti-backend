package vn.anvui.dt.repos.contract;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.springframework.util.StringUtils;

import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.model.ContractInfo;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.model.DailyReport;
import vn.anvui.dt.model.IncomeUser;
import vn.anvui.dt.model.RejectedContract;

public class ContractRepositoryImpl implements ContractRepositoryCustom {
    static final Logger log = Logger.getLogger(ContractRepositoryImpl.class.getSimpleName());

    @PersistenceContext
    EntityManager em;
    
    private String buildGetListContractQuery(String citizenId, String email, String phoneNumber, Date fromDate,
            Date toDate, Integer createdUserId, Boolean includeNullUser, Boolean isCountQuery) {
    	StringBuilder jpql = new StringBuilder("select ");
		if (isCountQuery) {
			jpql.append("count(1)");
		} else {
			jpql.append("c");
		}
		
		jpql.append(" from Contract c where c.id > 0");
		
		if (!StringUtils.isEmpty(citizenId)) {
			jpql.append(" and c.citizenId like :citizenId");
		}
		if (!StringUtils.isEmpty(email)) {
			jpql.append(" and c.email like :email");
		}
		if (!StringUtils.isEmpty(phoneNumber)) {
			jpql.append(" and c.phoneNumber like :phoneNumber");
		}
		if (createdUserId != null) {
			jpql.append(" and c.createdUserId = :createdUserId");
		}
		if (includeNullUser != null && includeNullUser == Boolean.TRUE) {
			jpql.append(" and c.createdUserId is null");
		}
		if (fromDate != null) {
			jpql.append(" and c.createdDate >= :fromDate");
		}
		if (toDate != null) {
			jpql.append(" and c.createdDate <= :toDate");
		}
		
		jpql.append(" order by c.id desc");
		
		return jpql.toString();
    }
    
    private void applyParamsToGetListContractQuery(TypedQuery<?> query, String citizenId, String email, String phoneNumber, Date fromDate,
            Date toDate, Integer createdUserId, Boolean includeNullUser) {
    	if (!StringUtils.isEmpty(citizenId)) {
			query.setParameter("citizenId", "%" + citizenId + "%");
		}
		if (!StringUtils.isEmpty(email)) {
			query.setParameter("email", "%" + email + "%");
		}
		if (!StringUtils.isEmpty(phoneNumber)) {
			query.setParameter("phoneNumber", "%" + phoneNumber + "%");
		}
		if (createdUserId != null) {
			query.setParameter("createdUserId", createdUserId);
		}
		if (fromDate != null) {
			query.setParameter("fromDate", fromDate);
		}
		if (toDate != null) {
			query.setParameter("toDate", toDate);
		}
    }
    
    @Override
    public Map<Object, Object> getListContract(String citizenId, String email, String phoneNumber, Date fromDate,
            Date toDate, Integer createdUserId, Boolean includeNullUser, Integer page, Integer size) {
    	// Get all of records base on request params
		String jpqlQuery = buildGetListContractQuery(citizenId, email, phoneNumber, fromDate,
	            toDate, createdUserId, includeNullUser, false);
		TypedQuery<Contract> query = em.createQuery(jpqlQuery, Contract.class);
		applyParamsToGetListContractQuery(query, citizenId, email, phoneNumber, fromDate,
	            toDate, createdUserId, includeNullUser);
		
		// Get total records base on request params
		String jpqlCountQuery = buildGetListContractQuery(citizenId, email, phoneNumber, fromDate,
	            toDate, createdUserId, includeNullUser, true);
		TypedQuery<Long> countQuery = em.createQuery(jpqlCountQuery, Long.class);
		applyParamsToGetListContractQuery(countQuery, citizenId, email, phoneNumber, fromDate,
	            toDate, createdUserId, includeNullUser);
		
		try {
			Long total = countQuery.getSingleResult();
			List<Contract> list = query.setFirstResult(page * size).setMaxResults(size).getResultList();
			
			// Return map contain list data and total record number
			Map<Object, Object> resultMap = new HashMap<>();
			resultMap.put("listData", list);
			resultMap.put("totalRecords", total);

			return resultMap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
    }
    
    @Override
    public List<ContractWrapper> getContractWithAgencyUser(List<Integer> contractStatuses, List<Integer> createdUserIds,
            List<Integer> exportStatus ,Date fromDate, Date toDate, Integer productId) {
        String sql = "select u.id as uid, u.user_name, c.id, c.contract_status, c.package_id, c.contract_code,\n" + 
                "    c.created_date, c.applied_date, c.start_date, c.end_date, c.created_time, c.updated_time,\n" + 
                "    cu.id as cus_id, cu.customer_name as cus_name, cu.citizen_id as cus_cid, cu.gender as cus_gender,\n" + 
                "    cu.dob as cus_dob, cu.photo as cus_photo, cu.email as cus_email, cu.phone_number as cus_phone,\n" + 
                "    cu.tag as cus_tag, cu.note as cus_note, cu.address as cus_addr, cu.province as cus_province,\n" + 
                "    pa.id as pa_id, pa.customer_name as pa_name, pa.citizen_id as pa_cid, pa.gender as pa_gender,\n" + 
                "    pa.dob as pa_dob, pa.photo as pa_photo, pa.email as pa_email, pa.phone_number as pa_phone,\n" + 
                "    pa.tag as pa_tag, pa.note as pa_note, pa.address as pa_addr, pa.province as pa_province,\n" + 
                "    c.commission, c.fee, c.income, c.product_id \n" + 
                "from contract c\n" + 
                "inner join customer cu on c.inherit_customer_id = cu.id\n" + 
                "left join customer pa on c.partner_id = pa.id\n" + 
                "left join user_closure uc\n" + 
                "    on uc.descendance_id = c.created_user_id and uc.depth = 1\n" + 
                "left join user u\n" + 
                "    on u.id = uc.ascendance_id\n" +
                "where true %s";
        
        String sqlPredicate = "";
        if (contractStatuses != null && !contractStatuses.isEmpty()) {
            sqlPredicate += "and c.contract_status in :contractStatus\n";
            log.info("contractStatus: " + contractStatuses.toString());
        }
        
        if (createdUserIds != null && !createdUserIds.isEmpty()) {
            sqlPredicate += "and c.created_user_id in :createdUserIds\n";
        }
        
        if (exportStatus != null && !exportStatus.isEmpty()) {
            sqlPredicate += "and c.export_status in :exportStatus\n";
            log.info("exportStatus: " + exportStatus);
        }
        
        if (fromDate != null && toDate !=null) {
            sqlPredicate += "and c.created_time between :fromDate and :toDate\n";
        }
        
        if (productId != null) {
            sqlPredicate += "and c.product_id = :productId\n";
        }
        
        sql = String.format(sql, sqlPredicate);
        
        log.info(sql);
        
        Query sqlQuery = em.createNativeQuery(sql, "ContractDTO");
        
        if (contractStatuses != null && !contractStatuses.isEmpty()) {
            sqlQuery.setParameter("contractStatus", contractStatuses);
        }
        
        if (createdUserIds != null && !createdUserIds.isEmpty()) {
            sqlQuery.setParameter("createdUserIds", createdUserIds);
        }
        
        if (exportStatus != null && !exportStatus.isEmpty()) {
            sqlQuery.setParameter("exportStatus", exportStatus);
        }
        
        if (fromDate != null && toDate !=null) {
            sqlQuery.setParameter("fromDate", fromDate);
            sqlQuery.setParameter("toDate", toDate);
        }
        
        if (productId != null) {
            sqlQuery.setParameter("productId", productId);
        }
        
        List<ContractWrapper> rs = sqlQuery.getResultList();
        
        return rs;
    }
    
    @Override
    public ContractInfo listContractBy(String customerName, String citizenId, String email, String phoneNumber, Long fromDate,
            Long toDate, List<Integer> userIds, boolean includeNullUser, List<Integer> contractStatus,
            Integer page, Integer size, String source, String medium, String campaign) throws Exception {
        String sql = "select c.* from contract c\n" +
                "inner join customer cu on c.inherit_customer_id = cu.id\n" + 
                "inner join customer pa on c.partner_id = pa.id\n";
        
        String countSql = "select count(1), sum(c.fee) from contract c\n" +
                "inner join customer cu on c.inherit_customer_id = cu.id\n" + 
                "inner join customer pa on c.partner_id = pa.id\n";
        
        String template;
        if (StringUtils.isEmpty(source) && StringUtils.isEmpty(medium)  && StringUtils.isEmpty(campaign)) {
            template = "        %s";
        } else {
            template = "inner join tracking t on t.contract_id = c.id\n" +
                       "        %s";
        }
        sql += template;
        countSql += template;
        
        Query sqlQuery = buildQuery(sql, customerName, citizenId, email, phoneNumber, 
                fromDate, toDate, userIds, includeNullUser, contractStatus, page, size, false, 
                source, medium, campaign, 1);
        Query countQuery = buildQuery(countSql, customerName, citizenId, email, phoneNumber, 
                fromDate, toDate, userIds, includeNullUser, contractStatus, page, size, true,
                source, medium, campaign, 1);
        
        List<Contract> rsList = sqlQuery.getResultList();
        
        for (Contract contract : rsList) {
            log.info("id: " + contract.getId());
        }
        
        List<Object[]> count = countQuery.getResultList();
        
        ContractInfo rs = new ContractInfo();
        rs.setContractList(rsList);
        rs.setTotal(((BigInteger) count.get(0)[0]).longValue());
        rs.setTotalIncome((Double) count.get(0)[1]);
        
        return rs;
    }
    
    @Override
    public ContractInfo listCodContract(String customerName, String citizenId, String email, String phoneNumber, Long fromDate,
            Long toDate, List<Integer> userIds, Integer page, Integer size, boolean includeNullUser) throws Exception {
        String sql = "select c.* from contract c\n" +
                "inner join customer cu on c.inherit_customer_id = cu.id and cu.contract_id = c.id\n" + 
                "inner join customer pa on c.partner_id = pa.id\n" +
                "        %s";
        
        List<Integer> status = new ArrayList<>(1);
        status.add(ContractStatus.COD.getValue());
        
        Query sqlQuery = buildQuery(sql, customerName, citizenId, email, phoneNumber, fromDate, toDate,
                userIds, includeNullUser, status, page, size, false, null, null, null, 1);
        
        List<Contract> rsList = sqlQuery.getResultList();
        ContractInfo rs = new ContractInfo();
        rs.setContractList(rsList);
        
        return rs;
    }
    
    private Query buildQuery(String sql, String customerName, String citizenId, String email, String phoneNumber, 
            Long fromDate, Long toDate, List<Integer> userIds, boolean includeNullUser, List<Integer> contractStatus,
            Integer page, Integer size, boolean isCount, String source, String medium, String campaign,
            Integer productId) throws Exception {
     // build query string
        String query = "where true";
        
        if (contractStatus != null) {
            log.info("contractStatuses:" + contractStatus.toString());
            query = "and c.contract_status in :contractStatus\n"
                    + "        " + query;
        }
        
        if (!StringUtils.isEmpty(customerName)) {
            query += " and (cu.customer_name like :customerName"
                    + " or pa.customer_name like :customerName)";
        }
        
        if (!StringUtils.isEmpty(citizenId)) {
            query += " and (cu.citizen_id like :citizenId or "
                    + "pa.citizen_id like :citizenId)";
        }
        
        if (!StringUtils.isEmpty(email)) {
            query += " and cu.email like :email";
        }
        
        if (!StringUtils.isEmpty(phoneNumber)) {
            query += " and cu.phone_number like :phoneNumber";
        }
        
        if (!StringUtils.isEmpty(source)) {
            query += " and t.source like :source";
        }
        
        if (!StringUtils.isEmpty(medium)) {
            query += " and t.medium like :medium";
        }

        if (!StringUtils.isEmpty(campaign)) {
            query += " and t.campaign like :campaign";
        }
        
        if (productId != null) {
            query += " and c.product_id = :productId";
        }
        
        if (toDate != null && fromDate != null) {
            query += " and c.created_date between :fromDate and :toDate";
        }
        
        if (userIds != null && !userIds.isEmpty()) {
            String userIdsStr = "(";
            for (Integer id : userIds) {
                userIdsStr += id + ", ";
            }
            
            userIdsStr = userIdsStr.substring(0, userIdsStr.length() - 2);
            userIdsStr += ")";
            
            if (includeNullUser) {
                userIdsStr += " or c.created_user_id is null";
            }
            
            query += " and (c.created_user_id in " + userIdsStr + " or cu.assigned_user_id in " + userIdsStr + ")";
        }
        
        
        if (!isCount) {
            if (page != null) {
                query += "\n    order by c.created_time desc limit :size";
            }
            query += " offset :offset";
        }
        
        sql = String.format(sql, query);
        log.info(sql);
        
        Query sqlQuery;
        if (!isCount) {
            sqlQuery = em.createNativeQuery(sql, Contract.class);
        } else {
            sqlQuery = em.createNativeQuery(sql);
        }
        
        //TODO: nothing
        /***************** set query parameters *******************/
        if (contractStatus != null) {
            sqlQuery.setParameter("contractStatus", contractStatus);
        }
        
        if (!StringUtils.isEmpty(customerName)) {
            sqlQuery.setParameter("customerName", "%" + customerName + "%");
        }
        if (!StringUtils.isEmpty(citizenId)) {
            sqlQuery.setParameter("citizenId", citizenId + "%");
        }
        
        if (!StringUtils.isEmpty(email)) {
            sqlQuery.setParameter("email", email + "%");
        }
        
        if (!StringUtils.isEmpty(phoneNumber)) {
            sqlQuery.setParameter("phoneNumber", "%" + phoneNumber + "%");
        }
        
        if (!StringUtils.isEmpty(source)) {
            sqlQuery.setParameter("source", source);
        }
        
        if (!StringUtils.isEmpty(medium)) {
            sqlQuery.setParameter("medium", medium);
        }

        if (!StringUtils.isEmpty(campaign)) {
            sqlQuery.setParameter("campaign", campaign);
        }
        
        if (productId != null) {
            sqlQuery.setParameter("productId", productId);
        }
        
        if (toDate != null && fromDate != null) {
            sqlQuery.setParameter("fromDate", new Date(fromDate));
            sqlQuery.setParameter("toDate", new Date(toDate));
        }
        
        if (!isCount) {
            if (size != null) {
                sqlQuery.setParameter("size", size);
            }
            if (page != null) {
                sqlQuery.setParameter("offset", size * page);
            }
        }
        
        return sqlQuery;
    }

    @Override
    public List<IncomeUser> reportIncomeByUser(Date toDate, Date fromDate, boolean includeNull, boolean asOne,
            Collection<Integer> ids) throws Exception {
        String queryStr = "select count(1), sum(c.fee)%s from contract c\n"
                + "    inner join package p on p.id = c.package_id\n"
                + "    left join user u on c.created_user_id = u.id\n"
                + "    where c.created_time between :fromDate and :toDate and c.contract_status in :completedStatus\n"
                + "        %s\n";
        
        String format = "";
        String select = "";
        if (ids != null && (!ids.isEmpty())) {
            
            format += "and (c.created_user_id in :userIds";
            if (includeNull)
                format += " or c.created_user_id is null)";
            else
                format += ")";
        }
        
        if (!asOne) {
            select = ", u.id, u.user_name, u.user_type";
            format += "\n        group by c.created_user_id";
        }
        
        queryStr = String.format(queryStr, select, format);
        
        Query sqlQuery = em.createNativeQuery(queryStr);
        
        sqlQuery.setParameter("fromDate", fromDate, TemporalType.TIMESTAMP);
        sqlQuery.setParameter("toDate", toDate, TemporalType.TIMESTAMP);
        sqlQuery.setParameter("completedStatus", ContractStatus.COMPLETED_SET);
        
        if (ids != null && (!ids.isEmpty()))
            sqlQuery.setParameter("userIds", ids);
        
        List<Object[]> rsList = sqlQuery.getResultList();
        List<IncomeUser> iList = new ArrayList<>();
        
        for (Object[] rs : rsList) {
            IncomeUser income = new IncomeUser();
            
            income.setTotalContracts(((BigInteger) rs[0]).longValue());
            income.setTotalFee((Double) rs[1]);
            if (income.getTotalFee() == null)
                income.setTotalFee(0.0);
            
            if (!asOne) {
                income.setUserId((Integer) rs[2]);
                income.setUserName((String) rs[3]);
                income.setUserType((String) rs[4]);
            }
            iList.add(income);
        }
        
        return iList;
    }

    @Override
    public List<RejectedContract> listRejectedReason(Date fromDate, Date toDate) throws Exception {
        String sql = "select c.phone_number, u.user_name, c.note,\n" +
                "rn.reject_note, t.medium, c.created_date from customer c\n" + 
                "inner join customer_reject_note crn on crn.customer_id = c.id\n" + 
                "inner join reject_note rn on rn.id = crn.reject_note_id\n" + 
                "inner join tracking t on t.customer_id = c.id\n" + 
                "left join user u on c.assigned_user_id = u.id\n" + 
                "        where c.created_date between :fromDate and :toDate";
        
        Query sqlQuery = em.createNativeQuery(sql);
        
        sqlQuery.setParameter("fromDate", fromDate, TemporalType.TIMESTAMP);
        sqlQuery.setParameter("toDate", toDate, TemporalType.TIMESTAMP);
        
        List<RejectedContract> rejectedList = new ArrayList<>();
        
        List<Object[]> rsList = sqlQuery.getResultList();
        for (Object[] rs : rsList) {
            RejectedContract rContract = new RejectedContract();
            rContract.setPhoneNumber((String) rs[0]);
            rContract.setUserName((String) rs[1]);
            rContract.setNote((String) rs[2]);
            rContract.setRejectReason((String) rs[3]);
            rContract.setMedium((String) rs[4]);
            rContract.setCreatedDate((Date) rs[5]);
            
            rejectedList.add(rContract);
        }
        
        return rejectedList;
    }

    @Override
    public DailyReport getContractReport(Date fromDate, Date toDate, Collection<Integer> userIds) throws Exception {
        String sql = "select paid.count as onlineContracts, paid.sum as onlineIncome,\n" + 
                "        cod.count as codContracts, cod.sum as codIncome\n" + 
                "from\n" + 
                "(select count(c.id) as count, coalesce(sum(c.fee), 0) as sum\n" +
                "from user u \n" + 
                "inner join \n" + 
                "    contract c on c.created_user_id = u.id\n" + 
                "inner join package p on c.package_id = p.id and contract_status = 5\n" + 
                "where c.created_time between :fromDate and :toDate\n" + 
                "        and u.id in :userIds) as paid\n" + 
                "inner join \n" + 
                "(select count(c.id) as count, coalesce(sum(c.fee), 0) as sum\n" +
                "from user u \n" + 
                "inner join \n" + 
                "    contract c on c.created_user_id = u.id\n" + 
                "inner join package p on c.package_id = p.id and contract_status in (7, 9)\n" + 
                "where c.created_time between :fromDate and :toDate\n" + 
                "        and u.id in :userIds) as cod";
        
        Query sqlQuery = em.createNativeQuery(sql, "DailyReport");
        sqlQuery.setParameter("fromDate", fromDate);
        sqlQuery.setParameter("toDate", toDate);
        sqlQuery.setParameter("userIds", userIds);
        
        List<DailyReport> rsList = sqlQuery.getResultList();
        
        DailyReport rs = rsList.get(0);
        rs.setDate(fromDate);
        
        return rs;
    }
    
    @Override
    public IncomeUser getContractReportForUser(Date fromDate, Date toDate, 
            Collection<Integer> userIds) throws Exception {
        String sql = "select paid.count as onlineContracts, paid.sum as onlineIncome,\n" + 
                "        cod.count as codContracts, cod.sum as codIncome,\n" + 
                "        (cod.comm + paid.comm) as totalCommission\n" +
                "from\n" + 
                "(select count(c.id) as count, coalesce(sum(c.fee), 0) as sum,\n" +
                "coalesce(sum(c.income), 0) as comm from user u \n" + 
                "inner join \n" + 
                "    contract c on c.created_user_id = u.id\n" + 
                "where c.created_time between :fromDate and :toDate\n" + 
                "        and contract_status = 5\n" + 
                "        and u.id in :userIds) as paid\n" + 
                "inner join \n" + 
                "(select count(c.id) as count, coalesce(sum(c.fee), 0) as sum,\n" +
                "coalesce(sum(c.income), 0) as comm from user u \n" + 
                "inner join \n" + 
                "    contract c on c.created_user_id = u.id\n" + 
                "where c.created_time between :fromDate and :toDate\n" + 
                "        and contract_status in (7, 9)\n" + 
                "        and u.id in :userIds) as cod";
        
        Query sqlQuery = em.createNativeQuery(sql, "IncomeUser");
        sqlQuery.setParameter("fromDate", fromDate);
        sqlQuery.setParameter("toDate", toDate);
        sqlQuery.setParameter("userIds", userIds);
        
        List<IncomeUser> rsList = sqlQuery.getResultList();
        
        IncomeUser rs = rsList.get(0);
        
        return rs;
    }

    @Override
    public Collection<Contract> miinListContract(String customerName, String citizenId, String email,
            String phoneNumber, Long fromDate, Long toDate, List<Integer> userIds, boolean includeNullUser,
            Collection<Integer> contractStatus, Integer productId, Integer page, Integer size) throws Exception {
        String sql = "select c.* from contract c\n" +
                "inner join customer cu on cu.id = c.inherit_customer_id\n" +
                "left join customer pa on pa.id = c.partner_id\n" +
                "   %s";
                
        Query sqlQuery = buildQuery(sql, customerName, citizenId, email, phoneNumber, fromDate, toDate, 
                userIds, includeNullUser, new ArrayList<>(contractStatus), page, size, 
                false, null, null, null, productId);
        
        Collection<Contract> rsList = sqlQuery.getResultList();
        return rsList;
    }
}
