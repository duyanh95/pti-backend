package vn.anvui.dt.repos.userSource;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import vn.anvui.dt.entity.UserSource;

public interface UserSourceRepository extends JpaRepository<UserSource, UserSource.UserSourceKey> {
    
    @Query(value = "select * from user_source where user_id = ?1 limit 1", nativeQuery = true)
    UserSource findUserSourceByUserId(Integer userId);
}
