package vn.anvui.dt.repos.user;

import java.util.Collection;
import java.util.List;

import vn.anvui.dt.entity.User;

public interface UserRepositoryCustom {
    public List<User> getListRequestedAgency(Integer page, Integer count, String citizenId, String phoneNumber);
    
    List<User> findUsersBy(String userName, String citizenId, String phoneNumber, String email, Collection<Integer> ids);
}
