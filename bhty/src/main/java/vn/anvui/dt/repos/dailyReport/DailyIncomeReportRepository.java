package vn.anvui.dt.repos.dailyReport;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.DailyIncomeReport;
import vn.anvui.dt.entity.DailyIncomeReport.ReportKey;

public interface DailyIncomeReportRepository extends CrudRepository<DailyIncomeReport, ReportKey>,
        JpaRepository<DailyIncomeReport, ReportKey>, DailyIncomeReportRepositoryCustom {
}
