package vn.anvui.dt.repos.contactLog;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.ContactLog;

public interface ContactLogRepository extends JpaRepository<ContactLog, Integer>,
        CrudRepository<ContactLog, Integer>{
    List<ContactLog> findFirst10ByPhoneNumberOrderByIdDesc(String phoneNumber);
}
