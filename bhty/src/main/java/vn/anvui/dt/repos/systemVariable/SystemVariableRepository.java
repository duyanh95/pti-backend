package vn.anvui.dt.repos.systemVariable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.SystemVariable;

public interface SystemVariableRepository extends JpaRepository<SystemVariable, String>,
        CrudRepository<SystemVariable, String>{
    
}
