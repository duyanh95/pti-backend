package vn.anvui.dt.repos;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public class CommonCustomRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID>
implements CommonCustomRepository<T, ID> {

	@SuppressWarnings("unused")
    private EntityManager entityManager;
	@SuppressWarnings("unused")
    private Class<T> domainClass;

	public CommonCustomRepositoryImpl(Class<T> domainClass, EntityManager entityManager) {
		super(domainClass, entityManager);
		this.entityManager = entityManager;
		this.domainClass = domainClass;
	}
}
