package vn.anvui.dt.repos.tracking;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.context.annotation.PropertySource;

import com.mysql.jdbc.StringUtils;

import vn.anvui.dt.model.TrackingRecord;

@PropertySource({ " classpath:${SYSTEM_ENVIRONMENT_VARIABLE}_mysql.properties " })
public class TrackingRepositoryImpl implements TrackingRepositoryCustom {
    final Logger log = Logger.getLogger(TrackingRepositoryCustom.class.getSimpleName());

    @PersistenceContext
    EntityManager em;
    
    @Override
    public List<TrackingRecord> getTrafficReport(Integer page, Integer count, String source, String agent,
            String medium, String campaign, String team, String channel, Long fromDate, Long toDate) throws Exception {
        String sql = "select t.team, t.agent, t.campaign, t.medium, t.source, t.channel,\n" + 
                "        count(1), count(distinct(t.customer_id)) from tracking t\n" +
                "        %s";
        
        log.info("detail query create...");
        Query sqlQuery = queryReportCreate(sql, page, count, source, agent, medium, campaign, 
                team, channel, fromDate, toDate, true);

        List<Object[]> rsList = sqlQuery.getResultList();
        List<TrackingRecord> recList = new ArrayList<>();

        for (Object[] rs : rsList) {
            TrackingRecord record = new TrackingRecord();

            record.setTeam((String) rs[0]);
            record.setAgent((String) rs[1]);
            record.setCampaign((String) rs[2]);
            record.setMedium((String) rs[3]);
            record.setSource((String) rs[4]);
            record.setChannel((String) rs[5]);
            record.setCountView(((BigInteger) rs[6]).intValue());
            record.setTotalCustomer(((BigInteger) rs[7]).intValue());
            
            recList.add(record);
        }
        
        return recList;
    }
    
    @Override
    public List<TrackingRecord> getContractReport(Integer page, Integer count, String source, String agent,
            String medium, String campaign, String team, String channel, Long fromDate, Long toDate) throws Exception {
        String sql = "select t.team, t.agent, t.campaign, t.medium, t.source, t.channel,\n" + 
                "        coalesce(sum(jt.fee), 0), count(jt.fee), coalesce(sum(cjt.fee), 0), count(cjt.id) from tracking t\n" + 
                "left join \n" + 
                "        (select c.id as id, p.fee as fee, c.created_time as created_date from contract c\n" + 
                "        inner join package p on c.package_id = p.id\n" + 
                "                and c.contract_status = 2\n" + 
                "        inner join customer cu on cu.id = c.inherit_customer_id\n" + 
                "                and cu.tag = 5) as jt on jt.id = t.contract_id\n" + 
                "left join \n" + 
                "        (select c.id as id, p.fee as fee, c.created_time as created_date from contract c\n" + 
                "        inner join package p on c.package_id = p.id\n" + 
                "                and c.contract_status in (2, 3)\n" + 
                "        inner join customer cu on cu.id = c.inherit_customer_id\n" + 
                "                and cu.tag = 3) as cjt on cjt.id = t.contract_id\n" + 
                "        %s";
        
        log.info("detail query create...");
        Query sqlQuery = queryReportCreate(sql, page, count, source, agent, medium, campaign, 
                team, channel, fromDate, toDate, false);

        List<Object[]> rsList = sqlQuery.getResultList();
        List<TrackingRecord> recList = new ArrayList<>();

        for (Object[] rs : rsList) {
            TrackingRecord record = new TrackingRecord();

            record.setTeam((String) rs[0]);
            record.setAgent((String) rs[1]);
            record.setCampaign((String) rs[2]);
            record.setMedium((String) rs[3]);
            record.setSource((String) rs[4]);
            record.setChannel((String) rs[5]);
            record.setTotalIncome((Double) rs[6]);
            record.setCountContract(((BigInteger) rs[7]).intValue());
            record.setCodIncome((Double) rs[8]);
            record.setCountCodContract(((BigInteger) rs[9]).intValue());
            
            recList.add(record);
        }
        
        return recList;
    }

    private Query queryReportCreate(String sql, Integer page, Integer count, String source, String agent,
            String medium, String campaign, String team, String channel, Long fromDate, Long toDate, 
            boolean isTrafficQuery) {
        if (page != null && page > 0)
            return null;
        
        String query = "where true";

        boolean sourceEmpty = StringUtils.isEmptyOrWhitespaceOnly(source);
        if (!sourceEmpty) {
            query += " and t.source = :source";
        }

        boolean agentEmpty = StringUtils.isEmptyOrWhitespaceOnly(agent);
        if (!agentEmpty) {
            query += " and t.agent = :agent";
        }

        boolean mediumEmpty = StringUtils.isEmptyOrWhitespaceOnly(medium);
        if (!mediumEmpty) {
            query += " and t.medium = :medium";
        }

        boolean campaignEmpty = StringUtils.isEmptyOrWhitespaceOnly(campaign);
        if (!campaignEmpty) {
            query += " and t.campaign = :campaign";
        }

        boolean channelEmpty = StringUtils.isEmptyOrWhitespaceOnly(channel);
        if (!channelEmpty) {
            query += " and t.channel = :channel";
        }

        if (fromDate != null && toDate != null) {
            if (isTrafficQuery)
                query += " and t.created_date between :fromDate and :toDate";
            else
                query += " and (jt.created_date between :fromDate and :toDate"
                        + " or cjt.created_date between :fromDate and :toDate)";
        }

        query += "\n    group by t.team, t.agent, t.campaign, t.medium, t.source, t.channel\n";
        
        if (count != null) {
            if (count > 0)
                query += "limit " + count + " offset " + page * count;
        }
        
        sql = String.format(sql, query);
        log.info("query: \n" + sql);

        Query sqlQuery = em.createNativeQuery(sql);

        if (!sourceEmpty) {
            sqlQuery.setParameter("source", source);
        }
        if (!agentEmpty) {
            sqlQuery.setParameter("agent", agent);
        }
        if (!mediumEmpty) {
            sqlQuery.setParameter("medium", medium);
        }
        if (!campaignEmpty) {
            sqlQuery.setParameter("campaign", campaign);
        }
        if (!channelEmpty) {
            sqlQuery.setParameter("channel", channel);
        }
        
        if (fromDate != null && toDate != null) {
            sqlQuery.setParameter("fromDate", new Date(fromDate));
            sqlQuery.setParameter("toDate", new Date(toDate));
        }
        
        
        return sqlQuery;
    }

    @Override
    public List<String> getAggregateValueOnField(String field) throws Exception {
        String sql = "select t.%s from tracking as t"
                + "    where t.%s is not null"
                + "        group by t.%s";
        
        
        switch (field) {
        case "team":
        case "agent":
        case "campaign":
        case "medium":
        case "source":
        case "channel":
            sql = String.format(sql, field, field,field);
            break;
            
        default:
            throw new Exception("field not existed");
        }
        
        Query query = em.createNativeQuery(sql);
        
        List<String> rsList = query.getResultList();
        
        return rsList;
    }

}