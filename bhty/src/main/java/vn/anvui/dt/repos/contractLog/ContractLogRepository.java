package vn.anvui.dt.repos.contractLog;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.ContractLog;

public interface ContractLogRepository
        extends CrudRepository<ContractLog, Integer>, JpaRepository<ContractLog, Integer> {
    List<ContractLog> findAllByContractIdOrderByCreatedDate(Integer contractId);
}
