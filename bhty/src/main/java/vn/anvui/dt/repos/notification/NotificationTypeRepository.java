package vn.anvui.dt.repos.notification;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.NotificationType;

public interface NotificationTypeRepository extends JpaRepository<NotificationType, Integer>,
        CrudRepository<NotificationType, Integer> {

}
