package vn.anvui.dt.repos.product;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Integer>,
        CrudRepository<Product, Integer> {
    Collection<Product> findAllByOrderByOrder();
}
