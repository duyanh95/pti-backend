package vn.anvui.dt.repos.tracking;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.Tracking;

public interface TrackingRepository extends JpaRepository<Tracking, Integer>, 
        CrudRepository<Tracking, Integer>, TrackingRepositoryCustom {
    Tracking findById(Integer id);
    
    Tracking findOneByCustomerId(Integer customerId);
    
    List<Tracking> findAllByCustomerId(Integer customerId);
    
}
