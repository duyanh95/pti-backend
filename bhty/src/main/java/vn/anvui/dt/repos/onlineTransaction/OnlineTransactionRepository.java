package vn.anvui.dt.repos.onlineTransaction;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.OnlineTransaction;

public interface OnlineTransactionRepository
        extends JpaRepository<OnlineTransaction, Integer>, CrudRepository<OnlineTransaction, Integer>,
        OnlineTransactionRepositoryCustom {
    OnlineTransaction findFirstByOrderByIdDesc();

    OnlineTransaction findFirstByPaymentId(String paymentId);

    @Query("select sum(t.amount) from OnlineTransaction t where t.paymentType = ?1" 
            + " and t.status = ?2 and t.createdDate between ?3 and ?4")
    Double findTotalAmountByPaymentType(Integer paymentType, Integer status, Date fromDate, Date toDate);
    
    getPaymentIdOnly findFirstByStatusAndContractId(Integer status, Integer contractId);
    
    public interface getPaymentIdOnly {
        String getPaymentId();
    }
}
