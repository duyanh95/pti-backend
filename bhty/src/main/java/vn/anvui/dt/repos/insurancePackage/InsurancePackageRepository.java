package vn.anvui.dt.repos.insurancePackage;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.InsurancePackage;

public interface InsurancePackageRepository extends JpaRepository<InsurancePackage, Integer>, 
        CrudRepository<InsurancePackage, Integer> {
    List<InsurancePackage> findAll();
    List<InsurancePackage> findAllByActive(boolean active);
    
    feeOnly findFirstById(Integer id);
    
    public interface feeOnly {
        Double getFee();
    }

    List<InsurancePackage> findAllByProductIdAndActive(Integer productId, boolean active);
}
