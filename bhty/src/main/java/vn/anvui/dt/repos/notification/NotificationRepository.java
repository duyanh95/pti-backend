package vn.anvui.dt.repos.notification;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.Notification;

public interface NotificationRepository extends JpaRepository<Notification, Integer>,
        CrudRepository<Notification, Integer> {
    Page<Notification> findAllByUserIdAndUserNotificationTypeIn(Integer userId, 
            Collection<Integer> userNotiType, Pageable page);
}
