package vn.anvui.dt.repos.tracking;

import java.util.List;

import vn.anvui.dt.model.TrackingRecord;

public interface TrackingRepositoryCustom {
    List<String> getAggregateValueOnField(String field) throws Exception;

    List<TrackingRecord> getTrafficReport(Integer page, Integer count, String source, String agent, String medium,
            String campaign, String team, String channel, Long fromDate, Long toDate) throws Exception;

    List<TrackingRecord> getContractReport(Integer page, Integer count, String source, String agent, String medium,
            String campaign, String team, String channel, Long fromDate, Long toDate) throws Exception;
}
