package vn.anvui.dt.repos.rejectNote;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.anvui.dt.entity.CustomerRejectNote;
import vn.anvui.dt.entity.RejectNote;

public interface CustomerRejectNoteRepository extends JpaRepository<CustomerRejectNote, Integer> {
    Integer countByNoteAndCreatedTimeBetween(RejectNote note, Date fromDate, Date toDate);
    Integer countByNote(RejectNote note);
}
