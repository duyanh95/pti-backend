package vn.anvui.dt.repos.contract;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.Customer;

public interface ContractRepository extends JpaRepository<Contract, Integer>, 
        CrudRepository<Contract, Integer>, ContractRepositoryCustom {
    
    @Query(value = "select * from contract c\n"
            + "inner join customer cu on cu.id = c.inherit_customer_id\n"
            + "where c.product_id = ?1 and c.end_date > ?2 \n"
            + "and c.contract_status in ?3 and cu.citizen_id = ?4 limit 1", nativeQuery = true)
    Contract findExistedContractInEffectiveTime(Integer productId, Date currentDate, Collection<Integer> status,
            String citizenId);
    
    Page<Contract> findAllByOrderByCreatedDateDesc(Pageable page);
    
    IdOnly findFirstByIdInAndContractStatusIn(List<Integer> ids, List<Integer> status);
    
    List<InheritCustomerOnly> findAllByContractStatus(Integer status);
    
    Page<InheritCustomerOnly> findAllByContractStatusAndCreatedUserIdIsNullOrderByCreatedDateDesc(Integer status, Pageable page);
    
    Page<Contract> findAllContractByCreatedUserIdIn(Set<Integer> userIds, Pageable page);
    
    @Query("select c.contractStatus from Contract c where c.id = ?1")
    Integer findContractStatusById(Integer id);
    
    public interface InheritCustomerOnly {
        Customer getInheritCustomer();
    }
    
    public interface IdOnly {
        Integer getId();
    }

    Page<Contract> findAllById(Set<Integer> ids, Pageable contractPage);

    Map<Object, Object> getListContract(String citizenId, String email, String phoneNumber, Date fromDate, Date toDate,
            Integer createdUserId, Boolean includeNullUser, Integer page, Integer size);
    
    @Query(value = "select c.* from contract c\n" + 
            "inner join customer cu\n" + 
            "        on cu.id = c.inherit_customer_id and c.contract_status = 2\n" + 
            "        and c.export_status in ?1" + 
            "    where cu.tag = ?2 and c.created_date between ?3 and ?4", nativeQuery = true)
    List<Contract> getNotExportedContract(List<Integer> exportStatus, Integer tag, Date fromdate, Date toDate);
}
