package vn.anvui.dt.repos.contract;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.model.ContractInfo;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.model.DailyReport;
import vn.anvui.dt.model.IncomeUser;
import vn.anvui.dt.model.RejectedContract;

public interface ContractRepositoryCustom {

    Map<Object, Object> getListContract(String citizenId, String email, String phoneNumber, Date fromDate, Date toDate,
            Integer createdUserId, Boolean includeNullUser, Integer page, Integer size);

    ContractInfo listContractBy(String customerName, String citizenId, String email, String phoneNumber, Long fromDate,
            Long toDate, List<Integer> userIds, boolean includeNullUser, List<Integer> contractStatus, Integer page,
            Integer size, String source, String medium, String campaign) throws Exception;

    List<IncomeUser> reportIncomeByUser(Date toDate, Date fromDate, boolean includeNull, boolean asOne,
            Collection<Integer> ids) throws Exception;

    ContractInfo listCodContract(String customerName, String citizenId, String email, String phoneNumber, Long fromDate,
            Long toDate, List<Integer> userIds, Integer page, Integer size, boolean includeNullUser) throws Exception;
    
    List<RejectedContract> listRejectedReason(Date fromDate, Date toDate) throws Exception;
    
    DailyReport getContractReport(Date fromDate, Date toDate, Collection<Integer> userIds) throws Exception;

    IncomeUser getContractReportForUser(Date fromDate, Date toDate, Collection<Integer> userIds) throws Exception;
    
    Collection<Contract> miinListContract(String customerName, String citizenId, String email, String phoneNumber,
            Long fromDate, Long toDate, List<Integer> userIds, boolean includeNullUser,
            Collection<Integer> contractStatus, Integer productId, Integer page, Integer size) throws Exception;

    List<ContractWrapper> getContractWithAgencyUser(List<Integer> contractStatuses, List<Integer> createdUserIds,
            List<Integer> exportStatus, Date fromDate, Date toDate, Integer productId);
}