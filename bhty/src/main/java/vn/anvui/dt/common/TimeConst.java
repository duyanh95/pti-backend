package vn.anvui.dt.common;

public interface TimeConst {

	public static final int TOKEN_EXPIRATION_IN_SECONDS = 30 * 24 * 60 * 60;
}
