package vn.anvui.dt.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class IdentityCard implements Serializable {

	String identityCardNumber;
    long issuedDate;
    String issuedPlace;
    
    public String getIdentityCardNumber()
    {
        return identityCardNumber;
    }
    public void setIdentityCardNumber(String identityCardNumber)
    {
        this.identityCardNumber = identityCardNumber;
    }
    public long getIssuedDate()
    {
        return issuedDate;
    }
    public void setIssuedDate(long issuedDate)
    {
        this.issuedDate = issuedDate;
    }
    public String getIssuedPlace()
    {
        return issuedPlace;
    }
    public void setIssuedPlace(String issuedPlace)
    {
        this.issuedPlace = issuedPlace;
    }
}
