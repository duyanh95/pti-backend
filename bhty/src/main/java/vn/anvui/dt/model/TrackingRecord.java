package vn.anvui.dt.model;

public class TrackingRecord {
    private Integer id;
    private String team;
    private String agent;
    private String campaign;
    private String medium;
    private String source;
    private String channel;
    private Integer countView = null;
    private Integer countContract = null;
    private Double totalIncome = null;
    private Integer totalCustomer = null;
    private Integer countCodContract = null;
    private Double codIncome = null;
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getCountView() {
        return countView;
    }

    public void setCountView(Integer countView) {
        this.countView = countView;
    }

    public Integer getCountContract() {
        return countContract;
    }

    public void setCountContract(Integer countContract) {
        this.countContract = countContract;
    }

    public Double getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(Double totalIncome) {
        this.totalIncome = totalIncome;
    }

    public Integer getTotalCustomer() {
        return totalCustomer;
    }

    public void setTotalCustomer(Integer totalCustomer) {
        this.totalCustomer = totalCustomer;
    }

    public Integer getCountCodContract() {
        return countCodContract;
    }

    public void setCountCodContract(Integer countCodContract) {
        this.countCodContract = countCodContract;
    }

    public Double getCodIncome() {
        return codIncome;
    }

    public void setCodIncome(Double codIncome) {
        this.codIncome = codIncome;
    }
}
