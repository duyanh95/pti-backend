package vn.anvui.dt.model;

import vn.anvui.dt.entity.OnlineTransaction;

public class ContractTransaction {
    private Integer transactionId;
    
    private Integer paymentType;
    
    private Long amount;
    
    private String returnURL;
    
    private String phoneNumber;
    
    public ContractTransaction(OnlineTransaction epayTxn) {
        this.transactionId = epayTxn.getId();
        this.paymentType = epayTxn.getPaymentType();
        this.amount = epayTxn.getAmount();
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getReturnURL() {
        return returnURL;
    }

    public void setReturnURL(String returnURL) {
        this.returnURL = returnURL;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
