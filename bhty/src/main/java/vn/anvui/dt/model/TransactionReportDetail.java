package vn.anvui.dt.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class TransactionReportDetail {
    
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+7:00")
    private Date createdDate;
    
    private Double amount;
    
    private String paymentId;
    
    private Integer paymentType;
    
    private String merchantId;
    
    private String contractCode;
    
    private Integer contractStatus;
    
    private String email;
    
    private String customerName;
    
    private String partnerName;
    
    private Integer status;
    
    public TransactionReportDetail(Date createdDate, Double amount, String paymentId, Integer paymentType,
            String merchantId, String contractCode, String email, String customerName, 
            String partnerName, Integer status) {
        this.createdDate = createdDate;
        this.amount = amount;
        this.paymentId = paymentId;
        this.paymentType = paymentType;
        this.merchantId = merchantId;
        this.contractCode = contractCode;
        this.email = email;
        this.customerName = customerName;
        this.partnerName = partnerName;
        this.status = status;
    }
    
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public Integer getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(Integer contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
