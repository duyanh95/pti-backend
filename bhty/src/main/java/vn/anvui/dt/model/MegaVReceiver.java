package vn.anvui.dt.model;

public class MegaVReceiver {
    String receiver_percentage;
    String receiver_phone;

    public MegaVReceiver(String receiver_percentage, String receiver_phone) {
        super();
        this.receiver_percentage = receiver_percentage;
        this.receiver_phone = receiver_phone;
    }
}
