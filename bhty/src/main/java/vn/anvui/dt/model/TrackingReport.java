package vn.anvui.dt.model;

import java.util.List;

public class TrackingReport {
    
    private Integer view;
    
    private Double totalIncome;
    
    private Integer numberOfContracts;
    
    private List<TrackingRecord> details;
    
    public Integer getView() {
        return view;
    }

    public void setView(Integer view) {
        this.view = view;
    }

    public Double getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(Double totalIncome) {
        this.totalIncome = totalIncome;
    }

    public Integer getNumberOfContracts() {
        return numberOfContracts;
    }

    public void setNumberOfContracts(Integer numberOfContracts) {
        this.numberOfContracts = numberOfContracts;
    }

    public List<TrackingRecord> getDetails() {
        return details;
    }

    public void setDetails(List<TrackingRecord> details) {
        this.details = details;
    }
}
