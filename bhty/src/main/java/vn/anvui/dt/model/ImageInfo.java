package vn.anvui.dt.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ImageInfo implements Serializable {

	String imageLink;

    public String getImageLink()
    {
        return imageLink;
    }

    public void setImageLink(String imageLink)
    {
        this.imageLink = imageLink;
    }
}
