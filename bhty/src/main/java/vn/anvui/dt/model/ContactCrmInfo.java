package vn.anvui.dt.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"customerName", "citizenId", "gender", "dob", "partnerName", "partnerCitizenId", 
    "partnerGender", "partnerDob", "phoneNumber", "email", "fee", "createdDate", "team", "agent",
    "campaign", "medium", "source", "channel"})
public class ContactCrmInfo {
    private String customerName;
    
    private String citizenId;
    
    private Integer gender;
    
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date dob;
    
    private String partnerName;
    
    private String partnerCitizenId;
    
    private Integer partnerGender;
    
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date partnerDob;
    
    private String phoneNumber;
    
    private String email;
    
    private Double fee;
    
    @JsonFormat(pattern = "HH:mm:ss dd-MM-yyyy", timezone = "GMT+7:00")
    private Date createdDate;
    
    private String team;
    
    private String agent;
    
    private String campaign;
    
    private String medium;
    
    private String source;
    
    private String channel;
    
    public ContactCrmInfo(String customerName, String citizenId, Date dob, Integer gender, 
            String partnerName, String partnerCitizenId, Date partnerDob, Integer partnerGender,
            String phoneNumber, String email, Date createdDate, Double fee, String team, String agent,
            String campaign, String medium, String source, String channel) {
        this.customerName = customerName;
        this.citizenId = citizenId;
        this.gender = gender;
        this.dob = dob;
        this.partnerName = partnerName;
        this.partnerCitizenId = partnerCitizenId;
        this.partnerGender = partnerGender;
        this.partnerDob = partnerDob;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.fee = fee;
        this.createdDate = createdDate;
        this.team = team;
        this.agent = agent;
        this.campaign = campaign;
        this.medium = medium;
        this.source = source;
        this.channel = channel;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPartnerCitizenId() {
        return partnerCitizenId;
    }

    public void setPartnerCitizenId(String partnerCitizenId) {
        this.partnerCitizenId = partnerCitizenId;
    }

    public Integer getPartnerGender() {
        return partnerGender;
    }

    public void setPartnerGender(Integer partnerGender) {
        this.partnerGender = partnerGender;
    }

    public Date getPartnerDob() {
        return partnerDob;
    }

    public void setPartnerDob(Date partnerDob) {
        this.partnerDob = partnerDob;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
