package vn.anvui.dt.model;

public class EpayParameters {
    private String status;
    
    private String message;
    
    private String data;
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }


    public class EpayData {
        private String status;
        
        private String paymentId;
        
        private String serverTxnDatetime;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPaymentId() {
            return paymentId;
        }

        public void setPaymentId(String paymentId) {
            this.paymentId = paymentId;
        }

        public String getServerTxnDatetime() {
            return serverTxnDatetime;
        }

        public void setServerTxnDatetime(String serverTxnDatetime) {
            this.serverTxnDatetime = serverTxnDatetime;
        }

    }
}
