package vn.anvui.dt.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class RejectedContract {
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+7:00")
    private Date createdDate;
    
    private String phoneNumber;
    
    private String note;
    
    private String rejectReason;
    
    private String userName;
    
    private String medium;

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }
}
