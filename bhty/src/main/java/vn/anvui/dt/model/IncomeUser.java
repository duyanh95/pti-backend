package vn.anvui.dt.model;

import vn.anvui.dt.entity.User;

public class IncomeUser {
    private Integer userId;
    
    private String userName;
    
    private String fullName;
    
    private String userType;
    
    private Long onlineContracts;
    
    private Double onlineIncome;
    
    private Long codContracts;
    
    private Double codIncome;
    
    private Double totalCommission;
    
    private Long totalContracts;
    
    private Double totalFee;
    
    public IncomeUser() {
        
    }
    
    public IncomeUser(Long onlineContracts, Double onlineIncome, Long codContracts, Double codIncome,
            Double totalCommission) {
        this.onlineContracts = onlineContracts;
        this.onlineIncome = onlineIncome;
        this.codContracts = codContracts;
        this.codIncome = codIncome;
        this.totalCommission = totalCommission;
    }
    
    public void setUser(User user) {
        this.userId = user.getId();
        this.userName = user.getUserName();
        this.fullName = user.getFullName();
        this.userType = user.getUserType();
    }
    
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Long getOnlineContracts() {
        return onlineContracts;
    }

    public void setOnlineContracts(Long onlineContracts) {
        this.onlineContracts = onlineContracts;
    }

    public Double getOnlineIncome() {
        return onlineIncome;
    }

    public void setOnlineIncome(Double onlineIncome) {
        this.onlineIncome = onlineIncome;
    }

    public Long getCodContracts() {
        return codContracts;
    }

    public void setCodContracts(Long codContracts) {
        this.codContracts = codContracts;
    }

    public Double getCodIncome() {
        return codIncome;
    }

    public void setCodIncome(Double codIncome) {
        this.codIncome = codIncome;
    }

    public Double getTotalCommission() {
        return totalCommission;
    }

    public void setTotalCommission(Double totalCommission) {
        this.totalCommission = totalCommission;
    }

    public Long getTotalContracts() {
        return totalContracts;
    }

    public void setTotalContracts(Long totalContracts) {
        this.totalContracts = totalContracts;
    }

    public Double getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Double totalFee) {
        this.totalFee = totalFee;
    }
}
