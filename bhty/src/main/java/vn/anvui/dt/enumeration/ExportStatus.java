package vn.anvui.dt.enumeration;

public enum ExportStatus {
    NOT_EXPORTED(0),
    EXPORTED(1),
    UPDATED(2),
    UPDATE_EXPORTED(3);
    
    final int value;
    
    ExportStatus(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
