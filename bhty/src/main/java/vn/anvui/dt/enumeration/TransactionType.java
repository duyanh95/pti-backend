package vn.anvui.dt.enumeration;

public enum TransactionType {
    ONLINE_TXN(0),
    COD_TXN(1),
    ACCOUNT_TRANSFER(2);
    
    final int value;
    
    TransactionType(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
