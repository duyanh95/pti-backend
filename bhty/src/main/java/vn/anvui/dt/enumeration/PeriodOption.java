package vn.anvui.dt.enumeration;

public enum PeriodOption {
    YEARLY(0),
    MONTHLY(1),
    WEEKLY(2);
    
    final int value;
    
    PeriodOption(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
