package vn.anvui.dt.enumeration;

import java.util.Arrays;
import java.util.Collection;

public enum ContractStatus {
    CANCELLED(0),
    CREATED(1),
    PAID(2),
    COD(3),
    AGENCY_CREATED(4),
    AGENCY_PAID(5),
    AGENCY_COD_CREATED(6),
    AGENCY_COD_DONE(7),
    AGENCY_TRANSFER(8),
    AGENCY_TRANSFER_DONE(9),
    AGENCY_TRANSFER_CREATED(10),
    CUSTOMER_CREATED(11),
    CUSTOMER_PAID(12);
    
    final int value;
    
    public static final Collection<Integer> COMPLETED_SET = Arrays.asList(2, 5, 7, 9, 12);
    
    ContractStatus(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
