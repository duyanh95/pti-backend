package vn.anvui.dt.enumeration;

public enum NotificationTypeEnum {
    TXN_SUCCESS(1),
    TXN_FAIL(2),
    REGISTER_SUCCESS(3),
    REGISTER_FAIL(4);
    
    final int value;
    
    NotificationTypeEnum(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
