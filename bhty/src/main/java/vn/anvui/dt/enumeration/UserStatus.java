package vn.anvui.dt.enumeration;

public enum UserStatus {
    
    DELETED(0),
    ACTIVE(1),
    AGENT_REGISTER_PENDING(2);
    
//    USER_NEW(10), 
//    USER_NORMAL(11), 
//    USER_LOCKED(12), 
//    USER_NEED_CENSORED(13), 
//    USER_DELETED(14), 
//    
//    DRIVER_NORMAL(21),
//    DRIVER_LOCKED(22),
//    DRIVER_NEED_CENSORED(23),
//    DRIVER_ON_THE_TRIP(24),
//    DRIVER_DELETED(25),
//    
//    ASSISTANT_NORMAL(31),
//    ASSISTANT_LOCKED(32),
//    ASSISTANT_NEED_CENSORED(33),
//    ASSISTANT_ON_THE_TRIP(34),
//    ASSISTANT_DELETED(35),
//    
//    ACCOUNTANT_NORMAL(41),
//    ACCOUNTANT_LOCKED(42),
//    ACCOUNTANT_NEED_CENSORED(43),
//    ACCOUNTANT_DELETED(44),
//    
//    ADMINISTRATIVE_STAFF_NORMAL(51),
//    ADMINISTRATIVE_STAFF_LOCKED(52),
//    ADMINISTRATIVE_STAFF_NEED_CENSORED(53),
//    ADMINISTRATIVE_STAFF_DELETED(54),
//    
//    INSPECTOR_NORMAL(61),
//    INSPECTOR_LOCKED(62),
//    INSPECTOR_NEED_CENSORED(63),
//    INSPECTOR_DELETED(64),
//    
//    ORGANIZATION_ADMIN_NORMAL(71),
//    ORGANIZATION_ADMIN_LOCKED(72),
//    ORGANIZATION_ADMIN_NEED_CENSORED(73),
//    ORGANIZATION_ADMIN_DELETED(74);
    ;
    private final int value;

    UserStatus(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
    
    public static boolean isMember(int value) {
		for (UserStatus item : UserStatus.values()) {
			if (item.getValue() == value) {
				return true;
			}
		}
		return false;
	}
}
