package vn.anvui.dt.enumeration;

public enum PeriodType {
    WEEKLY(0),
    MONTHLY(1),
    YEARLY(2);
    
    final int value;
    
    PeriodType(final int newValue) {
        value = newValue;
    }

    public int getValue() {
        return value;
    }
}
