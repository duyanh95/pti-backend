package vn.anvui.dt.enumeration;

public enum ProductType {
    LOVE(1),
    APARTMENT(2),
    HEALTH(3),
    BIKE(4);
    
    final int value;
    
    public static ProductType getProductTypeByValue(int value) {
        switch (value) {
        case 1: 
            return ProductType.LOVE;
            
        case 2:
            return ProductType.APARTMENT;
            
        case 3:
            return ProductType.HEALTH;
            
        case 4:
            return ProductType.BIKE;
            
        default:
            return null;
        }
    }
    
    ProductType(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
