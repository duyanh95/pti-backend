package vn.anvui.dt.enumeration;

public enum BannerType {
    BANNER(0),
    PROMOTION(1);
    
    final int value;
    
    BannerType(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
