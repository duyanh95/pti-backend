package vn.anvui.dt.enumeration;

public enum TrackingType {
    VIEW(1),
    CONTRACT(2);
    
    final int value;
    
    TrackingType(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
