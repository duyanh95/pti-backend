package vn.anvui.dt.enumeration;

public enum Gender {
    MALE(1),
    FEMALE(2);
    
    final int value;
    
    Gender(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
