package vn.anvui.dt.enumeration;

public enum PTIPrivileges {
    READ_CONTRACT("READ_CONTRACT"),
    CREATE_CONTRACT("CREATE_CONTRACT"),
    UPDATE_CONTRACT("UPDATE_CONTRACT"),
    DELETE_CONTRACT("DELETE_CONTRACT"),
    READ_ADMIN("READ_ADMIN"),
    CREATE_ADMIN("CREATE_ADMIN"),
    UPDATE_ADMIN("UPDATE_ADMIN"),
    DELETE_ADMIN("DELETE_ADMIN"),
    READ_USER("READ_USER"),
    CREATE_USER("CREATE_USER"),
    UPDATE_USER("UPDATE_USER"),
    DELETE_USER("DELETE_USER"),
    READ_AGENCY("READ_AGENCY"),
    CREATE_AGENCY("CREATE_AGENCY"),
    UPDATE_AGENCY("UPDATE_AGENCY"),
    DELETE_AGENCY("DELETE_AGENCY"),
    MARKETING_PARTNER("MARKETING_PARTNER");
    
    final String value;
    
    PTIPrivileges(final String newValue)
    {
        value = newValue;
    }

    public String getValue()
    {
        return value;
    }
}
