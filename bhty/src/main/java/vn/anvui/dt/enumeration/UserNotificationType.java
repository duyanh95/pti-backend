package vn.anvui.dt.enumeration;

public enum UserNotificationType {
    CUSTOMER(0),
    AGENCY(1),
    ALL(2);
    
    final int value;
    
    UserNotificationType(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
