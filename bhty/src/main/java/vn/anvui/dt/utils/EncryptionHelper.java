package vn.anvui.dt.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;


public class EncryptionHelper {
    static final Logger log = Logger.getLogger(EncryptionHelper.class.getSimpleName());
    
    public static String hmacSHA256encode(String data, String key) 
            throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException {
        String algo = "HMACSHA256";
        
        Mac mac = Mac.getInstance(algo);
        
        byte[] decodedKey = key.getBytes();
        SecretKey secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
        
        mac.init(secretKey);
        
        byte[] hex = new Hex().encode(mac.doFinal(data.getBytes()));
        return new String(hex);
    }
    
    static PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }
    
    private EncryptionHelper() {

    }

    private static String convertByteArrayToHexString(byte[] arrayBytes) {

        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrayBytes.length; i++) {
            stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return stringBuffer.toString();
    }

    public static String md5Encrypt(String input) throws AnvuiBaseException {
        return messageDigestEncrypt(input, "MD5");
    }
    
    public static String sha256Encrypt(String input) throws AnvuiBaseException {
        return messageDigestEncrypt(input, "SHA-256");
    }
    
    protected static String messageDigestEncrypt(String input, String algo) {
        String output;
        
        try {
            MessageDigest digest = MessageDigest.getInstance(algo);
            
            digest.reset();
            digest.update(input.getBytes("UTF-8"));

            output = convertByteArrayToHexString(digest.digest());
            
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            throw new AnvuiBaseException(new ErrorInfo(ErrorInfo.UNKNOWN_ERROR_CODE, e.getMessage()));
        }
        
        return output;
    }
    
    public static String hmacSHA1Encode(String input, String key) {
        return hmacEncrypt(input, key, "HmacSHA1");
    }
    
    protected static String hmacEncrypt(String input, String key, String algo) {
        try {
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), algo);
            Mac mac = Mac.getInstance(algo);
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(input.getBytes());
            return new String(Base64.getEncoder().encode(rawHmac));
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            
            throw new AnvuiBaseException(new ErrorInfo(ErrorInfo.UNKNOWN_ERROR_CODE, e.getMessage()));
        }
    }
    
    public static String bcryptEncrypt(String input) throws AnvuiBaseException {
        return encoder().encode(input);
    }
    
    public static boolean bcryptPasswordMatch(String input, String password) throws AnvuiBaseException {
        return encoder().matches(input, password);
    }
}
