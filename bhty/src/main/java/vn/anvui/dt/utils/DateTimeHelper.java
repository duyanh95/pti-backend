package vn.anvui.dt.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public final class DateTimeHelper {

    public DateTimeHelper() {

    }
    
    public static final long DAY_IN_MS = 86400000;
    public static final long MIN_IN_MS = 60000;

    public static final String YEAR = "year";
    public static final String MONTH = "month";
    public static final String DAY_OF_YEAR = "dayOfYear";
    public static final String WEEK = "week";

    public static final String HOUR = "hour";
    public static final String MINUTE = "minute";
    public static final String SECOND = "second";

    public static Date addDay(Date targetDate, String typeOfTime, int value) {
        return addDay(targetDate, typeOfTime, value, TimeZone.getTimeZone("UTC"));
    }
    
    public static Date addDay(Date targetDate, String typeOfTime, int value, TimeZone timeZone) {

        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTime(targetDate);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date date = addTime(calendar.getTime(), typeOfTime, value, timeZone);

        return date;
    }
    
    public static Date addTime(Date targetDate, String typeOfTime, int value, TimeZone timeZone) {
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTime(targetDate);

        if (YEAR.equals(typeOfTime)) {
            calendar.add(Calendar.YEAR, value);
        } else if (MONTH.equals(typeOfTime)) {
            calendar.add(Calendar.MONTH, value);
        } else if (DAY_OF_YEAR.equals(typeOfTime)) {
            calendar.add(Calendar.DAY_OF_YEAR, value);
        } else if (WEEK.equals(typeOfTime)) {
            calendar.add(Calendar.WEEK_OF_YEAR, value);
        } else if (HOUR.equals(typeOfTime)) {
            calendar.add(Calendar.HOUR, value);
        } else if (MINUTE.equals(typeOfTime)) {
            calendar.add(Calendar.MINUTE, value);
        } else if (SECOND.equals(typeOfTime)) {
            calendar.add(Calendar.SECOND, value);
        }

        return calendar.getTime();
    }
    
    public static Date getFirstTimeOfDay(Date date) {
        return getFirstTimeOfDay(date, TimeZone.getTimeZone("UTC"));
    }
    
    public static Date getFirstTimeOfDay(Date date, TimeZone timeZone) {
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        
        return calendar.getTime();
    }
    
    public static Date getFirstDayOfMonths(Date date) {
        return getFirstDayOfMonths(date, TimeZone.getTimeZone("UTC"));
    }
    
    public static Date getFirstDayOfMonths(Date date, TimeZone timeZone) {
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        
        return calendar.getTime();
    }
    
    public static List<Date> getAllDateBetween(Date fromDate, Date toDate) {
        
        return getAllDateBetween(fromDate, toDate, TimeZone.getTimeZone("UTC"));
    }
    
    public static List<Date> getAllDateBetween(Date fromDate, Date toDate, TimeZone timezone) {
        List<Date> date = new ArrayList<>(30);
        
        Date dateTmp = getFirstTimeOfDay(new Date(fromDate.getTime() + DAY_IN_MS), timezone);
        while (dateTmp.getTime() <= toDate.getTime() + DAY_IN_MS) {
            date.add(dateTmp);
            dateTmp = new Date(dateTmp.getTime() + DAY_IN_MS);
        }
        
        return date;
    }

}
