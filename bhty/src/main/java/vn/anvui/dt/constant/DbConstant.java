package vn.anvui.dt.constant;

public interface DbConstant {

	public static final String LIST_DATA = "listData";

	public static final String TOTAL_RECORDS = "totalRecords";
}
