package vn.anvui.dt.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "user_source")
public class UserSource {
    @EmbeddedId
    private UserSourceKey key;
    
    public UserSourceKey getKey() {
        return key;
    }

    public void setKey(UserSourceKey key) {
        this.key = key;
    }
    
    public Integer getUserId() {
        return this.key.userId;
    }
    
    public void setUserId(Integer id) {
        this.key.userId = id;
    }
    
    public String getSource() {
        return this.key.source;
    }
    
    public void setUserId(String source) {
        this.key.source = source;
    }
    
    @SuppressWarnings("serial")
    @Embeddable
    public static class UserSourceKey implements Serializable {
        
        @Column(name = "user_id")
        private Integer userId;
        
        @Column(name = "source")
        private String source;
        
        public UserSourceKey() {
            
        }
    }
}
