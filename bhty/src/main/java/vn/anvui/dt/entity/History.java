package vn.anvui.dt.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.data.annotation.ReadOnlyProperty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "history")
public class History {
    @JsonIgnore
    @EmbeddedId
    HistoryKey key;

    @Column(name = "user_id", updatable = false, insertable = false)
    @ReadOnlyProperty
    Integer userId;
    
    @Column(name = "product_id", updatable = false, insertable = false)
    @ReadOnlyProperty
    Integer productId;
    
    @Column(name = "package_id")
    Integer packageId;
    
    @Column(name = "input_data")
    String inputData;
    
    @Column(name = "created_time")
    private
    Date createdTime = new Date();
    
    public History() {
        super();
    }

    public History(Integer userId, Integer productId) {
        super();
        
        this.key = new HistoryKey(userId, productId);
    }

    public HistoryKey getKey() {
        return key;
    }

    public void setKey(HistoryKey key) {
        this.key = key;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getInputData() {
        return inputData;
    }

    public void setInputData(String inputData) {
        this.inputData = inputData;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }
    
    @SuppressWarnings("serial")
    @Embeddable
    public class HistoryKey implements Serializable {
        @Column(name = "user_id")
        Integer userId;
        
        @Column(name = "product_id")
        Integer productId;
        
        public HistoryKey() {
            super();
        }
        
        public HistoryKey(Integer userId, Integer productId) {
            super();
            this.userId = userId;
            this.productId = productId;
        }

        public Integer getUserId() {
            return userId;
        }
        
        public void setUserId(Integer userId) {
            this.userId = userId;
        }
        
        public Integer getProductId() {
            return productId;
        }
        
        public void setProductId(Integer productId) {
            this.productId = productId;
        }
    }
}
