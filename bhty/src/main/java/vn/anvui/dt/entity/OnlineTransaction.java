package vn.anvui.dt.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import vn.anvui.dt.model.TransactionReportDetail;

@SqlResultSetMapping(name = "TransactionReportDetail",
classes = {
        @ConstructorResult(targetClass = TransactionReportDetail.class,
                columns = {
                        @ColumnResult(name = "createdDate", type = Date.class),
                        @ColumnResult(name = "amount", type = Double.class),
                        @ColumnResult(name = "paymentId"),
                        @ColumnResult(name = "paymentType", type = Integer.class),
                        @ColumnResult(name = "merchantId"),
                        @ColumnResult(name = "contractCode"),
                        @ColumnResult(name = "email"),
                        @ColumnResult(name = "customerName"),
                        @ColumnResult(name = "partnerName"),
                        @ColumnResult(name = "status", type = Integer.class)
        })
})
@Entity
@Table(name = "online_transaction")
public class OnlineTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    private Date createdDate = new Date();
    
    @Column(name = "contract_id")
    private Integer contractId;
    
    @Column(name = "merchant_id")
    private String merchantId;
    
    @Column(name = "amount")
    private Long amount;
    
    @Column(name = "payment_type")
    private Integer paymentType;
    
    @Column(name = "status")
    private Integer status;
    
    @Column(name = "payment_id")
    private String paymentId;
    
    @Column(name = "payment_return_code")
    private Integer returnCode;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expired_date")
    private Date expiredDate;
    
    @Column(name = "source")
    private byte source;
    
    @Column(name = "return_url")
    private String returnURL;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public Integer getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public byte getSource() {
        return source;
    }

    public void setSource(byte source) {
        this.source = source;
    }

    public String getReturnURL() {
        return returnURL;
    }

    public void setReturnURL(String returnURL) {
        this.returnURL = returnURL;
    }

}
