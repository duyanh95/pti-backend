package vn.anvui.dt.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "user_closure")
public class UserClosure {
    
    @JsonIgnore
    @EmbeddedId
    private UserClosureKey key;
    
    @Column(name = "depth")
    private Integer depth;
    
    public UserClosure() {
        
    }
    
    public UserClosure(Integer id, Integer depth) {
        this.key = new UserClosureKey();
        this.key.ascendanceId = id;
        this.key.descendanceId = id;
        this.depth = depth;
    }
    
    @Override
    public UserClosure clone() {
        UserClosure newClosure = new UserClosure();
        newClosure.setAscendanceId(this.getAscendanceId());
        newClosure.setDescendanceId(this.getDescendanceId());
        newClosure.setDepth(this.depth);
        return newClosure;
    }
    
    public UserClosureKey getKey() {
        return key;
    }

    public void setKey(UserClosureKey key) {
        this.key = key;
    }
    
    public void setAscendanceId(Integer ascId) {
        if (this.key == null)
            this.key = new UserClosureKey();
        this.key.ascendanceId = ascId;
    }
    
    public Integer getAscendanceId() {
        return this.key.ascendanceId;
    }
    
    public void setDescendanceId(Integer descId) {
        if (this.key == null)
            this.key = new UserClosureKey();
        this.key.descendanceId = descId;
    }
    
    public Integer getDescendanceId() {
        return this.key.descendanceId;
    }
    
    public void setDepth(Integer depth) {
        this.depth = depth;
    }
    
    public Integer getDepth() {
        return this.depth;
    }

    @SuppressWarnings("serial")
    @Embeddable
    public static class UserClosureKey implements Serializable {
        @Column(name = "ascendance_id")
        protected Integer ascendanceId;
        
        @Column(name = "descendance_id")
        protected Integer descendanceId;
        
        public UserClosureKey() {
            
        }
        
        public UserClosureKey(Integer ascId, Integer descId) {
            this.ascendanceId = ascId;
            this.descendanceId = descId;
        }
    }
}
