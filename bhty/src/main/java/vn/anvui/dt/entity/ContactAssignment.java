package vn.anvui.dt.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "contact_assignment")
public class ContactAssignment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    
    @Column(name = "user_id")
    private Integer userId;
    
    @Column(name = "number_of_contact")
    private Integer noContact;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_time")
    private Date createdTime;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "assignmentId")
    private List<Customer> contacts;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getNoContact() {
        return noContact;
    }

    public void setNoContact(Integer noContact) {
        this.noContact = noContact;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public List<Customer> getContacts() {
        return contacts;
    }

    public void setContacts(List<Customer> contacts) {
        this.contacts = contacts;
    }
}
