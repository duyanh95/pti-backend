package vn.anvui.dt.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "notification")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    Integer id;
    
    @Column(name = "notification_type", nullable = false)
    Integer notificationType;
    
    @Column(name = "user_noti_type", nullable = false)
    Integer userNotificationType;
    
    @Column(name = "content", nullable = false)
    String content;
    
    @Column(name = "title", nullable = false)
    private
    String title;
    
    @Column(name = "icon")
    String icon;
    
    @Column(name = "image")
    String image;
    
    @Column(name = "redirect_link")
    String redirectLink;
    
    @Column(name = "read")
    Boolean read = false;
    
    @Column(name = "user_id")
    Integer userId;
    
    @Column(name = "created_date")
    Date createdDate = new Date();
    
    public Notification() {
        
    }

    public Notification(NotificationType type) {
        this.notificationType = type.getId();
        this.content = type.getContent();
        this.title = type.getTitle();
        this.icon = type.getIcon();
        this.image = type.getImage();
        this.redirectLink = type.getRedirectLink();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(Integer notificationType) {
        this.notificationType = notificationType;
    }

    public Integer getUserNotificationType() {
        return userNotificationType;
    }

    public void setUserNotificationType(Integer userNotificationType) {
        this.userNotificationType = userNotificationType;
    }

    public String getRedirectLink() {
        return redirectLink;
    }

    public void setRedirectLink(String redirectLink) {
        this.redirectLink = redirectLink;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
