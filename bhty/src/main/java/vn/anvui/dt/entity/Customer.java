package vn.anvui.dt.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.anvui.dt.model.ContactCrmInfo;

@SqlResultSetMapping(name = "ContactCrmInfo", classes = {
        @ConstructorResult(targetClass = ContactCrmInfo.class, columns = { @ColumnResult(name = "customerName"),
                @ColumnResult(name = "citizenId"), @ColumnResult(name = "dob", type = Date.class),
                @ColumnResult(name = "gender", type = Integer.class), @ColumnResult(name = "partnerName"),
                @ColumnResult(name = "partnerCitizenId"), @ColumnResult(name = "partnerDob", type = Date.class),
                @ColumnResult(name = "partnerGender", type = Integer.class), @ColumnResult(name = "phoneNumber"),
                @ColumnResult(name = "email"), @ColumnResult(name = "createdDate", type = Date.class),
                @ColumnResult(name = "fee", type = Double.class), @ColumnResult(name = "team"),
                @ColumnResult(name = "agent"), @ColumnResult(name = "campaign"), @ColumnResult(name = "medium"),
                @ColumnResult(name = "source"), @ColumnResult(name = "channel") }) })
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler" })
@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "customer_name", length = 500, nullable = false)
    private String customerName;

    @Column(name = "citizen_id", length = 40, unique = true)
    private String citizenId;

    @Column(name = "gender")
    private Integer gender;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dob")
    private Date dob;

    @Column(name = "photo", length = 500)
    private String photo;

    @Column(name = "email", length = 500)
    private String email;

    @Column(name = "phone_number", length = 20)
    private String phoneNumber;

    @Column(name = "contract_id")
    private Integer contractId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    private Date createdDate = new Date();

    @Column(name = "tag")
    private Integer tag = 0;

    @Column(name = "note")
    private String note;

    @Column(name = "assigned_user_id")
    private Integer assignedUserId;

    @Column(name = "assignment_id")
    private Integer assignmentId;

    @Column(name = "address")
    private String address;

    @Column(name = "export_status")
    private Byte exportStatus = 0;

    @Column(name = "province")
    private String province;

    public Customer() {
        super();
    }

    public Customer(Integer id, String customerName, String citizenId, Integer gender, Date dob, String photo,
            String email, String phoneNumber, Integer tag, String note, String address, String province) {
        super();
        this.id = id;
        this.customerName = customerName;
        this.citizenId = citizenId;
        this.gender = gender;
        this.dob = dob;
        this.photo = photo;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.tag = tag;
        this.note = note;
        this.address = address;
        this.province = province;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getAssignedUserId() {
        return assignedUserId;
    }

    public void setAssignedUserId(Integer assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    public Integer getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Integer assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Byte getExportStatus() {
        return exportStatus;
    }

    public void setExportStatus(Byte exportStatus) {
        this.exportStatus = exportStatus;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}
