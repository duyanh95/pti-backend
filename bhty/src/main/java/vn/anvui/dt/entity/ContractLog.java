package vn.anvui.dt.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "contract_log")
public class ContractLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "contract_id", nullable = false)
    private Integer contractId;
    
    @Column(name = "created_user_id")
    private Integer createdUserId;
    
    @Column(name = "contract_status")
    private Integer contractStatus;
    
    @Column(name = "contract_code")
    private String contractCode;
    
    @Column(name = "customer_name")
    private String customerName;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "customer_dob")
    private Date customerDob;
    
    @Column(name = "customer_citizen_id")
    private String customerCitizenId;
    
    @Column(name = "partner_name")
    private String partnerName;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "partner_dob")
    private Date partnerDob;
    
    @Column(name = "partner_citizen_id")
    private String partnerCitizenId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    private Date createdDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Integer getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(Integer contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Date getCustomerDob() {
        return customerDob;
    }

    public void setCustomerDob(Date customerDob) {
        this.customerDob = customerDob;
    }

    public String getCustomerCitizenId() {
        return customerCitizenId;
    }

    public void setCustomerCitizenId(String customerCitizenId) {
        this.customerCitizenId = customerCitizenId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public Date getPartnerDob() {
        return partnerDob;
    }

    public void setPartnerDob(Date partnerDob) {
        this.partnerDob = partnerDob;
    }

    public String getPartnerCitizenId() {
        return partnerCitizenId;
    }

    public void setPartnerCitizenId(String partnerCitizenId) {
        this.partnerCitizenId = partnerCitizenId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
