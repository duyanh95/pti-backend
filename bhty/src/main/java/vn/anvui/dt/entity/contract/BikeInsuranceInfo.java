package vn.anvui.dt.entity.contract;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import vn.anvui.dt.entity.Contract;

@Entity
@Table(name = "bike_insurance_info")
public class BikeInsuranceInfo extends AbstractContractInfo {
    @Column(name ="reg_number", nullable = false)
    String registeredNumber;
    
    @Column(name = "reg_address", nullable = false)
    String registeredAddress;
    
    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn(name = "contract_id", referencedColumnName = "id")
    Contract contract;

    public String getRegisteredNumber() {
        return registeredNumber;
    }

    public void setRegisteredNumber(String registeredNumber) {
        this.registeredNumber = registeredNumber;
    }

    public String getRegisteredAddress() {
        return registeredAddress;
    }

    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

}
