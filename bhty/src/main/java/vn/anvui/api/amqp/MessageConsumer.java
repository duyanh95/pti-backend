package vn.anvui.api.amqp;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.anvui.bsn.beans.contract.ContractService;
import vn.anvui.bsn.beans.customer.CustomerService;
import vn.anvui.bsn.beans.notification.NotificationService;
import vn.anvui.bsn.beans.rabbit.queues.AnvuiQueue;
import vn.anvui.bsn.beans.sms.SmsService;
import vn.anvui.bsn.beans.tracking.TrackingService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.customer.CustomerRequest;
import vn.anvui.bsn.dto.notification.NotificationDto;
import vn.anvui.bsn.dto.sms.SmsQueueDto;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.model.ContractWrapper;

@Component
public class MessageConsumer {
    
    private static final Logger logger = Logger.getLogger(MessageConsumer.class.getName());
    
    @Autowired
    ContractService contractService;
    
    @Autowired
    CustomerService customerService;
    
    @Autowired
    TrackingService trackingService;
    
    @Autowired
    SmsService smsService;
    
    @Autowired
    NotificationService notiService;
    
    @RabbitListener(queues = {AnvuiQueue.SMS_QUEUE})
    public void receiveMessage(SmsQueueDto sms) {
        logger.info("send sms to: " + sms.getReceivers());
        smsService.sendSms(sms.getReceivers(), sms.getContent());
    }
    
    @RabbitListener(queues = {AnvuiQueue.MAIL_QUEUE})
    public void receiveMessage(ContractWrapper contract) {
        try {
            logger.info("send mail for contract Id: " + contract.getContract().getId());
            contractService.sendMailToCustomer(contract);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    @RabbitListener(queues = {AnvuiQueue.CUSTOMER_QUEUE})
    public void createCustomer(CustomerRequest req) throws AnvuiBaseException {
        Customer customer = customerService.create(req);

        try {
            trackingService.updateCustomerTracking(req.getTrackingId(), customer);
        } catch (Exception e) {
            logger.log(Level.WARNING, "Tracking update failed");
        }
    }
    
    @RabbitListener(queues = {AnvuiQueue.PNS_QUEUE})
    public void sendNotification(NotificationDto notiDto) {
        notiService.createAndSendNotification(notiDto);
    }
}
