package vn.anvui.api.amqp;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.anvui.bsn.beans.rabbit.queues.AnvuiQueue;
import vn.anvui.bsn.dto.customer.CustomerRequest;
import vn.anvui.bsn.dto.notification.NotificationDto;
import vn.anvui.bsn.dto.sms.SmsQueueDto;
import vn.anvui.dt.model.ContractWrapper;

@Component
public class MessageProducer {

    @Autowired
    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public MessageProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendSms(SmsQueueDto message) {
        rabbitTemplate.convertAndSend(AnvuiQueue.SMS_QUEUE, message);
    }
    
    public void sendMailQueue(ContractWrapper contract) {
        rabbitTemplate.convertAndSend(AnvuiQueue.MAIL_QUEUE, contract);
    }
    
    public void createCustomerQueue(CustomerRequest customer) {
        rabbitTemplate.convertAndSend(AnvuiQueue.CUSTOMER_QUEUE, customer);
    }
    
    public void sendPushServiceQueue(NotificationDto noti) {
        rabbitTemplate.convertAndSend(AnvuiQueue.PNS_QUEUE, noti);
    }
}
