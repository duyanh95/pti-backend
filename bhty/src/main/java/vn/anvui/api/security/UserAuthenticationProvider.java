package vn.anvui.api.security;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.apache.ApacheHttpTransport;
import com.google.api.client.json.gson.GsonFactory;

import vn.anvui.bsn.auth.AnvuiAuthenticationToken;
import vn.anvui.bsn.auth.CredentialService;
import vn.anvui.bsn.beans.notification.NotificationService;
import vn.anvui.bsn.beans.user.UserService;
import vn.anvui.bsn.dto.account.LoginRequest;
import vn.anvui.bsn.dto.user.UserRequest;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.DeviceType;
import vn.anvui.dt.enumeration.UserStatus;
import vn.anvui.dt.repos.user.UserRepository;
import vn.anvui.dt.utils.EncryptionHelper;

public class UserAuthenticationProvider implements AuthenticationProvider {
    final Logger log = Logger.getLogger(AuthenticationProvider.class.getSimpleName());
    
    public static final String CLIENT_ID = "526683184146-csabk5g682g9jdr2kd4c2935ltcfh728.apps.googleusercontent.com";

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private CredentialService credentialSv;
    
    @Autowired
    NotificationService notiService;
    
    @Autowired
    UserService userService;
    
    @Bean
    public GoogleIdTokenVerifier verifier() {
        return new GoogleIdTokenVerifier.Builder(new ApacheHttpTransport(), new GsonFactory())
                .setAudience(Collections.singletonList(CLIENT_ID)).build();
    }
    
    PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Authentication authenticate(Authentication request) throws AuthenticationException {

        // if already been authenticated by previous filter then return
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.isAuthenticated())
            return auth;
        
        AnvuiAuthenticationToken authentication = null;
        User user = null;

        String username = (String) request.getPrincipal();
        String password = (String) request.getCredentials();
        LoginRequest requestData = (LoginRequest) request.getDetails();
        
        if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)) {

            user = userRepo.findFirstByUserName(username);
            
            if (user == null) {
                throw new AuthenticationCredentialsNotFoundException("Invalid credentials supplied");
            } else {
               
                if (!EncryptionHelper.bcryptPasswordMatch(password, user.getPassword())) {
                    throw new AuthenticationCredentialsNotFoundException("Invalid credentials supplied");
                }
            }
        } else if (!StringUtils.isEmpty(requestData.getGoogleId())) {
            GoogleIdToken ggToken;
            try {
                ggToken = verifier().verify(requestData.getGoogleId());
            } catch (Exception e) {
                log.log(Level.SEVERE, e.getMessage(), e);
                throw new BadCredentialsException("Invalid credentials supplied");
            }
            if (ggToken == null) {
                log.info("ggToken is null");
                throw new BadCredentialsException("Invalid credentials supplied");
            }
            
            Payload payload = ggToken.getPayload();
            
            user = userRepo.findFirstByGoogleId(payload.getSubject());
            if (user == null) {
                user = userService.createUserFromGooglePayload(payload);
            }
            
        } else {
            throw new BadCredentialsException("Invalid credentials supplied");
        }

        if (user.getStatus() == UserStatus.DELETED.getValue()) {
            throw new AuthenticationCredentialsNotFoundException("Account deactivated");
        }
        // Save this authentication
        authentication = credentialSv.store(user, requestData);
        
        if (!StringUtils.isEmpty(requestData.getDeviceToken())) {
            notiService.addUserToken(requestData.getDeviceToken(), user.getId(), 
                    DeviceType.ANDROID);
            
            notiService.subscribeSystemTopic(Arrays.asList(requestData.getDeviceToken()));
        }

        AuthenticationDetails details = (AuthenticationDetails) authentication.getDetails();

        credentialSv.increaseExpiredDate(details.getAccessToken().getToken());
        return authentication;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
