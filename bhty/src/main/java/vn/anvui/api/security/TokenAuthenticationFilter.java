package vn.anvui.api.security;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import vn.anvui.bsn.auth.AnvuiAuthenticationToken;
import vn.anvui.bsn.auth.CredentialService;
import vn.anvui.bsn.beans.user.UserService;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.AccessToken;

public class TokenAuthenticationFilter extends GenericFilterBean {

    public static final String PERMISSION_NEED_APPLY = "Permission-Apply";
    
    static final Logger log = Logger.getLogger(TokenAuthenticationFilter.class.getSimpleName());

    @Autowired
    private CredentialService credentialSv;

    @SuppressWarnings("unused")
    @Autowired
    private UserService userSv;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        String authToken = ((HttpServletRequest) request).getHeader(SecurityConst.AUTH_TOKEN);
        Authentication authentication = credentialSv.retrieve(authToken);
        log.info("authentication token: " + authToken);

        if (authentication != null) {

            // get details from cache
            AuthenticationDetails details = (AuthenticationDetails) authentication.getDetails();

            if (details != null && (details.getQueueCreated() == null || !details.getQueueCreated())) {
                AccessToken token = details.getAccessToken();
                details.setQueueCreated(true);

                // must save back details to cache to prevent create queue again
                credentialSv.store(token, authentication);
            }

            if (authentication instanceof AnvuiAuthenticationToken) {
                AnvuiAuthenticationToken token = (AnvuiAuthenticationToken) authentication;
                if (token.isPermissionChanged()) {
                    HttpServletResponse resp = (HttpServletResponse) response;
                    resp.addIntHeader(PERMISSION_NEED_APPLY, 1);
                }
            }
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        chain.doFilter(request, response); // always continue
    }
}
