package vn.anvui.api.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.anvui.api.common.HttpPrintErrorHelper;
import vn.anvui.api.rest.global.AccountDeactiveLoginExceptionHandler;
import vn.anvui.api.rest.global.AccountNotActiveLoginExceptionHandler;
import vn.anvui.api.rest.global.InvalidRoleException;
import vn.anvui.bsn.auth.AnvuiAuthenticationToken;
import vn.anvui.bsn.auth.MultipleAccountAuthenticationToken;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.account.LoginRequest;
import vn.anvui.bsn.dto.account.LoginResponse;
import vn.anvui.bsn.dto.account.MultipleAccountLoginResponse;

public class AuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    public static String detectClientIP(HttpServletRequest request) {
        String clientIp = request.getHeader("X-FORWARDED-FOR");
        if (clientIp == null) {
            clientIp = request.getRemoteAddr();
        }
        return clientIp;
    }

    public static String detectWebsiteIP(HttpServletRequest request) {
        String clientIp = request.getRemoteAddr();
        if (clientIp == null) {
            clientIp = request.getHeader("X-FORWARDED-FOR");
        }
        return clientIp;
    }

    final static Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);

    private AuthenticationManager authenticationManager;

    public AuthenticationFilter(String urlMapping, AuthenticationManager authenticationManager) {
        super(new AntPathRequestMatcher(urlMapping));
        this.authenticationManager = authenticationManager;
    }

    /**
     * @api {post} /login Login
     * @apiName Authentication
     * @apiDescription Let user log in to system with username and password
     * @apiGroup Authentication
     * @apiVersion 0.9.0
     * 
     * 
     * @apiHeaderExample {json} Header-Example: { "AV-Auth-Username": "tester",
     *                   "AV-Auth-Password": "123456dklflfl325435345" }
     * 
     * @apiExample {json} Example POST body: { "platformType": 1, // 1: IOS, 2:
     *             ANDROID, 3: WEB, 4: WEB_ADMIN "platformVersion": "1.0",
     *             "deviceToken": "DEVICE PUSH NOTIFICATION TOKEN", "deviceId":
     *             "DEVICE ID" }
     * 
     * @apiSuccessExample {json} Success-Response: HTTP/1.1 200 OK { "error": null,
     *                    "responseData": { "account": { "id": 1, "userName":
     *                    "tester", "fullName": "tester", "password":
     *                    "123456dklflfl325435345", "salt": null, "email": null,
     *                    "phoneNumber": "0984055083", "stateCode": 84, "companyId":
     *                    null, "dob": null, "facebookId": null, "googleId": null,
     *                    "createdDate": null, "gender": null, "avatar": null,
     *                    "district": null, "address": null, "lastLogin": null,
     *                    "notifyOption": null, "userType": 1, "userStatus": 1,
     *                    "otpCode": null, "groupPermissionId": null,
     *                    "educationLevel": null, "userReferrerId": null,
     *                    "phoneNumberReferrer": null, "role": "ROLE_ADMIN",
     *                    "personalRoutingKey": "admin.1.#", "admin": true },
     *                    "expiredDate": "Fri Jun 29 09:51:46 ICT 2018", "token":
     *                    "178cc922532f4ac3bd224467ad1383b7" }, "extraData": null,
     *                    "successMessage": null, "warningMessage": null }
     **/
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException, IOException, ServletException {

        try {
            LoginRequest payload = new ObjectMapper().readValue(request.getInputStream(), LoginRequest.class);

            logger.debug("Trying to authenticate user {} by AV-Auth-Username method", payload.getUserName());

            if (StringUtils.isEmpty(payload.getUserName()) && !StringUtils.isEmpty(payload.getGoogleId())) {
                logger.info(payload.getGoogleId());
                payload.setUserName(payload.getGoogleId());
            }
            
            final UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                    payload.getUserName(), payload.getPassword());

            authentication.setDetails(payload);

            return authenticationManager.authenticate(authentication);

        } catch (JsonProcessingException je) {
            logger.error(je.getMessage(), je);
            
            throw new InsufficientAuthenticationException("Request body is not a valid JSON", je);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
            Authentication authResult) throws IOException, ServletException {

        SecurityContextHolder.getContext().setAuthentication(authResult);
        response.setStatus(HttpServletResponse.SC_OK);

        String tokenJsonResponse;
        try {

            ResponseObject<Object> data = new ResponseObject<Object>();
            if (authResult instanceof MultipleAccountAuthenticationToken) {
                data.setError(ErrorInfo.MULTIPLE_ACCOUNT_LOGIN_ERROR);
                data.setResponseData(new MultipleAccountLoginResponse(authResult));
            } else {
                AnvuiAuthenticationToken auth = (AnvuiAuthenticationToken) authResult;
                LoginResponse res = new LoginResponse(auth);
                res.setRole(auth.getUser().getUserType());
                res.setUser(auth.getUser());
                data.setResponseData(res);
            }
            ObjectMapper mapper = new ObjectMapper();
            mapper.setSerializationInclusion(Include.NON_NULL);
            tokenJsonResponse = mapper.writeValueAsString(data);

        } catch (JsonProcessingException e) {
            logger.error("Error processing authentication", e);
            tokenJsonResponse = "";
        }
        response.addHeader("Content-Type", "application/json;charset=UTF-8");
        response.getWriter().print(tokenJsonResponse);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException failed) throws IOException, ServletException {
        
        logger.warn(failed.getMessage());

        if (failed instanceof AuthenticationCredentialsNotFoundException) {
            HttpPrintErrorHelper.printError(response, ErrorInfo.LOGIN_FAILED, HttpServletResponse.SC_UNAUTHORIZED);

        } else if (failed instanceof InsufficientAuthenticationException) {
            HttpPrintErrorHelper.printError(response, ErrorInfo.BAD_REQUEST, HttpServletResponse.SC_BAD_REQUEST);

        } else if (failed instanceof InternalAuthenticationServiceException) {
            logger.error("Internal authentication service exception", failed);
            HttpPrintErrorHelper.printError(response, ErrorInfo.INTERNAL_SERVER_ERROR,
                    HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

        } else if (failed instanceof BadCredentialsException) {

            HttpPrintErrorHelper.printError(response, ErrorInfo.STATE_INVALID, HttpServletResponse.SC_UNAUTHORIZED);

        } else if (failed instanceof AccountNotActiveLoginExceptionHandler) {
            HttpPrintErrorHelper.printError(response, ErrorInfo.ACCOUNT_NOT_ACTIVE_LOGIN_FAILED,
                    HttpServletResponse.SC_UNAUTHORIZED);
        } else if (failed instanceof AccountDeactiveLoginExceptionHandler) {
            HttpPrintErrorHelper.printError(response, ErrorInfo.ACCOUNT_DE_ACTIVE_LOGIN_FAILED,
                    HttpServletResponse.SC_UNAUTHORIZED);
        } else if (failed instanceof InvalidRoleException) {
            HttpPrintErrorHelper.printError(response, ErrorInfo.INVALID_LOGIN_ROLE,
                    HttpServletResponse.SC_UNAUTHORIZED);
        } else if (failed instanceof AuthenticationException) {
            Throwable t = failed.getCause();

            if (t != null && AnvuiBaseException.class.isAssignableFrom(t.getClass())) {
                HttpPrintErrorHelper.printError(response, ((AnvuiBaseException) t).getError(),
                        HttpServletResponse.SC_UNAUTHORIZED);
            } else {
                HttpPrintErrorHelper.printError(response, ErrorInfo.LOGIN_FAILED, HttpServletResponse.SC_UNAUTHORIZED);
            }
        }
    }
}
