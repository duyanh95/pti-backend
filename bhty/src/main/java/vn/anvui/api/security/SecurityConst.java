package vn.anvui.api.security;

public interface SecurityConst {

	public static final String USER_NAME = "AV-Auth-Username";
	public static final String PASSWORD = "AV-Auth-Password";
	public static final String AUTH_TOKEN = "AV-Auth-Token";

	public static final String REQUEST_BODY = "REQUEST_BODY";
	public static final String CLIENT_ID = "CLIENT_ID";
	public static final String AUTH_CLIENT_ID = "X-Auth-ClientId";
}
