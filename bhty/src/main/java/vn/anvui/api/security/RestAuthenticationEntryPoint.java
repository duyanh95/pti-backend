package vn.anvui.api.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import vn.anvui.api.common.HttpPrintErrorHelper;
import vn.anvui.bsn.common.ErrorInfo;

@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {

		int code = HttpServletResponse.SC_UNAUTHORIZED;

		ErrorInfo error = new ErrorInfo(code, 401, exception.getMessage());

		HttpPrintErrorHelper.printError(response, error, code);

	}
}
