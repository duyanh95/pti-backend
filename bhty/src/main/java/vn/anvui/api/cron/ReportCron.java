package vn.anvui.api.cron;

import java.util.Date;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import vn.anvui.bsn.auth.CredentialService;
import vn.anvui.bsn.beans.incomeReport.IncomeReportService;
import vn.anvui.bsn.beans.txnUtils.TransactionUtilsService;

@Component
public class ReportCron {
    private static final Logger log = Logger.getLogger(ReportCron.class.getName());
    
    @Autowired
    CredentialService credentialService;
    
    @Autowired
    IncomeReportService reportService;
    
    @Autowired
    TransactionUtilsService txnUtilsService;
    
    @Autowired
    @Qualifier("ZaloPayTransactionService")
    
    @Scheduled(cron = "0 30 0 * * *")
    public void createDailyIncomeReport() {
        reportService.createIncomeReportByDay(7, new Date());
    }
    
    @Scheduled(cron = "0 0 0 * * *") 
    public void cleanExpiredToken() {
        credentialService.removeExpiredToken();
    }
    
    //TODO: update expired zalo transactions every 20 minutes
    @Scheduled(fixedRate = 1200000)
    public void updateExpiredZaloTransaction() {
        
    }
}
