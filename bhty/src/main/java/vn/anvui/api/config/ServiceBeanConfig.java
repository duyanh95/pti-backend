package vn.anvui.api.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import vn.anvui.dt.common.ApiConts;

@Configuration
@ComponentScan(basePackages = { ApiConts.BUSINESS_BEAN_PACKAGE, ApiConts.BUSINESS_AUTH_PACKAGE })
public class ServiceBeanConfig {

}
