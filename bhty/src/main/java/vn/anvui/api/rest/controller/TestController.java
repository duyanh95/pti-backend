package vn.anvui.api.rest.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import vn.anvui.api.amqp.MessageProducer;
import vn.anvui.bsn.beans.contract.ContractService;
import vn.anvui.bsn.beans.incomeReport.IncomeReportService;
import vn.anvui.bsn.beans.mailing.MailingService;
import vn.anvui.bsn.beans.misc.MiscellaneousService;
import vn.anvui.bsn.beans.notification.NotificationService;
import vn.anvui.bsn.beans.pdf.PdfService;
import vn.anvui.bsn.beans.sms.SmsService;
import vn.anvui.bsn.beans.tracking.TrackingService;
import vn.anvui.bsn.beans.transaction.PayooMailService;
import vn.anvui.bsn.beans.user.UserService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.mailing.MailRequest;
import vn.anvui.bsn.dto.notification.NotificationDto;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.Tracking;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.customer.CustomerRepository;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.repos.tracking.TrackingRepository;
import vn.anvui.dt.repos.userClosure.UserClosureRepository;
import vn.anvui.dt.repos.userSource.UserSourceRepository;
import vn.anvui.dt.utils.DateTimeHelper;

@RestController
@RequestMapping
public class TestController {
    
    Logger log = Logger.getLogger(TestController.class.getName());
    
    @Autowired
    MailingService sendGridEmailSevice;
    
    @Autowired
    ContractService contractService;
    
    @Autowired
    PdfService pdfService;
    
    @Autowired
    TrackingService trackService;
    
    @Autowired
    TrackingRepository trackRepo;
    
    @Autowired
    ContractRepository contractRepo;
    
    @Autowired
    UserSourceRepository userSourceRepo;
    
    @Autowired
    UserClosureRepository closureRepo;
    
    @Autowired
    CustomerRepository customerRepo;
    
    @Autowired
    MessageProducer messageProducer;
    
    @Autowired
    SmsService smsService;
    
    @Autowired
    UserService userService;
    
    @Autowired
    MiscellaneousService miscService;
    
    @Autowired
    IncomeReportService reportService;
    
    @Autowired
    NotificationService notiService;
    
    @Autowired
    PayooMailService payooMailService;
    
    @GetMapping("/test/time")
    public Long getTime() {
        return DateTimeHelper.getFirstTimeOfDay(new Date(), TimeZone.getTimeZone("GMT+7:00")).getTime();
    }
    
    @GetMapping("/test/ping")
    public String ping() {
        return "pong";
    }

    @GetMapping("/test/payoo-mail")
    public String testPayooMail(@RequestParam String name, @RequestParam String email, @RequestParam String paymentId) throws IOException {
        
        payooMailService.sendGuideMail(email, name, paymentId);
        
        return "done";
    }
        
//    @PostMapping("/add-closure")
//    public ResponseEntity<Object> addClosure(@RequestParam Integer userId, 
//            @RequestParam(required = false) Integer parentId) {
//        ResponseObject<Object> response = new ResponseObject<Object>();
//        
//        if (parentId == null) {
//            UserClosure closure = new UserClosure(userId, 1);
//            closure = closureRepo.save(closure);
//            
//        } else {
//            if (!userRepo.exists(parentId))
//                throw new AnvuiBaseException(ErrorInfo.USER_PARENT_NOT_EXISTED);
//            
//            Integer depth = closureRepo.findDepthFromParent(parentId) + 1;
//            
//            List<UserClosure> ascendanceClosures = closureRepo.findAscendanceFromId(parentId);
//            List<UserClosure> closures = new LinkedList<>();
//            
//            ascendanceClosures.forEach((ascClosure) -> {
//                UserClosure closure = ascClosure.clone();
//                closure.setDescendanceId(userId);
//                closures.add(closure);
//            });
//            
//            closures.add(new UserClosure(userId, depth));
//            closureRepo.save(closures);
//        }
//        
//        response.setSuccessMessage("success");
//        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK); 
//    }

    @PostMapping("/test/send-test-mail")
    public String send(@RequestBody @Valid MailRequest req) {
        log.info("test mail requested");
        
        boolean success = false;
        try {
            success = sendGridEmailSevice.sendEmailWithAttachment(req.getToList(), req.getCcList(), req.getBccList(), req.getSubject(), 
                    req.getContent(), "", "/tmp/rs.pdf");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        if (success)
            return "success";
        else
            return "fail";
    }
    
    @PostMapping("/test/test-contract-mail")
    public String pdf(@RequestParam Integer id) {
        try { 
            ContractWrapper contract = contractService.getContractById(id);
            
            messageProducer.sendMailQueue(contract);
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            return "fail";
        }
        
        return "success";
    }
    
    @GetMapping("/test/update-tracking-data")
    public String update() {
        List<Tracking> rsList = trackRepo.findAll();
        
        for (Tracking rs : rsList) {
            if (rs.getContractId() != null) {
                Contract contract = contractRepo.findOne(rs.getContractId());
                rs.setCustomerId(contract.getInheritCustomer().getId());
            }
        }
        
        trackRepo.save(rsList);
        
        return "success";
    }
    
    @PostMapping("/test/send-sms")
    public ResponseEntity<Object> sendSms() throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();

        smsService.sendSms(Arrays.asList("0362205275"), "test sms");

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);   
    }

    @GetMapping("/test/report")
    public ResponseEntity<Object> testReport(
            @RequestParam @DateTimeFormat(pattern="dd-MM-yyyy") Date date,
            @RequestParam Integer days) throws Exception {
        ResponseObject<Object> res = new ResponseObject<Object>();
        
        reportService.createIncomeReportByDay(days, date);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);   
    }
    
    @PostMapping("/test/noti") 
    public ResponseEntity<Object> testReport(@RequestParam Integer type, @RequestParam Integer userId,
            @RequestParam Integer userType, @RequestBody Map<String, String> map) throws Exception {
        ResponseObject<Object> res = new ResponseObject<Object>();
        
        notiService.createAndSendNotification(new NotificationDto(type, userId, userType, map));
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);   
    }
    
//    @GetMapping("test/query")
//    public ResponseEntity<Object> testQuery() throws Exception {
//        ResponseObject<Object> res = new ResponseObject<Object>();
//        
//        List<ContractWrapper> rs = contractRepo.getContractWithAgencyUser();
//        res.setResponseData(rs);
//        
//        res.setSuccessMessage("success");
//        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);   
//    }
    
    @Autowired
    InsurancePackageRepository pkgRepo;
    
    @Autowired
    ObjectMapper mapper;
    
    @CacheEvict(allEntries = true, cacheNames = {"package", "token"})
    @DeleteMapping("/cache/all")
    public String deleteAllCache() {
        return "ok";
    }
}
