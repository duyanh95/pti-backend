package vn.anvui.api.rest.global;

import org.springframework.security.core.AuthenticationException;

public class AccountDeactiveLoginExceptionHandler extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public AccountDeactiveLoginExceptionHandler(String msg) {
		super(msg);
	}

	public AccountDeactiveLoginExceptionHandler(String msg, Throwable t) {
		super(msg, t);
	}
}
