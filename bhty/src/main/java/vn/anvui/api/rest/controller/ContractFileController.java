package vn.anvui.api.rest.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.anvui.bsn.beans.contract.ContractService;
import vn.anvui.bsn.beans.customer.CustomerService;
import vn.anvui.bsn.beans.pdf.PdfService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.dt.model.ContractWrapper;

@Controller
@RequestMapping("/contract")
public class ContractFileController {
    public final static Logger log = Logger.getLogger(ContractFileController.class.getSimpleName());
    
    @Autowired
    ContractService contractService;
    
    @Autowired
    CustomerService customerService;
    
    @Autowired
    PdfService pdfService;
    
    @GetMapping("/print/{id}.jpeg")
    public @ResponseBody ResponseEntity<Object> getContractCert(@PathVariable("id") Integer id)
            throws AnvuiBaseException {
        
        try {
            ContractWrapper contract = contractService.getContractById(id);
            
            File file = pdfService.loadPdfAsResource(contract, true, true);
            File jpegFile = pdfService.convertPdfToImage(file);
            
            Path path = Paths.get(jpegFile.getAbsolutePath());
            Resource resource = null;
            
            try {
                resource = new UrlResource(path.toUri());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
     
            if (resource.exists()) {
                return ResponseEntity.ok()
                        .contentType(MediaType.IMAGE_JPEG)
                        .body(resource);
     
                // return new ResponseEntity<Object>(resource, HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>("File Not Found ", HttpStatus.OK);
            }
        } catch (IOException e1) {
            return new ResponseEntity<Object>("File Not Found ", HttpStatus.OK);
        }
    }
    
    @GetMapping("/print/{id}.pdf")
    public @ResponseBody ResponseEntity<Object> getContractPdfCert(@PathVariable("id") Integer id)
            throws AnvuiBaseException {
        
            ContractWrapper contract = contractService.getContractById(id);
            
            File file = pdfService.loadPdfAsResource(contract, true, true);
            
            Path path = Paths.get(file.getAbsolutePath());
            Resource resource = null;
            
            try {
                resource = new UrlResource(path.toUri());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
     
            if (resource.exists()) {
                return ResponseEntity.ok()
                        .header("content-type", "application/pdf")
                        .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + resource.getFilename() + "\"")
                        .body(resource);
     
                // return new ResponseEntity<Object>(resource, HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>("File Not Found ", HttpStatus.OK);
            }
    }
}
