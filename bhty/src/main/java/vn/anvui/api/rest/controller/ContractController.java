package vn.anvui.api.rest.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.api.amqp.MessageProducer;
import vn.anvui.bsn.beans.contract.ContractService;
import vn.anvui.bsn.beans.transaction.PayooMailService;
import vn.anvui.bsn.beans.transaction.PayooTransactionService;
import vn.anvui.bsn.beans.transaction.TransactionService;
import vn.anvui.bsn.beans.transaction.ViettelPayAPIService;
import vn.anvui.bsn.beans.txnUtils.TransactionUtilsService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.contract.ExportRequest;
import vn.anvui.bsn.dto.customer.CoupleRequest;
import vn.anvui.bsn.dto.transaction.epay.EpayTransactionRequest;
import vn.anvui.bsn.dto.transaction.epay.EpayTransactionStatusRequest;
import vn.anvui.bsn.dto.transaction.napas.NapasTransactionRequest;
import vn.anvui.bsn.dto.transaction.napas.NapasTransactionStatusRequest;
import vn.anvui.bsn.dto.transaction.payoo.PayooTransactionRequest;
import vn.anvui.bsn.dto.transaction.payoo.PayooTransactionResponse;
import vn.anvui.bsn.dto.transaction.payoo.PayooTransactionStatusRequest;
import vn.anvui.bsn.dto.transaction.viettelpay.ViettelPayCheckTransactionRequest;
import vn.anvui.bsn.dto.transaction.viettelpay.ViettelPayCheckTransactionResponse;
import vn.anvui.bsn.dto.transaction.viettelpay.ViettelPayTransactionRequest;
import vn.anvui.bsn.dto.transaction.viettelpay.ViettelPayTransactionStatusRequest;
import vn.anvui.bsn.dto.transaction.viettelpay.ViettelPayTransactionStatusResponse;
import vn.anvui.bsn.dto.transaction.vipay.ViPayTransactionRequest;
import vn.anvui.bsn.dto.transaction.vipay.ViPayTransactionStatusRequest;
import vn.anvui.bsn.dto.transaction.vtcpay.VTCPayTransactionRequest;
import vn.anvui.bsn.dto.transaction.vtcpay.VTCPayTransactionStatusRequest;
import vn.anvui.bsn.dto.transaction.zalo.ZaloPayTransactionRequest;
import vn.anvui.bsn.dto.transaction.zalo.ZaloPayTransactionResponse;
import vn.anvui.bsn.dto.transaction.zalo.ZaloPayTransactionStatusRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.enumeration.TransactionStatus;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.model.RejectedContract;
import vn.anvui.dt.model.TransactionReport;
import vn.anvui.dt.model.TransactionReportDetail;

@RestController
@RequestMapping("/contract")
public class ContractController {
    final Logger log = Logger.getLogger(ContractController.class.getSimpleName());

    @Autowired
    ContractService contractService;

    @Autowired
    @Qualifier("EpayTransactionService")
    TransactionService<EpayTransactionRequest, EpayTransactionStatusRequest> txnService;

    @Autowired
    @Qualifier("NapasTransactionService")
    TransactionService<NapasTransactionRequest, NapasTransactionStatusRequest> napasTxnService;

    @Autowired
    @Qualifier("VTCPayTransactionService")
    TransactionService<VTCPayTransactionRequest, VTCPayTransactionStatusRequest> vtcTxnService;

    @Autowired
    @Qualifier("PayooTransactionService")
    TransactionService<PayooTransactionRequest, PayooTransactionStatusRequest> payooTxnService;
    
    @Autowired
    @Qualifier("ZaloPayTransactionService")
    TransactionService<ZaloPayTransactionRequest, ZaloPayTransactionStatusRequest> zaloTxnService;
    
    @Autowired
    @Qualifier("ViPayTransactionService")
    TransactionService<ViPayTransactionRequest, ViPayTransactionStatusRequest> vipayTxnService;
    
    @Autowired
    @Qualifier("ViettelPayTransactionService")
    TransactionService<ViettelPayTransactionRequest, ViettelPayTransactionStatusRequest> viettelTxnservice;

    @Autowired
    ViettelPayAPIService viettelpayAPIService;
    
    @Autowired
    TransactionUtilsService txnUtilsService;
    
    @Autowired
    PayooMailService payooMailService;
    
    @Autowired
    MessageProducer messageProducer;

    @CrossOrigin
    @PostMapping(value = {"/transact", "/transact/epay"})
    @ResponseBody
    ResponseEntity<Object> contractOnlinePayment(@RequestBody @Valid EpayTransactionRequest req) {
        ResponseObject<Object> res = new ResponseObject<>();

        try {
            ContractWrapper contract = contractService.getContractById(req.getContractId());

            String URL = txnService.createTransaction(contract, req);

            res.setResponseData(URL);
        } catch (AnvuiBaseException e) {
            log.log(Level.WARNING, e.getMessage(), e);
            res.setError(e.getError());
            return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.valueOf(e.getError().getHttpStatus()));
        }

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @CrossOrigin
    @PostMapping("/transact/viettelpay")
    ResponseEntity<Object> viettelPayGatewayPayment(@RequestBody ViettelPayTransactionRequest req) 
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();
        
        ContractWrapper contract = contractService.getContractById(req.getContractId());
        
        String URL = viettelTxnservice.createTransaction(contract, req);
        
        res.setResponseData(URL);
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK); 
    }

    @CrossOrigin
    @PostMapping("/transact/napas")
    @ResponseBody
    ResponseEntity<Object> napasOnlinePayment(@RequestBody NapasTransactionRequest req) throws AnvuiBaseException {
        // String ipAddr = request.getRemoteAddr();

        ResponseObject<Object> res = new ResponseObject<>();

        ContractWrapper contract = contractService.getContractById(req.getContractId());

        String URL = napasTxnService.createTransaction(contract, req);

        res.setResponseData(URL);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @CrossOrigin
    @PostMapping("/transact/zalo")
    @ResponseBody
    ResponseEntity<Object> zaloOnlinePayment(@RequestBody @Valid ZaloPayTransactionRequest req) throws AnvuiBaseException {
        // String ipAddr = request.getRemoteAddr();

        ResponseObject<Object> res = new ResponseObject<>();

        ContractWrapper contract = contractService.getContractById(req.getContractId());

        String URL = zaloTxnService.createTransaction(contract, req);

        res.setResponseData(URL);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
//    @CrossOrigin
//    @PostMapping("/transact/vtc-pay")
//    ResponseEntity<Object> napasOnlinePayment(@RequestBody VTCPayTransactionRequest req) throws AnvuiBaseException {
//
//        ResponseObject<Object> res = new ResponseObject<>();
//
//        log.info("id: " + req.getContractId());
//        ContractWrapper contract = contractService.getContractById(req.getContractId());
//
//        String URL = vtcTxnService.createTransaction(contract, req);
//
//        res.setResponseData(URL);
//
//        res.setSuccessMessage("success");
//        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
//    }

    @CrossOrigin
    @PostMapping("/transact/payoo")
    @ResponseBody
    ResponseEntity<Object> payooOnlinePayment(@RequestBody PayooTransactionRequest req) throws AnvuiBaseException, IOException {
        // String ipAddr = request.getRemoteAddr();

        ResponseObject<Object> res = new ResponseObject<>();

        ContractWrapper contract = contractService.getContractById(req.getContractId());

        String paymentId = payooTxnService.createTransaction(contract, req);
        
        payooMailService.sendGuideMail(contract.getContract().getInheritCustomer().getEmail(), 
                contract.getContract().getInheritCustomer().getCustomerName(), paymentId);

        res.setResponseData(paymentId);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @CrossOrigin
    @PostMapping("/transact/vipay")
    @ResponseBody
    ResponseEntity<Object> vipayOnlinePayment(@RequestBody ViPayTransactionRequest req,
            HttpServletRequest request, HttpServletResponse respond) throws AnvuiBaseException {
        if (StringUtils.isEmpty(req.getIp())) {
            req.setIp(request.getRemoteAddr());
        }

        ResponseObject<Object> res = new ResponseObject<>();

        ContractWrapper contract = contractService.getContractById(req.getContractId());

        String paymentId = vipayTxnService.createTransaction(contract, req);

        res.setResponseData(paymentId);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/transaction/status")
    @ResponseBody
    ResponseEntity<Object> updateContractStatus(@RequestBody @Valid EpayTransactionStatusRequest req)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        ContractWrapper contract = txnService.updateTransactionStatus(req);

        messageProducer.sendMailQueue(contract);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping(value = "/transaction/viettelpay/status", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    ResponseEntity<Object> updateViettelPayContract(@RequestParam String billcode, @RequestParam("cust_msisdn") String custMsisdn,
            @RequestParam("error_code") String errorCode, @RequestParam("merchant_code") String merchantCode,
            @RequestParam("order_id") String orderId, @RequestParam("payment_status") Integer paymentStatus,
            @RequestParam("trans_amount") Long transAmount, @RequestParam("vt_transaction_id") String vtTransactionId,
            @RequestParam("check_sum") String checksum) {
        ViettelPayTransactionStatusRequest req = 
                new ViettelPayTransactionStatusRequest(billcode, custMsisdn, errorCode, merchantCode, 
                        orderId, paymentStatus, transAmount, vtTransactionId, checksum);
        
        String returnErrorCode = "00";
        ViettelPayTransactionStatusResponse response;
        
        try {
            ContractWrapper wrapper = viettelTxnservice.updateTransactionStatus(req);
            
            response = viettelpayAPIService.getTransactionStatusResponse(wrapper, returnErrorCode);
        } catch (AnvuiBaseException e) {
            
            returnErrorCode = "01";
            response = viettelpayAPIService.getTransactionStatusResponse(req, returnErrorCode);
        }
        
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/transaction/napas/status")
    @ResponseBody
    ResponseEntity<Object> updateContractStatus(HttpServletRequest req, HttpServletResponse resp,
            @RequestParam String vpc_Version, @RequestParam(required = false) String vpc_Locale,
            @RequestParam String vpc_Command, @RequestParam String vpc_Merchant, @RequestParam String vpc_MerchTxnRef,
            @RequestParam(required = false) String vpc_Amount, @RequestParam(required = false) String vpc_CurrencyCode,
            @RequestParam(required = false) String vpc_CardType, @RequestParam(required = false) String vpc_OrderInfo,
            @RequestParam Integer vpc_ResponseCode, @RequestParam(required = false) String vpc_TransactionNo,
            @RequestParam(required = false) String vpc_BatchNo,
            @RequestParam(required = false) String vpc_AcqResponseCode,
            @RequestParam(required = false) String vpc_Message,
            @RequestParam(required = false) String vpc_AdditionalData, @RequestParam String vpc_SecureHash)
            throws AnvuiBaseException {
        Map<String, String[]> reqMap = req.getParameterMap();
        Set<String> keys = reqMap.keySet();

        String param = "";
        for (String key : keys) {
            param += !key.equals("vpc_SecureHash") ? reqMap.get(key)[0] : "";
        }
        log.info("param: " + param);

        NapasTransactionStatusRequest request = new NapasTransactionStatusRequest();
        request.setPaymentId(vpc_MerchTxnRef);
        request.setParam(param);
        request.setResponseCode(vpc_ResponseCode);
        request.setHash(vpc_SecureHash);

        ResponseObject<Object> res = new ResponseObject<>();

        try {
            ContractWrapper contract = napasTxnService.updateTransactionStatus(request);

            if (contract.getTxn().getStatus().equals(TransactionStatus.SUCCESS.getValue())) {
                resp.sendRedirect("https://baohiemtinhyeu.vn/hoanthanh.html");
                messageProducer.sendMailQueue(contract);
            } else if (contract.getTxn().getStatus().equals(TransactionStatus.FAIL.getValue())) {
                resp.sendRedirect("https://baohiemtinhyeu.vn/thatbai.html");
            } else if (contract.getTxn().getStatus().equals(TransactionStatus.PENDING.getValue())) {
                resp.sendRedirect("https://baohiemtinhyeu.vn/pending.html?txnId=" + contract.getTxn().getId());
            }

        } catch (IOException e) {
            AnvuiBaseException ex = new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<Object>(res, new HttpHeaders(),
                    HttpStatus.valueOf(ex.getError().getHttpStatus()));
        }

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/transaction/vipay/status")
    @ResponseBody
    ResponseEntity<Object> updateContractStatus(HttpServletRequest req, HttpServletResponse resp)
            throws AnvuiBaseException {

        ViPayTransactionStatusRequest request = new ViPayTransactionStatusRequest();
        request.setParams(req.getParameterMap());

        ResponseObject<Object> res = new ResponseObject<>();

        try {
            ContractWrapper contract = vipayTxnService.updateTransactionStatus(request);

            if (contract.getTxn().getStatus().equals(TransactionStatus.SUCCESS.getValue())) {
                resp.sendRedirect("https://baohiemtinhyeu.vn/hoanthanh.html");
                messageProducer.sendMailQueue(contract);
            } else if (contract.getTxn().getStatus().equals(TransactionStatus.FAIL.getValue())) {
                resp.sendRedirect("https://baohiemtinhyeu.vn/thatbai.html");
            } else if (contract.getTxn().getStatus().equals(TransactionStatus.PENDING.getValue())) {
                resp.sendRedirect("https://baohiemtinhyeu.vn/pending.html?txnId=" + contract.getTxn().getId());
            }

        } catch (IOException e) {
            AnvuiBaseException ex = new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<Object>(res, new HttpHeaders(),
                    HttpStatus.valueOf(ex.getError().getHttpStatus()));
        }

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/vtc-pay/status", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    ResponseEntity<Object> updateVtcpayTxnStatus(HttpServletResponse resp, 
            @RequestParam String data, @RequestParam String signature)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();
        VTCPayTransactionStatusRequest req = new VTCPayTransactionStatusRequest();
        req.setData(data);
        log.info(data);
        req.setSignature(signature);
        log.info(signature);

        ContractWrapper contract = vtcTxnService.updateTransactionStatus(req);
        
        try {
            if (contract.getTxn().getStatus().equals(TransactionStatus.SUCCESS.getValue())) {
                resp.sendRedirect("https://baohiemtinhyeu.vn/hoanthanh.html");
                messageProducer.sendMailQueue(contract);
            } else if (contract.getTxn().getStatus().equals(TransactionStatus.FAIL.getValue())) {
                resp.sendRedirect("https://baohiemtinhyeu.vn/thatbai.html");
            } else if (contract.getTxn().getStatus().equals(TransactionStatus.PENDING.getValue())) {
                resp.sendRedirect("https://baohiemtinhyeu.vn/pending.html?txnId=" + contract.getTxn().getId());
            }

        } catch (IOException e) {
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping(value = "/transaction/payoo/status")
    @ResponseBody
    ResponseEntity<Object> updatePayooTxnStatus(@RequestBody @Valid PayooTransactionStatusRequest req)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        ContractWrapper contract = payooTxnService.updateTransactionStatus(req);

        messageProducer.sendMailQueue(contract);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping(value = "/transaction/zalo/status")
    @ResponseBody
    ResponseEntity<Object> updateZaloTxnStatus(HttpServletResponse resp,
            @RequestBody ZaloPayTransactionStatusRequest req)
            throws AnvuiBaseException {
        ZaloPayTransactionResponse res = new ZaloPayTransactionResponse(); 
        
        ContractWrapper contract;
        try {
            contract = zaloTxnService.updateTransactionStatus(req);
        } catch (AnvuiBaseException e) {
            if (e.getError().getCode() == 3002) {
                res.setReturncode(0);
                res.setReturnmessage(e.getMessage());
            } else if (e.getError().getCode() == 3005) {
                res.setReturncode(2);
                res.setReturnmessage(e.getMessage());
            } else {
                res.setReturncode(3);
                res.setReturnmessage("unknown error");
            }
            
            return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
        messageProducer.sendMailQueue(contract);
        
        res.setReturncode(1);
        res.setReturnmessage("success");

        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping(value = "/transaction/payoo/{paymentId}")
    @ResponseBody
    ResponseEntity<Object> getPayooTransaction(@PathVariable String paymentId) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        PayooTransactionResponse txn = ((PayooTransactionService) payooTxnService).getPayooTransaction(paymentId);

        res.setResponseData(txn);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping(value = "/transaction/gateway/{paymentId}")
    @ResponseBody
    ResponseEntity<Object> getTransaction(@PathVariable String paymentId) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        TransactionReportDetail txn = txnUtilsService.getTransaction(paymentId);

        res.setResponseData(txn);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/transaction")
    ResponseEntity<Object> getTransactionStatus(@RequestParam Integer txnId) {
        ResponseObject<Object> res = new ResponseObject<>();

        if (((PayooTransactionService) payooTxnService).checkTxnSuccess(txnId))
            res.setSuccessMessage("success");
        else
            res.setSuccessMessage("fail");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping(value = "/transaction/get/viettelpay", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    ResponseEntity<Object> getViettelpayTransaction(@RequestParam String billcode, @RequestParam String merchant_code,
            @RequestParam String order_id, @RequestParam String check_sum) {
        
        ViettelPayCheckTransactionRequest req = 
                new ViettelPayCheckTransactionRequest(billcode, merchant_code, order_id, check_sum);
        
        ViettelPayCheckTransactionResponse res = viettelpayAPIService.checkViettelPayTransaction(req);
        
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping("/{id}")
    @ResponseBody
    ResponseEntity<Object> updateContract(@PathVariable("id") Integer id, @RequestParam boolean doSendEmail,
            @RequestBody CoupleRequest req) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        ContractWrapper contract = contractService.updateContract(id, req);

        if (doSendEmail) {
            messageProducer.sendMailQueue(contract);
        }

        res.setResponseData(contract);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<Object> listContracts(@RequestParam(value = "citizenId", required = false) String citizenId,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "phoneNumber", required = false) String phoneNumber,
            @RequestParam(value = "createdUserId", required = false) Integer createdUserId,
            @RequestParam(value = "includeNullUser", required = false) Boolean includeNullUser,
            @RequestParam(value = "fromDate", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date fromDate,
            @RequestParam(value = "toDate", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date toDate,
            @RequestParam(value = "page", required = true) Integer page,
            @RequestParam(value = "size", required = true) Integer size) {
        if (size > 200) {
            size = 200;
        }

        ResponseObject<Object> res = new ResponseObject<Object>();

        Map<Object, Object> list = contractService.getListContract(citizenId, email, phoneNumber, fromDate, toDate,
                createdUserId, includeNullUser, page, size);
        res.setResponseData(list);
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Object> cancelContract(@PathVariable("id") Integer id) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        Contract contract = contractService.cancelContract(id);

        res.setResponseData(contract);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/not-exported")
    @ResponseBody
    public ResponseEntity<Object> getNotExportedContract(@RequestParam(required = false) Boolean includeExported, 
            @RequestParam(required = false) Long fromDate, @RequestParam(required = false) Long toDate) 
                    throws AnvuiBaseException {
        if (includeExported == null)
            includeExported = false;
        if (fromDate == null)
            fromDate = 0L;
        if (toDate == null)
            toDate = System.currentTimeMillis();

        ResponseObject<Object> res = new ResponseObject<>();

        List<ContractWrapper> contract = contractService.getNotExportedContract(includeExported, fromDate,
                toDate);

        res.setResponseData(contract);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/export")
    @ResponseBody
    public ResponseEntity<Object> exportContract(@RequestBody ExportRequest req) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        contractService.exportContracts(req.getIds());

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/reject-reason")
    @ResponseBody
    public ResponseEntity<Object> exportRejectReason(@RequestParam Long fromDate, @RequestParam Long toDate)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        List<RejectedContract> rejectedContracts = contractService.listRejectedContract(fromDate, toDate);

        res.setResponseData(rejectedContracts);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping("/{id}/cancel-cod")
    @ResponseBody
    public ResponseEntity<Object> cancelCodContract(@PathVariable Integer id) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        Contract contract = contractService.cancelCodContract(id);

        res.setResponseData(contract);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/transaction/report")
    @ResponseBody
    public ResponseEntity<Object> getTransactionReport(
            @RequestParam(required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date fromDate,
            @RequestParam(required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date toDate,
            @RequestParam(required = false) String txnType,
            @RequestParam(required = false) Boolean isSuccess) throws AnvuiBaseException {
        if (isSuccess == null)
            isSuccess = true;
        
        ResponseObject<Object> res = new ResponseObject<>();

        List<TransactionReport> report = txnUtilsService.getTransactionReport(fromDate, toDate, txnType, isSuccess);
        res.setResponseData(report);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/transaction/txn-type")
    @ResponseBody
    public ResponseEntity<Object> getTransactionType() {
        ResponseObject<Object> res = new ResponseObject<>();

        List<String> report = txnUtilsService.getPaymentType();
        res.setResponseData(report);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
}
