package vn.anvui.api.rest.controller;

import java.util.List;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.bsn.beans.tracking.TrackingService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.tracking.TrackingRequest;
import vn.anvui.dt.entity.Tracking;

@RestController
@RequestMapping("/tracking")
public class TrackingController {

    final Logger log = Logger.getLogger(TrackingController.class.getSimpleName());

    @Autowired
    TrackingService trackService;

    @PostMapping
    @ResponseBody
    ResponseEntity<Object> create(@RequestBody @Valid TrackingRequest req) throws AnvuiBaseException {

        ResponseObject<Object> response = new ResponseObject<Object>();

        Tracking track = trackService.createTracking(req);

        response.setResponseData(track);

        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/unique")
    @ResponseBody
    ResponseEntity<Object> create(@RequestParam String field) throws AnvuiBaseException {
        ResponseObject<Object> response = new ResponseObject<Object>();

        List<String> values = trackService.getUniqueValues(field);

        response.setResponseData(values);

        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }

}
