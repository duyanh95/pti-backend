package vn.anvui.api.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.bsn.beans.history.HistoryService;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.misc.HistoryCreationRequest;
import vn.anvui.dt.entity.History;

@RestController
@RequestMapping("/history")
public class HistoryController {
    
    @Autowired
    HistoryService histService;
    
    @PostMapping
    @ResponseBody
    public ResponseEntity<Object> addHistory(@RequestBody @Valid HistoryCreationRequest req) {
        ResponseObject<Object> res = new ResponseObject<Object>();
        
        History hist = histService.createHistory(req);
        res.setResponseData(hist);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping
    @ResponseBody
    public ResponseEntity<Object> getUserHistory(@RequestParam(required = false) Integer page, 
            @RequestParam(required = false) Integer size) {
        ResponseObject<Object> res = new ResponseObject<Object>();
        
        if (page == null) 
            page = 0;
        if (size == null || size > 100)
            size = 10;
        
        List<History> rsList = histService.getHistory(page, size);
        res.setResponseData(rsList);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
}
