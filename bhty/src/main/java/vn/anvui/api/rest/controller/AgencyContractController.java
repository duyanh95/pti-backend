package vn.anvui.api.rest.controller;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.api.amqp.MessageProducer;
import vn.anvui.bsn.beans.contract.ContractService;
import vn.anvui.bsn.beans.customer.CustomerService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.customer.CoupleRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.model.ContractInfo;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.model.DailyReport;
import vn.anvui.dt.model.IncomeUser;

@RestController
@RequestMapping("/agency/contract")
public class AgencyContractController {

    Logger log = Logger.getLogger(AgencyContractController.class.getName());

    @Autowired
    CustomerService customerService;

    @Autowired
    ContractService contractService;
    
    @Autowired
    MessageProducer messageProducer;

    @PostMapping
    @ResponseBody
    ResponseEntity<Object> createAgencyContract(@RequestBody @Valid CoupleRequest req,
            @RequestParam(required = false) boolean doSendEmail) throws AnvuiBaseException {

        if (doSendEmail) {
            log.info("send email");
        }

        ResponseObject<Object> response = new ResponseObject<Object>();
        ContractWrapper contract = customerService.createAgencyContract(req);
        
        if (doSendEmail || contract.getContract().getContractStatus()
                    .equals(ContractStatus.AGENCY_TRANSFER_DONE.getValue())) {
            messageProducer.sendMailQueue(contract);
        }
        
        response.setResponseData(contract);

        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<Object> listContracts(@RequestParam(required = false) String citizenId,
            @RequestParam(required = false) String customerName, @RequestParam(required = false) String email,
            @RequestParam(required = false) String phoneNumber, @RequestParam(required = false) Long fromDate,
            @RequestParam(required = false) Long toDate, @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size, @RequestParam(required = false) Boolean isCompleted,
            @RequestParam(required = false) Integer txnType,
            @RequestParam(required = false) List<Integer> userIds) throws AnvuiBaseException {
        if (page == null)
            page = 0;
        if (size == null)
            size = 50;
        if (size > 200)
            size = 200;
        if (isCompleted == null)
            isCompleted = false;

        ResponseObject<Object> res = new ResponseObject<Object>();
        ContractInfo contracts = contractService.listContractForAgency(customerName, citizenId, email, phoneNumber,
                isCompleted, fromDate, toDate, page, size, userIds, txnType);

        res.setResponseData(contracts);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/user/income-report")
    @ResponseBody
    public ResponseEntity<Object> reportIncome(
            @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(required = false) Date fromDate,
            @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(required = false) Date toDate,
            @RequestParam(required = false) Integer userId) throws AnvuiBaseException {

        ResponseObject<Object> res = new ResponseObject<Object>();

        List<IncomeUser> rsList = contractService.getIncomeReportByAgency(fromDate, toDate, userId);

        res.setResponseData(rsList);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Object> updateContract(@PathVariable("id") Integer id, @RequestBody CoupleRequest req,
            @RequestParam Boolean doSendEmail) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        ContractWrapper contract = contractService.updateContract(id, req);

        if (doSendEmail) {
            contractService.sendMailToCustomer(contract);
        }

        res.setResponseData(contract);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/{id}/confirm")
    @ResponseBody
    public ResponseEntity<Object> confirmAgencyContract(@PathVariable("id") Integer id) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        Contract contract = contractService.completeCodContract(id);
        
        messageProducer.sendMailQueue(new ContractWrapper(contract));
        
        res.setResponseData(contract);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/{id}/transfer-confirm")
    @ResponseBody
    public ResponseEntity<Object> confirmAgencyTransferContract(@PathVariable("id") Integer id) 
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        Contract contract = contractService.completeAccountTransferContract(id);
        
        messageProducer.sendMailQueue(new ContractWrapper(contract));

        res.setResponseData(contract);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/income-report")
    @ResponseBody
    public ResponseEntity<Object> getAgencyDayByDayReport(
            @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(required = false) Date fromDate,
            @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(required = false) Date toDate)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        List<DailyReport> dailyReports = contractService.getDailyReportForAgency(fromDate, toDate);

        res.setResponseData(dailyReports);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
}
