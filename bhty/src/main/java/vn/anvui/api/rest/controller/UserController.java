package vn.anvui.api.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.bsn.beans.user.UserService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.user.ChangePasswordRequest;
import vn.anvui.bsn.dto.user.UserRequest;
import vn.anvui.dt.entity.AccessToken;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.UserStatus;
import vn.anvui.dt.enumeration.UserType;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;
    
    @PostMapping("/agency")
    @ResponseBody
    public final ResponseEntity<Object> createAgency(@RequestBody @Valid UserRequest request)
            throws AnvuiBaseException {
        ResponseObject<Object> response = new ResponseObject<Object>();

        User user = userService.createUser(request, UserType.AGENCY);

        response.setResponseData(user);

        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/pti-user")
    @ResponseBody
    public final ResponseEntity<Object> createPTIUser(@RequestBody @Valid UserRequest request)
            throws AnvuiBaseException {
        ResponseObject<Object> response = new ResponseObject<Object>();

        User user = userService.createUser(request, UserType.PTI_USER);

        response.setResponseData(user);

        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/agency/register")
    public ResponseEntity<Object> registerAsAgency(@RequestBody UserRequest req) {
        ResponseObject<Object> res = new ResponseObject<Object>();

        User user = userService.registerAgency(req);
        res.setResponseData(user);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/agency/{id}")
    @ResponseBody
    public final ResponseEntity<Object> deleteAgency(@PathVariable("id") Integer id) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        userService.updateUserStatus(id, UserType.AGENCY, UserStatus.DELETED);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/agency-user/{id}")
    @ResponseBody
    public final ResponseEntity<Object> deleteAgencyUser(@PathVariable("id") Integer id) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        userService.updateUserStatus(id, UserType.AGENCY, UserStatus.DELETED);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/pti-user/{id}")
    @ResponseBody
    public final ResponseEntity<Object> deletePTIUser(@PathVariable("id") Integer id) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        userService.updateUserStatus(id, UserType.AGENCY, UserStatus.DELETED);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/admin")
    @ResponseBody
    public final ResponseEntity<Object> createAdmin(@RequestBody @Valid UserRequest request) throws AnvuiBaseException {
        ResponseObject<Object> response = new ResponseObject<Object>();

        User user = userService.createUser(request, UserType.ADMIN);

        response.setResponseData(user);

        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/password")
    @ResponseBody
    public ResponseEntity<Object> changePassword(@RequestBody @Valid ChangePasswordRequest req)
            throws AnvuiBaseException {
        ResponseObject<Object> response = new ResponseObject<Object>();

        userService.changePassword(req);

        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> list(@RequestParam(value = "userType", required = true) String userType) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        List<User> users = userService.findByUserType(userType);
        response.setResponseData(users);
        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/agency/children")
    @ResponseBody
    public ResponseEntity<Object> getListAgencyChildren(@RequestParam(required = false) Integer agencyId,
            @RequestParam(required = false) String userName, @RequestParam(required = false) String citizenId,
            @RequestParam(required = false) String phoneNumber, @RequestParam(required = false) String email)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();

        User rsList = userService.listAgencyUserByRootId(agencyId, userName, citizenId, phoneNumber, email);

        res.setResponseData(rsList);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/agency")
    @ResponseBody
    public ResponseEntity<Object> getListAgency(
            @RequestParam(required = false) String userName, @RequestParam(required = false) String citizenId,
            @RequestParam(required = false) String phoneNumber, @RequestParam(required = false) String email)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();

        List<User> rsList = userService.getAllRootAgencies(userName, citizenId, phoneNumber, email);

        res.setResponseData(rsList);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PutMapping("/agency-user/{id}")
    @ResponseBody
    public ResponseEntity<Object> updateAgencyUser(@RequestBody UserRequest req, @PathVariable("id") Integer id)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();

        User user = userService.updateUser(req, id);

        res.setResponseData(user);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PutMapping("/agency/{id}")
    public ResponseEntity<Object> updateAgency(@RequestBody UserRequest req, @PathVariable("id") Integer id)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();

        User user = userService.updateUser(req, id);

        res.setResponseData(user);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    
    @PutMapping("/info")
    public ResponseEntity<Object> updateUserInfo(@RequestBody UserRequest req)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();

        User user = userService.updateUser(req, null);

        res.setResponseData(user);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/info")
    public ResponseEntity<Object> updateUserInfo()
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();

        User user = userService.getUserInfo();

        res.setResponseData(user);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/agency/{id}/codAllowed")
    public ResponseEntity<Object> updateAgencyCodAllow(@PathVariable("id") Integer id, @RequestParam Boolean enable)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();

        userService.allowAgencyCod(id, enable);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/{id}/reset-password")
    public ResponseEntity<Object> resetPassword(@PathVariable Integer id, @RequestParam(required=false) String email) {
        ResponseObject<Object> res = new ResponseObject<>();
        
        userService.resetUserPassword(id, email);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/agency/{id}/token")
    public ResponseEntity<Object> createAPIToken(@PathVariable Integer id) {
        ResponseObject<Object> res = new ResponseObject<Object>();

        AccessToken token = userService.createAPIKey(id);
        res.setResponseData(token);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/{id}/agency/confirm")
    public ResponseEntity<Object> registerAsAgency(@RequestBody UserRequest req, @PathVariable() Integer id) {
        if (req.getConfirm() == null)
            req.setConfirm(true);
        
        ResponseObject<Object> res = new ResponseObject<Object>();

        User user = userService.confirmAgentAsAgency(req, id);
        res.setResponseData(user);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    } 
    
    @GetMapping("/agency/pending-request")
    public ResponseEntity<Object> listPendingAgencyRegisterUsers(@RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer count, @RequestParam(required = false) String phoneNumber,
            @RequestParam(required = false) String citizenId) {
        if (page == null)
            page = 0;
        if (count == null)
            count = 25;
        
        ResponseObject<Object> res = new ResponseObject<Object>();
        
        List<User> users = userService.listPendingAgencyRequestUsers(page, count, citizenId, phoneNumber);
        res.setResponseData(users);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
}
