package vn.anvui.api.rest.global;

import org.springframework.security.core.AuthenticationException;

public class InvalidRoleException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public InvalidRoleException(String msg) {
		super(msg);
	}

	public InvalidRoleException(String msg, Throwable t) {
		super(msg, t);
	}
}
