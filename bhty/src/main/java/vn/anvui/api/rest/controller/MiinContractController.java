package vn.anvui.api.rest.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.bsn.beans.insurancePackage.InsurancePackageService;
import vn.anvui.bsn.beans.miinContract.apartment.ApartmentContractService;
import vn.anvui.bsn.beans.miinContract.bike.BikeContractService;
import vn.anvui.bsn.beans.miinContract.general.MiinGeneralContractService;
import vn.anvui.bsn.beans.miinContract.health.HealthContractService;
import vn.anvui.bsn.beans.miinContract.love.LoveContractService;
import vn.anvui.bsn.beans.product.ProductService;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.miinContract.ApartmentContractRequest;
import vn.anvui.bsn.dto.miinContract.BikeContractRequest;
import vn.anvui.bsn.dto.miinContract.HealthContractRequest;
import vn.anvui.bsn.dto.miinContract.LoveContractRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.entity.Product;
import vn.anvui.dt.utils.DateTimeHelper;

@RestController
@RequestMapping("/miin")
public class MiinContractController {
    @Autowired
    ProductService productService;
    
    @Autowired
    LoveContractService loveService;
    
    @Autowired
    ApartmentContractService apartmentService;
    
    @Autowired
    HealthContractService healthService;
    
    @Autowired
    BikeContractService bikeService;
    
    @Autowired
    MiinGeneralContractService generalService;
    
    @Autowired
    InsurancePackageService pkgService;
    
    /**
     ******************************** General APIs *****************************************
     */
    @GetMapping("/products")
    @ResponseBody
    public ResponseEntity<Object> getProductList() {
        ResponseObject<Object> res = new ResponseObject<>();
        
        List<Product> products = productService.getProducts();
        res.setResponseData(products);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/product/{id}/packages")
    @ResponseBody
    public ResponseEntity<Object> getProductPackageList(@PathVariable("id") Integer id) {
        ResponseObject<Object> res = new ResponseObject<>();
        
        List<InsurancePackage> pkgs = pkgService.getAllPackages(id);
        res.setResponseData(pkgs);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    
    @GetMapping("/product/contracts")
    @ResponseBody
    public ResponseEntity<Object> getContractList(
            @RequestParam(value = "citizenId", required = false) String citizenId,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "phoneNumber", required = false) String phoneNumber,
            @RequestParam(value = "customerName", required = false) String customerName,
            @RequestParam(value = "fromDate", required = false) Long fromDate,
            @RequestParam(value = "toDate", required = false) Long toDate,
            @RequestParam(value = "productId", required = false) Integer productId,
            @RequestParam(value = "isCompleted", required = false) Boolean isCompleted,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size) {
        if (page == null)
            page = 0;
        if (size == null || size > 200)
            size = 50;
        if (isCompleted == null)
            isCompleted = false;
        if (fromDate == null || toDate == null) {
            toDate = System.currentTimeMillis();
            fromDate = DateTimeHelper.getFirstDayOfMonths(new Date()).getTime();
        }
        
        ResponseObject<Object> res = new ResponseObject<>();
        
        List<Contract> contract = generalService.listCustomerContract(customerName, citizenId, email, phoneNumber,
                fromDate, toDate, page, size, productId, isCompleted);
        res.setResponseData(contract);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("agency/product/contracts")
    @ResponseBody
    public ResponseEntity<Object> getAgencyContractList(
            @RequestParam(value = "citizenId", required = false) String citizenId,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "phoneNumber", required = false) String phoneNumber,
            @RequestParam(value = "customerName", required = false) String customerName,
            @RequestParam(value = "fromDate", required = false) Long fromDate,
            @RequestParam(value = "toDate", required = false) Long toDate,
            @RequestParam(value = "productId", required = false) Integer productId,
            @RequestParam(value = "isCompleted", required = false) Boolean isCompleted,
            @RequestParam(value = "txnType", required = false) Integer txnType,
            @RequestParam(value = "agencyId", required = false) Integer agencyId,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size) {
        if (page == null)
            page = 0;
        if (size == null || size > 200)
            size = 50;
        if (isCompleted == null)
            isCompleted = false;
        if (fromDate == null || toDate == null) {
            toDate = System.currentTimeMillis();
            fromDate = DateTimeHelper.getFirstDayOfMonths(new Date()).getTime();
        }
        
        ResponseObject<Object> res = new ResponseObject<>();
        
        List<Contract> contract = generalService.listAgencyContract(customerName, citizenId, email, phoneNumber,
                fromDate, toDate, page, size, productId, isCompleted, txnType, agencyId);
        res.setResponseData(contract);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    /**
     ******************************** Love Insurance APIs *****************************************
     */
    @PostMapping("product/love-insurance/contract")
    @ResponseBody
    public ResponseEntity<Object> createLoveInsuranceContract(@RequestBody @Valid LoveContractRequest req) {
        ResponseObject<Object> res = new ResponseObject<>();
        
        Contract contract = loveService.createCustomerContract(req);
        res.setResponseData(contract);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/agency/product/love-insurance/contract")
    @ResponseBody
    public ResponseEntity<Object> createAgencyLoveInsuranceContract(@RequestBody @Valid LoveContractRequest req) {
        ResponseObject<Object> res = new ResponseObject<>();
        
        Contract contract = loveService.createAgencyContract(req);
        res.setResponseData(contract);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    /**
     ******************************** Apartment Insurance APIs *****************************************
     */
//    @PostMapping("product/apartment-insurance/contract")
//    @ResponseBody
//    public ResponseEntity<Object> createApartmentInsuranceContract(@RequestBody @Valid ApartmentContractRequest req) {
//        ResponseObject<Object> res = new ResponseObject<>();
//        
//        Contract contract = apartmentService.createCustomerContract(req);
//        res.setResponseData(contract);
//        
//        res.setSuccessMessage("success");
//        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
//    }
//    
//    @PostMapping("/agency/product/apartment-insurance/contract")
//    @ResponseBody
//    public ResponseEntity<Object> createAgencyApartmentInsuranceContract(@RequestBody @Valid ApartmentContractRequest req) {
//        ResponseObject<Object> res = new ResponseObject<>();
//        
//        Contract contract = apartmentService.createAgencyContract(req);
//        res.setResponseData(contract);
//        
//        res.setSuccessMessage("success");
//        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
//    }
//    
//    @GetMapping("product/apartment-insurance/contract/{id}")
//    @ResponseBody
//    public ResponseEntity<Object> getApartmentContractDetails(@PathVariable Integer id) {
//        ResponseObject<Object> res = new ResponseObject<>();
//        
//        Contract resp = apartmentService.getContratInfo(id);
//        res.setResponseData(resp);
//        
//        res.setSuccessMessage("success");
//        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
//    }
    
    /**
     ******************************** Health Insurance APIs *****************************************
     */
    @PostMapping("product/health-insurance/contract")
    @ResponseBody
    public ResponseEntity<Object> createHealthInsuranceContract(@RequestBody @Valid HealthContractRequest req) {
        ResponseObject<Object> res = new ResponseObject<>();
        
        Contract contract = healthService.createCustomerContract(req);
        res.setResponseData(contract);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/agency/product/health-insurance/contract")
    @ResponseBody
    public ResponseEntity<Object> createAgencyHealthInsuranceContract(@RequestBody @Valid HealthContractRequest req) {
        ResponseObject<Object> res = new ResponseObject<>();
        
        Contract contract = healthService.createAgencyContract(req);
        res.setResponseData(contract);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    /**
     ********************************  Bike Insurance APIs *****************************************
     */
    @PostMapping("product/bike-insurance/contract")
    @ResponseBody
     public ResponseEntity<Object> createBikeInsuranceContract(@RequestBody @Valid BikeContractRequest req) {
         ResponseObject<Object> res = new ResponseObject<>();
         
         Contract contract = bikeService.createCustomerContract(req);
         res.setResponseData(contract);
         
         res.setSuccessMessage("success");
         return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
     }
   
    @PostMapping("/agency/product/bike-insurance/contract")
    @ResponseBody
     public ResponseEntity<Object> createAgencyBikeInsuranceContract(@RequestBody @Valid BikeContractRequest req) {
         ResponseObject<Object> res = new ResponseObject<>();
         
         Contract contract = bikeService.createAgencyContract(req);
         res.setResponseData(contract);
         
         res.setSuccessMessage("success");
         return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
     }
}
