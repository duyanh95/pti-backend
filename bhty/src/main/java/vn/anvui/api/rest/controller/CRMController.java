package vn.anvui.api.rest.controller;

import java.io.File;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.bsn.beans.contactLog.ContactLogService;
import vn.anvui.bsn.beans.contract.ContractService;
import vn.anvui.bsn.beans.customer.CustomerService;
import vn.anvui.bsn.beans.pdf.PdfService;
import vn.anvui.bsn.beans.rejectNote.RejectNoteService;
import vn.anvui.bsn.beans.tracking.TrackingService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.crm.ContactAssignRequest;
import vn.anvui.bsn.dto.crm.RejectReport;
import vn.anvui.bsn.dto.crm.UpdateTagRequest;
import vn.anvui.bsn.dto.user.UserContactStatus;
import vn.anvui.dt.entity.ContactLog;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.entity.RejectNote;
import vn.anvui.dt.model.ContractInfo;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.model.IncomeUser;
import vn.anvui.dt.model.TrackingRecord;

@RestController
@RequestMapping("/crm")
public class CRMController {
    final Logger log = Logger.getLogger(CRMController.class.getSimpleName());

    @Autowired
    TrackingService trackService;

    @Autowired
    ContractService contractService;

    @Autowired
    CustomerService customerService;

    @Autowired
    PdfService pdfService;

    @Autowired
    RejectNoteService noteService;

    @Autowired
    ContactLogService logService;

    @GetMapping("/tracking-info")
    @ResponseBody
    public ResponseEntity<Object> getTrackInfo(@RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size, @RequestParam(required = false) String source,
            @RequestParam(required = false) String agent, @RequestParam(required = false) String medium,
            @RequestParam(required = false) String campaign, @RequestParam(required = false) String team,
            @RequestParam(required = false) String channel, @RequestParam(required = false) Long fromDate,
            @RequestParam(required = false) Long toDate, @RequestParam(required = false) Boolean isTrafficReport)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();
        if (page != null && size == null) {
            page = 0;
            size = 50;
        }
        if (isTrafficReport == null) {
            isTrafficReport = true;
        }

        List<TrackingRecord> report = trackService.getTrackingReport(page, size, source, agent, medium, campaign, team,
                channel, fromDate, toDate, isTrafficReport);

        res.setResponseData(report);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/contracts")
    @ResponseBody
    ResponseEntity<Object> getContracts(@RequestParam(required = false) String citizenId,
            @RequestParam(required = false) String source, @RequestParam(required = false) String medium,
            @RequestParam(required = false) String campaign, @RequestParam(required = false) String customerName,
            @RequestParam(required = false) String email, @RequestParam(required = false) String phoneNumber,
            @RequestParam(required = false) Long fromDate, @RequestParam(required = false) Long toDate,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size,
            @RequestParam(required = false) List<Integer> userIds) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        if (page == null)
            page = 0;
        if (size == null)
            size = 50;

        ContractInfo contractRes;
        contractRes = contractService.listContractPTI(customerName, citizenId, email, phoneNumber, fromDate, toDate,
                page, size, userIds, source, medium, campaign);

        res.setResponseData(contractRes);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/customer/potential")
    @ResponseBody
    ResponseEntity<Object> getPotentialCustomer(@RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size, @RequestParam boolean onContract,
            @RequestParam(required = false) String customerName, @RequestParam(required = false) String phoneNumber,
            @RequestParam(required = false) String email, @RequestParam(required = false) Long toDate,
            @RequestParam(required = false) Long fromDate, @RequestParam(required = false) Integer tag)
            throws AnvuiBaseException {
        ResponseObject<Object> response = new ResponseObject<>();
        if (page == null)
            page = 0;

        List<Customer> customers = customerService.getPotentialCustomerInfo(page, size, customerName, email,
                phoneNumber, fromDate, toDate, tag, onContract);

        response.setResponseData(customers);

        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/customer/potential/total-unassigned")
    @ResponseBody
    ResponseEntity<Object> countUnassignedContacts() throws AnvuiBaseException {
        ResponseObject<Object> response = new ResponseObject<>();

        ContactAssignRequest totalContacts = customerService.countTotalUnassignedContact();

        response.setResponseData(totalContacts);

        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("/customer/potential/update-tag/{id}")
    @ResponseBody
    ResponseEntity<Object> updateCustomerTag(@PathVariable("id") Integer id, @RequestBody @Valid UpdateTagRequest req)
            throws AnvuiBaseException {

        ResponseObject<Object> res = new ResponseObject<>();

        Customer customer = customerService.updateCustomerTag(req, id);

        res.setResponseData(customer);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("print/{id}")
    public @ResponseBody ResponseEntity<Object> getContractCert(@PathVariable("id") Integer id,
            @RequestParam boolean sealed) {
        try {
            ContractWrapper contract = contractService.getContractById(id);

            File file = pdfService.loadPdfAsResource(contract, false, sealed);
            log.warning(file.getAbsolutePath());

            Path path = Paths.get(file.getAbsolutePath());
            Resource resource = null;

            try {
                resource = new UrlResource(path.toUri());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            if (resource.exists()) {
                return ResponseEntity.ok().header("content-type", "application/pdf")
                        .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + resource.getFilename() + "\"")
                        .body(resource);

                // return new ResponseEntity<Object>(resource, HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>("File Not Found ", HttpStatus.NOT_FOUND);
            }
        } catch (AnvuiBaseException e) {
            return new ResponseEntity<Object>("File Not Found ", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/income-report")
    public @ResponseBody ResponseEntity<Object> getReport(@RequestParam(required = true) Long fromDate,
            @RequestParam(required = true) Long toDate) {
        ResponseObject<Object> res = new ResponseObject<>();

        try {
            List<IncomeUser> rs = contractService.getIncomeReportByPTI(fromDate, toDate);

            res.setResponseData(rs);
        } catch (AnvuiBaseException e) {
            res.setError(e.getError());
            return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.valueOf(e.getError().getHttpStatus()));
        }

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/contract/{id}")
    public @ResponseBody ResponseEntity<Object> getContractById(@PathVariable("id") Integer id) {
        ResponseObject<Object> res = new ResponseObject<>();

        try {
            ContractWrapper rs = contractService.getContractById(id);

            res.setResponseData(rs);
        } catch (AnvuiBaseException e) {
            res.setError(e.getError());
            return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.valueOf(e.getError().getHttpStatus()));
        }

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PutMapping("/contract/{id}/confirm")
    public @ResponseBody ResponseEntity<Object> confirmCompletedContract(@PathVariable("id") Integer id)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        ContractWrapper rs = contractService.confirmContract(id);

        res.setResponseData(rs);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/customer/assign")
    public @ResponseBody ResponseEntity<Object> assignContactToSale(@RequestBody @Valid ContactAssignRequest req)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        int contactAssigned = customerService.assignContactToUser(req);

        res.setResponseData(contactAssigned);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/customer/potential/status")
    public @ResponseBody ResponseEntity<Object> getAssgignContactStatus(@RequestParam(required = false) Integer userId,
            @RequestParam(required = false) Long fromDate, @RequestParam(required = false) Long toDate)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        List<UserContactStatus> contactAssigned = customerService.getAssignedContactStatus(userId, fromDate, toDate);

        res.setResponseData(contactAssigned);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/customer/reject-note")
    public @ResponseBody ResponseEntity<Object> createRejectNote(@RequestParam String note) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        RejectNote rNote = noteService.create(note);

        res.setResponseData(rNote);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PutMapping("/customer/reject-note/{id}")
    public @ResponseBody ResponseEntity<Object> updateRejectNote(@PathVariable("id") Integer id,
            @RequestParam String note) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        RejectNote rNote = noteService.update(id, note);

        res.setResponseData(rNote);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/customer/reject-note/{id}")
    public @ResponseBody ResponseEntity<Object> deleteRejectNote(@PathVariable("id") Integer id)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        noteService.delete(id);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/customer/reject-note")
    public @ResponseBody ResponseEntity<Object> getRejectNote() throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        List<RejectNote> rNote = noteService.get();

        res.setResponseData(rNote);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/customer/{id}/reject-note")
    public @ResponseBody ResponseEntity<Object> getCustomerRejectNote(@PathVariable("id") Integer id)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        String rNote = noteService.getCustomerRejectNote(id);

        res.setResponseData(rNote);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/customer/{id}")
    public @ResponseBody ResponseEntity<Object> deleteContact(@PathVariable("id") Integer id)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        customerService.deleteCustomers(id);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/customer/reject-report")
    public @ResponseBody ResponseEntity<Object> getRejectReport(@RequestParam(required = false) Long fromDate,
            @RequestParam(required = false) Long toDate) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        List<RejectReport> rsList = noteService.getRejectReport(fromDate, toDate);

        res.setResponseData(rsList);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/customer/contact-history")
    ResponseEntity<Object> getContactHistory(@RequestParam String phoneNumber) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();

        List<ContactLog> ctsLog = logService.getContactLog(phoneNumber);

        res.setResponseData(ctsLog);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("contract/{id}/send-mail")
    ResponseEntity<Object> sendContractMail(@PathVariable Integer id) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();

        ContractWrapper contract = contractService.getContractById(id);

        contractService.sendMailToCustomer(contract);

        res.setResponseData(contract);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/customer/export")
    public ResponseEntity<Object> testQuery(@RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") Date fromDate,
            @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") Date toDate,
            @RequestParam(required = false) Boolean notExportedOnly) throws AnvuiBaseException {
        if (notExportedOnly == null)
            notExportedOnly = true;

        String data = customerService.exportCsv(fromDate, toDate, notExportedOnly);

        HttpHeaders header = new HttpHeaders();
        header.add("content-type", "application/csv");
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"contacts.csv\"");

        return new ResponseEntity<Object>(data, header, HttpStatus.OK);
    }
}
