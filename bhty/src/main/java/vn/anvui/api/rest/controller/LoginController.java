package vn.anvui.api.rest.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.api.amqp.MessageProducer;
import vn.anvui.bsn.auth.CredentialService;
import vn.anvui.bsn.beans.user.UserService;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.account.LoginRequest;
import vn.anvui.bsn.dto.account.LoginResponse;
import vn.anvui.bsn.dto.account.OTPResetRequest;
import vn.anvui.bsn.dto.user.PhoneLoginDto;

@RestController
@RequestMapping("/login")
public class LoginController {
    
    @Autowired
    UserService userService;
    
    @Autowired
    CredentialService credentialService;
    
    @Autowired
    MessageProducer msgProducer;
    
    @PostMapping("/register")
    @ResponseBody
    public ResponseEntity<Object> miinUserRegister(@RequestBody @Valid LoginRequest req) {
        ResponseObject<Object> res = new ResponseObject<>();
        
        LoginResponse user = userService.registerMiinUser(req);
        res.setResponseData(user);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/send-otp")
    @ResponseBody
    public ResponseEntity<Object> sendOTP() {
        ResponseObject<Object> res = new ResponseObject<>();
        
        userService.sendOTP();
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/verify")
    @ResponseBody
    public ResponseEntity<Object> verifyPhone(@RequestParam String otp) {
        ResponseObject<Object> res = new ResponseObject<>();
        
        userService.verifyPhoneNumber(otp);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/token")
    public ResponseEntity<Object> loginWithFCMTOken(@RequestParam String deviceToken) {
        ResponseObject<Object> res = new ResponseObject<>();
        
        userService.updateDeviceToken(deviceToken);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/reset-otp")
    @ResponseBody
    public ResponseEntity<Object> getResetOtp(@RequestParam String phoneNumber) {
        ResponseObject<Object> res = new ResponseObject<>();
        
        PhoneLoginDto dto = userService.sendResetOtp(phoneNumber);
        msgProducer.sendSms(dto.getSms());
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/reset-password")
    @ResponseBody
    public ResponseEntity<Object> resetPassword(@RequestBody @Valid OTPResetRequest req) {
        ResponseObject<Object> res = new ResponseObject<>();
        
        userService.resetByOtp(req);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
}
