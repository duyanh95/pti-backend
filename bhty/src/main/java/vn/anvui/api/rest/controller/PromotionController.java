package vn.anvui.api.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.bsn.beans.banner.BannerService;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.banner.BannerRequest;
import vn.anvui.dt.entity.Banner;
import vn.anvui.dt.enumeration.BannerType;

@RestController
@RequestMapping("/promo")
public class PromotionController {

    @Autowired
    BannerService promoService;
    
    private final BannerType type = BannerType.PROMOTION;
    
    @GetMapping
    public ResponseEntity<Object> listPromotion(@RequestParam(required = false) Boolean deleted) {
        if (deleted == null)
            deleted = false;
        
        ResponseObject<Object> res = new ResponseObject<Object>();

        List<Banner> promo = promoService.listBanner(deleted, type);
        res.setResponseData(promo);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping
    @ResponseBody
    public ResponseEntity<Object> createPromo(@RequestBody @Valid BannerRequest req) {
        ResponseObject<Object> res = new ResponseObject<Object>();

        Banner promo = promoService.createBanner(req, type);
        res.setResponseData(promo);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Object> createPromo(@PathVariable Integer id, @RequestBody BannerRequest req) {
        ResponseObject<Object> res = new ResponseObject<Object>();

        Banner banner = promoService.updateBanner(id, req, type);
        res.setResponseData(banner);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PatchMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Object> restorePromo(@PathVariable Integer id) {
        ResponseObject<Object> res = new ResponseObject<Object>();

        promoService.updateActiveBanner(id, true, type);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Object> deletePromo(@PathVariable Integer id) {
        ResponseObject<Object> res = new ResponseObject<Object>();

        promoService.updateActiveBanner(id, false, type);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
}
