package vn.anvui.api.rest.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.ResponseObject;

@RestController
@RequestMapping("/image")
public class ImageStorageController {
    
    private final Logger log = Logger.getLogger(ImageStorageController.class.getName());
    
    private String STORAGE_PATH = System.getenv("STORAGE_PATH");
    
    @PostMapping("/upload")
    public @ResponseBody ResponseEntity<Object> uploadImage(@RequestParam("file") MultipartFile file) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        
        String fileName = null;
        try {
            byte[] bytes = file.getBytes();
            fileName = System.currentTimeMillis() + 
                    file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            log.info("upload to: " + STORAGE_PATH + fileName);
            Path path = Paths.get(STORAGE_PATH + fileName);
            Files.write(path, bytes);
        } catch (IOException e) {
            e.printStackTrace();
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
        
        response.setResponseData("/pti/image/view/" + fileName);
        
        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/view/{image:.+}")
    @ResponseBody public ResponseEntity<byte[]> getImage(@PathVariable("image") String image) {
        File file = new File(STORAGE_PATH + image);
        
        log.info(image.substring(image.lastIndexOf(".") + 1));
        MediaType mediaType;
        switch (image.substring(image.lastIndexOf("."))) {
        case ".png":
            mediaType = MediaType.IMAGE_PNG;
            break;
        
        case ".jpeg":
        case ".jpg":
            mediaType = MediaType.IMAGE_JPEG;
            break;
            
        default:
            throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST);
        }
        
        byte[] imageByte = null;
        try {
            InputStream InStr = new FileInputStream(file);
            imageByte = IOUtils.toByteArray(InStr);
        } catch (IOException e) {
            throw new AnvuiBaseException(ErrorInfo.FILE_NOT_FOUND);
        }
        
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        headers.setContentType(mediaType);
        
        
        return new ResponseEntity<byte[]>(imageByte, headers, HttpStatus.OK);
    }
}
