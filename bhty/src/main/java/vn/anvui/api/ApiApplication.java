package vn.anvui.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import vn.anvui.api.config.CacheConfig;
import vn.anvui.api.config.MVCConfig;
import vn.anvui.api.config.RequestLoggingFilterConfigurer;
import vn.anvui.api.config.ServiceBeanConfig;
import vn.anvui.bsn.beans.rabbit.RabbitListenerConfig;
import vn.anvui.bsn.beans.rabbit.RabbitTemplateConfig;
import vn.anvui.dt.common.ApiConts;
import vn.anvui.dt.dbconfig.MySqlConfig;

@SpringBootApplication
@Import({ ServiceBeanConfig.class, MySqlConfig.class, MVCConfig.class, RequestLoggingFilterConfigurer.class,
    CacheConfig.class, RabbitTemplateConfig.class, RabbitListenerConfig.class })
@EnableJpaRepositories( basePackages = { ApiConts.REPO_BEAN_PACKAGE } )
@EnableCaching
@EnableScheduling
@EnableAsync
public class ApiApplication extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ApiApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}
}
