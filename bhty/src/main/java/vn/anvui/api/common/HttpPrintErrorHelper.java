package vn.anvui.api.common;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.ResponseObject;

public final class HttpPrintErrorHelper {

	private HttpPrintErrorHelper() {
	}

	public static void printError(HttpServletResponse httpResponse, ErrorInfo error, int code) throws IOException {
		printError(httpResponse, error, code, null);
	}
	
	public static void printError(HttpServletResponse httpResponse, ErrorInfo error, int code, Object data) throws IOException {

		ResponseObject<Object> responseObj = new ResponseObject<Object>();
		responseObj.setError(error);
		if (data != null) {
			responseObj.setResponseData(data);
		}

		httpResponse.setStatus(code);
		httpResponse.addHeader("Content-Type", "application/json");
		ServletOutputStream sos = httpResponse.getOutputStream();
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		sos.print(mapper.writeValueAsString(responseObj));
		sos.close();
	}
}
