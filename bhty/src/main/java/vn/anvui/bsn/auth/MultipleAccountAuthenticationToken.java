package vn.anvui.bsn.auth;

import java.util.List;
import java.util.Map;

import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

public class MultipleAccountAuthenticationToken extends PreAuthenticatedAuthenticationToken {
	private static final long serialVersionUID = -7669577294110378621L;

	public MultipleAccountAuthenticationToken(Object aPrincipal, Object aCredentials) {
		super(aPrincipal, aCredentials);
	}

	private List<Map<String, String>> tickets;

	public List<Map<String, String>> getTickets() {
		return tickets;
	}

	public void setTickets(List<Map<String, String>> tickets) {
		this.tickets = tickets;
	}
}
