package vn.anvui.bsn.auth;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.event.CacheEventListener;
import vn.anvui.bsn.dto.account.LoginRequest;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.common.StaticValue;
import vn.anvui.dt.common.TimeConst;
import vn.anvui.dt.entity.AccessToken;
import vn.anvui.dt.entity.Privilege;
import vn.anvui.dt.entity.Role;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.repos.accesstoken.AccessTokenRepository;
import vn.anvui.dt.repos.role.RoleRepository;
import vn.anvui.dt.repos.user.UserRepository;
import vn.anvui.dt.utils.DateTimeHelper;
import vn.anvui.dt.utils.StringHelper;

@Service
public class CredentialService {

    final Logger logger = Logger.getLogger(CredentialService.class.getSimpleName());

    @Autowired
    private UserRepository userRepo;
    
    @SuppressWarnings("unused")
    @Autowired 
    private RoleRepository roleRepo;

    @Autowired
    private AccessTokenRepository accessTokenRepo;

//    @Autowired
//    private EhCacheCacheManager cacheManager;
//
//    private Ehcache authToken;
//
//    public static final String CACHE_NAME = CredentialService.class.getName();
//    public static final int TOKEN_EXPIRATION_IN_SECONDS = 30 * 24 * 60 * 60;
//
//    @PostConstruct
//    public void init() {
//
//        logger.info("Token service initialized");
//
//        // create search attribute for cache
//        Cache tokenCache = new Cache(
//
//                new CacheConfiguration(CACHE_NAME, 1000)
//
//                        /**
//                         * must enable copy on read and write so we can get value from cache instead of
//                         * reference
//                         */
//                        .copyOnRead(true).copyOnWrite(true)
//
//                        .memoryStoreEvictionPolicy(MemoryStoreEvictionPolicy.LRU)
//
//                        .timeToIdleSeconds(TOKEN_EXPIRATION_IN_SECONDS)
//
//                        .searchable(
//
//                                new Searchable()
//
//                                        .searchAttribute(new SearchAttribute().name("username")
//                                                .expression("value.getPrincipal().getUsername()"))
//
//                                        .searchAttribute(new SearchAttribute().name("pwd")
//                                                .expression("value.getPrincipal().getPassword()"))
//
//                                        .searchAttribute(new SearchAttribute().name("platformType")
//                                                .expression("value.getDetails().getPlatformType()"))
//
//                                        .searchAttribute(new SearchAttribute().name("deviceToken")
//                                                .expression("value.getDetails().getDeviceToken()"))
//
//                )
//
//        );
//
//        cacheManager.getCacheManager().addCache(tokenCache);
//        tokenCache.getCacheEventNotificationService().registerListener(new TokenCacheEventListener());
//
//        authToken = (Ehcache) cacheManager.getCache(CACHE_NAME).getNativeCache();
//    }

    public Authentication retrieve(String token) {
        logger.info("retrieve token");
        AnvuiAuthenticationToken auth = null;

        AccessToken accessToken = accessTokenRepo.findFirstByTokenOrderByCreatedDateDesc(token);

        if (accessToken != null) {

            Date expiredDate = accessToken.getExpiredDate();
            User user = userRepo.findOne(accessToken.getUser().getId());
            
            List<Role> roles = userRepo.getRolesByUserId(user.getId());
            Set<Privilege> privileges = new HashSet<>();
            
            if (accessToken.getHasPrivilege()) {
                roles.forEach((role) -> {
                    privileges.addAll(role.getPrivileges());
                });
            }

            if (user != null && (expiredDate == null || 
                    (expiredDate != null && Calendar.getInstance().getTimeInMillis() < expiredDate.getTime()))) {
                try {
                    auth = new AnvuiAuthenticationToken(user, privileges);
                    auth.setDetails(new AuthenticationDetails(accessToken, user));
                    store(accessToken, auth);
                } catch (Exception ex) {
                    logger.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);;
                }
            }
        }
        return auth;
    }

    public void increaseExpiredDate(String token) {
        logger.info("increase token expire date");
        Date now = new Date();
        Date expiredDate = DateTimeHelper.addDay(now, DateTimeHelper.SECOND, StaticValue.TOKEN_EXPIRATION_IN_SECONDS);

        Authentication authentication = retrieve(token);
        AuthenticationDetails details = (AuthenticationDetails) authentication.getDetails();
        details.setExpiredDate(expiredDate);

        AccessToken accessToken = accessTokenRepo.findFirstByTokenOrderByCreatedDateDesc(token);
        if (accessToken != null) {
            accessToken.setExpiredDate(expiredDate);
            accessTokenRepo.save(accessToken);
        }
    }

    public void removeAllAuthenticationOfThisDevice(String deviceToken, int platformType) {
        List<AccessToken> tokens = accessTokenRepo.findByDeviceToken(deviceToken);
        if (!CollectionUtils.isEmpty(tokens)) {
            accessTokenRepo.delete(tokens);
        }
    }
    
    public AnvuiAuthenticationToken store(User user, LoginRequest request) {
        return store(user, request, false);
    }

    public AnvuiAuthenticationToken store(User user, LoginRequest request, boolean logByCustomer) {
        logger.info("store token");
        AccessToken accessToken = createToken(user, request, false);

        accessTokenRepo.save(accessToken);
        
        List<Role> roles = userRepo.getRolesByUserId(user.getId());
        Set<Privilege> privileges = new HashSet<>();
        
        if (!logByCustomer) {
            roles.forEach((role) -> {
                privileges.addAll(role.getPrivileges());
            });
        }

        AnvuiAuthenticationToken authToken = new AnvuiAuthenticationToken(user, privileges);
        AuthenticationDetails details = new AuthenticationDetails(accessToken, user);
        authToken.setDetails(details);

        return authToken;
    }
    
    public AccessToken createToken(User user, LoginRequest request, boolean isKey) {
        
        Integer platformType = null;
        String platformVersion = null;
        String deviceId = null;
        String deviceToken = null;
        if (request != null) {
            platformType = request.getPlatformType();
            platformVersion = request.getPlatformVersion();
            deviceId = request.getDeviceId();
            deviceToken = request.getDeviceToken();
            if (request.getPlatformType() != null) {
                removeAllAuthenticationOfThisDevice(deviceToken, platformType);
            }
        }
        
        Date expiredDate;
        if (!isKey) {
            expiredDate = DateTimeHelper.addDay(new Date(), DateTimeHelper.SECOND,
                    TimeConst.TOKEN_EXPIRATION_IN_SECONDS);
        } else {
            expiredDate = null;
        }
        
        AccessToken accessToken = new AccessToken(user, StringHelper.generateUniqueString(), platformType,
                platformVersion, deviceToken, deviceId, expiredDate);
        accessToken.setCreatedDate(new Date());

        return accessTokenRepo.save(accessToken);
    }

    public void store(AccessToken token, Authentication authentication) {
        store(token.getToken(), authentication);
    }

    public void store(String token, Authentication authentication) {
//        authToken.put(new Element(token, authentication));
    }

    @SuppressWarnings("unused")
    private class TokenCacheEventListener implements CacheEventListener {

        @Override
        public void notifyElementRemoved(Ehcache cache, Element element) throws CacheException {

            logger.info("notifyElementRemoved " + element.getObjectValue().toString());

            removeRabbitUser(getAccessToken(element));
        }

        @Override
        public void notifyElementPut(Ehcache cache, Element element) throws CacheException {
            logger.info("notifyElementPut " + element.getObjectValue().toString());
        }

        @Override
        public void notifyElementUpdated(Ehcache cache, Element element) throws CacheException {
            logger.info("notifyElementUpdated " + element.getObjectValue().toString());
        }

        @Override
        public void notifyElementExpired(Ehcache cache, Element element) {
            logger.info("Element {} expired, remove rabbit user");
            AccessToken token = getAccessToken(element);
            if (token != null) {
                removeRabbitUser(token);
                remove(token.getToken());
            }
        }

        @Override
        public void notifyElementEvicted(Ehcache cache, Element element) {
            logger.info("Element {} evicted, remove rabbit user");
            AccessToken token = getAccessToken(element);
            if (token != null) {
                removeRabbitUser(token);
                remove(token.getToken());
            }
        }

        @Override
        public void notifyRemoveAll(Ehcache cache) {
            logger.info("notifyRemoveAll ");

        }

        @Override
        public void dispose() {

        }

        public Object clone() throws CloneNotSupportedException {
            throw new CloneNotSupportedException();
        }

        private AccessToken getAccessToken(Element element) {
            Object value = element.getObjectValue();

            if (value == null || !(value instanceof AnvuiAuthenticationToken)) {
                return null;
            }

            AnvuiAuthenticationToken token = (AnvuiAuthenticationToken) value;
            AuthenticationDetails details = (AuthenticationDetails) token.getDetails();

            return details.getAccessToken();
        }

        private void removeRabbitUser(AccessToken accessToken) {
            // Remove rabbit user if platform type = 3
            // if (accessToken != null && PlatformType.WEB.getCode() ==
            // accessToken.getPlatformType()
            // && accessToken.getAccount() != null && !accessToken.getAccount().isWeb()) {
            //
            // rabbitAdminClient.deleteRabbitUser(accessToken);
            // rbAdmin.deleteQueue(accessToken.getQueueNameByDeviceToken());
            // }
        }
    }

    public AccessToken remove(String token) {

        // Remove from cache
//        authToken.remove(token);

        // Remove from db
        AccessToken at = accessTokenRepo.findFirstByTokenOrderByCreatedDateDesc(token);
        if (at != null) {
            accessTokenRepo.delete(at);
        }
        return at;
    }
    
    public void removeTokenFromUser(Integer userId) {
        List<AccessToken> tokens = evictTokenCache(userId);
        accessTokenRepo.delete(tokens);
    }
    
    public void removeExpiredToken() {
        List<AccessToken> tokens = accessTokenRepo.findAllByExpiredDateLessThan(new Date());
        accessTokenRepo.delete(tokens);
    }
    
    @CacheEvict(cacheNames = "token")
    private List<AccessToken> evictTokenCache(Integer userId) {
        return accessTokenRepo.findAllByUserId(userId);
    }
}
