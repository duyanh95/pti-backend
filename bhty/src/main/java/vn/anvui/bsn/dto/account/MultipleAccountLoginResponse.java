package vn.anvui.bsn.dto.account;

import java.util.List;
import java.util.Map;

import org.springframework.security.core.Authentication;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import vn.anvui.bsn.auth.MultipleAccountAuthenticationToken;

public class MultipleAccountLoginResponse {

	private Authentication authentication;

	public MultipleAccountLoginResponse() {

	}

	public MultipleAccountLoginResponse(Authentication authentication) {
		this.authentication = authentication;
	}


	@JsonProperty("accounts")
	public List<Map<String, String>> getTickets() {
		if (authentication != null && authentication instanceof MultipleAccountAuthenticationToken) {
			MultipleAccountAuthenticationToken authToken = (MultipleAccountAuthenticationToken)authentication;
			return authToken.getTickets();
		}
		return null;
	}

	public void setAuthentication(Authentication authentication) {
		this.authentication = authentication;
	}

	@JsonIgnore
	public Authentication getAuthentication() {
		return this.authentication;
	}
}
