package vn.anvui.bsn.dto.user;

import vn.anvui.dt.entity.AccessToken;
import vn.anvui.dt.entity.User;

public class ActiveUserResponse {

	private User user;
	
	private AccessToken accessToken;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public AccessToken getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(AccessToken accessToken) {
		this.accessToken = accessToken;
	}
}
