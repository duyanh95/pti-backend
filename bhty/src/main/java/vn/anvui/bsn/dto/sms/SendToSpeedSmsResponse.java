package vn.anvui.bsn.dto.sms;

import java.util.List;

public class SendToSpeedSmsResponse {
    private String status;
    
    private String code;
    
    private Data data;
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {
        public String tranId;
        
        public Integer totalSms;
        
        public Double totalPrice;
        
        public List<String> invalidPhone;
    }
}
