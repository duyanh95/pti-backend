package vn.anvui.bsn.dto.contract;

import javax.validation.constraints.NotNull;

public class ContractRequest {
    @NotNull
    Integer packageId;
    
    @NotNull
    Integer inheritCustomerId;
    
    @NotNull
    Integer partnerId;

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Integer getInheritCustomerId() {
        return inheritCustomerId;
    }

    public void setInheritCustomerId(Integer inheritCustomerId) {
        this.inheritCustomerId = inheritCustomerId;
    }

    public Integer getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }
    
    
}
