package vn.anvui.bsn.dto.user;

import org.hibernate.validator.constraints.NotEmpty;

public class ChangePasswordRequest {
    @NotEmpty
    private String oldPassword;
    
    @NotEmpty
    private String newPassword;
    
    @NotEmpty
    private String confirmNewPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }
    
    
}
