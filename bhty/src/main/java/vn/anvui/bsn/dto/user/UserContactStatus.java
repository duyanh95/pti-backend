package vn.anvui.bsn.dto.user;

import java.util.List;

import vn.anvui.dt.entity.User;
import vn.anvui.dt.model.ContactReport;

public class UserContactStatus {
    User user;
    List<ContactReport> contactStatus;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<ContactReport> getContactStatus() {
        return contactStatus;
    }

    public void setContactStatus(List<ContactReport> contactStatus) {
        this.contactStatus = contactStatus;
    }
}
