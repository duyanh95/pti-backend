package vn.anvui.bsn.dto.transaction.napas;

import vn.anvui.bsn.dto.transaction.TransactionStatusRequest;

public class NapasTransactionStatusRequest extends TransactionStatusRequest {
    private String paymentId;
    
    private Integer responseCode;
    
    private String param;
    
    private String hash;

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
