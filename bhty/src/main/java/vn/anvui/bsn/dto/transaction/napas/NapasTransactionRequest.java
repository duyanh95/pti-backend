package vn.anvui.bsn.dto.transaction.napas;

import vn.anvui.bsn.dto.transaction.TransactionRequest;

public class NapasTransactionRequest extends TransactionRequest {
    private String ip;
    private String paymentGateway;
    private String cardType;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

}
