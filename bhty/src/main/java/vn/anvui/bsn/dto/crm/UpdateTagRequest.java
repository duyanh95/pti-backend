package vn.anvui.bsn.dto.crm;

import javax.validation.constraints.NotNull;

public class UpdateTagRequest {
    @NotNull
    private Integer tag;
    
    private String note;
    
    private Integer noteId;

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getNoteId() {
        return noteId;
    }

    public void setNoteId(Integer noteId) {
        this.noteId = noteId;
    }
    
}
