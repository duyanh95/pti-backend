package vn.anvui.bsn.dto.transaction.viettelpay;

import vn.anvui.dt.entity.OnlineTransaction;

public class ViettelPayCheckTransactionResponse {
    String billcode;
    
    String error_code;
    
    String merchant_code;
    
    String order_id;
    
    Long trans_amount;
    
    String check_sum;
    
    public ViettelPayCheckTransactionResponse() {
        super();
    }

    public ViettelPayCheckTransactionResponse(OnlineTransaction txn, String status, String checksum) {
        this.billcode = txn.getPaymentId();
        this.merchant_code = txn.getMerchantId();
        this.order_id = txn.getPaymentId();
        this.trans_amount = txn.getAmount();

        this.error_code = status;
        this.check_sum = checksum;
    }

    public ViettelPayCheckTransactionResponse(ViettelPayCheckTransactionRequest req, String errorCode, String hash,
            Long amount) {
        this.billcode = req.getBillCode();
        this.merchant_code = req.getMerchant_code();
        this.order_id = req.getOrder_id();
        
        this.error_code = errorCode;
        this.trans_amount = amount;
        this.check_sum = hash;
    }

    public String getBillcode() {
        return billcode;
    }

    public void setBillcode(String billcode) {
        this.billcode = billcode;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getMerchant_code() {
        return merchant_code;
    }

    public void setMerchant_code(String merchant_code) {
        this.merchant_code = merchant_code;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public Long getTrans_amount() {
        return trans_amount;
    }

    public void setTrans_amount(Long trans_amount) {
        this.trans_amount = trans_amount;
    }

    public String getCheck_sum() {
        return check_sum;
    }

    public void setCheck_sum(String check_sum) {
        this.check_sum = check_sum;
    }
}
