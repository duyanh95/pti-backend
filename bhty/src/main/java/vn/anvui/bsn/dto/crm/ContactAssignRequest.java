package vn.anvui.bsn.dto.crm;

import javax.validation.constraints.NotNull;

public class ContactAssignRequest {
    @NotNull
    private Integer userId;
    
    @NotNull
    private Integer noRegisteredContact;
    
    @NotNull
    private Integer noConsultContact;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getNoRegisteredContact() {
        return noRegisteredContact;
    }

    public void setNoRegisteredContact(Integer noRegisteredContact) {
        this.noRegisteredContact = noRegisteredContact;
    }

    public Integer getNoConsultContact() {
        return noConsultContact;
    }

    public void setNoConsultContact(Integer noConsultContact) {
        this.noConsultContact = noConsultContact;
    }
}
