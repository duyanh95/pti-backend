package vn.anvui.bsn.dto.account;

import org.springframework.security.core.Authentication;

import com.fasterxml.jackson.annotation.JsonIgnore;

import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.User;

public class LoginResponse {

    private User user;

	private Authentication authentication;
	
	private String role;

	public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LoginResponse() {

	}

	public LoginResponse(Authentication authentication) {
		this.authentication = authentication;
	}

    public String getAccount() {
		if (this.authentication != null) {
			return (String) authentication.getPrincipal();
		}
		return null;
	}

	public String getToken() {
		return ((AuthenticationDetails) authentication.getDetails()).getAccessToken().getToken();
	}
	
	public String getExpiredDate() {
		return ((AuthenticationDetails) authentication.getDetails()).getExpiredDate().toString();
	}

	public void setAuthentication(Authentication authentication) {
		this.authentication = authentication;
	}

	@JsonIgnore
	public Authentication getAuthentication() {
		return this.authentication;
	}

        public String getRole() {
            return role;
        }
    
        public void setRole(String role) {
            this.role = role;
        }
}
