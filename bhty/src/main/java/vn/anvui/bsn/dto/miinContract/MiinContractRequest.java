package vn.anvui.bsn.dto.miinContract;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public abstract class MiinContractRequest {
    @NotEmpty
    String customerName;
    
    @NotEmpty
    @Size(max = 11)
    String phoneNumber;
    
    String email;
    
    String buyerName;
    
    @Size(max = 11)
    String buyerPhone;
    
    Integer txnType;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerPhone() {
        return buyerPhone;
    }

    public void setBuyerPhone(String buyerPhone) {
        this.buyerPhone = buyerPhone;
    }

    public Integer getTxnType() {
        return txnType;
    }

    public void setTxnType(Integer txnType) {
        this.txnType = txnType;
    }
}