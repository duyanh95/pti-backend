package vn.anvui.bsn.dto.transaction.epay;

import org.hibernate.validator.constraints.NotEmpty;

import vn.anvui.bsn.dto.transaction.TransactionStatusRequest;

public class EpayTransactionStatusRequest extends TransactionStatusRequest {
    @NotEmpty
    String id;
    
    @NotEmpty
    String paramString;
    
    @NotEmpty
    String contractId;
    
    @NotEmpty
    String transactionId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParamString() {
        return paramString;
    }

    public void setParamString(String paramString) {
        this.paramString = paramString;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
