package vn.anvui.bsn.dto.banner;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;

import vn.anvui.dt.entity.Banner;
import vn.anvui.dt.enumeration.BannerType;

public class BannerRequest {
    @NotEmpty
    private String image;
    
    private String redirectURL;
    
    private String description;
    
    public Banner createBannerFromRequest(BannerType type) {
        return new Banner(this.image, this.redirectURL, this.getDescription(), type);
    }
    
    
    public Banner updateBannerFromRequest(Banner banner) {
        if (StringUtils.isNotEmpty(this.image)) {
            banner.setImage(this.image);
        }
        
        if (this.redirectURL != null) {
            banner.setRedirectURL(this.redirectURL);
        }
        
        if (this.description != null) {
            banner.setDescription(this.description);
        }
        
        return banner;
    }
    
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
