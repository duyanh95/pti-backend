package vn.anvui.bsn.dto.contract;

import java.util.List;

public class ExportRequest {
    private List<Integer> ids;

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }
}
