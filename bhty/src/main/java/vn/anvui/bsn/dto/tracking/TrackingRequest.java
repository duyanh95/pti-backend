package vn.anvui.bsn.dto.tracking;

public class TrackingRequest {
    
    private String utmTeam;

    private String utmAgent;
    
    private String utmSource;
    
    private String utmMedium;
    
    private String utmCampaign;
    
    private String utmChannel;
    
    private Integer contractId;

    public String getUtmTeam() {
        return utmTeam;
    }

    public void setUtmTeam(String utmTeam) {
        this.utmTeam = utmTeam;
    }

    public String getUtmAgent() {
        return utmAgent;
    }

    public void setUtmAgent(String utmAgent) {
        this.utmAgent = utmAgent;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getUtmChannel() {
        return utmChannel;
    }

    public void setUtmChannel(String utmChannel) {
        this.utmChannel = utmChannel;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }
}
