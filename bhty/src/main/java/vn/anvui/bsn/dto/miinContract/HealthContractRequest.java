package vn.anvui.bsn.dto.miinContract;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

public class HealthContractRequest extends MiinContractRequest {
    
    @NotNull
    Integer packageId;
    
    @NotNull
    Integer periodType;
    
    @JsonFormat(pattern = "dd/MM/yyyy")
    Date dob;

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Integer getPeriodType() {
        return periodType;
    }

    public void setPeriodType(Integer periodType) {
        this.periodType = periodType;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }
}
