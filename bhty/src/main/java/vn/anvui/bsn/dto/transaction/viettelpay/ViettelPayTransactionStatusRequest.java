package vn.anvui.bsn.dto.transaction.viettelpay;

import vn.anvui.bsn.dto.transaction.TransactionStatusRequest;

public class ViettelPayTransactionStatusRequest extends TransactionStatusRequest {
    private String billcode;
    
    private String cust_msisdn;
    
    private String error_code;
    
    private String merchant_code;
    
    private String order_id;
    
    private Integer payment_status;
    
    private Long trans_amount;
    
    private String vt_transaction_id;
    
    private String check_sum;
    
    public ViettelPayTransactionStatusRequest() {
        super();
    }
    
    public ViettelPayTransactionStatusRequest(String billcode, String custMsisdn, String errorCode, String merchantCode,
            String orderId, Integer paymentStatus, Long transAmount, String vtTransactionId, String checksum) {
        super();
        this.billcode = billcode;
        this.cust_msisdn = custMsisdn;
        this.error_code = errorCode;
        this.merchant_code = merchantCode;
        this.order_id = orderId;
        this.payment_status = paymentStatus;
        this.trans_amount = transAmount;
        this.vt_transaction_id = vtTransactionId;
        this.check_sum = checksum;
    }

    public String getBillCode() {
        return billcode;
    }

    public String getCustMsisdn() {
        return cust_msisdn;
    }

    public String getErrorCode() {
        return error_code;
    }

    public String getMerchantCode() {
        return merchant_code;
    }

    public String getOrderId() {
        return order_id;
    }

    public Integer getPaymentStatus() {
        return payment_status;
    }

    public Long getTransAmount() {
        return trans_amount;
    }

    public String getVtTransactionId() {
        return vt_transaction_id;
    }

    public String getChecksum() {
        return check_sum;
    }
    
    
}
