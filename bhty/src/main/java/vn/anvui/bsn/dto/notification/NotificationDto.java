package vn.anvui.bsn.dto.notification;

import java.util.HashMap;
import java.util.Map;

import vn.anvui.dt.enumeration.UserNotificationType;

public class NotificationDto {
    Integer typeId;
    
    Integer userId;
    
    Integer userNotificationType;
    
    Map<String, String> fields = new HashMap<>();
    
    public NotificationDto() {
        super();
    }

    public NotificationDto(Integer typeId, Integer userId, UserNotificationType userNotificationType,
            Map<String, String> fields) {
        super();
        this.typeId = typeId;
        this.userId = userId;
        this.userNotificationType = userNotificationType.getValue();
        this.fields = fields;
    }

    public NotificationDto(Integer typeId, Integer userId, Integer userNotificationType, Map<String, String> fields) {
        super();
        this.typeId = typeId;
        this.userId = userId;
        this.userNotificationType = userNotificationType;
        this.fields = fields;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
        
    }

    public Integer getUserNotificationType() {
        return userNotificationType;
    }

    public void setUserNotificationType(Integer userNotificationType) {
        this.userNotificationType = userNotificationType;
    }

    public Map<String, String> getFields() {
        return fields;
    }

    public void setFields(Map<String, String> fields) {
        this.fields = fields;
    }
    
    public void putField(String key, String value) {
        this.fields.put(key, value);
    }
    
}
