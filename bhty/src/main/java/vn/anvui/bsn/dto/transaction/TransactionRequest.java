package vn.anvui.bsn.dto.transaction;


import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import vn.anvui.dt.enumeration.TransactionSource;

public abstract class TransactionRequest {
    @NotNull
    Integer contractId;
    
    @Min(0)
    @Max(4)
    Integer source = TransactionSource.BHTY.getValue();
    
    private String returnURL;
    
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public String getReturnURL() {
        return returnURL;
    }

    public void setReturnURL(String returnURL) {
        this.returnURL = returnURL;
    }
}
