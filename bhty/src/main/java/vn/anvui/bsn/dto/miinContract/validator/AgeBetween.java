package vn.anvui.bsn.dto.miinContract.validator;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * The annotated element must be between min and max value
 * Accepts {@code Date}.
 *
 * @author Tran Duy Anh
 */
@Constraint(validatedBy = AgeBetweenValidator.class)
@Target({ METHOD, FIELD, PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface AgeBetween {
    String message() default "is not between ";
    
    int min();
    
    int max();
    
    Class<?>[] groups() default {};
    
    Class<? extends Payload>[] payload() default {};
}
