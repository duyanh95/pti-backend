package vn.anvui.bsn.beans.txnUtils;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.dt.enumeration.PaymentType;
import vn.anvui.dt.enumeration.TransactionStatus;
import vn.anvui.dt.model.TransactionReport;
import vn.anvui.dt.model.TransactionReportDetail;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.onlineTransaction.OnlineTransactionRepository;

@Service
public class TransactionUtilsImpl implements TransactionUtilsService {

    @Autowired
    OnlineTransactionRepository txnRepo;
    
    @Autowired
    ContractRepository contractRepo;

    @Override
    public List<TransactionReport> getTransactionReport(Date fromDate, Date toDate, String txnType, boolean isSuccess)
            throws AnvuiBaseException {
        
        Integer status = null;
        if (isSuccess)
            status = TransactionStatus.SUCCESS.getValue();
            
        List<TransactionReport> rsList = new LinkedList<>();

        if (txnType == null) {
            for (PaymentType type : PaymentType.values()) {
                TransactionReport report = new TransactionReport(type.name());

                report.setAmount(txnRepo.findTotalAmountByPaymentType(type.getValue(),
                        status, fromDate, toDate));
                if (report.getAmount() == null) {
                    report.setAmount(0.0);
                }

                report.setTxnList(txnRepo.findTransactionDetail(type.getValue(),
                        status, fromDate, toDate));

                rsList.add(report);
            }
        } else {
            try {
                txnType = txnType.toUpperCase();
                TransactionReport report = new TransactionReport(txnType.toUpperCase());

                Integer paymentType = Enum.valueOf(PaymentType.class, txnType).getValue();
                
                report.setAmount(txnRepo.findTotalAmountByPaymentType(paymentType,
                        TransactionStatus.SUCCESS.getValue(), fromDate, toDate));

                report.setTxnList(txnRepo.findTransactionDetail(paymentType,
                        TransactionStatus.SUCCESS.getValue(), fromDate, toDate));

                rsList.add(report);
            } catch (IllegalArgumentException e) {
                throw new AnvuiBaseException(ErrorInfo.TRANSACTION_PAYMENT_TYPE_INVALID);
            }
        }

        return rsList;
    }
    
    @Override
    public TransactionReportDetail getTransaction(String paymentId) throws AnvuiBaseException {
        TransactionReportDetail detail = txnRepo.findTransactionDetail(paymentId);
        
        if (detail == null)
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_NULL);
        
        return detail;
    }

    @Override
    public List<String> getPaymentType() throws AnvuiBaseException {
        List<String> rs = new LinkedList<>();
        
        for (PaymentType type : PaymentType.values()) {
            rs.add(type.name());
        }
        
        return rs;
    }
}
