package vn.anvui.bsn.beans.miinContract.apartment;

import vn.anvui.bsn.beans.miinContract.MiinContractService;
import vn.anvui.bsn.dto.miinContract.ApartmentContractRequest;
import vn.anvui.dt.entity.Contract;

public interface ApartmentContractService extends MiinContractService<ApartmentContractRequest> {
    public Contract getContratInfo(Integer id); 
}
