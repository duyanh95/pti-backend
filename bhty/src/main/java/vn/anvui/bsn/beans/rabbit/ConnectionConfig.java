package vn.anvui.bsn.beans.rabbit;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:${SYSTEM_ENVIRONMENT_VARIABLE}_rabbitmq.properties")
public class ConnectionConfig {

	@Value("${rabbit.hostname}")
	private String hostname;
	
	@Value("${rabbit.username}")
	private String username;
	
	@Value("${rabbit.password}")
	private String password;
	
	@Value("${rabbit.port}")
	private int port;
	
	@Value("${rabbit.vhost}")
	private String vhost;

	@Bean
	public ConnectionFactory connectionFactory() {
		
		CachingConnectionFactory connection = new CachingConnectionFactory(
				hostname);
		
		connection.setUsername(username);
		connection.setPassword(password);
		connection.setPort(port);
		connection.setVirtualHost(vhost);
		
		return connection;
	}
}
