package vn.anvui.bsn.beans.transaction;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.transaction.viettelpay.ViettelPayCheckTransactionRequest;
import vn.anvui.bsn.dto.transaction.viettelpay.ViettelPayCheckTransactionResponse;
import vn.anvui.bsn.dto.transaction.viettelpay.ViettelPayTransactionRequest;
import vn.anvui.bsn.dto.transaction.viettelpay.ViettelPayTransactionStatusRequest;
import vn.anvui.bsn.dto.transaction.viettelpay.ViettelPayTransactionStatusResponse;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.OnlineTransaction;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.PaymentType;
import vn.anvui.dt.enumeration.TransactionStatus;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.utils.EncryptionHelper;

@Service
@Qualifier("ViettelPayTransactionService")
public class ViettelPayTransactionService extends TransactionAbstractService
        implements TransactionService<ViettelPayTransactionRequest, ViettelPayTransactionStatusRequest>,
            ViettelPayAPIService {
    static final Logger log = Logger.getLogger(ViettelPayTransactionService.class.getSimpleName());
   
    static final String GATEWAY_URL = "http://125.235.40.34:8801/PaymentGateway/payment?";
    static final String COMMAND = "PAYMENT";
    static final String MERCHANT_CODE = "MIIN";
    static final String DEFAULT_RETURN_URL = "https://baohiemtinhyeu.vn/hoanthanh.html";
    static final String DEFAULT_CANCEL_URL = "https://baohiemtinhyeu.vn/thatbai.html";
    static final String VERSION = "2.0";
    
    @Value("${pti.viettel.access}")
    String ACCESS_CODE;
    
    @Value("${pti.viettel.hash}")
    String HASH_KEY;
    
    @Override
    public String createTransaction(ContractWrapper contract, ViettelPayTransactionRequest req)
            throws AnvuiBaseException {
        
        OnlineTransaction txn = new OnlineTransaction();
        
        if (contract.getContract().getContractStatus().equals(ContractStatus.PAID.getValue())
                || contract.getContract().getContractStatus().equals(ContractStatus.AGENCY_PAID.getValue())) {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_COMPLETED);
        }
        
        long amount = getAmount(contract.getContract());
        
        txn.setStatus(TransactionStatus.PENDING.getValue());
        txn.setContractId(contract.getContract().getId());
        txn.setMerchantId(MERCHANT_CODE);
        txn.setAmount(amount);
        txn.setPaymentType(PaymentType.VIETTEL.getValue());
        
        txn = txnRepo.save(txn);
        
        txn.setPaymentId("VTEL" + txn.getId());
        txn = txnRepo.save(txn);
        
        String returnUrl = getReturnURL(req);
        
        StringBuilder urlBuilder = new StringBuilder(GATEWAY_URL);
        urlBuilder.append("billcode=" + txn.getPaymentId());
        urlBuilder.append("&command=" + COMMAND);
        urlBuilder.append("&merchant_code=" + MERCHANT_CODE);
        urlBuilder.append("&order_id=" + txn.getPaymentId());
        urlBuilder.append("&return_url=" + returnUrl);
        urlBuilder.append("&trans_amount=" + amount);
        urlBuilder.append("&version=" + VERSION);

        String hashInput = ACCESS_CODE + txn.getPaymentId() + COMMAND + MERCHANT_CODE + txn.getPaymentId() +
                amount + VERSION;
        String hash = getViettelPayChecksum(hashInput, true);
        
        urlBuilder.append("&check_sum=" + hash);
        
        return urlBuilder.toString() ;
    }

    @Override
    public ContractWrapper updateTransactionStatus(ViettelPayTransactionStatusRequest req) throws AnvuiBaseException {
        
        String reqHashInput = ACCESS_CODE + req.getBillCode() + req.getCustMsisdn() + req.getErrorCode()
            + req.getMerchantCode() + req.getOrderId() + req.getPaymentStatus() + req.getTransAmount() + req.getVtTransactionId();
        String reqHash = getViettelPayChecksum(reqHashInput, false);
        
        if (!reqHash.equals(req.getChecksum())) {
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);
        }
        
        OnlineTransaction txn = txnRepo.findFirstByPaymentId(req.getOrderId());
        if (txn == null) {
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);
        }
        
        Contract contract = contractRepo.findOne(txn.getContractId());
        
        if (req.getErrorCode().equals("00") && req.getPaymentStatus() == 1) {
            txn.setStatus(TransactionStatus.SUCCESS.getValue());
            
            //TODO: set txn returnURL
            
            contract = updateContractStatus(contract);
            contractRepo.save(contract);
        } else {
            txn.setStatus(TransactionStatus.FAIL.getValue());
        }
        txnRepo.save(txn);
        
        notify(contract, txn);
        
        return new ContractWrapper(contract, txn);
    }

    @Override
    public ViettelPayCheckTransactionResponse checkViettelPayTransaction(ViettelPayCheckTransactionRequest req) {
        String errorCode = "00";
        Long amount = 0L;
        
        log.info(req.getBillCode());
        log.info(req.getCheck_sum());
        log.info(req.getMerchant_code());
        log.info(req.getOrder_id());
        
        if (StringUtils.isBlank(req.getBillCode()) || StringUtils.isBlank(req.getCheck_sum())
                || StringUtils.isBlank(req.getMerchant_code()) || StringUtils.isBlank(req.getOrder_id())
                || !req.getBillCode().equals(req.getOrder_id())) {
            log.warning("error: properties missing");
            errorCode = "01";
        } else {
            OnlineTransaction txn = txnRepo.findFirstByPaymentId(req.getOrder_id());
            
            if (txn == null ) {
                log.warning("error: can't find transaction");
                errorCode = "01";
            } else {
                amount = txn.getAmount();
            }
        }
        
        String hashInput = ACCESS_CODE + req.getBillCode() + req.getMerchant_code() + req.getOrder_id() + amount;
        String hash = getViettelPayChecksum(hashInput, false);
        
        if (!hash.equals(req.getCheck_sum())) {
            log.warning("error: invalid checksum");
            log.info("calculated: " + hash + ", get: " + req.getCheck_sum());
            errorCode = "02";
        }
        
        hashInput = ACCESS_CODE + req.getBillCode() + errorCode + req.getOrder_id() + amount;
        hash = getViettelPayChecksum(hashInput, true);
        return new ViettelPayCheckTransactionResponse(req, errorCode, hash, amount);
    }
    
    @Override
    public ViettelPayTransactionStatusResponse getTransactionStatusResponse(ContractWrapper contract, String errorCode) {
        String hashInput = ACCESS_CODE + errorCode + MERCHANT_CODE + contract.getTxn().getPaymentId();
        String checksum = EncryptionHelper.hmacSHA1Encode(hashInput, HASH_KEY);
        
        return new ViettelPayTransactionStatusResponse(contract.getTxn(), errorCode, checksum);
    }

    @Override
    public ViettelPayTransactionStatusResponse getTransactionStatusResponse(ViettelPayTransactionStatusRequest req,
            String errorCode) {
        String hashInput = ACCESS_CODE + errorCode + MERCHANT_CODE + req.getOrderId();
        String checksum = getViettelPayChecksum(hashInput, false);
        
        return new ViettelPayTransactionStatusResponse(req, errorCode, checksum);
    }
    
    private String getViettelPayChecksum(String hashInput, boolean urlEncode) {
        try {
            String encodedStr = EncryptionHelper.hmacSHA1Encode(hashInput, HASH_KEY);
            if (urlEncode) {
                return URLEncoder.encode(encodedStr, "UTF-8");
            } else {
                return encodedStr;
            }
        } catch (UnsupportedEncodingException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
    }
}
