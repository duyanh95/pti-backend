package vn.anvui.bsn.beans.insurancePackage;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.entity.PackagePeriod;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.repos.insurancePackage.PackagePeriodRepository;

@Service
public class InsurancePackageServiceImpl implements InsurancePackageService {
    
    @Autowired
    InsurancePackageRepository packageRepo;
    
    @Autowired
    PackagePeriodRepository periodRepo;

    @Override
    @Cacheable("package")
    public List<InsurancePackage> getAllPackages(Integer productId) throws AnvuiBaseException {
        List<InsurancePackage> rs = packageRepo.findAllByProductIdAndActive(productId, true);
        
        rs.forEach(pkg -> {
            if (pkg.getHasPeriod()) {
                List<PackagePeriod> periods = periodRepo.findAllByPackageId(pkg.getId());
                pkg.setPeriod(periods);
            }
        });
        
        return rs;
    }

    @Override
    public List<InsurancePackage> getAllPackages() throws AnvuiBaseException {
        return getAllPackages(1);
    }

}
