package vn.anvui.bsn.beans.contactLog;

import java.util.List;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.dt.entity.ContactLog;

public interface ContactLogService {
    List<ContactLog> getContactLog(String phoneNumber) throws AnvuiBaseException;
}
