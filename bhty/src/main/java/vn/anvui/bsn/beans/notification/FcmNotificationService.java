package vn.anvui.bsn.beans.notification;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.TopicManagementResponse;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.notification.NotificationDto;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.Device;
import vn.anvui.dt.entity.Notification;
import vn.anvui.dt.entity.NotificationType;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.DeviceType;
import vn.anvui.dt.enumeration.UserNotificationType;
import vn.anvui.dt.repos.fcmNotification.DeviceRepository;
import vn.anvui.dt.repos.notification.NotificationRepository;
import vn.anvui.dt.repos.notification.NotificationTypeRepository;
import vn.anvui.dt.utils.StringHelper;

@Service
public class FcmNotificationService implements NotificationService {
    final static Logger log = Logger.getLogger(FcmNotificationService.class.getName());
    
    @Autowired
    NotificationRepository notiRepo;
    
    @Autowired
    NotificationTypeRepository notiTypeRepo;
    
    @Autowired
    DeviceRepository deviceRepo;
    
    public final static String SYS_TOPIC = "system";
    
    @PostConstruct
    public void initFireBase() throws IOException {
        try {
            FileInputStream serviceAccount = new FileInputStream(System.getenv("FIREBASE_CREDENTIAL"));
    
            FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://miin-6a54d.firebaseio.com/")
                .build();
    
            FirebaseApp.initializeApp(options);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    @Override
    public void createAndSendNotification(NotificationDto dto) {
        NotificationType type = notiTypeRepo.findOne(dto.getTypeId());
        
        Notification noti = new Notification(type);
 
        String content = noti.getContent();
        for (String field : dto.getFields().keySet()) {
            content = StringHelper.replaceTemplate(content, field, dto.getFields().get(field));
        }
        
        noti.setContent(content);
        noti.setUserId(dto.getUserId());
        noti.setUserNotificationType(dto.getUserNotificationType());
        
        notiRepo.save(noti);
        
        sendNotification(noti);
    }
    
    private void sendNotification(Notification noti) {
        
//        try {
//            initFireBase();
//        } catch (IOException e1) {
//            e1.printStackTrace();
//        }
        
        com.google.firebase.messaging.Notification notification = 
                new com.google.firebase.messaging.Notification(noti.getTitle(), noti.getContent());
        
        Message message;
        if (noti.getUserId() != null) {
            String token = deviceRepo.findOne(noti.getUserId()).getToken();
            log.info(token);
            
            message = Message.builder()
                    .setNotification(notification)
                    .setToken(token)
                    .build();
        } else {
            message = Message.builder()
                    .setNotification(notification)
                    .setTopic(SYS_TOPIC)
                    .build();
        }
        
        try {
            String response = FirebaseMessaging.getInstance().send(message);
            log.info(response);
        } catch (Exception e) {
            log.warning(e.getMessage());
        }
    }

    @Override
    public List<Notification> listNotification(Integer page, Integer count, boolean isAgency) {
        
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getAuthenticatedUser();

            if (user == null)
                throw new Exception();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        
        Pageable pageable = new PageRequest(page, count, sort);
        
        List<Integer> userNotiTypes = new ArrayList<>();
        userNotiTypes.add(UserNotificationType.ALL.getValue());
        if (isAgency) {
            userNotiTypes.add(UserNotificationType.AGENCY.getValue());
        } else {
            userNotiTypes.add(UserNotificationType.CUSTOMER.getValue());
        }
        
        Page<Notification> notis = 
                notiRepo.findAllByUserIdAndUserNotificationTypeIn(user.getId(), userNotiTypes, pageable);
        
        return notis.getContent();
    }

    @Override
    public void markAsRead(List<Integer> notiIds) {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getAuthenticatedUser();

            if (user == null)
                throw new Exception();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        List<Notification> notiList = notiRepo.findAll(notiIds);
        log.info(notiList.toString());
        
        for (Notification noti : notiList) {
            if (!noti.getUserId().equals(user.getId()))
                throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
            
            noti.setRead(true);
        }
        
        notiRepo.save(notiList);
    }

    @Override
    @Transactional
    public void subscribeSystemTopic(List<String> tokens) {
        
        List<Device> device = deviceRepo.findAllByTokenIn(tokens);
        
        if (device == null) {
            log.warning("device token not existed");
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
        
        List<String> deviceTokens = device.stream()
                .map(Device::getToken)
                .collect(Collectors.toList());
        
        try {
            TopicManagementResponse resp = FirebaseMessaging.getInstance()
                    .subscribeToTopic(deviceTokens, SYS_TOPIC);
            
            if (resp.getFailureCount() > 0) {
                resp.getErrors().forEach(err -> log.warning(err.getReason()));
                throw new Exception();
            }
            
        } catch (Exception e) {
            log.warning("topic subcription failed");
        }
    }
    
    @Override
    public void addUserToken(String token, Integer userId, DeviceType type) {
        Device device = deviceRepo.findOne(userId);
        
        if (device == null) {
            device = new Device(token, userId, type);
        } else {
            device.setToken(token);
        }
        
        deviceRepo.save(device);
    }

}