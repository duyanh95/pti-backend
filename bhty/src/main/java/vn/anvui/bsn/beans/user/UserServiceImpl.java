package vn.anvui.bsn.beans.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;

import vn.anvui.api.amqp.MessageProducer;
import vn.anvui.bsn.auth.CredentialService;
import vn.anvui.bsn.beans.mailing.MailingService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.account.LoginRequest;
import vn.anvui.bsn.dto.account.LoginResponse;
import vn.anvui.bsn.dto.account.OTPResetRequest;
import vn.anvui.bsn.dto.notification.NotificationDto;
import vn.anvui.bsn.dto.sms.SmsQueueDto;
import vn.anvui.bsn.dto.user.ActiveUserRequest;
import vn.anvui.bsn.dto.user.ActiveUserResponse;
import vn.anvui.bsn.dto.user.ChangePasswordRequest;
import vn.anvui.bsn.dto.user.PhoneLoginDto;
import vn.anvui.bsn.dto.user.UserRequest;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.AccessToken;
import vn.anvui.dt.entity.Device;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.entity.UserClosure;
import vn.anvui.dt.enumeration.NotificationTypeEnum;
import vn.anvui.dt.enumeration.SystemVariableKey;
import vn.anvui.dt.enumeration.UserNotificationType;
import vn.anvui.dt.enumeration.UserStatus;
import vn.anvui.dt.enumeration.UserType;
import vn.anvui.dt.repos.fcmNotification.DeviceRepository;
import vn.anvui.dt.repos.role.RoleRepository;
import vn.anvui.dt.repos.systemVariable.SystemVariableRepository;
import vn.anvui.dt.repos.user.UserRepository;
import vn.anvui.dt.repos.userClosure.UserClosureRepository;
import vn.anvui.dt.utils.DateTimeHelper;
import vn.anvui.dt.utils.EncryptionHelper;

@Service
@PropertySource("classpath:system_messages.properties")
public class UserServiceImpl implements UserService {

    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RoleRepository roleRepo;

    @Autowired
    UserClosureRepository closureRepo;

    @Autowired
    private CredentialService credentialService;

    @Autowired
    private MailingService mailService;
    
    @Autowired
    SystemVariableRepository sysVarRepo;
    
    @Autowired
    MessageProducer msgProducer;
    
    @Autowired
    DeviceRepository deviceRepo;
    
    public static final String ANVUI_KEY = "anvui";

    @Override
    public ActiveUserResponse activeUser(ActiveUserRequest request) throws AnvuiBaseException {
        return null;
    }
    
    @Override
    public User createUserFromGooglePayload(Payload payload) {
        User user = new User();
        
        user.setUserName(payload.getSubject());
        user.setEmail(payload.getEmail());
        user.setGoogleId(payload.getSubject());
        user.setFullName((String) payload.get("name"));
        user.setAvatar((String) payload.get("picture"));
        user.setStatus(UserStatus.ACTIVE.getValue());
        
        user.setUserType(UserType.CUSTOMER.getValue());
        
        List<String> roles = new ArrayList<>();
        roles.add(UserType.CUSTOMER.getValue());
        
        user.setRoles(roleRepo.findAllByNameIn(roles));
        
        return userRepo.save(user);
    }

    @Override
    public User createUser(UserRequest request, UserType type) throws AnvuiBaseException {
        User loggedUser;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            loggedUser = userDetails.getUser();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        User user = new User();
        if (StringUtils.isEmpty(request.getUserName())) {
            throw new AnvuiBaseException(ErrorInfo.USER_NAME_NULL);
        }
        user.setUserName(request.getUserName());

        if (userRepo.existsByUserName(request.getUserName())) {
            throw new AnvuiBaseException(ErrorInfo.USER_NAME_EXISTED);
        }
        
        if (userRepo.existsByEmail(request.getEmail())) {
            throw new AnvuiBaseException(ErrorInfo.EMAIL_EXISTED);
        }

        if (!request.getPassword().equals(request.getPasswordConfirm())) {
            throw new AnvuiBaseException(ErrorInfo.USER_PASSWORD_NOT_MATCHED);
        }
        
        if (phoneNumberExisted(request.getPhoneNumber())) {
            throw new AnvuiBaseException(ErrorInfo.PHONE_NUMBER_EXISTED);
        }
        
        user.setPassword(EncryptionHelper.bcryptEncrypt(request.getPassword()));
        user.setFullName(request.getFullName());
        user.setPhoneNumber(request.getPhoneNumber());
        user.setEmail(request.getEmail());
        user.setUserType(type.getValue());
        user.setAvatar(request.getAvatar());
        user.setAddress(request.getAddress());
        user.setProvince(request.getProvince());
        user.setStatus(UserStatus.ACTIVE.getValue());

        List<String> roles = new ArrayList<>();
        if (!StringUtils.isEmpty(request.getRoleName())) {
            roles.add(request.getRoleName());
        } else {
            roles.add(type.getValue());
        }
        user.setRoles(roleRepo.findAllByNameIn(roles));

        if (user.getRoles() == null || user.getRoles().isEmpty())
            throw new AnvuiBaseException(ErrorInfo.ROLE_NULL);

        switch (type) {
        case PTI_USER:
            user.setParentId(loggedUser.getId());
            break;

        case AGENCY:
            if (!(StringUtils.isEmpty(loggedUser.getParentId()) || loggedUser.getId().equals(loggedUser.getParentId())))
                throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);

            if (request.getCommission() != null)
                if (request.getCommission() < 0 || request.getCommission() > 1)
                    throw new AnvuiBaseException(ErrorInfo.USER_COMMISSION_INVALID);

            if (request.getAgencyCode() != null)
                if (userRepo.countByAgencyCode(request.getAgencyCode()) > 0)
                    throw new AnvuiBaseException(ErrorInfo.AGENCY_CODE_EXISTED);

            user.setAgencyCode(request.getAgencyCode());

            if (request.getCodAllowed() != null)
                user.setCodAllowed(request.getCodAllowed());
            else
                user.setCodAllowed(true);

            log.info("parent id: " + request.getParentId());
            user.setParentId(request.getParentId());

            user.setCitizenId(request.getCitizenId());
            user.setBankAccount(request.getBankAccount());
            user.setBankInfo(request.getBankInfo());

            // check if requested users is a 1st level agency
            if (loggedUser.getUserType().equals(UserType.AGENCY.getValue())) {
                if (loggedUser.getParentId() != null && (!loggedUser.getParentId().equals(loggedUser.getId())))
                    throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);

                if (user.getParentId() == null) {
                    user.setParentId(loggedUser.getId());
                } else {
                    List<Integer> descIds = new ArrayList<>(closureRepo.findDescendanceIdFromId(loggedUser.getId()));
                    if (!descIds.contains(user.getParentId())) {
                        throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
                    }
                }
            }
            
            user.setCommission(request.getCommission());
            
            if (user.getParentId() != null) {
                // if parentId is a agency
                User parent = userRepo.findOne(user.getParentId());
                if (parent != null) {
                    user.setAgencyCode(parent.getAgencyCode());
                    if (!parent.isCodAllowed()) {
                        user.setCodAllowed(false);
                    }
                    
                    if (request.getCommission() == null || request.getCommission() > parent.getCommission()) {
                        user.setCommission(parent.getCommission());
                    }
                }

                log.info("agency code: " + user.getAgencyCode());
            }
            
            // check commission before save
            if (user.getCommission() == null)
                throw new AnvuiBaseException(ErrorInfo.USER_COMMISSION_INVALID);

            // check validity of agency code
            if (StringUtils.isEmpty(user.getAgencyCode()))
                throw new AnvuiBaseException(ErrorInfo.AGENCY_CODE_IS_MISSING);

            break;

        case ADMIN:
            break;

        default:
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }

        user = userRepo.save(user);
        if (type.equals(UserType.AGENCY))
            createUserClosure(user, user.getParentId());

        return user;
    }

    @Override
    public boolean changePassword(ChangePasswordRequest req) throws AnvuiBaseException {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        if (!req.getNewPassword().equals(req.getConfirmNewPassword()))
            throw new AnvuiBaseException(ErrorInfo.USER_PASSWORD_NOT_MATCHED);

        if (!EncryptionHelper.bcryptPasswordMatch(req.getOldPassword(), user.getPassword()))
            throw new AnvuiBaseException(ErrorInfo.USER_PASSWORD_NOT_MATCHED);

        user.setPassword(EncryptionHelper.bcryptEncrypt(req.getNewPassword()));

        userRepo.save(user);
        credentialService.removeTokenFromUser(user.getId());

        return true;
    }

    @Override
    public List<User> findByUserType(String userType) {
        return userRepo.findByUserTypeAndStatusOrderByFullNameAsc(userType, UserStatus.ACTIVE.getValue());
    }

    @Override
    public User listAgencyUserByRootId(Integer agencyId, String userName, 
            String citizenId, String phoneNumber, String email) throws AnvuiBaseException {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        if (user.getUserType().equals(UserType.AGENCY.getValue())) {
            agencyId = user.getId();
        } else if (user.getUserType().equals(UserType.ADMIN.getValue())) {

        } else {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        if (StringUtils.isEmpty(userName) && StringUtils.isEmpty(citizenId)
                && StringUtils.isEmpty(phoneNumber) && StringUtils.isEmpty(email)) {
            User pUsers = userRepo.findOne(agencyId);
            if (pUsers == null)
                throw new AnvuiBaseException(ErrorInfo.USER_NOT_EXISTED);
    
            log.info(pUsers.getId().toString());
    
            pUsers.setChildUsers(getListUserHierachy(agencyId));
    
            return pUsers;
        } else {
            User pUsers = userRepo.findOne(agencyId);
            Collection<Integer> userIds = closureRepo.findDescendanceIdFromId(agencyId);
            userIds.remove(pUsers.getId());
             
            pUsers.setChildUsers(userRepo.findUsersBy(userName, citizenId, phoneNumber, email, userIds));
            return pUsers;
        }
    }

    private List<User> getListUserHierachy(Integer userId) {
        List<User> users = userRepo.getAllByParentId(userId);

        if (!(users.isEmpty() || users == null)) {
            users.forEach((user) -> {
                log.info(user.getId().toString());
                List<User> userC = getListUserHierachy(user.getId());
                user.setChildUsers(userC);
            });
        }

        return users;
    }

    @Override
    public User updateUser(UserRequest req, Integer id) throws AnvuiBaseException {
       
        User reqUser;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            reqUser = userDetails.getUser();

            if (reqUser == null)
                throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        User user;
        if (id != null) {
            user = userRepo.findOne(id);
            if (user == null) {
                throw new AnvuiBaseException(ErrorInfo.EMPTY);
            }
        } else {
            user = reqUser;
        }

        List<Integer> descIds = new ArrayList<>(closureRepo.findDescendanceIdFromId(reqUser.getId()));
        log.info(descIds.toString());
        if ((!descIds.contains(id)) && (!reqUser.getUserType().equals(UserType.ADMIN.getValue())
                && (!user.getId().equals(reqUser.getId()))))
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);

        if (reqUser.getCommission() != null) {
            if (user.getUserType().equals(UserType.ADMIN.getValue())) {
                user.setCommission(user.getCommission());
            }
        }

        if (!StringUtils.isEmpty(req.getAgencyCode()))
            user.setAgencyCode(req.getAgencyCode());

        user = req.updateUser(user);

        if (id != null) {
            if (!StringUtils.isEmpty(req.getCodAllowed()))
                user.setCodAllowed(req.getCodAllowed());
        }
        
        return userRepo.save(user);
    }

    @Override
    public void updateUserStatus(Integer id, UserType type, UserStatus status) throws AnvuiBaseException {
        User user;

        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            user = userDetails.getAuthenticatedUser();

            if (user == null)
                throw new Exception();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        User deletedUser = userRepo.findOne(id);
        if (deletedUser == null)
            throw new AnvuiBaseException(ErrorInfo.USER_NOT_EXISTED);

        if (!deletedUser.getUserType().equals(type.getValue()))
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);

        switch (type) {
        case PTI_USER:
        case AGENCY_USER:
            if (!(user.getUserType().equals(UserType.ADMIN.getValue()) && user.getParentId().equals(user.getId())))
                throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        case AGENCY:
            if (!user.getUserType().equals(UserType.ADMIN.getValue()))
                throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);

            List<Integer> userIds = new ArrayList<>(closureRepo.findDescendanceIdFromId(deletedUser.getId()));
            List<User> agencyUsers = userRepo.findAll(userIds);

            agencyUsers.forEach((agencyUser) -> {
                agencyUser.setStatus(status.getValue());
            });

            if (status.equals(UserStatus.DELETED)) {
                List<UserClosure> closures = new ArrayList<>(closureRepo.findDescendanceFromId(deletedUser.getId()));
                closureRepo.delete(closures);
            }
            userRepo.save(agencyUsers);
            break;

        default:
            break;

        }
        deletedUser.setStatus(status.getValue());
        credentialService.removeTokenFromUser(deletedUser.getId());

        userRepo.save(deletedUser);

    }

    private void createUserClosure(User user, Integer parentId) throws AnvuiBaseException {
        if (parentId == null) {
            UserClosure closure = new UserClosure(user.getId(), 1);
            closure = closureRepo.save(closure);

        } else {
            if (!userRepo.exists(parentId))
                throw new AnvuiBaseException(ErrorInfo.USER_PARENT_NOT_EXISTED);

            Integer depth = closureRepo.findDepthFromId(parentId) + 1;

            List<UserClosure> ascendanceClosures = new ArrayList<>(closureRepo.findAscendanceFromId(parentId));
            List<UserClosure> closures = new LinkedList<>();

            ascendanceClosures.forEach((ascClosure) -> {
                UserClosure closure = ascClosure.clone();
                closure.setDescendanceId(user.getId());
                closures.add(closure);
            });
            
            closures.add(new UserClosure(user.getId(), depth));
            closureRepo.save(closures);
        }
    }

    @Override
    public List<User> getAllRootAgencies(String userName, String citizenId, String phoneNumber, String email) 
            throws AnvuiBaseException {
        Set<Integer> userIds = closureRepo.findRootUsers();

        return userRepo.findUsersBy(userName, citizenId, phoneNumber, email, userIds);
    }

    @Override
    public void allowAgencyCod(Integer userId, Boolean enable) throws AnvuiBaseException {
        User loggedUser;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            loggedUser = userDetails.getUser();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        if (loggedUser.getParentId() != null)
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);

        Set<Integer> userIds = new HashSet<>(closureRepo.findDescendanceIdFromId(loggedUser.getId()));
        if (userIds.contains(userId))
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);

        User updatingUser = userRepo.findOne(userId);

        if (updatingUser == null)
            throw new AnvuiBaseException(ErrorInfo.USER_NOT_EXISTED);

        updatingUser.setCodAllowed(enable);
        userRepo.save(updatingUser);
    }

    public boolean hasUpdateUserAccess(User loggedUser, Integer updateUserId) {
        List<Integer> descIds = new ArrayList<>(closureRepo.findDescendanceIdFromId(loggedUser.getId()));
        log.info(descIds.toString());
        if ((!descIds.contains(updateUserId)) && (!loggedUser.getUserType().equals(UserType.ADMIN.getValue())))
            return false;
        else
            return true;
    }

    public void resetPassword(User user, String email) throws AnvuiBaseException {
        String sendEmail = user.getEmail();
        if (StringUtils.isEmpty(user.getEmail())) {
            if (StringUtils.isEmpty(email))
                throw new AnvuiBaseException(ErrorInfo.CUSTOMER_EMAIL_EMPTY);
            sendEmail = email;
            user.setEmail(email);
        }

        String pass = RandomStringUtils.randomAlphanumeric(12);
        
        user.setPassword(EncryptionHelper.bcryptEncrypt(pass));
        userRepo.save(user);
        
        // send email to user
        String body = "Your account \"%s\" password has been reset to \"%s\".";
        body = String.format(body, user.getUserName(), pass);

        List<String> listTo = new ArrayList<>(Arrays.asList(sendEmail));
        mailService.sendEmail(listTo, null, null, "Your password has been reset", body, "text/plain");
    }

    @Override
    public void resetUserPassword(Integer id, String email) throws AnvuiBaseException {
        User loggedUser;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            loggedUser = userDetails.getUser();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        Optional<User> updatedUser = Optional.ofNullable(userRepo.findOne(id));
        updatedUser.orElseThrow(() -> new AnvuiBaseException(ErrorInfo.USER_NOT_EXISTED));

        if (!hasUpdateUserAccess(loggedUser, updatedUser.get().getId())) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        updatedUser.ifPresent((user) -> resetPassword(user, email));
    }
    
    public PhoneLoginDto generateOTPCode(User user) {
        if (StringUtils.isEmpty(user.getOtpCode()) || 
                (System.currentTimeMillis() > user.getOtpExpiredDate().getTime())) {
            user.setOtpCode(RandomStringUtils.randomNumeric(6));
            user.setOtpExpiredDate(new Date(System.currentTimeMillis() + 30 * DateTimeHelper.MIN_IN_MS));
        }
        
        SmsQueueDto sms = new SmsQueueDto();
        String content = "Ma xac nhan cua ban la " + user.getOtpCode();
        sms.setContent(content);
        sms.setReceivers(Arrays.asList(user.getPhoneNumber()));

        userRepo.save(user);
        
        return new PhoneLoginDto(user, sms);
    }
    
    @Override
    public User registerAgency(UserRequest req) throws AnvuiBaseException {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        if (!user.getUserType().equals(UserType.CUSTOMER.getValue())) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        req.updateUser(user);
        
        if (StringUtils.isEmpty((user.getCitizenId()))
                || StringUtils.isEmpty(user.getAddress())
                || StringUtils.isEmpty(user.getProvince())) {
            throw new AnvuiBaseException(ErrorInfo.USER_INFO_MISSING);
        }
        
        user.setStatus(UserStatus.AGENT_REGISTER_PENDING.getValue());
        return userRepo.save(user);
    }

    @Override
    public User confirmAgentAsAgency(UserRequest req, Integer uid) throws AnvuiBaseException {
        User user = userRepo.findOne(uid);
        
        if (user == null) {
            throw new AnvuiBaseException(ErrorInfo.USER_NOT_EXISTED);
        }

        if (userRepo.existsByUserName(user.getPhoneNumber())) {
            throw new AnvuiBaseException(ErrorInfo.USER_NAME_EXISTED);
        }

        String password = RandomStringUtils.randomAlphanumeric(10);
        
        user.setStatus(UserStatus.ACTIVE.getValue());
        
        if (!StringUtils.isEmpty(req.getUserName())) {
            if (userRepo.existsByUserName(req.getUserName())) {
                throw new AnvuiBaseException(ErrorInfo.USER_NAME_EXISTED);
            }
            
            user.setUserName(req.getUserName());
        }
            
        user.setPassword(EncryptionHelper.bcryptEncrypt(password));
        
        if (req.getConfirm() == null)
            req.setConfirm(true);
        
        if (req.getConfirm()) {
            user.setUserType(UserType.COLLAB.getValue());
            
            user.setRoles(roleRepo.findAllByNameIn(Arrays.asList(UserType.COLLAB.getValue())));
            
            if (!StringUtils.isEmpty(req.getAgencyCode())) {
                user.setAgencyCode(req.getAgencyCode());
            } else {
                user.setAgencyCode("COL");
            }
            
            if (!StringUtils.isEmpty(req.getCommission())) {
                user.setCommission(req.getCommission());
            } else {
                try {
                    user.setCommission(
                        Double.valueOf(sysVarRepo.findOne(SystemVariableKey.COLLAB_COMMISSION.name()).getValue()));
                } catch (NumberFormatException e) {
                    throw new AnvuiBaseException(ErrorInfo.DATA_NOT_EXIST);
                }
            }
            
            user.setCodAllowed(false);
        } else {
            user.setUserType(UserType.CUSTOMER.getValue());
        }
        
        sendConfirmAgencyNotification(user, password, req.getConfirm());
        
        return userRepo.save(user);
    }

    @Override
    public PhoneLoginDto sendResetOtp(String account) {
        User user = userRepo.findFirstByPhoneNumber(account);
        
        if (user != null) {
            return generateOTPCode(user);
        } else {
            throw new AnvuiBaseException(ErrorInfo.USER_NOT_EXISTED);
        }
    }

    @Override
    public void resetByOtp(OTPResetRequest req) {
        User user = userRepo.findFirstByPhoneNumber(req.getPhoneNumber());
        
        if (user == null)
            throw new AnvuiBaseException(ErrorInfo.USER_NOT_EXISTED);
        log.info("user id: " + user.getId());
        
        if (!req.getPassword().equals(req.getPasswordConfirm()))
            throw new AnvuiBaseException(ErrorInfo.USER_PASSWORD_NOT_MATCHED);

        if (!isValidOtp(user, req.getOtpCode())) {
            user.setOtpCode(null);
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        user.setPassword(EncryptionHelper.bcryptEncrypt(req.getPassword()));

        userRepo.save(user);
        credentialService.removeTokenFromUser(user.getId());
    }
    
    protected boolean isValidOtp(User user, String otp) {
        if (!otp.equals(user.getOtpCode()))
            return false;

        if (user.getOtpExpiredDate().getTime() < System.currentTimeMillis())
            return false;
        
        return true;
    }

    protected boolean phoneNumberExisted(String phoneNumber) {
        return userRepo.findFirstByPhoneNumber(phoneNumber) != null;
    }

    @Override
    public AccessToken createAPIKey(Integer id) throws AnvuiBaseException {
        User user = userRepo.findOne(id);
        if (user == null)
            throw new AnvuiBaseException(ErrorInfo.USER_NOT_EXISTED);
        
        return credentialService.createToken(user, null, true);
    }

    @Override
    public User getUserInfo() throws AnvuiBaseException {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        return user;
    }

    @Override
    public List<User> listPendingAgencyRequestUsers(Integer page, Integer count, String citizenId, String phoneNumber)
            throws AnvuiBaseException {
        try {
            return userRepo.getListRequestedAgency(page, count, citizenId, phoneNumber);
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
    }
    
    protected void sendConfirmAgencyNotification(User user, String password, boolean permit) {
        NotificationTypeEnum type;
        Map<String, String> fields = new HashMap<>();
        
        if (permit) {
            type = NotificationTypeEnum.REGISTER_SUCCESS;
            
            fields.put("userName", user.getUserName());
            fields.put("password", password);
        } else {
            type = NotificationTypeEnum.REGISTER_FAIL;
        }
        
        NotificationDto noti = new NotificationDto(type.getValue(), user.getId(), 
                UserNotificationType.ALL , fields);
        
        msgProducer.sendPushServiceQueue(noti);
    }

    @Override
    public void updateDeviceToken(String token) throws AnvuiBaseException {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        Device device = deviceRepo.findOne(user.getId());
        
        device.setToken(token);
        deviceRepo.save(device);
    }

    @Override
    public LoginResponse registerMiinUser(LoginRequest req) {
        User user = userRepo.findFirstByPhoneNumber(req.getUserName());
        
        if (user == null) {
            user = new User();
            user.setUserName(req.getUserName());
            user.setPhoneNumber(req.getUserName());
            
            user.setUserType(UserType.CUSTOMER.getValue());
            user.setStatus(UserStatus.ACTIVE.getValue());
            user.setCodAllowed(false);
            
            user.setPassword(EncryptionHelper.bcryptEncrypt(req.getPassword()));
        } else {
            throw new AnvuiBaseException(ErrorInfo.USER_NAME_EXISTED);
        }
        
        userRepo.save(user);
       
        Authentication auth = credentialService.store(user, req);
        
        LoginResponse res = new LoginResponse(auth);
        res.setUser(user);
        
        return res;
    }
    
    @Override 
    public void sendOTP() {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        PhoneLoginDto dto = generateOTPCode(user);
        msgProducer.sendSms(dto.getSms());
    }

    @Override
    public User verifyPhoneNumber(String otp) {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        if (!isValidOtp(user, otp)) {
            throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST);
        } else {
            user.setPhoneVerified(true);
            user.setOtpCode(null);
            user.setOtpExpiredDate(null);
        }
        
        return userRepo.save(user);
    }

}
