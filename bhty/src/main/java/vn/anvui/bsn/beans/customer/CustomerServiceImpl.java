package vn.anvui.bsn.beans.customer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.transaction.Transactional;

import org.apache.commons.lang3.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import vn.anvui.bsn.beans.contract.ContractServiceImpl;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.crm.ContactAssignRequest;
import vn.anvui.bsn.dto.crm.UpdateTagRequest;
import vn.anvui.bsn.dto.customer.CoupleRequest;
import vn.anvui.bsn.dto.customer.CustomerRequest;
import vn.anvui.bsn.dto.user.UserContactStatus;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.ContactAssignment;
import vn.anvui.dt.entity.ContactLog;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.ContractNumber;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.entity.CustomerRejectNote;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.entity.RejectNote;
import vn.anvui.dt.entity.Tracking;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.ContactLogType;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.CustomerTag;
import vn.anvui.dt.enumeration.ProductType;
import vn.anvui.dt.enumeration.TransactionType;
import vn.anvui.dt.enumeration.UserType;
import vn.anvui.dt.model.ContactCrmInfo;
import vn.anvui.dt.model.ContactReport;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.contactAssignment.ContactAssignmentRepository;
import vn.anvui.dt.repos.contactLog.ContactLogRepository;
import vn.anvui.dt.repos.contract.ContractNumberRepository;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.customer.CustomerRepository;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.repos.rejectNote.CustomerRejectNoteRepository;
import vn.anvui.dt.repos.rejectNote.RejectNoteRepository;
import vn.anvui.dt.repos.tracking.TrackingRepository;
import vn.anvui.dt.repos.user.UserRepository;
import vn.anvui.dt.utils.DateTimeHelper;

@Service
public class CustomerServiceImpl implements CustomerService {
    final Logger log = Logger.getLogger(CustomerServiceImpl.class.getSimpleName());

    @Autowired
    CustomerRepository customerRepo;

    @Autowired
    ContractRepository contractRepo;

    @Autowired
    InsurancePackageRepository packageRepo;

    @Autowired
    TrackingRepository trackingRepo;

    @Autowired
    UserRepository userRepo;

    @Autowired
    ContactAssignmentRepository assignRepo;

    @Autowired
    ContractNumberRepository noRepo;
    
    @Autowired
    RejectNoteRepository noteRepo;
    
    @Autowired
    CustomerRejectNoteRepository crNoteRepo;
    
    @Autowired ContactLogRepository logRepo;

    @Override
    public Customer create(CustomerRequest customerReq) throws AnvuiBaseException {
        List<Integer> tagList = new ArrayList<>();
        tagList.add(CustomerTag.NOT_PROCESS.getValue());
        tagList.add(CustomerTag.PENDING.getValue());
        tagList.add(CustomerTag.PRE_COD.getValue());
        Customer customer = customerRepo.findContractCreatedContactByPhoneNumberAndTag(tagList,
                customerReq.getPhoneNumber());

        if (customer != null) {
            log.warning("reject contact");
            ContactLog ctsLog = new ContactLog(customer, null, null, ContactLogType.CREATED, null);
            logRepo.save(ctsLog);
            return customer;
        }
        
        customer = customerRepo.findFirstByPhoneNumberAndTagInAndContractIdIsNull(customerReq.getPhoneNumber(),
                tagList);

        if (customer == null) {
            customer = new Customer();
        }

        if (!StringUtils.isEmpty(customerReq.getCitizenId()))
            customer.setCitizenId(customerReq.getCitizenId());

        if (!StringUtils.isEmpty(customerReq.getCustomerName()))
            customer.setCustomerName(WordUtils.capitalizeFully(customerReq.getCustomerName()));

        if (customerReq.getGender() != null)
            customer.setGender(customerReq.getGender());

        if (customerReq.getDob() != null)
            customer.setDob(new Date(customerReq.getDob()));

        if (!StringUtils.isEmpty(customerReq.getPhoneNumber()))
            customer.setPhoneNumber(customerReq.getPhoneNumber());

        if (!StringUtils.isEmpty(customerReq.getEmail()))
            customer.setEmail(customerReq.getEmail());
        
        if (!StringUtils.isEmpty(customerReq.getNote()))
            customer.setNote(customerReq.getNote());

        customer.setCreatedDate(new Date(System.currentTimeMillis()));
        customer = customerRepo.save(customer);
        
        ContactLog ctsLog = new ContactLog(customer, null, null, ContactLogType.CREATED, null);
        logRepo.save(ctsLog);

        return customer;
    }

    @Override
    public Customer updateInfo(Integer id, CustomerRequest req) {
        Customer customer = customerRepo.findOne(id);

        if (customer == null) {
            return null;
        }
        if (!StringUtils.isEmpty(req.getCitizenId()))
            customer.setCitizenId(req.getCitizenId());

        if (!StringUtils.isEmpty(req.getCustomerName()))
            customer.setCustomerName(req.getCustomerName());

        if (req.getGender() != null)
            customer.setGender(req.getGender());

        if (req.getDob() != null)
            customer.setDob(new Date(req.getDob()));

        if (!StringUtils.isEmpty(req.getPhoneNumber()))
            customer.setPhoneNumber(req.getPhoneNumber());

        customer = customerRepo.save(customer);

        return customer;
    }

    @Override
    public Customer get(Integer id) throws AnvuiBaseException {
        return customerRepo.findOne(id);
    }

    @Override
    public boolean contractIsExisted(Integer idContract, String citizenId, String partnerCitizenId) {
        log.info("check contract existed " + citizenId);
        List<CustomerRepository.ContractIdOnly> contractIds = customerRepo.findAllByCitizenId(citizenId);
        List<Integer> contractIdsList = new ArrayList<>();
        String idsStr = "";
        log.info(contractIds.toString());
        for (CustomerRepository.ContractIdOnly contractId : contractIds) {
            idsStr += contractId + ",";
            
            if (contractId != null)
                contractIdsList.add(contractId.getContractId());
        }
        log.info(String.valueOf(idsStr));

        String contractStr = "";
        List<CustomerRepository.CitizenAndContractIdOnly> citizenIds = customerRepo
                .findAllByContractIdIn(contractIdsList);
        List<Integer> contractIdsExisted = new ArrayList<>();
        for (CustomerRepository.CitizenAndContractIdOnly id : citizenIds) {
            if (id.getCitizenId().equals(partnerCitizenId)) {
                contractIdsExisted.add(id.getContractId());
            }
        }
        log.info(String.valueOf(contractStr));
        if (!contractIdsExisted.isEmpty()) {
            List<Integer> listStatus = new ArrayList<>();
            listStatus.add(ContractStatus.PAID.getValue());
            listStatus.add(ContractStatus.COD.getValue());
            listStatus.add(ContractStatus.AGENCY_PAID.getValue());
            listStatus.add(ContractStatus.AGENCY_COD_DONE.getValue());
            listStatus.add(ContractStatus.AGENCY_PAID.getValue());
            listStatus.add(ContractStatus.AGENCY_TRANSFER_DONE.getValue());
            listStatus.add(ContractStatus.CUSTOMER_PAID.getValue());

            ContractRepository.IdOnly idOnly = contractRepo.findFirstByIdInAndContractStatusIn(contractIdsExisted,
                    listStatus);
            if (idOnly != null) {
                if (idContract != null) {
                    if (idOnly.getId().equals(idContract))
                        return false;
                }
                log.info("existed contract: " + idOnly.getId());
                return true;
            }
        }
        return false;
    }

    @Transactional
    @Override
    public ContractWrapper createCouple(CoupleRequest req, ContractStatus status) throws AnvuiBaseException {
        log.info("start service");
        
        User user = null;
        try {
            AuthenticationDetails userDetail = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            user = userDetail.getAuthenticatedUser();

        } catch (Exception e) {
            log.warning("authentication exception");
        }
        log.info("got authentication details");
        
        validateCoupleRequest(req);
        
        Customer partner = new Customer();
        Customer customer = new Customer();
        Contract contract = null;
        if (status.equals(ContractStatus.CREATED)) {
            log.info("created new");
            List<Integer> tagList = new ArrayList<>(3);
            tagList.add(CustomerTag.NOT_PROCESS.getValue());
            tagList.add(CustomerTag.PENDING.getValue());
            tagList.add(CustomerTag.PRE_COD.getValue());
            customer = customerRepo.findFirstByPhoneNumberAndTagInAndContractIdIsNull(req.getPhoneNumber(),
                    tagList);

            log.info("checked non contract customers");
            if (customer == null) {
                
                customer = customerRepo.findContractCreatedContactByPhoneNumberAndTag(tagList, 
                        req.getPhoneNumber());
                log.info("checked contract customers");
                
                if (customer == null) {
                    customer = new Customer();
                } else {
                    contract = contractRepo.findOne(customer.getContractId());
                    if (contract.getContractStatus().equals(ContractStatus.PAID.getValue())) {
                        throw new AnvuiBaseException(ErrorInfo.UNABLE_TO_SAVE_DATA);
                    }
                    
                    partner = contract.getPartner();
                    log.info("existed partner id: " + partner.getId());
                }
            }
            log.info("checked existed");
        }
        
        partner.setCustomerName(WordUtils.capitalizeFully(req.getPartnerName()));
        partner.setCitizenId(req.getPartnerCitizenId());
        partner.setDob(new Date(req.getPartnerDob()));
        partner.setPhoneNumber(req.getPhoneNumber());
        partner.setEmail(req.getEmail());
        partner.setGender(req.getPartnerGender());

        log.info("prepare to save partner");
        try {
            partner = customerRepo.save(partner);
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.UNABLE_TO_SAVE_DATA);
        }

        if (!StringUtils.isEmpty(req.getNote()))
            customer.setNote(req.getNote());
            
        customer.setCustomerName(WordUtils.capitalizeFully(req.getCustomerName()));
        customer.setCitizenId(req.getCitizenId());
        customer.setDob(new Date(req.getDob()));
        customer.setPhoneNumber(req.getPhoneNumber());
        customer.setEmail(req.getEmail());
        customer.setGender(req.getGender());
        customer.setAddress(req.getAddress());
        
        if (status.equals(ContractStatus.COD))
            customer.setTag(CustomerTag.COD.getValue());
        
        if (user != null) {
            if (user.getUserType().equals(UserType.PTI_USER.getValue()))
                customer.setAssignedUserId(user.getId());
        }

        log.info("prepare to save customer");
        try {
            customer = customerRepo.save(customer);
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.UNABLE_TO_SAVE_DATA);
        }

        log.info("prepare to get package");
        InsurancePackage pack;
        try {
            pack = packageRepo.findOne(req.getPackageId());
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.PACKAGE_INVALID);
        }
        
        Date startDate = checkStartDate(req.getStartDate());

        return createContract(contract, customer, partner, pack, status, user, startDate);
    }
    
    protected ContractWrapper createContractFromConsultCustomer(Customer customer, Customer partner,
            InsurancePackage pack, User user, Date startDate) throws AnvuiBaseException {
        Contract contract = new Contract();
        
        if (user != null)
            contract.setCreatedUserId(user.getId());

        contract.setPackageId(pack.getId());
        contract.setFee(pack.getFee());
        contract.setIncome(contract.getFee() * (1 - contract.getCommission()));
        contract.setProductId(ProductType.LOVE.getValue());

        contract.setContractStatus(ContractStatus.CREATED.getValue());

        contract.setInheritCustomer(customer);
        contract.setPartner(partner);

        log.info("id: " + contract.getId());
        String code = "PTI";
        if (user != null) {
            if ((!StringUtils.isEmpty(user.getAgencyCode())))
                code = user.getAgencyCode();
        }

        customer.setTag(CustomerTag.PRE_COD.getValue());
        partner.setTag(CustomerTag.PRE_COD.getValue());

        contract.setContractCode(code);

        contract = contractRepo.save(contract);

        Date currentDate = new Date();
        contract.setCreatedTime(currentDate);
        
        if (startDate != null)
            currentDate = startDate;
            
        ContractServiceImpl.setDateForContract(contract, currentDate);

        customer.setContractId(contract.getId());

        partner.setContractId(contract.getId());

        try {
            customer = customerRepo.save(customer);
            partner = customerRepo.save(partner);
            contract = contractRepo.save(contract);
            
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.UNABLE_TO_SAVE_DATA);
        }

        ContactLog ctsLog = new ContactLog(customer, partner, contract, ContactLogType.UPDATED, user);
        logRepo.save(ctsLog);
        ContractWrapper res = new ContractWrapper(contract);

        res.setInsurancePackage(pack);

        return res;
    }

    public ContractWrapper createContract(Contract contract, Customer customer, Customer partner,
            InsurancePackage pack, ContractStatus status, User user, Date startDate) throws AnvuiBaseException {
        log.info("start creating contract");
        if (contract == null)
            contract = new Contract();
        
        if (user != null) {
            contract.setCreatedUserId(user.getId());
            if (user.getCommission() != null)
                contract.setCommission(user.getCommission());
        }
        
        contract.setProductId(ProductType.LOVE.getValue());
        contract.setFee(pack.getFee());
        contract.setIncome(contract.getFee() * (1 - contract.getCommission()));

        contract.setPackageId(pack.getId());

        contract.setContractStatus(status.getValue());

        contract.setInheritCustomer(customer);
        contract.setPartner(partner);

        log.info("id: " + contract.getId());
        String code = "PTI";
        if (user != null) {
            if ((!StringUtils.isEmpty(user.getAgencyCode())))
                code = user.getAgencyCode();
        }
        log.info("create contractCode");
        code = createContractCode(code);

        ContactLogType logType = ContactLogType.CREATED;
        if (status.equals(ContractStatus.COD) || status.equals(ContractStatus.AGENCY_COD_CREATED)
                || status.equals(ContractStatus.AGENCY_TRANSFER_CREATED)) {
            customer.setTag(CustomerTag.COD.getValue());
            partner.setTag(CustomerTag.COD.getValue());
            logType = ContactLogType.COD;
        }

        contract.setContractCode(code);

        log.info("get contract id");
        contract = contractRepo.save(contract);

        log.info("calculating date");
        Date currentDate = new Date();
        contract.setCreatedTime(currentDate);
        
        if (startDate != null)
            currentDate = startDate;
            
        ContractServiceImpl.setDateForContract(contract, currentDate);

        customer.setContractId(contract.getId());
        
        partner.setContractId(contract.getId());

        log.info("save contract");
        try {
            customer = customerRepo.save(customer);
            partner = customerRepo.save(partner);
            contract = contractRepo.save(contract);
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.UNABLE_TO_SAVE_DATA);
        }
        
        log.info("creating contact log");
        ContactLog ctsLog = new ContactLog(customer, partner, contract, logType, user);
        logRepo.save(ctsLog);
        
        log.info("return result");
        ContractWrapper res = new ContractWrapper(contract);
        
        res.setInsurancePackage(pack);
        return res;
    }

    protected ContractWrapper updateContractForExistingCustomer(Contract contract, Customer customer, Customer partner,
            InsurancePackage pack, User user, Date startDate) throws AnvuiBaseException {
        if (!contract.getContractStatus().equals(ContractStatus.CREATED.getValue())) {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_EXISTED);
        }

        Date currentDate = new Date();
        contract.setCreatedTime(currentDate);
        
        if (startDate != null)
            currentDate = startDate;
            
        ContractServiceImpl.setDateForContract(contract, currentDate);
        
        contract.setContractStatus(ContractStatus.COD.getValue());
        if (user != null)
            contract.setCreatedUserId(user.getId());
        
        customer.setTag(CustomerTag.COD.getValue());
        partner.setTag(CustomerTag.COD.getValue());
        
        String code = "PTI";
        code = createContractCode(code);
        contract.setContractCode(code);
        contract.setPackageId(pack.getId());
        contract.setFee(pack.getFee());
        contract.setIncome(contract.getFee() * (1 - contract.getCommission()));
        contract.setProductId(ProductType.LOVE.getValue());
        customer.setContractId(contract.getId());

        partner.setContractId(contract.getId());

        try {
            customer = customerRepo.save(customer);
            partner = customerRepo.save(partner);
            contract = contractRepo.save(contract);
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.UNABLE_TO_SAVE_DATA);
        }
        
        ContactLog ctsLog = new ContactLog(customer, partner, contract, ContactLogType.COD, user);
        logRepo.save(ctsLog);

        ContractWrapper res = new ContractWrapper(contract);

        res.setInsurancePackage(pack);

        return res;
    };

    @Override
    public List<Customer> getPotentialCustomerInfo(Integer page, Integer size, String customerName, String email,
            String phoneNumber, Long fromDate, Long toDate, Integer tag, boolean onContract) throws AnvuiBaseException {
        List<Customer> rsList;

        Date dateTo = null, dateFrom = null;
        if (toDate != null)
            dateTo = new Date(toDate);
        if (fromDate != null)
            dateFrom = new Date(fromDate);

        Integer userId = null;
        User user = null;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
        } catch (Exception e) {

        }
        if (user != null) {
            if (user.getUserType().equals(UserType.PTI_USER.getValue()))
                userId = user.getId();
        }

        Integer offset = null;
        if (size != null)
            offset = page * size;
        
        if (!onContract) {
            try {
                rsList = customerRepo.getPotentialCustomer(size, offset, phoneNumber, email, customerName,
                        dateFrom, dateTo, tag, userId);
            } catch (Exception e) {
                log.log(Level.SEVERE, e.getMessage(), e);
                throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
            }
        } else {
            try {
                rsList = customerRepo.getPotentialCustomerOnContract(size, offset, phoneNumber, email,
                        customerName, dateFrom, dateTo, tag, userId);
            } catch (Exception e) {
                log.log(Level.SEVERE, e.getMessage(), e);
                throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
            }
        }

        return rsList;
    }

    @Override
    public Customer updateCustomerTag(UpdateTagRequest req, Integer customerId) throws AnvuiBaseException {
        Customer customer = customerRepo.findOne(customerId);
        
        
        User user = null;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
        } catch (Exception e) {
            log.warning("anonymous");
        }
        
        if (customer == null)
            throw new AnvuiBaseException(ErrorInfo.CUSTOMER_NULL);

        if (!StringUtils.isEmpty(req.getNote()))
            customer.setNote(req.getNote());
        
        Integer tag = req.getTag();
        if (customer.getContractId() != null) {
            Integer status = contractRepo.findContractStatusById(customer.getContractId());
            log.info("contract status: " + status);
            if (status == ContractStatus.COD.getValue() || status == ContractStatus.PAID.getValue()) {
                    tag = customer.getTag();
            } else if (req.getTag().equals(CustomerTag.APPROVE.getValue())) {
                tag = customer.getTag();
            }
        }
        
        
        customer.setTag(tag);
        
        if (customer.getTag().equals(CustomerTag.DONE.getValue())) {
            if (req.getNoteId() != null) {
                RejectNote rNote = noteRepo.findOne(req.getNoteId());
                
                CustomerRejectNote crNote = new CustomerRejectNote();
                crNote.setCustomerId(customerId);
                crNote.setNote(rNote);
                crNote.setCreatedTime(new Date());
                
                crNoteRepo.save(crNote);
            }
            
            ContactLogType ctsLogType;
            switch (customer.getTag()) {
            case 2:
                ctsLogType = ContactLogType.REJECTED;
                break;
                
            case 3:
                ctsLogType = ContactLogType.COD;
                break;
                
            case 5:
                ctsLogType = ContactLogType.ONLINE;
                break;
                
            default:
                ctsLogType = ContactLogType.UPDATED;
                break;
            }
            
            ContactLog ctsLog = new ContactLog(customer, null, null, ctsLogType, user);
            logRepo.save(ctsLog);
        }
        
        
        customerRepo.save(customer);

        return customerRepo.save(customer);
    }

    @Transactional
    @Override
    public ContractWrapper createContractFromExistedCustomer(CoupleRequest req, Integer customerId)
            throws AnvuiBaseException {

        User user = null;

        try {
            AuthenticationDetails userDetail = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            user = userDetail.getAuthenticatedUser();

        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        Customer customer = customerRepo.findOne(customerId);
        if (customer == null)
            throw new AnvuiBaseException(ErrorInfo.CUSTOMER_NULL);

        if (contractIsExisted(null, req.getCitizenId(), req.getPartnerCitizenId()))
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_EXISTED);

        if (StringUtils.isEmpty(req.getCustomerName()) || StringUtils.isEmpty(req.getPartnerName()))
            throw new AnvuiBaseException(ErrorInfo.CUSTOMER_NAME_EMPTY);

        if (StringUtils.isEmpty(req.getDob()) || StringUtils.isEmpty(req.getPartnerDob()))
            throw new AnvuiBaseException(new Exception("date of birth empty"));

        if (StringUtils.isEmpty(req.getCitizenId()) || StringUtils.isEmpty(req.getPartnerCitizenId()))
            throw new AnvuiBaseException(ErrorInfo.CUSTOMER_CITIZEN_ID_EMPTY);

        if (req.getGender() == null || req.getPartnerGender() == null)
            throw new AnvuiBaseException(ErrorInfo.CUSTOMER_GENDER_MISSIING);

        if (req.getGender().equals(req.getPartnerGender()))
            throw new AnvuiBaseException(ErrorInfo.CUSTOMER_SAME_GENDER);
        
        customer.setCustomerName(req.getCustomerName());
        customer.setCitizenId(req.getCitizenId());
        customer.setDob(new Date(req.getDob()));
        customer.setGender(req.getGender());
        customer.setAddress(req.getAddress());
        
        if (!StringUtils.isEmpty(req.getEmail()))
            customer.setEmail(req.getEmail());
        
        Tracking track = trackingRepo.findOneByCustomerId(customer.getId());
        if (track == null)
            track = new Tracking();
        track.setCreatedDate(new Date());
        
        if (customer.getContractId() == null) {

            Customer partner = new Customer();
            partner.setCustomerName(req.getPartnerName());
            partner.setCitizenId(req.getPartnerCitizenId());
            partner.setDob(new Date(req.getPartnerDob()));
            partner.setPhoneNumber(customer.getPhoneNumber());
            partner.setEmail(customer.getEmail());
            partner.setGender(req.getPartnerGender());

            try {
                partner = customerRepo.save(partner);
            } catch (Exception e) {
                log.log(Level.WARNING, e.getMessage(), e);
                throw new AnvuiBaseException(ErrorInfo.UNABLE_TO_SAVE_DATA);
            }

            InsurancePackage pack;
            try {
                pack = packageRepo.findOne(req.getPackageId());
            } catch (Exception e) {
                log.log(Level.WARNING, e.getMessage(), e);
                throw new AnvuiBaseException(ErrorInfo.PACKAGE_INVALID);
            }
            
            Date startDate = checkStartDate(req.getStartDate());

            ContractWrapper res = createContractFromConsultCustomer(customer, partner, pack, user, startDate);
            track.setContractId(res.getContract().getId());
            trackingRepo.save(track);

            return res;
        } else {
            Contract contract = contractRepo.findOne(customer.getContractId());

            Customer partner = contract.getPartner();
            partner.setCustomerName(req.getPartnerName());
            partner.setCitizenId(req.getPartnerCitizenId());
            partner.setDob(new Date(req.getPartnerDob()));
            partner.setGender(req.getPartnerGender());
            partner.setEmail(customer.getEmail());

            InsurancePackage pack;
            try {
                pack = packageRepo.findOne(req.getPackageId());
            } catch (Exception e) {
                log.log(Level.WARNING, e.getMessage(), e);
                throw new AnvuiBaseException(ErrorInfo.PACKAGE_INVALID);
            }
            
            trackingRepo.save(track);
            
            Date startDate = checkStartDate(req.getStartDate());

            return updateContractForExistingCustomer(contract, customer, partner, pack, user, startDate);
        }
    }

    @Override
    public int assignContactToUser(ContactAssignRequest req) throws AnvuiBaseException {
        User user = userRepo.findOne(req.getUserId());
        if (user == null)
            throw new AnvuiBaseException(ErrorInfo.USER_NOT_EXISTED);

        if (!user.getUserType().equals(UserType.PTI_USER.getValue()))
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);

        int size = 0;
        
        ContactAssignment assignment = new ContactAssignment();
        assignment.setCreatedTime(new Date());
        assignment.setUserId(user.getId());
        assignment.setNoContact(0);
        assignment = assignRepo.save(assignment);

        
        List<Customer> customers = customerRepo.findPotentialCustomer(req.getNoConsultContact());

        for (Customer customer : customers) {
            customer.setAssignedUserId(user.getId());
            customer.setAssignmentId(assignment.getId());
        }
        size += customers.size();
        customerRepo.save(customers);
        
        List<Customer> contractCustomers = customerRepo.findContractFilledCustomer(req.getNoRegisteredContact());

        for (Customer customer : contractCustomers) {
            customer.setAssignedUserId(user.getId());
            customer.setAssignmentId(assignment.getId());
        }
        size += contractCustomers.size();
        customerRepo.save(contractCustomers);
        
        assignment.setNoContact(size);

        log.info("number of contacts: " + assignment.getNoContact());
        assignRepo.save(assignment);

        return assignment.getNoContact();
    }

    @Override
    public ContactAssignRequest countTotalUnassignedContact() throws AnvuiBaseException {
        ContactAssignRequest res = new ContactAssignRequest();
        
        res.setNoConsultContact(customerRepo.countPotentialCustomer());
        
        res.setNoRegisteredContact(customerRepo.countContractFilledCustomer());
        
        return res;
    }

    public String createContractCode(String oldCode) {
        if (oldCode.length() > 15)
            return oldCode;

        ContractNumber num = noRepo.save(new ContractNumber());
        log.info("contract number: " + num.getNumber());
        
        Integer year = Calendar.getInstance(TimeZone.getTimeZone("GMT+7:00")).get(Calendar.YEAR);
        
        String code = oldCode + "%07d/HD/045-HN.01/PHH.TN.15.%d";
        code = String.format(code, num.getNumber(), year);
        log.info("code: " + code);

        return code;
    }

    @Override
    public List<UserContactStatus> getAssignedContactStatus(Integer userId, Long fromDate, Long toDate) throws AnvuiBaseException {
        User user = null;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
            if (user == null)
                    throw new Exception();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        List<User> userList = new ArrayList<>();
        List<UserContactStatus> rsList = new ArrayList<>();
        if (!user.getUserType().equals(UserType.ADMIN.getValue())) {
            userId = user.getId();
            userList.add(user);
        } else {
            List<String> userTypes = new ArrayList<>();
            userTypes.add(UserType.PTI_USER.getValue());
            userList = userRepo.getAllByUserTypeIn(userTypes);
        }
        User other = new User();
        other.setUserName("other");
        other.setFullName("other");
        userList.add(other);
        
        Date dateFrom = null, dateTo = null;
        if (fromDate != null && toDate != null) {
            dateFrom = new Date(fromDate);
            dateTo = new Date(toDate);
        } else {
            dateFrom = DateTimeHelper.getFirstTimeOfDay(new Date(), TimeZone.getTimeZone("GMT+7:00"));
            log.info(String.valueOf(dateFrom));
            dateTo = DateTimeHelper.addDay(dateFrom, DateTimeHelper.DAY_OF_YEAR, 1, 
                    TimeZone.getTimeZone("GMT+7:00"));
            log.info(String.valueOf(dateTo));
        }
        for (User userL : userList) {
            UserContactStatus status = new UserContactStatus();
            status.setUser(userL);
            
            List<ContactReport> repList;
            try {
                repList = customerRepo.getContactReportForSale(userL.getId(), dateFrom, dateTo);
            } catch (Exception e) {
                log.log(Level.SEVERE, e.getMessage(), e);
                throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
            }
            
            status.setContactStatus(repList);
            
            rsList.add(status);
        }
        
        return rsList;
    }

    @Transactional
    @Override
    public void deleteCustomers(Integer id) throws AnvuiBaseException {
        User user = null;
        try {
            AuthenticationDetails userDetail = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            user = userDetail.getAuthenticatedUser();

        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        
        Customer customer = customerRepo.findOne(id);
        ContactLog ctsLog = new ContactLog(customer, null, null, ContactLogType.DELETED, user);
        logRepo.save(ctsLog);
        if (customer == null)
            throw new AnvuiBaseException(ErrorInfo.CUSTOMER_NULL);
        
        if (customer.getContractId() != null) {
            Contract contract = contractRepo.findOne(customer.getContractId());
            log.info("id: " + contract.getId());

            contractRepo.delete(contract);
        } else {
            customerRepo.delete(customer);
        }
        
        
        List<Tracking> trackList = trackingRepo.findAllByCustomerId(customer.getId());
        if (!trackList.isEmpty())
            trackingRepo.delete(trackList);
        
    }
    
    public Date checkStartDate(Date startDate) throws AnvuiBaseException {
        if (startDate != null) {
            if (startDate.getTime() < (System.currentTimeMillis() - 86400000)) {
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_DATE_INVALID);
            } else {
                return startDate;
            }
        } else {
            return null;
        }
    }

    @Override
    public String exportCsv(Date fromDate, Date toDate, boolean notExportedOnly) throws AnvuiBaseException {
        List<ContactCrmInfo> info = customerRepo.getFullContactInformation(fromDate, toDate, notExportedOnly);
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = mapper.schemaFor(ContactCrmInfo.class).withHeader();
        
        try {
            return mapper.writer(schema).writeValueAsString(info);
        } catch (JsonProcessingException e) {
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ContractWrapper createAgencyContract(CoupleRequest req) throws AnvuiBaseException {
        log.info("start service");
        User user = null;
        try {
            AuthenticationDetails userDetail = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            user = userDetail.getAuthenticatedUser();

        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        log.info("got authentication details");
        
        validateCoupleRequest(req);
        
        Customer partner = new Customer();
        Customer customer = new Customer();
        Contract contract = null;
        
        ContractStatus status;
        if (req.getTxnType() == null || req.getTxnType().equals(TransactionType.ONLINE_TXN.getValue())) {
            status = ContractStatus.AGENCY_CREATED;
        } else if (req.getTxnType().equals(TransactionType.COD_TXN.getValue())) {
            if (!user.isCodAllowed()) {
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_COD_CREATION_NOT_ALLOWED);
            }
            status = ContractStatus.AGENCY_COD_CREATED;
        } else if (req.getTxnType().equals(TransactionType.ACCOUNT_TRANSFER.getValue())) {
            if (!user.isCodAllowed()) {
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_COD_CREATION_NOT_ALLOWED);
            }
            
            if (user.getIsGateway()) {
                status = ContractStatus.AGENCY_TRANSFER_DONE;
            } else {
                status = ContractStatus.AGENCY_TRANSFER;
            }
            
        } else {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_TRANSACTION_TYPE_INVALID);
        }
        
        partner.setCustomerName(WordUtils.capitalizeFully(req.getPartnerName()));
        partner.setCitizenId(req.getPartnerCitizenId());
        partner.setDob(new Date(req.getPartnerDob()));
        partner.setPhoneNumber(req.getPhoneNumber());
        partner.setEmail(req.getEmail());
        partner.setGender(req.getPartnerGender());
        
        customer.setCreatedDate(new Date());
        partner.setCreatedDate(new Date());
        
        log.info("prepare to save partner");
        try {
            partner = customerRepo.save(partner);
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.UNABLE_TO_SAVE_DATA);
        }

        if (!StringUtils.isEmpty(req.getNote()))
            customer.setNote(req.getNote());
            
        customer.setCustomerName(WordUtils.capitalizeFully(req.getCustomerName()));
        customer.setCitizenId(req.getCitizenId());
        customer.setDob(new Date(req.getDob()));
        customer.setPhoneNumber(req.getPhoneNumber());
        customer.setEmail(req.getEmail());
        customer.setGender(req.getGender());
        customer.setAddress(req.getAddress());
        
        if (user != null) {
            if (user.getUserType().equals(UserType.PTI_USER.getValue()))
                customer.setAssignedUserId(user.getId());
        }

        log.info("prepare to save customer");
        try {
            customer = customerRepo.save(customer);
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.UNABLE_TO_SAVE_DATA);
        }

        log.info("prepare to get package");
        InsurancePackage pack;
        try {
            pack = packageRepo.findOne(req.getPackageId());
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.PACKAGE_INVALID);
        }

        Date startDate = checkStartDate(req.getStartDate());

        return createContract(contract, customer, partner, pack, status, user, startDate);
    }
    
    private void validateCoupleRequest(CoupleRequest req) {
        if (contractIsExisted(null, req.getCitizenId(), req.getPartnerCitizenId()))
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_EXISTED);

        if (StringUtils.isEmpty(req.getCustomerName()) || StringUtils.isEmpty(req.getPartnerName()))
            throw new AnvuiBaseException(ErrorInfo.CUSTOMER_NAME_EMPTY);

        if (StringUtils.isEmpty(req.getDob()) || StringUtils.isEmpty(req.getPartnerDob()))
            throw new AnvuiBaseException(new Exception("date of birth empty"));
        
        if (!req.ageIsValid()) {
            throw new AnvuiBaseException(ErrorInfo.DOB_INVALID);
        }

        if (StringUtils.isEmpty(req.getEmail()))
            throw new AnvuiBaseException(ErrorInfo.CUSTOMER_EMAIL_EMPTY);

        if (StringUtils.isEmpty(req.getCitizenId()) || StringUtils.isEmpty(req.getPartnerCitizenId()))
            throw new AnvuiBaseException(ErrorInfo.CUSTOMER_CITIZEN_ID_EMPTY);

        if (req.getGender() == null || req.getPartnerGender() == null)
            throw new AnvuiBaseException(ErrorInfo.CUSTOMER_GENDER_MISSIING);

        if (req.getGender().equals(req.getPartnerGender()))
            throw new AnvuiBaseException(ErrorInfo.CUSTOMER_SAME_GENDER);
        
        log.info("process request");
    }
}
