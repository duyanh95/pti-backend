package vn.anvui.bsn.beans.rejectNote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.crm.RejectReport;
import vn.anvui.dt.entity.CustomerRejectNote;
import vn.anvui.dt.entity.RejectNote;
import vn.anvui.dt.repos.rejectNote.CustomerRejectNoteRepository;
import vn.anvui.dt.repos.rejectNote.RejectNoteRepository;

@Service
public class RejectNoteServiceImpl implements RejectNoteService {
    @Autowired
    RejectNoteRepository noteRepo;
    
    @Autowired
    CustomerRejectNoteRepository crNoteRepo;
    
    @Override
    public RejectNote create(String note) {
        RejectNote rNote = new RejectNote();
        rNote.setNote(note);
        
        return noteRepo.save(rNote);
    }

    @Override
    public List<RejectNote> get() throws AnvuiBaseException {
        return new ArrayList<RejectNote>(noteRepo.findAllByActive(true));
    }

    @Override
    public String getCustomerRejectNote(Integer id) throws AnvuiBaseException {
        CustomerRejectNote crNote = crNoteRepo.findOne(id);
        
        if (crNote == null)
            return null;
        
        return crNote.getNote().getNote();
    }

    @Override
    public RejectNote update(Integer id, String note) throws AnvuiBaseException {
        RejectNote rNote = noteRepo.findOne(id);
        if (rNote == null)
            throw new AnvuiBaseException(ErrorInfo.DATA_NOT_EXIST);
        
        rNote.setNote(note);
        
        return noteRepo.save(rNote);
    }

    @Override
    public void delete(Integer id) throws AnvuiBaseException {
        RejectNote rNote = noteRepo.findOne(id);
        if (rNote == null)
            throw new AnvuiBaseException(ErrorInfo.DATA_NOT_EXIST);
        
        rNote.setActive(false);
        
        noteRepo.save(rNote);
    }

    @Override
    public List<RejectReport> getRejectReport(Long fromDate, Long toDate) throws AnvuiBaseException {
        Set<RejectNote> noteSet = noteRepo.findAllByActive(true);
        
        Date dateTo = null, dateFrom = null;
        if (fromDate != null && toDate != null) {
            dateTo = new Date(toDate);
            dateFrom = new Date(fromDate);
        }
        
        
        List<RejectReport> rsList = new ArrayList<>();
        for (RejectNote note : noteSet) {
            RejectReport report = new RejectReport();
            report.setReason(note.getNote());
            Integer count;
            
            if (dateFrom != null && dateTo != null) {
                count = crNoteRepo.countByNoteAndCreatedTimeBetween(note, dateFrom, dateTo);
            } else {
                count = crNoteRepo.countByNote(note);
            }
            
            report.setCount(count);
            
            rsList.add(report);
        }
        
        return rsList;
    }

}
