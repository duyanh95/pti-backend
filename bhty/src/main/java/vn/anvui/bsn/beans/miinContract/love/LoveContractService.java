package vn.anvui.bsn.beans.miinContract.love;

import vn.anvui.bsn.beans.miinContract.MiinContractService;
import vn.anvui.bsn.dto.miinContract.LoveContractRequest;

public interface LoveContractService extends MiinContractService<LoveContractRequest> {

}
