package vn.anvui.bsn.beans.pdf;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import com.itextpdf.text.DocumentException;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.dt.model.ContractWrapper;

public interface PdfService {
    
    File loadPdfAsResource(ContractWrapper contract, boolean preview, boolean sealed) throws AnvuiBaseException;

    boolean AddTextToCertificate(String pdfDest, String contractCode, String createdTime, String createdDate, String startDate,
            String endDate, String waitDate, String maleName, String maleDob, String maleId, String femaleName,
            String femaleDob, String femaleId, boolean maleInherit, String fee, String insurance, boolean preview,
            boolean sealed) throws IOException, DocumentException, URISyntaxException;
    
    File convertPdfToImage(File pdfFile) throws IOException;
}
