package vn.anvui.bsn.beans.miinContract.bike;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;

import vn.anvui.bsn.beans.authentication.AuthenticationHelperInterface;
import vn.anvui.bsn.beans.miinContract.ContractAbstractService;
import vn.anvui.bsn.beans.product.ProductService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.miinContract.BikeContractRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.entity.contract.BikeInsuranceInfo;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.ProductType;
import vn.anvui.dt.enumeration.UserNotificationType;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.contract.bike.BikeInsuranceInfoRepository;
import vn.anvui.dt.repos.customer.CustomerRepository;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.utils.DateTimeHelper;
import vn.anvui.dt.utils.StringHelper;

@Service
public class BikeContractServiceImpl extends ContractAbstractService<BikeContractRequest>
        implements BikeContractService {
    static final Logger log = Logger.getLogger(BikeContractServiceImpl.class.getName());
    
    private static final Integer PRODUCT_ID = ProductType.BIKE.getValue();
    private static final String HTML = "Email/BHXM.html";
    
    @Autowired
    AuthenticationHelperInterface authHelper;
    
    @Autowired
    ContractRepository contractRepo;
    
    @Autowired
    CustomerRepository customerRepo;
    
    @Autowired
    InsurancePackageRepository pkgRepo;
    
    @Autowired
    BikeInsuranceInfoRepository infoRepo;
    
    @Autowired 
    ProductService prodService;

    @Override
    public Contract createCustomerContract(BikeContractRequest request) throws AnvuiBaseException {
        User user;
        try {
            user = authHelper.getUserFromAuthentication();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        if (contractExisted(request)) {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_EXISTED);
        }
        
        Customer customer = createCustomer(request);
        log.info("customerId: " + customer.getId());
        
        Contract contract = createContract(request, user, customer, ContractStatus.CUSTOMER_CREATED);
        
        BikeInsuranceInfo info = saveAdditionalInfo(request, contract);
        contract.setInfo(info);
        
        sendNotify(contract, UserNotificationType.CUSTOMER);
        
        return contract;
    }

    @Override
    public Contract createAgencyContract(BikeContractRequest request) throws AnvuiBaseException {
        User user;
        try {
            user = authHelper.getUserFromAuthentication();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        if (contractExisted(request)) {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_EXISTED);
        }
        
        Customer customer = createCustomer(request);
        log.info("customerId: " + customer.getId());
        
        ContractStatus status = getStatusFromTxnType(request.getTxnType(), user);
        Contract contract = createContract(request, user, customer, status);
        customer.setContractId(contract.getId());
        customerRepo.save(customer);
        
        BikeInsuranceInfo info = saveAdditionalInfo(request, contract);
        contract.setInfo(info);
        
        sendNotify(contract, UserNotificationType.CUSTOMER);
        
        return contract;
    }
        
    @Override
    protected Customer createCustomer(BikeContractRequest req) {
        Customer customer = new Customer();
        
        customer.setCustomerName(req.getCustomerName());
        customer.setPhoneNumber(req.getPhoneNumber());
        customer.setEmail(req.getEmail());
        
        return customerRepo.save(customer);
    }

    @Override
    @Transactional
    protected Contract createContract(BikeContractRequest req, User user, Customer customer, ContractStatus status) {
        Contract contract = new Contract();
        
        contract.setProductId(PRODUCT_ID);
        
        InsurancePackage pkg = pkgRepo.findOne(req.getPackageId());
        
        if (!packageIdIsValid(pkg, PRODUCT_ID)) {
            throw new AnvuiBaseException(ErrorInfo.PACKAGE_INVALID);
        }
        contract.setPackageId(pkg.getId());
        
        contract.setContractCode(createContractCode(user.getAgencyCode()));
        
        Date startDate = DateTimeHelper.addDay(new Date(), 
                DateTimeHelper.DAY_OF_YEAR, 1);
        
        if (req.getStartDate() != null) {
            if (req.getStartDate().getTime() > startDate.getTime())
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_DATE_INVALID);

            startDate = req.getStartDate();
        }
        
        contract.setStartDate(startDate);
        contract.setEndDate(DateTimeHelper.addDay(startDate, DateTimeHelper.YEAR, 1));
        
        contract.setCreatedUserId(user.getId());
        contract.setInheritCustomer(customer);
        
        contract.setContractStatus(status.getValue());
        
        contract = updateContractPricing(contract, pkg, user);
        
        contract = contractRepo.save(contract);
        customer.setContractId(contract.getId());
        
        customerRepo.save(customer);
        
        return contract;
    }

    @Override
    protected String createContractCode(String agencyCode) {
        Integer number = prodService.getProductContractNumber(PRODUCT_ID);
        
        if (!StringUtils.isEmpty(agencyCode)) {
            agencyCode = "-" + agencyCode;
        } else {
            agencyCode = "";
        }

        return String.format("MIIN-TNDS%s%011d", agencyCode, number);
    }
    
    protected BikeInsuranceInfo saveAdditionalInfo(BikeContractRequest req, Contract contract) {
        BikeInsuranceInfo info = new BikeInsuranceInfo();
        
        info.setId(contract.getId());
        info.setContract(contract);
        info.setRegisteredAddress(req.getRegisteredAddress());
        info.setRegisteredNumber(req.getRegisteredNumber());
        
        return infoRepo.save(info);
    }
    
    protected boolean contractExisted(BikeContractRequest req) {
        Integer count = infoRepo.checkContractExistedByRegNumAndDateAndStatus(
                req.getRegisteredNumber(), req.getStartDate(), ContractStatus.COMPLETED_SET);
        return count > 0;
    }

    @Override
    public void sendContractMail(ContractWrapper contract) throws AnvuiBaseException {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        
        ClassPathResource classPath = new ClassPathResource(HTML);
        
        String html;
        try {
            html = new String(FileCopyUtils.copyToByteArray(classPath.getInputStream()), "UTF-8");
        } catch (IOException e) {
            log.log(Level.WARNING, e.getMessage(), e);
            return;
        }
        
        Contract con = contract.getContract();
        
        if (!ContractStatus.COMPLETED_SET.contains(con.getContractStatus())) {
            log.warning("transaction failed, mail not sent");
            return;
        }
        
        BikeInsuranceInfo info = infoRepo.findOne(con.getId());
        
        html = StringHelper.replaceTemplate(html, "customerName", con.getInheritCustomer().getCustomerName());
        html = StringHelper.replaceTemplate(html, "contractCode", con.getContractCode());
        html = StringHelper.replaceTemplate(html, "startDate", format.format(con.getStartDate()));
        html = StringHelper.replaceTemplate(html, "endDate", format.format(con.getEndDate()));
        html = StringHelper.replaceTemplate(html, "address", info.getRegisteredAddress());
        
        if (!StringUtils.isEmpty(con.getInheritCustomer().getEmail())) {
            List<String> listTo = new ArrayList<>(Arrays.asList(con.getInheritCustomer().getEmail()));
           
            try {
                mailService.sendEmail(listTo, null, null, "Bảo hiểm trách nhiệm dân sự xe máy", html, null);
            } catch (Exception e) {
                log.log(Level.WARNING, e.getMessage(), e);
                throw new AnvuiBaseException(ErrorInfo.MAIL_SEND_FAIL);
            }
        } else {
            log.warning("no email address provided");
        }
    }

}
