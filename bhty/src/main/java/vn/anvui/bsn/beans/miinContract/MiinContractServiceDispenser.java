package vn.anvui.bsn.beans.miinContract;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.beans.miinContract.bike.BikeContractService;
import vn.anvui.bsn.beans.miinContract.health.HealthContractService;
import vn.anvui.bsn.beans.miinContract.love.LoveContractService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.dt.enumeration.ProductType;

@Service
public class MiinContractServiceDispenser {
    @Autowired
    LoveContractService loveService;
    
    @Autowired
    BikeContractService bikeService;
    
    @Autowired
    HealthContractService healthService;
    
    public MiinContractService<?> getServiceByProduct(ProductType type) {
        switch (type) {
        case LOVE:
            return loveService;
            
        case BIKE:
            return bikeService;
            
        case HEALTH:
            return healthService;
            
        default:
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);    
        }
    }
}
