package vn.anvui.bsn.beans.transaction;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.transaction.zalo.ZaloPayTransactionRequest;
import vn.anvui.bsn.dto.transaction.zalo.ZaloPayTransactionStatusRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.OnlineTransaction;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.PaymentType;
import vn.anvui.dt.enumeration.TransactionStatus;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.onlineTransaction.OnlineTransactionRepository;
import vn.anvui.dt.utils.EncryptionHelper;

@Service
@Qualifier("ZaloPayTransactionService")
public class ZaloPayTransactionService
        extends TransactionAbstractService
        implements TransactionService<ZaloPayTransactionRequest, ZaloPayTransactionStatusRequest> {
    static final Logger log = Logger.getLogger(ZaloPayTransactionService.class.getSimpleName());
    
    public static final Integer APP_ID = 418;
    public static final String MAC_KEY = "5ednTKiVJyLAHhXRkyOyw9FohzsNcqGb";
    public static final String CALLBACK_KEY = "5QpeHexNlXeUjatp8khtJcc0dl3DSgdV";
    public static final String embbeddata = "{\"promotioninfo\":\"\",\"merchantinfo\":\"bao hiem tinh yeu\"}";
    public static final String GATEWAY_API = "https://gateway.zalopay.vn/pay?order=";
    public static final String ORDER_API = "https://zalopay.com.vn/v001/tpe/createorder";
    public static final String STATUS_API = "https://zalopay.com.vn/v001/tpe/getstatusbyapptransid";
    
    @Autowired
    OnlineTransactionRepository txnRepo;
    
    @Autowired
    ObjectMapper mapper;

    @Override
    public String createTransaction(ContractWrapper contract, ZaloPayTransactionRequest req)
            throws AnvuiBaseException {
        OnlineTransaction txn = new OnlineTransaction();
        
        if (contract.getContract().getContractStatus().equals(ContractStatus.PAID.getValue())
                || contract.getContract().getContractStatus().equals(ContractStatus.AGENCY_PAID.getValue())) {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_COMPLETED);
        }
        
        long amount = getAmount(contract.getContract());
        
        Date currentDate = new Date();

        txn.setAmount(amount);
        txn.setContractId(contract.getContract().getId());
        txn.setStatus(TransactionStatus.PENDING.getValue());
        txn.setMerchantId(String.valueOf(APP_ID));
        txn.setPaymentType(PaymentType.ZALOPAY.getValue());
        
        txn = txnRepo.save(txn);
        
        DateFormat format = new SimpleDateFormat("yyMMdd");
        String apptransid = "%s-%08d";
        apptransid = String.format(apptransid, format.format(currentDate), txn.getId());
        
        List<Item> item = new LinkedList<>();
        item.add(new Item(contract.getContract().getId(), contract.getContract().getProductId()));
        
        String data;
        try {
            ZaloTransaction res = new ZaloTransaction();
            res.setAppid(APP_ID);
            res.setAppuser(contract.getContract().getInheritCustomer().getPhoneNumber());
            res.setApptime(currentDate.getTime());
            res.setAmount(amount);
            res.setApptransid(apptransid);
            res.setEmbeddata(embbeddata);
            res.setItem(mapper.writeValueAsString(item));
            
            String hmacinput = res.getAppid() + "|"
                    + res.getApptransid() + "|"
                    + res.getAppuser() + "|"
                    + res.getAmount() + "|"
                    + res.getApptime() + "|"
                    + res.getEmbeddata() + "|"
                    + mapper.writeValueAsString(res.getItem());
            log.info(hmacinput);
            
            res.setMac(EncryptionHelper.hmacSHA256encode(hmacinput, MAC_KEY));
        
            String formData = "appid=" + res.getAppid() + "&apptransid=" + res.getApptransid()
                    + "&appuser=" + res.getAppuser() + "&amount=" + res.getAmount()
                    + "&apptime=" + res.getApptime() + "&embeddata=" + res.getEmbeddata()
                    + "&item=" + mapper.writeValueAsString(res.getItem())
                    + "&mac=" + res.getMac();
            log.info(formData);
            
            byte[] postData = formData.getBytes(StandardCharsets.UTF_8);
            int postDataLength = postData.length;
            String request = ORDER_API;
            URL url = new URL(request);
            HttpURLConnection conn= (HttpURLConnection) url.openConnection();         
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            conn.setUseCaches(false);
            try(DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
               wr.write(postData);
            }
            
            ZaloTransactionToken token = mapper.readValue(conn.getInputStream(), ZaloTransactionToken.class);
            token.setAppid(String.valueOf(APP_ID));
            
            txn.setPaymentId(apptransid);
            txn = txnRepo.save(txn);
            
            log.info("txnId: " + txn.getId());
            
            data = mapper.writeValueAsString(token);
            log.info(data);
            
            data = new String(Base64.getEncoder().encodeToString(data.getBytes()));
            
            data = URLEncoder.encode(data, StandardCharsets.UTF_8.toString());
        } catch (Exception e) {
            txnRepo.delete(txn);
            log.log(Level.SEVERE, e.getMessage(), e);
            
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
        
        return GATEWAY_API + data;
    }

    @Override
    public ContractWrapper updateTransactionStatus(ZaloPayTransactionStatusRequest req) throws AnvuiBaseException {
        try {
            ZaloTransaction request = mapper.readValue(req.getData(), ZaloTransaction.class);
        
            OnlineTransaction txn = txnRepo.findFirstByPaymentId(request.getApptransid());
            if (txn == null) {
                log.info("txn not existed");
                throw new AnvuiBaseException(ErrorInfo.TRANSACTION_NULL);
            }
            
            if (txn.getStatus().equals(TransactionStatus.SUCCESS.getValue()) 
                    || txn.getStatus().equals(TransactionStatus.FAIL.getValue())) {
                throw new AnvuiBaseException(ErrorInfo.TRANSACTION_ALREADY_COMPLETE);
            }
            
            Contract contract = contractRepo.findOne(txn.getContractId());
            if (contract == null) {
                log.info("contract not existed");
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_NOT_EXISTED);
            }
            
            if (!req.getMac().equals(EncryptionHelper.hmacSHA256encode(req.getData(), CALLBACK_KEY))) {
                    throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);
            }
            
            String api = STATUS_API + "?appid=" + APP_ID + "&apptransid=" + request.getApptransid() + "&mac=" + 
                    EncryptionHelper.hmacSHA256encode(
                            APP_ID + "|" + request.getApptransid() + "|" + MAC_KEY, MAC_KEY);
            log.info("get txn status from:" + api);
            
//                URL url = new URL(api);
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();   
//                conn.setDoOutput(true);
//                conn.setRequestMethod("GET");
//        
//                ZaloPayStatusResponse resp = mapper.readValue(conn.getInputStream(), ZaloPayStatusResponse.class);
//                log.info(mapper.writeValueAsString(resp));
                
            txn.setStatus(TransactionStatus.SUCCESS.getValue());
            
            contract = updateContractStatus(contract);
            
            contractRepo.save(contract);
            
            ContractWrapper conRes = new ContractWrapper();
            conRes.setContract(contract);
            conRes.setTxn(txn);
            
            return conRes;
        } catch (IOException | InvalidKeyException | NoSuchAlgorithmException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.STATE_INVALID);
        }
    }
    
    public static class ZaloPayStatusResponse {
        Integer returncode;
        
        String returnmessage;
        
        Boolean isprocessing;
        
        Long amount;
        
        Long zptransid;
        
        Integer sub_error_code;

        public Integer getReturncode() {
            return returncode;
        }

        public void setReturncode(Integer returncode) {
            this.returncode = returncode;
        }

        public String getReturnmessage() {
            return returnmessage;
        }

        public void setReturnmessage(String returnmessage) {
            this.returnmessage = returnmessage;
        }

        public Boolean getIsprocessing() {
            return isprocessing;
        }

        public void setIsprocessing(Boolean isprocessing) {
            this.isprocessing = isprocessing;
        }

        public Long getAmount() {
            return amount;
        }

        public void setAmount(Long amount) {
            this.amount = amount;
        }

        public Long getZptransid() {
            return zptransid;
        }

        public void setZptransid(Long zptransid) {
            this.zptransid = zptransid;
        }

        public Integer getSub_error_code() {
            return sub_error_code;
        }

        public void setSub_error_code(Integer sub_error_code) {
            this.sub_error_code = sub_error_code;
        }
    }
    
    public static class Item {
        Integer contractId;
        
        Integer productId;
        
        public Item(Integer contractId, Integer productId) {
            super();
            this.contractId = contractId;
            this.productId = productId;
        }

        public Integer getContractId() {
            return contractId;
        }

        public void setContractId(Integer contractId) {
            this.contractId = contractId;
        }

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }
    }
    
    public static class ZaloTransaction {
        Integer appid;
        
        String apptransid;
        
        Long apptime;
        
        String appuser;
        
        Long amount;
        
        String embeddata;
        
        String item;
        
        Long zptransid;
        
        Long servertime;
        
        Integer channel;
        
        String merchanuserid;
        
        Long userfeeamount;
        
        Long discountamount;
        
        String bankcode;
        
        private String mac;

        public Integer getAppid() {
            return appid;
        }

        public void setAppid(Integer appid) {
            this.appid = appid;
        }

        public String getApptransid() {
            return apptransid;
        }

        public void setApptransid(String apptransid) {
            this.apptransid = apptransid;
        }

        public Long getApptime() {
            return apptime;
        }

        public void setApptime(Long apptime) {
            this.apptime = apptime;
        }

        public String getAppuser() {
            return appuser;
        }

        public void setAppuser(String appuser) {
            this.appuser = appuser;
        }

        public Long getAmount() {
            return amount;
        }

        public void setAmount(Long amount) {
            this.amount = amount;
        }

        public String getEmbeddata() {
            return embeddata;
        }

        public void setEmbeddata(String embeddata) {
            this.embeddata = embeddata;
        }

        public String getItem() {
            return item;
        }

        public void setItem(String item) {
            this.item = item;
        }

        public Long getZptransid() {
            return zptransid;
        }

        public void setZptransid(Long zptransid) {
            this.zptransid = zptransid;
        }

        public Long getServertime() {
            return servertime;
        }

        public void setServertime(Long servertime) {
            this.servertime = servertime;
        }

        public Integer getChannel() {
            return channel;
        }

        public void setChannel(Integer channel) {
            this.channel = channel;
        }

        public String getMerchanuserid() {
            return merchanuserid;
        }

        public void setMerchanuserid(String merchanuserid) {
            this.merchanuserid = merchanuserid;
        }

        public Long getUserfeeamount() {
            return userfeeamount;
        }

        public void setUserfeeamount(Long userfeeamount) {
            this.userfeeamount = userfeeamount;
        }

        public Long getDiscountamount() {
            return discountamount;
        }

        public void setDiscountamount(Long discountamount) {
            this.discountamount = discountamount;
        }

        public String getBankcode() {
            return bankcode;
        }

        public void setBankcode(String bankcode) {
            this.bankcode = bankcode;
        }

        public String getMac() {
            return mac;
        }

        public void setMac(String mac) {
            this.mac = mac;
        }
    }
    
    public static class ZaloTransactionToken {
        String zptranstoken;
        
        String appid;

        public String getZptranstoken() {
            return zptranstoken;
        }

        public void setZptranstoken(String zptranstoken) {
            this.zptranstoken = zptranstoken;
        }

        public String getAppid() {
            return appid;
        }

        public void setAppid(String appid) {
            this.appid = appid;
        }
    }
}
