package vn.anvui.bsn.beans.miinContract.general;

import java.util.List;

import vn.anvui.dt.entity.Contract;

public interface MiinGeneralContractService {

    List<Contract> listCustomerContract(String customerName, String citizenId, String email, String phoneNumber,
            Long fromDate, Long toDate, Integer page, Integer size, Integer productId, Boolean isCompleted);
    
    List<Contract> listAgencyContract(String customerName, String citizenId, String email, String phoneNumber,
            Long fromDate, Long toDate, Integer page, Integer size, Integer productId, Boolean isCompleted,
            Integer txnType, Integer agencyId);

    Contract updateEffectiveDate(Contract contract);
}
