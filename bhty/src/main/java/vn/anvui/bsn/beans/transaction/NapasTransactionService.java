package vn.anvui.bsn.beans.transaction;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.transaction.napas.NapasTransactionRequest;
import vn.anvui.bsn.dto.transaction.napas.NapasTransactionStatusRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.ContractNumber;
import vn.anvui.dt.entity.OnlineTransaction;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.CustomerTag;
import vn.anvui.dt.enumeration.PaymentType;
import vn.anvui.dt.enumeration.TransactionStatus;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.contract.ContractNumberRepository;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.repos.onlineTransaction.OnlineTransactionRepository;
import vn.anvui.dt.repos.user.UserRepository;
import vn.anvui.dt.utils.EncryptionHelper;

@Service
@Qualifier("NapasTransactionService")
public class NapasTransactionService extends TransactionAbstractService
        implements TransactionService<NapasTransactionRequest, NapasTransactionStatusRequest> {
    final Logger log = Logger.getLogger(NapasTransactionService.class.getSimpleName());
    
    static final String VERSION = "2.0";
    static final String ACCESS_CODE = "P1T2I4B5H6T7Y8";
    static final String MERCHANT = "PTIBHTY";
    static final String BACK_URL = "https://baohiemtinhyeu.vn/ketthuc.html?txnId=";
//    static final String RETURN_URL = "http%3A%2F%2Fbaohiemtinhyeu.vn";
    static final String RETURN_URL = "https://baohiemtinhyeu.vn:8443/pti/contract/transaction/napas/status";
//    static final String BACK_URL = "http%3A%2F%2Fbaohiemtinhyeu.vn%3A8081%2Fpti%2Fcontract%2Ftransaction%2Fnapas%2Fstatus";
    static final String CURRENCY = "VND";
    static final String HASH = "EF3E5831A656617BC30F3ACCC0A7CDA7";
    static final String NAPAS_URL = "https://payment.napas.com.vn/gateway/vpcpay.do?";
    
    @Autowired
    OnlineTransactionRepository txnRepo;
    
    @Autowired
    ContractRepository contractRepo;
    
    @Autowired
    InsurancePackageRepository pkgRepo;
    
    @Autowired
    UserRepository userRepo;
    
    @Autowired
    ContractNumberRepository noRepo;
    
    @Transactional
    @Override
    public ContractWrapper updateTransactionStatus(NapasTransactionStatusRequest req)
            throws AnvuiBaseException {
        try {
            req.setPaymentId(URLDecoder.decode(req.getPaymentId(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_NULL);
        }
        log.info("paymentId: " + req.getPaymentId());
        String idStr = req.getPaymentId().substring(3, req.getPaymentId().length() - 2);
        OnlineTransaction txn = txnRepo.findOne(Integer.parseInt(idStr));
        
        if (txn == null)
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_NULL);
        
        txn.setPaymentId(req.getPaymentId());
        
        String hash = EncryptionHelper.md5Encrypt(HASH + req.getParam());
        log.info("hash: " + hash);
        
        if (!req.getHash().equals(hash.toUpperCase()))
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);

        Contract contract = contractRepo.findOne(txn.getContractId());
        ContractWrapper res = new ContractWrapper();
        
        if (req.getResponseCode() == 0) {
            txn.setStatus(TransactionStatus.SUCCESS.getValue());
        
            contract = updateContractStatus(contract);
            
            contractRepo.save(contract);
            
        } else {
            txn.setStatus(TransactionStatus.FAIL.getValue());
        }
        
        res.setContract(contract);
        
        res.setInsurancePackage(pkgRepo.findOne(res.getContract().getPackageId()));
        
        txnRepo.save(txn);
        
        res.setTxn(txn);
        
        notify(contract, txn);
        
        return res;
    }

    @Override
    public String createTransaction(ContractWrapper contract, NapasTransactionRequest req) throws AnvuiBaseException {
        OnlineTransaction txn = new OnlineTransaction();
        
        if (contract.getContract().getContractStatus().equals(ContractStatus.PAID.getValue())
                || contract.getContract().getContractStatus().equals(ContractStatus.AGENCY_PAID.getValue())) {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_COMPLETED);
        }
        
        long amount = getAmount(contract.getContract());
        
        txn.setStatus(TransactionStatus.PENDING.getValue());
        txn.setContractId(contract.getContract().getId());
        txn.setMerchantId(MERCHANT);
        txn.setAmount(amount);
        txn.setPaymentType(PaymentType.NAPAS.getValue());
        
        txn = txnRepo.save(txn);
        
        txn.setPaymentId("PTI" + txn.getId() + "/1");
        
//        if (req.getPaymentGateway().equals("INT")) {
//            String[] international = {"Visa", "MasterCard", "Amex", "JCB"};
//            if (!Arrays.asList(international).contains(req.getCardType()))
//                throw new Exception("card type invalid");
//        } else if (req.getPaymentGateway().equals("ATM")) {
//            String[] atm = {"VCB", "TCB", "VIB", "ABB", "STB", "MSB", "NVB", "CTG", "DAB", "HDB",
//                    "VAB", "VPB", "ACB", "MB", "GPB", "EIB", "OJB", "NASB", "OCB", "TPB", "LPB",
//                    "SEAB", "BIDV", "VARB", "BVB", "SHB", "KLB", "SCB"};
//            if (!Arrays.asList(atm).contains(req.getCardType()))
//                log.warning("card type invalid");
//                throw new Exception("card type invalid");
//        } else {
//            log.warning("gateway invalid");
//            throw new Exception("gateway invalid");
//        }
        
        Map<String, String> paramSet = new TreeMap<>();
        paramSet.put("vpc_Version", VERSION);
        paramSet.put("vpc_Command", "pay");
        paramSet.put("vpc_AccessCode", ACCESS_CODE);
        paramSet.put("vpc_MerchTxnRef", txn.getPaymentId());
        paramSet.put("vpc_Merchant", MERCHANT);
        paramSet.put("vpc_OrderInfo", "PTI" + txn.getId());
        paramSet.put("vpc_Amount", txn.getAmount() + "00");
        paramSet.put("vpc_ReturnURL", RETURN_URL);
        paramSet.put("vpc_BackURL", BACK_URL + txn.getId());
        paramSet.put("vpc_Locale", "vn");
        paramSet.put("vpc_CurrencyCode", "VND");
        paramSet.put("vpc_TicketNo", req.getIp());
        
        Iterator<String> iter = paramSet.keySet().iterator();
        
        String pathParams = "", dataParams = "";
        while (iter.hasNext()) {
            String paramKey = iter.next();
            pathParams += paramKey + "=" + paramSet.get(paramKey)+ "&";
            dataParams += paramSet.get(paramKey);
        }
        

//        String pathParams = "vpc_Version=" + "2.1&"
//            + "vpc_Command=pay&"
//            + "vpc_AccessCode=" + ACCESS_CODE + "&"
//            + "vpc_MerchTxnRef=" + txn.getPaymentId() + "&"
//            + "vpc_Merchant=" + MERCHANT + "&"
//            + "vpc_OrderInfo=" + "PTI" + txn.getId() + "&"
//            + "vpc_Amount=" + txn.getAmount() + "00&"
//            + "vpc_ReturnURL=" + RETURN_URL + "&"
//            + "vpc_BackURL=" + BACK_URL + "&"
//            + "vpc_Locale=vn&"
//            + "vpc_CurrencyCode=VND&"
//            + "vpc_TicketNo=" + req.getIp();// + "&"
//            + "vpc_CardType=" + req.getCardType() + "&"
//            + "vpc_PaymentGateway=" + req.getPaymentGateway();
        
        
        String hash = EncryptionHelper.md5Encrypt(HASH + dataParams).toUpperCase();
        pathParams += "&vpc_SecureHash=" + hash;
        
        log.info("params: " + pathParams);
        
        return NAPAS_URL + pathParams;
    }
    
    public boolean checkTxnSuccess(Integer txnId) {
        OnlineTransaction txn = txnRepo.findOne(txnId);
        
        if (txn.getStatus().equals(TransactionStatus.SUCCESS.getValue()))
            return true;
        else
            return false;
    }
}
