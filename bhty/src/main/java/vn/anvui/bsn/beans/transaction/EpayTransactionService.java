package vn.anvui.bsn.beans.transaction;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.transaction.epay.EpayTransactionRequest;
import vn.anvui.bsn.dto.transaction.epay.EpayTransactionStatusRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.ContractNumber;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.entity.OnlineTransaction;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.CustomerTag;
import vn.anvui.dt.enumeration.PaymentType;
import vn.anvui.dt.enumeration.TransactionSource;
import vn.anvui.dt.enumeration.TransactionStatus;
import vn.anvui.dt.model.AnvuiResponse;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.model.ContractTransaction;
import vn.anvui.dt.repos.contract.ContractNumberRepository;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.repos.onlineTransaction.OnlineTransactionRepository;
import vn.anvui.dt.repos.user.UserRepository;

@Service
@Qualifier("EpayTransactionService")
public class EpayTransactionService extends TransactionAbstractService 
        implements TransactionService<EpayTransactionRequest, EpayTransactionStatusRequest> {
    public static final String MERCHANT_ID_ANVUI = "10004";
    public static final String HOST = "https://megav.vn/ec_payment/?ref=";
    public static final String PAYMENT_ID = "paymentId";
    public static final String TXN_AMOUNT = "txnAmount";
    public static final String TXN_TERMDATETIME = "txnTermDateTime";
    public static final String USERNAME = "username";
    public static final String PAYMENT_TYPE = "payment_type";
    public static final String PATMENT_DATA = "payment_data";
    public static final String MERCHANT_ID = "merchantId";
    public static final String REMARK = "remark";
    public static final String SIGNATURE = "signature";
    public static final String DATA = "data";
    public static final String STATUS = "status";
    public static final String YEN = "0902050545";
    public static final String MIIN = "0936999880";
    public static final String DOBODY_URL = "http://dobody-anvui.appspot.com/ePay/contract";
    public static final String MIIN_ANDROID_RETURN_URL = "intent://scan/#Intent;scheme=http;package=vn.anvui.miin;end";
    public static final String DEFAULT_RETURN_URL = "https://baohiemtinhyeu.vn";

    @Autowired
    OnlineTransactionRepository txnRepo;

    @Autowired
    ContractRepository contractRepo;
    
    @Autowired
    InsurancePackageRepository pkgRepo;
    
    @Autowired
    UserRepository userRepo;

    @Autowired
    ObjectMapper mapper;
    
    @Autowired
    ContractNumberRepository noRepo;

    Logger log = Logger.getLogger(EpayTransactionService.class.getName());

    @Override
    public String createTransaction(ContractWrapper contract, EpayTransactionRequest req) throws AnvuiBaseException {
        // String merchantId = MERCHANT_ID_ANVUI;
        // Date currentDate = new Date(System.currentTimeMillis());
        //
        // DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        // String txtTermDateTime = format.format(currentDate);
        //
        // String username = "";
        //
        // Long amount = contract.getInsurancePackage().getFee().longValue();
        //
        // format = new SimpleDateFormat("yyyyMMdd");
        //
        // String paymentId = merchantId + "_" + format.format(currentDate)
        // + "_" + buildTransactionNo(txnRepo.findFirstOrderByIdDesc().getId());
        //
        // String remark = "Thanh toan ma hop dong" +
        // contract.getContract().getContractCode();
        // log.log(Level.INFO, "remark " + remark);
        // org.json.JSONObject data = new org.json.JSONObject();
        // data.put(PAYMENT_ID, paymentId);
        // data.put(TXN_AMOUNT, amount);
        // data.put(TXN_TERMDATETIME, txtTermDateTime);
        // data.put(USERNAME, username);
        // data.put(REMARK, remark);
        // data.put(PAYMENT_TYPE, req.getPaymentType());
        //
        // List<MegaVReceiver> listMegaV = new ArrayList<MegaVReceiver>();
        //
        // listMegaV.add(new MegaVReceiver("100", YEN));
        //
        // ObjectMapper mapper = new ObjectMapper();
        //
        // data.put(PATMENT_DATA, mapper.writeValueAsString(listMegaV));
        // org.json.JSONObject json = new org.json.JSONObject();
        // json.put(MERCHANT_ID, merchantId);
        // json.put(SIGNATURE, RSA.sign(data.toString()));
        // json.put(DATA, data.toString());
        // // String dataString = GsonUltilities.toJson(data.toString());
        // // System.out.println(paymentId.length());
        // URL url = new URL(HOST + URLEncoder.encode(json.toString()));
        //
        // EpayTransaction txn = new EpayTransaction();
        //
        // txn.setMerchantId(MERCHANT_ID_ANVUI);
        // txn.setMoney(amount);
        // txn.setReq(url.toString());
        // // tObject.setPaymentId(paymentId);
        // txn.setPaymentId(paymentId);
        //
        // txnRepo.save(txn);
        
        if (contract.getContract().getContractStatus().equals(ContractStatus.PAID.getValue())
                || contract.getContract().getContractStatus().equals(ContractStatus.AGENCY_PAID.getValue())) {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_COMPLETED);
        }
        
        long amount = getAmount(contract.getContract());
        
        OnlineTransaction txn = new OnlineTransaction();
        txn.setContractId(req.getContractId());
        txn.setAmount(amount);
        txn.setPaymentType(PaymentType.EPAY.getValue());
        txn.setStatus(TransactionStatus.PENDING.getValue());
        txn.setSource(req.getSource().byteValue());
        
        List<TransactionSource> sources = new ArrayList<>(Arrays.asList(TransactionSource.values()));
        List<Integer> sourcesInt = sources.stream().map(TransactionSource::getValue)
                .collect(Collectors.toList());
        if (!sourcesInt.contains(req.getSource()))
            throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST);

        txn = txnRepo.save(txn);
        
        ContractTransaction contractTxn = new ContractTransaction(txn);
        switch (txn.getSource()) {
        case 0:
            contractTxn.setPhoneNumber(YEN);
        case 1:
            contractTxn.setPhoneNumber(MIIN);
        }
        
        if (req.getSource().equals(TransactionSource.MIIN_ANDROID.getValue())) {
            contractTxn.setReturnURL(MIIN_ANDROID_RETURN_URL);
        } if (!StringUtils.isEmpty(req.getReturnURL())) {
            contractTxn.setReturnURL(req.getReturnURL());
        } else {
            contractTxn.setReturnURL(DEFAULT_RETURN_URL);
        }

        mapper.setSerializationInclusion(Include.NON_NULL);
        log.info("create object");
        String json;
        try {
            json = mapper.writeValueAsString(contractTxn);
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST);
        }
        
        
        log.info(json);

        InputStream is;
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(DOBODY_URL).openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(json);
            writer.flush();
            writer.close();

            log.info("connecting");
            connection.connect();
            if (connection.getResponseCode() != 200) {
                throw new AnvuiBaseException(ErrorInfo.TRANSACTION_FAIL);
            }
            try {
                is = connection.getInputStream();
            } catch (FileNotFoundException e) {
                log.log(Level.WARNING, e.getMessage(), e);
                is = connection.getErrorStream();
            }
            log.info("get response");
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_FAIL);
        }

        AnvuiResponse anvui;
        
        try {
            anvui = mapper.readValue(is, AnvuiResponse.class);
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST);
        }

        txn.setMerchantId(anvui.getResults().getTransaction().getMerchantId());

        String URL = anvui.getResults().getRedirect();
        txnRepo.save(txn);

        log.info(URL);
        return URL;
    }

    @Transactional
    @Override
    public ContractWrapper updateTransactionStatus(EpayTransactionStatusRequest req)
            throws AnvuiBaseException {
        try {
            String paramString = req.getParamString();
            log.info(paramString);
            
            org.json.JSONObject json = new org.json.JSONObject(paramString);
            // System.out.println(json.getString("status"));
            org.json.JSONObject data = new org.json.JSONObject(json.getString("data"));
            // System.out.println(data);
            log.log(Level.WARNING, "paymentId " + data.getString("paymentId"));
            
            OnlineTransaction txn = txnRepo.findOne(Integer.parseInt(req.getId()));
            
            if (txn == null)
                throw new AnvuiBaseException(ErrorInfo.TRANSACTION_NULL);
            Contract contract = contractRepo.findOne(txn.getContractId());
            
            txn.setPaymentId(data.getString("paymentId"));
            if (!data.getString("status").equals("00")) {
                txn.setStatus(TransactionStatus.FAIL.getValue());
            } else {
                txn.setStatus(TransactionStatus.SUCCESS.getValue());
                Customer customer = contract.getInheritCustomer();
                Customer partner = contract.getPartner();
                customer.setTag(CustomerTag.ONLINE.getValue());
                partner.setTag(CustomerTag.ONLINE.getValue());
    
                DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
                try {
                    contract.setPaymentDate(format.parse(data.getString("serverTxnDatetime")));
                } catch (Exception e) {
                    throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST);
                }
                
                if (contract.getContractStatus().equals(ContractStatus.CREATED.getValue())) {
                    contract.setContractStatus(ContractStatus.PAID.getValue());
                } else if (contract.getContractStatus().equals(ContractStatus.AGENCY_CREATED.getValue())) {
                    contract.setContractStatus(ContractStatus.AGENCY_PAID.getValue());
                }
                
                if (contract.getContractCode().length() < 15) {
                    ContractNumber num = noRepo.save(new ContractNumber());
                    String contractCode = contract.getContractCode() + "%07d/HD/045-HN.01/PHH.TN.15.2018";
                    contractCode = String.format(contractCode, num.getNumber());
                    contract.setContractCode(contractCode);
                    log.info("code: " + contract.getContractCode().split("/")[0]);
                }
    
                contractRepo.save(contract);
            }
            txnRepo.save(txn);
    
            ContractWrapper conRes = new ContractWrapper(contract);
            conRes.setInsurancePackage(pkgRepo.findOne(contract.getPackageId()));
            conRes.setTxn(txn);
            
            notify(contract, txn);
            
            return conRes;
        } catch (JSONException e) {
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
    }
}
