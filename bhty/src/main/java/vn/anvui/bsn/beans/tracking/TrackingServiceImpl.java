package vn.anvui.bsn.beans.tracking;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.tracking.TrackingRequest;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.entity.Tracking;
import vn.anvui.dt.enumeration.TrackingType;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.model.TrackingRecord;
import vn.anvui.dt.repos.tracking.TrackingRepository;
import vn.anvui.dt.utils.DateTimeHelper;

@Service
public class TrackingServiceImpl implements TrackingService {
    final Logger log = Logger.getLogger(TrackingService.class.getSimpleName());

    public static final String ALL = "all";

    @Autowired
    TrackingRepository trackRepo;

    public boolean invalidRequest(TrackingRequest req) {
        return (StringUtils.isEmpty(req.getUtmAgent()) && StringUtils.isEmpty(req.getUtmTeam())
                && StringUtils.isEmpty(req.getUtmCampaign()) && StringUtils.isEmpty(req.getUtmMedium())
                && StringUtils.isEmpty(req.getUtmSource()) && StringUtils.isEmpty(req.getUtmChannel()));
    }

    @Override
    public Tracking createTracking(TrackingRequest req) throws AnvuiBaseException {
        Tracking track = new Tracking();

        track.setTeam(req.getUtmTeam());
        track.setAgent(req.getUtmAgent());
        track.setCampaign(req.getUtmCampaign());
        track.setMedium(req.getUtmMedium());
        track.setSource(req.getUtmSource());
        track.setChannel(req.getUtmChannel());
        track.setType(TrackingType.VIEW.getValue());

        track = trackRepo.save(track);

        return track;
    }

    @Override
    public Tracking updateContractTracking(Integer id, ContractWrapper contractRes) throws AnvuiBaseException {
        
        log.info("update tracking info");
        List<Tracking> existTrackList = trackRepo
                .findAllByCustomerId(contractRes.getContract().getInheritCustomer().getId());
        log.info("get existed trackings");
        for (Tracking existTrack : existTrackList) {
            existTrack.setCustomerId(null);
            existTrack.setContractId(null);
        }
        trackRepo.save(existTrackList);
        
        log.info("update tracking info");
        Tracking track;
        if (id == null) {
            track = new Tracking();
        } else {
            track = trackRepo.findById(id);
        }
        
        log.info("set tracking information");
        if (track == null)
            throw new AnvuiBaseException(ErrorInfo.EMPTY);
        
        if (track.getCustomerId() != null) {
            Tracking newTrack = new Tracking(track);
            track = newTrack;
        }

        track.setContractId(contractRes.getContract().getId());
        track.setCustomerId(contractRes.getContract().getInheritCustomer().getId());

        log.info("saving to db");
        trackRepo.save(track);

        return track;
    }

    @Override
    public List<TrackingRecord> getTrackingReport(Integer page, Integer count, String source, String agent,
            String medium, String campaign, String team, String channel, 
            Long fromDate, Long toDate, Boolean isTrafficReport) throws AnvuiBaseException {
        
        if (fromDate == null && toDate == null) {
            fromDate = DateTimeHelper.getFirstTimeOfDay(new Date(), TimeZone.getTimeZone("GMT+7:00")).getTime();
            
            toDate = fromDate + DateTimeHelper.DAY_IN_MS;
        }
        
        List<TrackingRecord> rsList = new ArrayList<>();

        Integer totalView = 0;
        Integer totalContract = 0;
        Double totalIncome = 0.0;
        Integer totalCustomer = 0;
        Double totalCodIncome = 0.0;
        Integer totalCodContract = 0;

        TrackingRecord all = new TrackingRecord();
        all.setAgent(ALL);
        all.setCampaign(ALL);
        all.setChannel(ALL);
        all.setMedium(ALL);
        all.setSource(ALL);
        all.setTeam(ALL);

        rsList.add(all);
        log.info("get report service");
        try {
            if (isTrafficReport) {
                rsList.addAll(trackRepo.getTrafficReport(page, count, source, 
                        agent, medium, campaign, team, channel, fromDate, toDate));
            } else {
                rsList.addAll(trackRepo.getContractReport(page, count, source, agent, medium, 
                        campaign, team, channel, fromDate, toDate));
            }
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }

        for (TrackingRecord record : rsList) {
            if (record.getCountView() != null)
                totalView += record.getCountView();
            if (record.getCountContract() != null)
                totalContract += record.getCountContract();
            if (record.getTotalIncome() != null)
                totalIncome += record.getTotalIncome();
            if (record.getTotalCustomer() != null)
                totalCustomer += record.getTotalCustomer();
            if (record.getCountCodContract() != null)
                totalCodContract += record.getCountCodContract();
            if (record.getCodIncome() != null)
                totalCodIncome += record.getCodIncome();
        }

        if (isTrafficReport) {
            all.setTotalCustomer(totalCustomer);
            all.setCountView(totalView);
        } else {
            all.setTotalIncome(totalIncome);
            all.setCountContract(totalContract);
            all.setCountCodContract(totalCodContract);
            all.setCodIncome(totalCodIncome);
        }

        return rsList;
    }

    @Override
    public Tracking updateCustomerTracking(Integer id, Customer customer) throws AnvuiBaseException {
        List<Tracking> existTrackList = trackRepo
                .findAllByCustomerId(customer.getId());
        for (Tracking existTrack : existTrackList) {
            existTrack.setCustomerId(null);
            existTrack.setContractId(null);
        }
        trackRepo.save(existTrackList);

        Tracking track;
        if (id == null) {
            track = new Tracking();
        } else {
            track = trackRepo.findById(id);
        }

        if (track == null) {
            log.warning("tracking not exists");
        }
        
        if (track.getCustomerId() != null) 
            track = new Tracking();

        track.setCustomerId(customer.getId());

        trackRepo.save(track);

        return track;
    }

    @Override
    public List<String> getUniqueValues(String field) throws AnvuiBaseException {
        List<String> rsList;
        try {
            rsList = trackRepo.getAggregateValueOnField(field);
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);

            throw new AnvuiBaseException(ErrorInfo.TRACKING_FIELD_NOT_EXISTED);
        }

        return rsList;
    }

}
