package vn.anvui.bsn.beans.rabbit.queues;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@PropertySource("classpath:${SYSTEM_ENVIRONMENT_VARIABLE}_rabbitmq.properties")
public class AnvuiQueue {

    private static final Logger logger = LoggerFactory.getLogger(AnvuiQueue.class);
    
    public static final String SMS_QUEUE = "dev_anvui_sms";
    public static final String MAIL_QUEUE = "dev_anvui_mail";
    public static final String CUSTOMER_QUEUE = "dev_anvui_customer";
    public static final String PNS_QUEUE = "dev_anvui_pns";

    @Autowired
    private RabbitAdmin rabbitAdmin;
    
    @Value("${rabbit.exchange.dev}")
    private String exchange;

    static Properties prop = new Properties();

    static {
        try {
            InputStream is = AnvuiQueue.class.getResourceAsStream(
                    String.format("/%s_rabbitmq.properties", System.getenv("SYSTEM_ENVIRONMENT_VARIABLE")));

            prop.load(is);

        } catch (Exception e) {
            logger.error("Unable to load properties file..", e);
        }
    }

    public static String[] getQueueNameList() {

        String[] queues = {};
        try {

            Set<String> queueNames = new HashSet<>();
            Set<Object> keys = prop.keySet();

            for (Object k : keys) {
                String key = (String) k;
                if (key.startsWith("rabbit.") && key.endsWith(".queue")
                        && !StringUtils.isEmpty(prop.getProperty(key))) {
                    queueNames.add(prop.getProperty(key));
                }
            }

            queues = queueNames.toArray(new String[queueNames.size()]);

        } catch (Exception e) {
            logger.error("Unable to load queue name list..", e);
        }

        return queues;
    }

    // public static <T> String getQueue(Class<T> clazz) {
    //
    // String clazzName = clazz.getSimpleName().replace("Queue", ".Queue");
    //
    // String key = String.format("rabbit.%s", clazzName.toLowerCase());
    //
    // return prop.getProperty(key);
    // }

    @PostConstruct
    public void init() {
        for (String key : AnvuiQueue.getQueueNameList()) {
            try {
                logger.warn("create queue", key);
                Queue queue = new Queue(key, false, false, true);

                TopicExchange topic = new TopicExchange(exchange);

                Binding binding = BindingBuilder.bind(queue).to(topic).with(key);

                rabbitAdmin.declareExchange(topic);
                rabbitAdmin.declareQueue(queue);
                rabbitAdmin.declareBinding(binding);

            } catch (Exception e) {
                logger.error("Unable to init {} queue. Caused by {}", key, e.getMessage());
            }
        }
    }

}
