package vn.anvui.bsn.beans.tracking;

import java.util.List;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.tracking.TrackingRequest;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.entity.Tracking;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.model.TrackingRecord;

public interface TrackingService {
    Tracking createTracking(TrackingRequest req) throws AnvuiBaseException;
    
    Tracking updateContractTracking(Integer id, ContractWrapper contractRes) throws AnvuiBaseException;
    Tracking updateCustomerTracking(Integer id, Customer customer) throws AnvuiBaseException;
    
    List<String> getUniqueValues(String field) throws AnvuiBaseException;

    List<TrackingRecord> getTrackingReport(Integer page, Integer count, String source, String agent, String medium,
            String campaign, String team, String channel, Long fromDate, Long toDate, Boolean isTrafficReport)
            throws AnvuiBaseException;
}
