package vn.anvui.bsn.beans.role;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.role.RoleRequest;
import vn.anvui.dt.entity.Role;

public interface RoleService {
    Role createRole(RoleRequest req) throws AnvuiBaseException;
}
