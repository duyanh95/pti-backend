package vn.anvui.bsn.beans.product;

import java.util.LinkedList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.anvui.dt.entity.Product;
import vn.anvui.dt.repos.product.ProductRepository;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepo;
    
    public List<Product> getProducts() {
        return new LinkedList<Product>(productRepo.findAllByOrderByOrder());
    }
    
    @Transactional
    public Integer getProductContractNumber(Integer id) {
        Product product = productRepo.findOne(id);
        
        Integer number = product.getContractNumber();
        product.setContractNumber(number + 1);
        productRepo.save(product);
        
        return number;
    }
}
