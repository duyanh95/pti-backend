package vn.anvui.bsn.beans.rejectNote;

import java.util.List;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.crm.RejectReport;
import vn.anvui.dt.entity.RejectNote;

public interface RejectNoteService {
    public RejectNote create(String note) throws AnvuiBaseException;
    
    public List<RejectNote> get() throws AnvuiBaseException;
    
    public String getCustomerRejectNote(Integer id) throws AnvuiBaseException;
    
    public RejectNote update(Integer id, String note) throws AnvuiBaseException;
    
    public void delete(Integer id) throws AnvuiBaseException;
    
    public List<RejectReport> getRejectReport(Long fromDate, Long toDate) throws AnvuiBaseException;
}
