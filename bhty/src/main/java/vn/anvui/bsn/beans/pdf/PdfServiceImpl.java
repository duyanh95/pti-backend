package vn.anvui.bsn.beans.pdf;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.enumeration.Gender;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.utils.StringHelper;

@Service
public class PdfServiceImpl implements PdfService {
    Logger log = Logger.getLogger(PdfService.class.getName());

    public final String CERT_PDF = "certificate.pdf";
    public final String NON_CERT_PDF = "non-cert.pdf";
    public final String FONT = "Myriad.ttf";
    
    private enum TextPosition {
        CONTRACT_CODE(165, 737.5f), CREATED_DATE(470, 737.5f), 
        START_DATE(291, 483), END_DATE(406, 483), 
        WAIT_DATE(291, 514f), END_WAIT_DATE(406, 514f), 
        MALE_DOB(215, 609), MALE_ID(215, 590), MALE_NAME(215, 628),
        FEMALE_DOB(407, 609), FEMALE_ID(407, 590), FEMALE_NAME(407, 628),
        FEE(270, 259f), INSURANCE(270, 281f),
        CURRENT_DAY(455, 171), CURRENT_MONTH(504, 171), CURRENT_YEAR(550, 171)
        ;

        final float x;
        final float y;

        private TextPosition(float x, float y) {
            this.x = x;
            this.y = y;
        }

        public float getX() {
            return this.x;
        }

        public float getY() {
            return this.y;
        }
    }

    @Override
    public boolean AddTextToCertificate(String pdfDest, String contractCode, String createdTime, String createdDate, String startDate,
            String endDate, String waitDate, String maleName, String maleDob, String maleId, String femaleName,
            String femaleDob, String femaleId, boolean maleInherit, String fee, String insurance, boolean preview,
            boolean sealed)
            throws IOException, DocumentException, URISyntaxException {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+07:00"));
        
        ClassPathResource pdfClassPath;
        if (sealed) {
            log.info("adding text from: " + CERT_PDF);
            pdfClassPath = new ClassPathResource(CERT_PDF);
        } else {
            log.info("adding text from: " + NON_CERT_PDF);
            pdfClassPath = new ClassPathResource(NON_CERT_PDF);
        }
        byte[] bytes = FileCopyUtils.copyToByteArray(pdfClassPath.getInputStream());
        PdfReader reader = new PdfReader(bytes);
        File yourFile = new File(pdfDest);
        yourFile.createNewFile();
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(pdfDest));

        PdfContentByte canvas = stamper.getOverContent(1);

        ClassPathResource fontClassPath = new ClassPathResource(FONT);
        byte[] fontBytes = FileCopyUtils.copyToByteArray(fontClassPath.getInputStream());
        BaseFont times = BaseFont.createFont(FONT, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, true, fontBytes, null);
        Font font = new Font(times, 10);
        
        if (preview) {
            Font f = new Font(times, 50);
            PdfContentByte over = stamper.getOverContent(1);
            Phrase p = new Phrase("Preview chứng nhận bảo hiểm", f);
            over.saveState();
            PdfGState gs1 = new PdfGState();
            gs1.setFillOpacity(0.3f);
            over.setGState(gs1);
            ColumnText.showTextAligned(over, Element.ALIGN_CENTER, p, 297, 450, -50);
            over.restoreState();
        }

        font.setStyle(1);
        Phrase phrase = new Phrase(createdTime, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.CREATED_DATE.getX(),
                TextPosition.CREATED_DATE.getY(), 0);

        phrase = new Phrase(startDate, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.START_DATE.getX(),
                TextPosition.START_DATE.getY(), 0);

        phrase = new Phrase(endDate, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.END_DATE.getX(),
                TextPosition.END_DATE.getY(), 0);

        phrase = new Phrase(createdDate, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.WAIT_DATE.getX(),
                TextPosition.WAIT_DATE.getY(), 0);

        phrase = new Phrase(startDate, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.END_WAIT_DATE.getX(),
                TextPosition.END_WAIT_DATE.getY(), 0);

        phrase = new Phrase(contractCode, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.CONTRACT_CODE.getX(),
                TextPosition.CONTRACT_CODE.getY(), 0);

        phrase = new Phrase(maleName, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.MALE_NAME.getX(),
                TextPosition.MALE_NAME.getY(), 0);
        
        phrase = new Phrase(maleDob, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.MALE_DOB.getX(),
                TextPosition.MALE_DOB.getY(), 0);

        phrase = new Phrase(maleId, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.MALE_ID.getX(),
                TextPosition.MALE_ID.getY(), 0);

        phrase = new Phrase(femaleName, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.FEMALE_NAME.getX(),
                TextPosition.FEMALE_NAME.getY(), 0);

        phrase = new Phrase(femaleDob, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.FEMALE_DOB.getX(),
                TextPosition.FEMALE_DOB.getY(), 0);

        phrase = new Phrase(femaleId, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.FEMALE_ID.getX(),
                TextPosition.FEMALE_ID.getY(), 0);
        
        fee +=  " VND";
        phrase = new Phrase(fee, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_RIGHT, phrase, TextPosition.FEE.getX(),
                TextPosition.FEE.getY(), 0);
        
        insurance += " VND";
        phrase = new Phrase(insurance, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_RIGHT, phrase, TextPosition.INSURANCE.getX(),
                TextPosition.INSURANCE.getY(), 0);
        
        font.setStyle(0);
        font.setSize(9);
        phrase = new Phrase(String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)), font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.CURRENT_DAY.getX(),
                TextPosition.CURRENT_DAY.getY(), 0);
        
        phrase = new Phrase(String.format("%02d", calendar.get(Calendar.MONTH) + 1), font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.CURRENT_MONTH.getX(),
                TextPosition.CURRENT_MONTH.getY(), 0);
        
        phrase = new Phrase(String.format("%02d", calendar.get(Calendar.YEAR)), font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.CURRENT_YEAR.getX(),
                TextPosition.CURRENT_YEAR.getY(), 0);

        stamper.close();

        return true;
    }
    
    @Override
    public File loadPdfAsResource(ContractWrapper contract, boolean preview, boolean sealed) throws AnvuiBaseException {
        String filePath = "/tmp/" + System.currentTimeMillis() + ".pdf";
        File file = null;
        try {
            file = fillPdf(contract, filePath, preview, sealed);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            try {
                if (file != null) {
                    Files.deleteIfExists(file.toPath());
                }
            } catch (IOException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }
            throw new AnvuiBaseException(ErrorInfo.FILE_NOT_FOUND);
        }
        return file;
    }
    
    @Override
    public File convertPdfToImage(File pdfFile) throws IOException {
        String outputFileDir = pdfFile.getAbsolutePath();
        outputFileDir = outputFileDir.substring(0, outputFileDir.lastIndexOf(".")) + ".jpeg";
        log.info("file: " + outputFileDir);
        
        File image = new File(outputFileDir);
        FileOutputStream str = new FileOutputStream(image);
        
        PDDocument document = PDDocument.load(pdfFile);
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        for (int page = 0; page < document.getNumberOfPages(); ++page) {
            BufferedImage bim = pdfRenderer.renderImageWithDPI(
              page, 150, ImageType.RGB);
            ImageIOUtil.writeImage(bim, "jpeg", str, 300);
        }
        document.close();
        
        return image;
    }
    
    public File fillPdf(ContractWrapper contract, String filePath, boolean preview,
            boolean sealed) throws AnvuiBaseException {
        Customer customer = contract.getContract().getInheritCustomer();
        Customer partner = contract.getContract().getPartner();
        
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String createdTime = format.format(contract.getContract().getCreatedTime());
        String createdDate = format.format(contract.getContract().getCreatedDate());
        String startDate = format.format(contract.getContract().getStartDate());
        String endDate = format.format(contract.getContract().getEndDate());
        String waitDate = format.format(contract.getContract().getCreatedDate());

        String maleName = null, maleDob = null, maleId = null, femaleName = null, femaleDob = null, femaleId = null;
        boolean maleInherited = false;

        if (partner.getGender().equals(customer.getGender()))
            throw new AnvuiBaseException(ErrorInfo.CUSTOMER_SAME_GENDER);

        if (customer.getGender().equals(Gender.MALE.getValue())) {
            maleName = customer.getCustomerName();
            maleDob = format.format(customer.getDob());
            maleId = customer.getCitizenId();

            femaleName = partner.getCustomerName();
            femaleDob = format.format(partner.getDob());
            femaleId = partner.getCitizenId();

            maleInherited = true;
        } else {
            femaleName = customer.getCustomerName();
            femaleDob = format.format(customer.getDob());
            femaleId = customer.getCitizenId();

            maleName = partner.getCustomerName();
            maleDob = format.format(partner.getDob());
            maleId = partner.getCitizenId();

            maleInherited = false;
        }

        if (maleName == null || maleDob == null || maleId == null || femaleName == null || femaleDob == null
                || femaleId == null)
            throw new AnvuiBaseException(ErrorInfo.MAIL_CUSTOMER_INFO_MISSING);
        String fee = StringHelper.formatDouble(contract.getContract().getFee());
        String insurance = StringHelper.formatDouble(contract.getInsurancePackage().getInsurance());
        
        String contractCode = contract.getContract().getContractCode();

        try {
            AddTextToCertificate(filePath, contractCode, createdTime, createdDate, startDate, endDate, waitDate, 
                    maleName, maleDob, maleId, femaleName, femaleDob, femaleId, maleInherited, 
                    fee, insurance, preview, sealed);
        } catch (IOException | DocumentException | URISyntaxException e) {
            log.log(Level.WARNING, e.getMessage(), e);

            File file = new File(filePath);
            try {
                Files.deleteIfExists(file.toPath());
            } catch (IOException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }

            throw new AnvuiBaseException(ErrorInfo.MAIL_PDF_CREATED_FAIL);
        }
        return new File(filePath);
    }
}
