package vn.anvui.bsn.beans.incomeReport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.DailyIncomeReport;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.entity.UserClosure.UserClosureKey;
import vn.anvui.dt.model.IncomeUser;
import vn.anvui.dt.model.MonthsReport;
import vn.anvui.dt.repos.dailyReport.DailyIncomeReportRepository;
import vn.anvui.dt.repos.user.UserRepository;
import vn.anvui.dt.repos.userClosure.UserClosureRepository;
import vn.anvui.dt.utils.DateTimeHelper;

@Service
public class IncomeReportServiceImpl implements IncomeReportService {
    static final Logger log = Logger.getLogger(IncomeReportServiceImpl.class.getSimpleName());
    
    @Autowired
    DailyIncomeReportRepository reportRepo;
    
    @Autowired
    UserClosureRepository closureRepo;
    
    @Autowired
    UserRepository userRepo;
    
    @Override
    public void createIncomeReportByDay(Integer noDay, Date lockDate) {
        Date currentDate = DateTimeHelper.getFirstTimeOfDay(lockDate, TimeZone.getTimeZone("GMT+7:00"));
        
        List<Date> dateList = DateTimeHelper.getAllDateBetween(
                new Date(lockDate.getTime() - noDay * DateTimeHelper.DAY_IN_MS), currentDate);
        
        for (Date date : dateList) {
            log.info(date.toString());
            List<DailyIncomeReport> report = null;
            try {
                report = new ArrayList<>(reportRepo.listDailyReport(date));
            } catch (Exception e) {
                log.log(Level.WARNING, e.getMessage(), e);
            }
            
            if (report != null && !report.isEmpty()) {
                log.info("save report");
                reportRepo.save(report);
            }
        }
    }

    @Override
    public List<MonthsReport> getMonthsReport(Integer productId) {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
            
            if (user == null) {
                throw new Exception();
            }
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        Date currentMonth = DateTimeHelper.getFirstDayOfMonths(new Date());
        List<Date> months = new ArrayList<>(6);
        
        for (int i = 6; i > 0; i--) {
            months.add(DateTimeHelper.addTime(currentMonth, DateTimeHelper.MONTH, -i+1, 
                    TimeZone.getTimeZone("GMT+7:00")));
        }
        
        Collection<Integer> userIds = closureRepo.findDescendanceIdFromId(user.getId());
        
        return reportRepo.getMonthReport(months, userIds, productId);
    }

    @Override
    public List<IncomeUser> getIncomeReportByAgency(Date fromDate, Date toDate, Integer userId, Integer productId,
            Integer page, Integer count) {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
            
            if (user == null) {
                throw new Exception();
            }
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        if (fromDate == null && toDate == null) {
            fromDate = DateTimeHelper.getFirstDayOfMonths(new Date(), TimeZone.getTimeZone("GMT+7:00"));
            toDate = DateTimeHelper.addDay(fromDate, DateTimeHelper.MONTH, 1, TimeZone.getTimeZone("GMT+7:00"));
        }

        if (fromDate == null ^ toDate == null) {
            throw new AnvuiBaseException(ErrorInfo.DATE_FORMAT_INVALID);
        }
        
        if (userId != null) {
            if (!closureRepo.existsByKey(new UserClosureKey(user.getId(), userId)))
                throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        Integer parentId = userId == null ? user.getId() : userId;
        log.info(count.toString());
        
        List<Integer> userIds = 
                new ArrayList<>(closureRepo.findDescendanceIdFromId(parentId, count, page * count));
        
        return reportRepo.getSubAgencyReport(fromDate, toDate, userIds, productId);
    }

    @Override
    public List<DailyIncomeReport> getDailyIncomeReport(Date fromDate, Date toDate, Integer productId) {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
            
            if (user == null) {
                throw new Exception();
            }
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        if (fromDate == null ^ toDate == null) {
            throw new AnvuiBaseException(ErrorInfo.DATE_FORMAT_INVALID);
        }
        
        if (fromDate == null && toDate == null) {
            fromDate = DateTimeHelper.getFirstDayOfMonths(new Date(), TimeZone.getTimeZone("GMT+7:00"));
            toDate = DateTimeHelper.addDay(fromDate, DateTimeHelper.MONTH, 1, TimeZone.getTimeZone("GMT+7:00"));
        }
        
        List<Date> dateRange = DateTimeHelper.getAllDateBetween(fromDate, toDate, TimeZone.getTimeZone("GMT+7:00"));
        
        Collection<Integer> userIds = closureRepo.findDescendanceIdFromId(user.getId());
        
        List<DailyIncomeReport> report = new LinkedList<>();
        
        for (Date date : dateRange) {
            report.add(reportRepo.getReportByDay(date, userIds, productId));
        }
        
        return report;
    }
}