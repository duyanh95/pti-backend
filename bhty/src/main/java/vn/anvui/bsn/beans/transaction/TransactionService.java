package vn.anvui.bsn.beans.transaction;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.transaction.TransactionRequest;
import vn.anvui.bsn.dto.transaction.TransactionStatusRequest;
import vn.anvui.dt.model.ContractWrapper;

public interface TransactionService<T extends TransactionRequest, U extends TransactionStatusRequest> {
    public String createTransaction(ContractWrapper contract, T req) 
            throws AnvuiBaseException;
    
    public ContractWrapper updateTransactionStatus(U req) throws AnvuiBaseException;
}
