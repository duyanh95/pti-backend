package vn.anvui.bsn.beans.miinContract;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import vn.anvui.api.amqp.MessageProducer;
import vn.anvui.bsn.beans.mailing.MailingService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.miinContract.MiinContractRequest;
import vn.anvui.bsn.dto.notification.NotificationDto;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.NotificationTypeEnum;
import vn.anvui.dt.enumeration.TransactionType;
import vn.anvui.dt.enumeration.UserNotificationType;

public abstract class ContractAbstractService<T extends MiinContractRequest> {
    public final String BCC = "chamsockhachhang@epti.vn";
    
    @Autowired
    protected MessageProducer msgProducer;
    
    @Autowired
    protected MailingService mailService;
    
    protected Contract updateContractPricing(Contract contract, InsurancePackage pkg, User createdUser) {
        contract.setFee(pkg.getFee());
        contract.setIncome(pkg.getFee() * (1 - createdUser.getCommission()));
        contract.setCommission(contract.getFee() - contract.getIncome());
        
        return contract;
    }
    
    protected ContractStatus getStatusFromTxnType(Integer txnType, User user) {
        ContractStatus status;
        if (txnType == null || txnType.equals(TransactionType.ONLINE_TXN.getValue())) {
            status = ContractStatus.AGENCY_CREATED;
        } else if (txnType.equals(TransactionType.COD_TXN.getValue())) {
            if (!user.isCodAllowed()) {
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_COD_CREATION_NOT_ALLOWED);
            }
            status = ContractStatus.AGENCY_COD_DONE;
        } else if (txnType.equals(TransactionType.ACCOUNT_TRANSFER.getValue())) {
            if (!user.isCodAllowed()) {
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_COD_CREATION_NOT_ALLOWED);
            }
            status = ContractStatus.AGENCY_TRANSFER;
        } else {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_TRANSACTION_TYPE_INVALID);
        }
        
        return status;
    }
    
    protected void sendNotify(Contract contract, UserNotificationType type) {
        Map<String, String> fields = new HashMap<>();
        fields.put("contractCode", contract.getContractCode());
        
        if (contract.getContractStatus().equals(ContractStatus.AGENCY_COD_DONE.getValue()) 
                || contract.getContractStatus().equals(ContractStatus.AGENCY_TRANSFER_DONE.getValue())
                || contract.getContractStatus().equals(ContractStatus.AGENCY_PAID.getValue())) {
            
            NotificationDto noti = new NotificationDto(NotificationTypeEnum.TXN_SUCCESS.getValue(),
                    contract.getCreatedUserId(), type, fields);
            msgProducer.sendPushServiceQueue(noti);
        }
    }
    
    protected boolean packageIdIsValid(InsurancePackage pkg, Integer productId) {
        return pkg.getProductId().equals(productId);
    }
    
    protected abstract String createContractCode(String agencyCode);
    
    protected abstract Customer createCustomer(T req);
    
    protected abstract Contract createContract(T req, User user, Customer customer,
            ContractStatus status);
}
