package vn.anvui.bsn.beans.history;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.beans.authentication.AuthenticationHelper;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.misc.HistoryCreationRequest;
import vn.anvui.dt.entity.History;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.entity.Product;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.repos.history.HistoryRepository;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.repos.product.ProductRepository;

@Service
public class HistoryService {
    @Autowired
    ProductRepository productRepo;

    @Autowired
    InsurancePackageRepository pkgRepo;

    @Autowired
    HistoryRepository historyRepo;
    
    @Autowired
    AuthenticationHelper authHelper;

    public History createHistory(HistoryCreationRequest req) {
        User user;
        try {
            user = authHelper.getUserFromAuthentication();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        Product product = productRepo.findOne(req.getProductId());

        if (product == null) {
            throw new AnvuiBaseException(ErrorInfo.DATA_NOT_EXIST);
        }

        if (req.getPackageId() != null) {
            InsurancePackage pkg = pkgRepo.findOne(req.getPackageId());

            if (pkg == null || !pkg.getProductId().equals(product.getId())) {
                throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST);
            }
        }

        History history = req.createHistory(user);
        history.setUserId(user.getId());

        return historyRepo.save(history);
    }

    public List<History> getHistory(int page, int size) {
        User user;
        try {
            user = authHelper.getUserFromAuthentication();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        Pageable pageable = new PageRequest(page, size);
        
        return historyRepo.findAllByUserId(user.getId(), pageable);
    }
}
