package vn.anvui.bsn.beans.miinContract.love;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.itextpdf.text.DocumentException;

import vn.anvui.api.amqp.MessageProducer;
import vn.anvui.bsn.beans.customer.CustomerService;
import vn.anvui.bsn.beans.miinContract.ContractAbstractService;
import vn.anvui.bsn.beans.pdf.PdfService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.miinContract.LoveContractRequest;
import vn.anvui.bsn.dto.miinContract.MiinContractRequest;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.CustomerTag;
import vn.anvui.dt.enumeration.Gender;
import vn.anvui.dt.enumeration.ProductType;
import vn.anvui.dt.enumeration.TransactionStatus;
import vn.anvui.dt.enumeration.TransactionType;
import vn.anvui.dt.enumeration.UserNotificationType;
import vn.anvui.dt.enumeration.UserType;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.customer.CustomerRepository;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.repos.onlineTransaction.OnlineTransactionRepository;
import vn.anvui.dt.repos.onlineTransaction.OnlineTransactionRepository.getPaymentIdOnly;
import vn.anvui.dt.utils.StringHelper;

@Service
public class LoveContractServiceImpl extends ContractAbstractService<LoveContractRequest>  
        implements LoveContractService {
    final static Logger log = Logger.getLogger(LoveContractServiceImpl.class.getSimpleName());
    
    protected final static Integer PRODUCT_ID = ProductType.LOVE.getValue();
    
    @Autowired
    CustomerService customerService;
    
    @Autowired
    CustomerRepository customerRepo;
    
    @Autowired
    ContractRepository contractRepo;
    
    @Autowired
    InsurancePackageRepository packageRepo;
    
    @Autowired
    OnlineTransactionRepository txnRepo;
    
    @Autowired
    MessageProducer msgProducer;
    
    @Autowired
    PdfService pdfService;
    
    @Override
    public Contract createCustomerContract(LoveContractRequest req) throws AnvuiBaseException {
        User user = null;
        try {
            AuthenticationDetails userDetail = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            user = userDetail.getAuthenticatedUser();

        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        log.info("got authentication details");
        log.info(req.getCitizenId());
        log.info(req.getPartnerCitizenId());
        if (customerService.contractIsExisted(null, req.getCitizenId(), req.getPartnerCitizenId()))
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_EXISTED);

        Contract contract = createCustomer(req, ContractStatus.CREATED, user);
        
        log.info("prepare to get package");
        InsurancePackage pack;
        try {
            pack = packageRepo.findOne(req.getPackageId());
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.PACKAGE_INVALID);
        }
        
        Date startDate = customerService.checkStartDate(req.getStartDate());

        ContractWrapper res = customerService.createContract(contract, contract.getInheritCustomer(), 
                contract.getPartner(), pack, 
                ContractStatus.CUSTOMER_CREATED, user, startDate);
        
        sendNotify(contract, UserNotificationType.CUSTOMER);
        
        return res.getContract();
    }

    protected Contract createCustomer(LoveContractRequest req, ContractStatus status, User user) {
        Customer partner = new Customer();
        Customer customer = new Customer();
        Contract contract = new Contract();
        
        if (status.equals(ContractStatus.CREATED)) {
            log.info("created new");
            List<Integer> tagList = new ArrayList<>(3);
            tagList.add(CustomerTag.NOT_PROCESS.getValue());
            tagList.add(CustomerTag.PENDING.getValue());
            tagList.add(CustomerTag.PRE_COD.getValue());
            customer = customerRepo.findFirstByPhoneNumberAndTagInAndContractIdIsNull(req.getPhoneNumber(),
                    tagList);

            log.info("checked non contract customers");
            if (customer == null) {
                
                customer = customerRepo.findContractCreatedContactByPhoneNumberAndTag(tagList, 
                        req.getPhoneNumber());
                
                if (customer == null) {
                    log.info("customer not existed");
                    customer = new Customer();
                } else {
                    log.info("existed customer id: " + customer.getId());
                    
                    contract = contractRepo.findOne(customer.getContractId());
                    if (contract.getContractStatus().equals(ContractStatus.PAID.getValue())) {
                        throw new AnvuiBaseException(ErrorInfo.UNABLE_TO_SAVE_DATA);
                    }
                    
                    if (contract.getPartner() != null) {
                        partner = contract.getPartner();
                    } else {
                        partner = new Customer();
                    }
                }
            }
            log.info("checked existed");
        }
        
        partner.setCustomerName(WordUtils.capitalizeFully(req.getPartnerName()));
        partner.setCitizenId(req.getPartnerCitizenId());
        partner.setDob(req.getPartnerDob());
        partner.setPhoneNumber(req.getPhoneNumber());
        partner.setEmail(req.getEmail());
        partner.setGender(req.getPartnerGender());

        log.info("prepare to save partner");
        try {
            partner = customerRepo.save(partner);
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.UNABLE_TO_SAVE_DATA);
        }

        customer.setCustomerName(WordUtils.capitalizeFully(req.getCustomerName()));
        customer.setCitizenId(req.getCitizenId());
        customer.setDob(req.getDob());
        customer.setPhoneNumber(req.getPhoneNumber());
        customer.setEmail(req.getEmail());
        customer.setGender(req.getGender());
        customer.setAddress(req.getAddress());
        
        
        if (user != null) {
            if (user.getUserType().equals(UserType.PTI_USER.getValue()))
                customer.setAssignedUserId(user.getId());
        }

        log.info("prepare to save customer");
        try {
            customer = customerRepo.save(customer);
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.UNABLE_TO_SAVE_DATA);
        }

        
        contract.setInheritCustomer(customer);
        contract.setPartner(partner);

        return contract;
    }

    @Override
    public Contract createAgencyContract(LoveContractRequest req)
            throws AnvuiBaseException {
        User user = null;
        try {
            AuthenticationDetails userDetail = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            user = userDetail.getAuthenticatedUser();

        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        log.info("got authentication details");
        if (customerService.contractIsExisted(null, req.getCitizenId(), req.getPartnerCitizenId()))
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_EXISTED);
        
        ContractStatus status;
        if (req.getTxnType() == null || req.getTxnType().equals(TransactionType.ONLINE_TXN.getValue())) {
            status = ContractStatus.AGENCY_CREATED;
        } else if (req.getTxnType().equals(TransactionType.COD_TXN.getValue())) {
            if (!user.isCodAllowed()) {
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_COD_CREATION_NOT_ALLOWED);
            }
            status = ContractStatus.AGENCY_COD_DONE;
        } else if (req.getTxnType().equals(TransactionType.ACCOUNT_TRANSFER.getValue())) {
            if (!user.isCodAllowed()) {
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_COD_CREATION_NOT_ALLOWED);
            }
            status = ContractStatus.AGENCY_TRANSFER;
        } else {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_TRANSACTION_TYPE_INVALID);
        }

        Contract contract = createCustomer(req, status, user);
        
        log.info("prepare to get package");
        InsurancePackage pack;
        try {
            pack = packageRepo.findOne(req.getPackageId());
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.PACKAGE_INVALID);
        }
        if (!pack.getProductId().equals(PRODUCT_ID)) {
            throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST.addInfo("field", "packageId"));
        }
        
        Date startDate = customerService.checkStartDate(req.getStartDate());

        ContractWrapper res = customerService.createContract(contract, contract.getInheritCustomer(), 
                contract.getPartner(), pack, 
                status, user, startDate);
        
        sendNotify(contract, UserNotificationType.AGENCY);
        
        return res.getContract();
    }

    @Override
    protected Customer createCustomer(LoveContractRequest req) {
        return null;
    }

    @Override
    protected Contract createContract(LoveContractRequest req, User user, Customer customer, ContractStatus status) {
        return null;
    }

    @Override
    protected String createContractCode(String agencyCode) {
        return null;
    }

    @Override
    public void sendContractMail(ContractWrapper contract) throws AnvuiBaseException {
        String filePath = "/tmp/" + System.currentTimeMillis();
        // String filePath = "/tmp/rs.pdf";
        String paymentString = "";
        getPaymentIdOnly txnPaymentId = txnRepo.findFirstByStatusAndContractId(TransactionStatus.SUCCESS.getValue(),
                contract.getContract().getId());
        if (txnPaymentId != null) {
            paymentString = "với mã thanh toán: " + txnPaymentId.getPaymentId();
        }

        Customer customer = contract.getContract().getInheritCustomer();
        Customer partner = contract.getContract().getPartner();
        String body = "<!DOCTYPE html>\n" + "<html lang=\"en-US\" class=\"no-js no-svg\">\n" + "<head>\n"
                + "<meta charset=\"UTF-8\">\n"
                + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "<link rel=\"profile\" href=\"http://gmpg.org/xfn/11\">\n" + "<link />\n" + "\n"
                + "<title>thank you &#8211; Bảo Hiểm Tình Yêu</title>\n"
                + "<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.3.1/css/all.css\" integrity=\"sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU\" crossorigin=\"anonymous\">\n"
                + "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/style-thankyou.css\">\n"
                + "                <style type=\"text/css\">\n" + "img.wp-smiley,\n" + "img.emoji {\n"
                + "        display: inline !important;\n" + "        border: none !important;\n"
                + "        box-shadow: none !important;\n" + "        height: 1em !important;\n"
                + "        width: 1em !important;\n" + "        margin: 0 .07em !important;\n"
                + "        vertical-align: -0.1em !important;\n" + "        background: none !important;\n"
                + "        padding: 0 !important;\n" + "}\n" + ".center.title-email i {\n" + "    position: absolute;\n"
                + "    left: 20px;\n" + "    font-size: 60px;\n" + "    color:#3eaa5f;\n" + "}\n" + ".title-email{\n"
                + "    position:relative;\n" + "    line-height: 60px;\n" + "    font-family: 'Roboto', sans-serif\n"
                + "}\n" + ".center.content {\n" + "    color: #3eaa5f;\n" + "    text-align: justify;\n"
                + "    font-size: 20px;\n" + "    font-family: 'Roboto', sans-serif\n" + "}\n"
                + ".center.content h4 {\n" + "    text-align: center;\n" + "    font-size: 30px;\n" + "}\n"
                + "</style> \n" + "<style type=\"text/css\">\n" + " footer {\n" + "    float: left;\n"
                + "    width: 100%;\n" + "    }\n" + " \n" + " .destop-hidden{\n" + "    display: none;\n" + " }\n"
                + " html,body{\n" + "    \n" + "    background: #f1f0ee;\n" + " }\n" + " #thank-you {\n"
                + "    width: 60%;\n" + "    height: 100%;\n" + "    display: block;\n" + "    margin: 0 auto;\n"
                + "    text-align: center;\n" + "    background: #f1f0ee;\n" + "}\n" + " #thank-you .mobi-hidden img{\n"
                + "    width: 60%;\n" + "    margin: 0 auto;\n" + " }\n" + "  .section-heading h2 {\n"
                + "    font-size: 50px;\n" + "  }\n" + " .section-heading h2 span {\n" + "    position: relative;\n"
                + "    color: #3eaa5f;\n" + "    \n" + "}\n" + "#thank-you .destop-hidden img{\n" + "    width: 100%;\n"
                + "    text-align: center;\n" + "}\n" + ".title-email {\n" + "    border: 5px solid #3eaa5f;\n" + "}\n"
                + " @media (max-width:760px){\n" + "    {}\n" + "    .mobi-hidden{\n" + "        display: none;\n"
                + "    }\n" + "    .destop-hidden{\n" + "        display: block;\n" + "    }\n" + " }\n" + "</style>\n"
                + "<link href=\"https://fonts.googleapis.com/css?family=Roboto\" rel=\"stylesheet\">\n" + "</head>\n"
                + "<body>\n" + "        <section id=\"thank-you\" class=\"thank-you\" style=\"    width: 70%;\n"
                + "    height: 100%;\n" + "    display: block;\n" + "    margin: 0 auto;\n"
                + "    text-align: center;\n" + "    background: #f1f0ee; padding:15px\">\n"
                + "            <div class=\"section-heading text-center col-md-12\">\n"
                + "                                        <h2 style=\"    font-size: 50px; text-align:center\"><span style=\"position: relative;\n"
                + "    color: #3eaa5f;\">Thank</span> You</h2>\n" + "                                </div>\n"
                + "                                <div class=\"center title-email\" style=\"border: 2px solid #3eaa5f; position: relative;\n"
                + "    line-height: 60px;\n" + "    font-family: 'Roboto', sans-serif;\">\n"
                + "                        <h3 style=\"display: block;\n" + "    font-size: 1.17em;\n"
                + "    -webkit-margin-before: 1em;\n" + "    -webkit-margin-after: 1em;\n"
                + "    -webkit-margin-start: 0px;\n" + "    -webkit-margin-end: 0px;\n"
                + "    font-weight: bold;text-align:center\"> <i class=\"fas fa-check-circle\" style=\"position: absolute;\n"
                + "    left: 20px;\n" + "    font-size: 60px;\n"
                + "    color: #3eaa5f; \"></i> <p style=\"font-size: 20px\">Chúc mừng các bạn đã đăng ký thành công \"Bảo hiểm tình yêu\"</p></h3>\n"
                + "                     </div>\n"
                + "                     <div class=\"center content\" style=\"    color: #3eaa5f;\n"
                + "    text-align: justify;\n" + "    font-size: 20px;\n"
                + "    font-family: 'Roboto', sans-serif;\">\n"
                + "                        <h4 style=\"text-align: center;\n" + "    font-size: 30px;\">Xin chào: "
                + customer.getCustomerName() + "</h4>\n" + "\n" + "                        <p>\n"
                + "                           Chúng tôi rất vui mừng được thông báo rằng hợp đồng bảo hiểm tình yêu mã số : <span style=\"color: #500050\">"
                + contract.getContract().getContractCode()
                + "</span> của bạn đã được tạo trên hệ thống của BaoHiemTinhYeu.Vn" + paymentString + "\n"
                + "                        <p>\n"
                + "                            Chúc 2 bạn có những ngày tháng thật đẹp bên nhau. Hãy yêu thương nhau thật nhiều và cùng nhau vượt qua muôn ngàn sóng gió phía trước nhé!\n"
                + "                        </p>\n" + "\n" + "                         \n"
                + "                        <p>\n" + "                            Cảm ơn!\n"
                + "                        </p>\n" + "                    </div>\n" + "    </section>\n" + "\n"
                + "</body>\n" + "</html>\n" + "";

        List<String> listEmail = new ArrayList<>();
        listEmail.add(customer.getEmail());

        // if payment success
        if (contract.getContract().getContractStatus() == ContractStatus.PAID.getValue()
                || contract.getContract().getContractStatus() == ContractStatus.COD.getValue()
                || contract.getContract().getContractStatus() == ContractStatus.AGENCY_COD_DONE.getValue()
                || contract.getContract().getContractStatus() == ContractStatus.AGENCY_PAID.getValue()
                || contract.getContract().getContractStatus() == ContractStatus.AGENCY_TRANSFER_DONE.getValue()) {
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            String createdTime = format.format(contract.getContract().getCreatedTime());
            String createdDate = format.format(contract.getContract().getCreatedDate());
            String startDate = format.format(contract.getContract().getStartDate());
            String endDate = format.format(contract.getContract().getEndDate());
            String waitDate = format.format(contract.getContract().getCreatedDate());

            String maleName = null, maleDob = null, maleId = null, femaleName = null, femaleDob = null, femaleId = null;
            boolean maleInherited = false;

            if (partner.getGender().equals(customer.getGender()))
                throw new AnvuiBaseException(ErrorInfo.CUSTOMER_SAME_GENDER);

            if (customer.getGender().equals(Gender.MALE.getValue())) {
                maleName = customer.getCustomerName();
                maleDob = format.format(customer.getDob());
                maleId = customer.getCitizenId();

                femaleName = partner.getCustomerName();
                femaleDob = format.format(partner.getDob());
                femaleId = partner.getCitizenId();

                maleInherited = true;
            } else {
                femaleName = customer.getCustomerName();
                femaleDob = format.format(customer.getDob());
                femaleId = customer.getCitizenId();

                maleName = partner.getCustomerName();
                maleDob = format.format(partner.getDob());
                maleId = partner.getCitizenId();

                maleInherited = false;
            }

            if (maleName == null || maleDob == null || maleId == null || femaleName == null || femaleDob == null
                    || femaleId == null)
                throw new AnvuiBaseException(ErrorInfo.MAIL_CUSTOMER_INFO_MISSING);

            if (contract.getInsurancePackage() == null) {
                contract.setInsurancePackage(packageRepo.findOne(contract.getContract().getPackageId()));
            }
            String fee = StringHelper.formatDouble(contract.getInsurancePackage().getFee());
            String insurance = StringHelper.formatDouble(contract.getInsurancePackage().getInsurance());
            String contractCode = contract.getContract().getContractCode();

            try {
                pdfService.AddTextToCertificate(filePath, contractCode, createdTime, createdDate, startDate, endDate,
                        waitDate, maleName, maleDob, maleId, femaleName, femaleDob, femaleId, maleInherited, fee,
                        insurance, false, true);
            } catch (IOException | DocumentException | URISyntaxException e) {
                log.log(Level.WARNING, e.getMessage(), e);

                File file = new File(filePath);
                try {
                    Files.deleteIfExists(file.toPath());
                } catch (IOException ex) {
                    log.log(Level.WARNING, ex.getMessage(), ex);
                }

                throw new AnvuiBaseException(ErrorInfo.MAIL_PDF_CREATED_FAIL);
            }
            try {
                mailService.sendEmailWithAttachment(listEmail, null, null, "BHTY", body, "", filePath);
            } catch (Exception e) {
                log.log(Level.WARNING, e.getMessage(), e);

                File file = new File(filePath);
                try {
                    Files.deleteIfExists(file.toPath());
                } catch (IOException ex) {
                    log.log(Level.WARNING, ex.getMessage(), ex);
                }

                throw new AnvuiBaseException(ErrorInfo.MAIL_SEND_FAIL);
            }
            File file = new File(filePath);
            try {
                Files.deleteIfExists(file.toPath());
            } catch (IOException e) {
                log.log(Level.WARNING, e.getMessage(), e);
            }
        } else {
            log.info("transaction failed, mail not sent");
            return;
        }
    }
    
}