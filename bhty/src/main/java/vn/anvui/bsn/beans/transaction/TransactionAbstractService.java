package vn.anvui.bsn.beans.transaction;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import vn.anvui.api.amqp.MessageProducer;
import vn.anvui.bsn.beans.miinContract.general.MiinGeneralContractService;
import vn.anvui.bsn.dto.notification.NotificationDto;
import vn.anvui.bsn.dto.transaction.TransactionRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.ContractNumber;
import vn.anvui.dt.entity.OnlineTransaction;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.CustomerTag;
import vn.anvui.dt.enumeration.NotificationTypeEnum;
import vn.anvui.dt.enumeration.TransactionSource;
import vn.anvui.dt.enumeration.TransactionStatus;
import vn.anvui.dt.enumeration.UserNotificationType;
import vn.anvui.dt.enumeration.UserType;
import vn.anvui.dt.repos.contract.ContractNumberRepository;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.onlineTransaction.OnlineTransactionRepository;
import vn.anvui.dt.repos.user.UserRepository;

public abstract class TransactionAbstractService {
    static final String BHTY_RETURN_URL = "https://baohiemtinhyeu.vn/";
    static final String MIIN_ANDROID_URL = "";
    static final String MIIN_IOS_URL = "";
    static final String AGENCY_URL = "https://daily.baohiemtinhyeu.vn/";
    
    @Autowired
    MessageProducer msgProducer;
    
    @Autowired
    ContractNumberRepository noRepo;
    
    @Autowired
    ContractRepository contractRepo;
    
    @Autowired
    UserRepository userRepo;
    
    @Autowired
    OnlineTransactionRepository txnRepo;
    
    @Autowired
    MiinGeneralContractService miinService;
    
    protected void notify(Contract contract, OnlineTransaction txn) {
        if (contract.getCreatedUserId() == null) {
            return;
        }
        
        NotificationTypeEnum type;
        UserNotificationType userType = UserNotificationType.CUSTOMER;
        Map<String, String> fields = new HashMap<>();
        fields.put("contractCode", contract.getContractCode());
        
       
        if (txn.getStatus().equals(TransactionStatus.SUCCESS.getValue())) {
            type = NotificationTypeEnum.TXN_SUCCESS;
            
            if (contract.getContractStatus().equals(ContractStatus.AGENCY_PAID.getValue())) {
                userType = UserNotificationType.AGENCY;
            }
            
            NotificationDto dto = new NotificationDto(type.getValue(), contract.getCreatedUserId(), 
                    userType, fields);
            
            msgProducer.sendPushServiceQueue(dto);
            
        } else if (txn.getStatus().equals(TransactionStatus.EXPIRED.getValue())
                || txn.getStatus().equals(TransactionStatus.FAIL.getValue())) {
            type = NotificationTypeEnum.TXN_FAIL;
            
            if (contract.getContractStatus().equals(ContractStatus.AGENCY_CREATED.getValue())) {
                userType = UserNotificationType.AGENCY;
            }
            
            NotificationDto dto = new NotificationDto(type.getValue(), contract.getCreatedUserId(), 
                    userType, fields);
            
            msgProducer.sendPushServiceQueue(dto);
        }
    }
    
    long getAmount(Contract contract) {
        User createdUser = null;
        if (contract.getCreatedUserId() != null) {
            createdUser = userRepo.findOne(contract.getCreatedUserId());
        }
        
        long amount = contract.getIncome().longValue();
        if (createdUser != null && createdUser.getUserType().equals(UserType.AGENCY.getValue())) {
            amount = contract.getFee().longValue();
        }
        
        return amount;
    }
    
    Contract updateContractStatus(Contract contract) {
        if (contract.getContractStatus().equals(ContractStatus.CREATED.getValue())) {
            contract.setContractStatus(ContractStatus.PAID.getValue());
        } else if (contract.getContractStatus().equals(ContractStatus.AGENCY_CREATED.getValue())) {
            contract.setContractStatus(ContractStatus.AGENCY_PAID.getValue());
        }
        
        if (contract.getContractCode().length() < 15) {
            ContractNumber num = noRepo.save(new ContractNumber());
            String contractCode = contract.getContractCode() + "%07d/HD/045-HN.01/PHH.TN.15.%d";
            contractCode = String.format(contractCode, num.getNumber(), Calendar.getInstance().get(Calendar.YEAR));
            contract.setContractCode(contractCode);
        }
        contract.getInheritCustomer().setTag(CustomerTag.ONLINE.getValue());
        
        contract = miinService.updateEffectiveDate(contract);
        
        return contract;
    }
    
    String getReturnURL(TransactionRequest req) {
        if (StringUtils.isNotEmpty(req.getReturnURL())) {
            return req.getReturnURL();
        } else if (req.getSource().equals(TransactionSource.BHTY.getValue())) {
            return BHTY_RETURN_URL;
        } else if (req.getSource().equals(TransactionSource.MIIN_ANDROID.getValue())) {
            return MIIN_ANDROID_URL;
        } else if (req.getSource().equals(TransactionSource.MIIN_IOS.getValue())) {
            return MIIN_IOS_URL;
        } else  {
            return AGENCY_URL;
        } 
    }
}
