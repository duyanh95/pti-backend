package vn.anvui.bsn.beans.mailing;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface MailingService {
    boolean sendEmail(List<String> listTo, List<String> listCC, List<String> listBCC,
            String subject, String body, String format);
    
    boolean sendEmailWithAttachment(List<String> listTo, List<String> listCC, List<String> listBCC,
            String subject, String body, String format, String attachmentURL) throws FileNotFoundException, IOException;
    
}
