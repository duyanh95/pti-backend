package vn.anvui.bsn.beans.miinContract.bike;

import java.util.Date;

import vn.anvui.bsn.beans.miinContract.MiinContractService;
import vn.anvui.bsn.dto.miinContract.BikeContractRequest;
import vn.anvui.dt.entity.Contract;

public interface BikeContractService extends MiinContractService<BikeContractRequest> {

}
