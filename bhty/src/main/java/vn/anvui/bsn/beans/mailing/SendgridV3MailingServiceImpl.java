package vn.anvui.bsn.beans.mailing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.sendgrid.Attachments;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Personalization;
import com.sendgrid.Request;
import com.sendgrid.SendGrid;

@Service
public class SendgridV3MailingServiceImpl implements MailingService {
    public final static String HOST_MAIL = "chamsockhachhang@epti.vn";
    
    Logger log = Logger.getLogger(SendgridV3MailingServiceImpl.class.getName());
    
    SendGrid sendGrid;
    
    public SendgridV3MailingServiceImpl(SendGrid sendGrid) {
        this.sendGrid = sendGrid;
    }

    @Override
    public boolean sendEmail(List<String> listTo, List<String> listCC, List<String> listBCC, String subject,
            String body, String format) {
        Email from = new Email(HOST_MAIL);
        Email to = new Email();
        if (StringUtils.isEmpty(format)) {
            format = "text/html";
        }
        Content content = new Content(format, body);
        Mail mail = new Mail();
        mail.setSubject(subject);
        log.info("subject: " + subject);
        mail.addContent(content);
        log.info("content: " + body);
        mail.setFrom(from);
        Personalization p = new Personalization();
        for (String toEmail : listTo) {
            to = new Email(toEmail);
            log.info("send email to " + toEmail);
            p.addTo(to);
        }
        if (listCC != null) {
            for (String toEmail : listCC) {
                to = new Email(toEmail);
                p.addCc(to);
            }
        }
        if (listBCC != null) {
            for (String toEmail : listBCC) {
                to = new Email(toEmail);
                p.addBcc(to);
            }
        }
        mail.addPersonalization(p);
        com.sendgrid.Request request = new Request();
        try {
            request.setMethod(com.sendgrid.Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            com.sendgrid.Response response = sendGrid.api(request);
            log.info("reponse status: " + response.getStatusCode());
            log.info(response.getBody());
            log.info(response.getHeaders().toString());
            
            if (response.getStatusCode() > 400) {
                return false;
            }
            return true;
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            return false;
        }
    }

    @Override
    public boolean sendEmailWithAttachment(List<String> listTo, List<String> listCC, List<String> listBCC,
            String subject, String body, String format, String attachmentURL) throws FileNotFoundException, IOException {
        Email from = new Email(HOST_MAIL);
        Email to = new Email();
        if (StringUtils.isEmpty(format)) {
            format = "text/html";
        }
        Content content = new Content(format, body);
        Mail mail = new Mail();
        mail.setSubject(subject);
        log.info("subject: " + subject);
        mail.addContent(content);
        log.info("content: " + body);
        mail.setFrom(from);
        Personalization p = new Personalization();
        for (String toEmail : listTo) {
            to = new Email(toEmail);
            log.info("send email to " + toEmail);
            p.addTo(to);
        }
        if (listCC != null) {
            for (String toEmail : listCC) {
                to = new Email(toEmail);
                p.addCc(to);
            }
        }
        if (listBCC != null) {
            for (String toEmail : listBCC) {
                to = new Email(toEmail);
                p.addBcc(to);
            }
        }
            
        p.addBcc(new Email(HOST_MAIL));   
            
        mail.addPersonalization(p);
        com.sendgrid.Request request = new Request();
        
        File file = new File(attachmentURL);
        byte[] fileContent = IOUtils.toByteArray(new FileInputStream(file));
        
        Attachments attachment = new Attachments();
        Base64 x = new Base64();
        String dataString = x.encodeAsString(fileContent);
        attachment.setContent(dataString);
        attachment.setType("application/pdf");
        attachment.setFilename("CNBH.pdf");
        attachment.setDisposition("attachment");
        attachment.setContentId("Banner");
        mail.addAttachments(attachment);
        
        try {
            request.setMethod(com.sendgrid.Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            
            if (sendGrid == null) {
                log.severe("sendGrid not working");
            }
            
            com.sendgrid.Response response = sendGrid.api(request);
            log.info("reponse status: " + response.getStatusCode());
            log.info(response.getBody());
            log.info(response.getHeaders().toString());
            
            if (response.getStatusCode() > 400) {
                return false;
            }
            return true;
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            return false;
        }
    }
}
