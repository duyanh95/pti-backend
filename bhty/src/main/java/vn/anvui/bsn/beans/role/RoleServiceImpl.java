package vn.anvui.bsn.beans.role;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.role.RoleRequest;
import vn.anvui.dt.entity.Privilege;
import vn.anvui.dt.entity.Role;
import vn.anvui.dt.repos.privilege.PrivilegeRepository;
import vn.anvui.dt.repos.role.RoleRepository;

@Service
public class RoleServiceImpl implements RoleService {
    
    @Autowired
    RoleRepository roleRepo;
    
    @Autowired
    PrivilegeRepository priRepo;

    @Override
    public Role createRole(RoleRequest req) throws AnvuiBaseException {
        List<Privilege> privileges = priRepo.findAllByNameIn(req.getPrivileges());
        
        if (privileges == null || privileges.isEmpty())
            throw new AnvuiBaseException(ErrorInfo.ROLE_PRIVILEGES_NULL);
        
        Role role = new Role();
        role.setName(req.getName());
        role.setPrivileges(privileges);
        
        return roleRepo.save(role);
    }

}
