package vn.anvui.bsn.beans.rabbit;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.utils.DataTransferObjectHelper;
import vn.anvui.dt.entity.AccessToken;

@Service
@PropertySource("classpath:${SYSTEM_ENVIRONMENT_VARIABLE}_rabbitmq.properties")
public class RabbitAdminClient {

    private static final Logger logger = LoggerFactory.getLogger(RabbitAdminClient.class);

    public static final String ADMIN_TAG = "administrator";
    public static final String MONITORING_TAG = "monitoring";
    public static final String POLICY_MARKER_TAG = "policymaker";
    public static final String MANAGEMENT_TAG = "management";
    public static final String NONE_TAG = "";

    @Value("${rabbit.username}")
    private String username;

    @Value("${rabbit.password}")
    private String password;

    @Value("${rabbit.api.endpoint}")
    private String endpoint;

    @Value("${rabbit.vhost}")
    private String vhost;

    private Map<String, String> headers;

    @PostConstruct
    public void init() {

        headers = createHeaders();

        // Create VirtualHost for this project
        createVHost();

        // Set permission for 'guest' user
        setPermissions(username);

    }

    public void sendMessage(String msg) {
        String url = String.format("%s/test/post-message", endpoint);

        Map<String, String> body = new HashMap<>();
        body.put("msg", msg);

        createPutHttpRequest(url, headers, body);
    }

    private String getRabbitUser(AccessToken token) {

        return String.format("%s_%d", token.getDeviceToken(), token.getUser().getId());
    }

    public void createRabbitUser(AccessToken token) {
        createRabbitUser(token, RabbitAdminClient.NONE_TAG);
    }

    public void createRabbitUser(AccessToken token, String tags) {
        if (token == null) {
            logger.warn("createRabbitUser get called with NULL token");
            return;
        }

        String user = getRabbitUser(token);

        String url = String.format("%s/users/%s", endpoint, user);

        Map<Object, Object> body = new HashMap<>();
        body.put("password", token.getToken());
        body.put("tags", tags);

        createPutHttpRequest(url, headers, body);
        setPermissions(user);

        logger.info("Rabbit user created {}", user);

    }

    public void deleteRabbitUser(AccessToken token) {

        if (token == null) {
            logger.warn("createRabbitUser get called with NULL token");
            return;
        }

        String user = getRabbitUser(token);

        // Revoke permission from vhost
        createDeleteHttpRequest(String.format("%s/permissions/%s/%s", endpoint, vhost, user), headers);

        // Delete user from Message broker
        createDeleteHttpRequest(String.format("%s/users/%s", endpoint, user), headers);

    }

    private void setPermissions(String user) {

        Map<Object, Object> body = new HashMap<>();
        body.put("read", ".*");
        body.put("write", ".*");
        body.put("configure", ".*");

        String url = String.format("%s/permissions/%s/%s", endpoint, vhost, user);

        createPutHttpRequest(url, headers, body);
    }

    public void createVHost() {
        String url = String.format("%s/vhosts/%s", endpoint, vhost);
        createPutHttpRequest(url, headers, null);
    }

    private void createPutHttpRequest(String url, Map<String, String> headers, 
            Map<? extends Object, ? extends Object> body) {

        try {
            HttpPut request = new HttpPut(url);

            if (headers != null && !headers.isEmpty()) {
                for (Map.Entry<String, String> header : headers.entrySet()) {
                    request.addHeader(header.getKey(), header.getValue());
                }
            }

            if (body != null) {
                request.setEntity(new StringEntity(DataTransferObjectHelper.toJsonString(body)));
            }

            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpResponse res = httpClient.execute(request);
            logger.warn(String.valueOf(res.getStatusLine().getStatusCode()));
            
            httpClient.close();

        } catch (Throwable e) {
        }

    }

    private void createDeleteHttpRequest(String url, Map<String, String> headers) {
        try {

            HttpDelete request = new HttpDelete(url);

            if (headers != null && !headers.isEmpty()) {
                for (Map.Entry<String, String> header : headers.entrySet()) {
                    request.addHeader(header.getKey(), header.getValue());
                }
            }
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            httpClient.execute(request);

            httpClient.close();

        } catch (Throwable e) {
        }
    }

    private Map<String, String> createHeaders() {

        String basicAuthentication = "Basic "
                + new String(Base64.encodeBase64(String.format("%s:%s", username, password).getBytes()));

        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", basicAuthentication);
        headers.put("content-type", "application/json");

        return headers;

    }
}
