package vn.anvui.bsn.beans.miinContract.apartment;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.transaction.Transactional;

import org.apache.commons.lang3.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import vn.anvui.api.amqp.MessageProducer;
import vn.anvui.bsn.beans.miinContract.ContractAbstractService;
import vn.anvui.bsn.beans.product.ProductService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.miinContract.ApartmentContractRequest;
import vn.anvui.bsn.dto.miinContract.MiinContractRequest;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.entity.contract.ApartmentInsuranceInfo;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.CustomerTag;
import vn.anvui.dt.enumeration.UserNotificationType;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.contract.apartment.ApartmentInsuranceInfoRepository;
import vn.anvui.dt.repos.customer.CustomerRepository;
import vn.anvui.dt.utils.DateTimeHelper;

@Service
public class ApartmentContractServiceImpl extends ContractAbstractService<ApartmentContractRequest>
        implements ApartmentContractService {
    final static Logger log = Logger.getLogger(ApartmentContractServiceImpl.class.getSimpleName());
    
    public static final Double TIMECONSTANT[] = {1.0, 0.9, 0.85, 0.8, 0.7};
    public static final Integer PRODUCT_ID = 2;
    public static final Double RATE = 0.001;
    

    @Autowired
    CustomerRepository customerRepo;
    
    @Autowired
    ContractRepository contractRepo;
    
    @Autowired
    ApartmentInsuranceInfoRepository infoRepo;
    
    @Autowired 
    ProductService productService;
    
    @Autowired
    MessageProducer msgProducer;

    @Override
    public Contract createCustomerContract(ApartmentContractRequest req) throws AnvuiBaseException {
        User user = null;
        try {
            AuthenticationDetails userDetail = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            user = userDetail.getAuthenticatedUser();

        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        
        Customer customer = createCustomer(req);
        log.info("customerId: " + customer.getId());
        
        Contract contract = createContract(req, user, customer, ContractStatus.CUSTOMER_CREATED);
        
        sendNotify(contract, UserNotificationType.CUSTOMER);
        
        return contract;
    }

    @Override
    public Contract createAgencyContract(ApartmentContractRequest req) throws AnvuiBaseException {
        User user = null;
        try {
            AuthenticationDetails userDetail = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            user = userDetail.getAuthenticatedUser();

        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        
        Customer customer = createCustomer(req);
        log.info("customerId: " + customer.getId());
        
        ContractStatus status = getStatusFromTxnType(req.getTxnType(), user);
        
        Contract contract = createContract(req, user, customer, status);
        
        sendNotify(contract, UserNotificationType.AGENCY);
        
        return contract;
    }

    @Override
    protected Customer createCustomer(ApartmentContractRequest req) {
        Customer customer = new Customer();
        customer.setCustomerName(WordUtils.capitalizeFully(req.getCustomerName()));
        customer.setPhoneNumber(req.getPhoneNumber());
        customer.setEmail(req.getEmail());
        customer.setAddress(req.getAddress());
        customer.setTag(CustomerTag.DONE.getValue());
        customer.setCreatedDate(new Date());
        
        return customerRepo.save(customer);
    }

    @Override
    protected Contract createContract(ApartmentContractRequest req, User user, Customer customer,
            ContractStatus status) {
        Contract contract = new Contract();
        contract.setInheritCustomer(customer);
        contract.setContractStatus(status.getValue());
        contract.setProductId(PRODUCT_ID);
        contract.setBuyerName(req.getBuyerName());
        contract.setBuyerPhone(req.getBuyerPhone());
        
        if (user != null) {
            contract.setCreatedUserId(user.getId());
            if (user.getCommission() != null)
                contract.setCommission(user.getCommission());
        }
        
        contract.setContractCode(createContractCode(user.getAgencyCode()));
        
        contract = setContractDates(req, contract);
        
        contract.setFee(calculateFee(req.getDuration(), req.getValue()));
        contract.setIncome(contract.getFee() * (1 - contract.getCommission()));
        contract = contractRepo.save(contract);
        
        ApartmentInsuranceInfo info = new ApartmentInsuranceInfo();
        info.setAddress(req.getAddress());
        info.setApartmentValue(req.getValue());
        info.setContract(contract);
        infoRepo.save(info);
        log.info("contractId: " + contract.getId());

        contract.setInfo(info);
       
        customer.setContractId(contract.getId());
        
        customerRepo.save(customer);
        
        return contract;
    }
    
    protected Contract setContractDates(ApartmentContractRequest req, Contract contract) {
        Date currentDate = DateTimeHelper.getFirstTimeOfDay(new Date());
        
        contract.setCreatedTime(new Date());
        contract.setCreatedDate(currentDate);
        contract.setAppliedDate(currentDate);
        contract.setStartDate(currentDate);
        contract.setEndDate(DateTimeHelper.addDay(contract.getStartDate(), "year", req.getDuration()));
        
        return contract;
    }
    
    protected Double calculateFee(Integer duration, Double value) {
        return value * RATE * TIMECONSTANT[duration - 1] * 1.1;
    }

    @Override
    public Contract getContratInfo(Integer id) {
        User user = null;
        try {
            AuthenticationDetails userDetail = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            user = userDetail.getAuthenticatedUser();

        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }

        Contract contract = contractRepo.findOne(id);
        
        if (contract == null) {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_NOT_EXISTED);
        }
        
        if (!contract.getCreatedUserId().equals(user.getId())) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        ApartmentInsuranceInfo info = infoRepo.findOne(contract.getId());
        contract.setInfo(info);
        
        return contract;
    }

    @Override
    protected String createContractCode(String agencyCode) {
        String contractCode = "MIINHOME%07d";
        Integer contractNo = productService.getProductContractNumber(PRODUCT_ID);
        
        return String.format(contractCode, contractNo);
    }

    @Override
    public void sendContractMail(ContractWrapper contract) throws AnvuiBaseException {
        return;
    }
}
