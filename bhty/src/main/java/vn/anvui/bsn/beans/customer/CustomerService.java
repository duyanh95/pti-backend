package vn.anvui.bsn.beans.customer;

import java.util.Date;
import java.util.List;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.crm.ContactAssignRequest;
import vn.anvui.bsn.dto.crm.UpdateTagRequest;
import vn.anvui.bsn.dto.customer.CoupleRequest;
import vn.anvui.bsn.dto.customer.CustomerRequest;
import vn.anvui.bsn.dto.user.UserContactStatus;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.model.ContractWrapper;

public interface CustomerService {
    public boolean contractIsExisted(Integer idContract, String citizenId, String partnerCitizenId) throws AnvuiBaseException;
    
    Customer create(CustomerRequest customer) throws AnvuiBaseException;
    
    Customer updateInfo(Integer id, CustomerRequest customer) throws AnvuiBaseException;
    
    Customer get(Integer id) throws AnvuiBaseException;
    
    ContractWrapper createCouple(CoupleRequest req, ContractStatus status) throws AnvuiBaseException;

    Customer updateCustomerTag(UpdateTagRequest req, Integer customerId) throws AnvuiBaseException;
    
    ContractWrapper createContractFromExistedCustomer(CoupleRequest req, Integer customerId) throws AnvuiBaseException;

    List<Customer> getPotentialCustomerInfo(Integer page, Integer size, String customerName, String email,
            String phoneNumber, Long fromDate, Long toDate, Integer tag, boolean onContract) throws AnvuiBaseException;
    
    int assignContactToUser(ContactAssignRequest req) throws AnvuiBaseException;
    
    ContactAssignRequest countTotalUnassignedContact() throws AnvuiBaseException;
    
    List<UserContactStatus> getAssignedContactStatus(Integer userId, Long fromDate, Long toDate) throws AnvuiBaseException;
    
    void deleteCustomers(Integer id) throws AnvuiBaseException;
    
    public String exportCsv(Date fromDate, Date toDate, boolean notExportedOnly) throws AnvuiBaseException;
    
    public ContractWrapper createAgencyContract(CoupleRequest req) throws AnvuiBaseException;

    public Date checkStartDate(Date startDate);

    public ContractWrapper createContract(Contract contract, Customer inheritCustomer, Customer partner,
            InsurancePackage pack, ContractStatus created, User user, Date startDate);
}
