define({ "api": [
  {
    "type": "post",
    "url": "/login",
    "title": "Login",
    "name": "Authentication",
    "description": "<p>Let user log in to system with username and password</p>",
    "group": "Authentication",
    "version": "0.9.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Username",
            "description": "<p>username</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Password",
            "description": "<p>Password of user encrypted using MD5</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n   \"AV-Auth-Username\": \"tester\",\n   \"AV-Auth-Password\": \"123456dklflfl325435345\"\n}",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "Example POST body:",
        "content": "{\n   \"platformType\": 1, // 1: IOS, 2: ANDROID, 3: WEB, 4: WEB_ADMIN\n   \"platformVersion\": \"1.0\",\n   \"deviceToken\": \"DEVICE PUSH NOTIFICATION TOKEN\",\n   \"deviceId\": \"DEVICE ID\"\n}",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"error\": null,\n    \"responseData\": {\n        \"account\": {\n            \"id\": 1,\n            \"userName\": \"tester\",\n            \"fullName\": \"tester\",\n            \"password\": \"123456dklflfl325435345\",\n            \"salt\": null,\n            \"email\": null,\n            \"phoneNumber\": \"0984055083\",\n            \"stateCode\": 84,\n            \"companyId\": null,\n            \"dob\": null,\n            \"facebookId\": null,\n            \"googleId\": null,\n            \"createdDate\": null,\n            \"gender\": null,\n            \"avatar\": null,\n            \"district\": null,\n            \"address\": null,\n            \"lastLogin\": null,\n            \"notifyOption\": null,\n            \"userType\": 1,\n            \"userStatus\": 1,\n            \"otpCode\": null,\n            \"groupPermissionId\": null,\n            \"educationLevel\": null,\n            \"userReferrerId\": null,\n            \"phoneNumberReferrer\": null,\n            \"role\": \"ROLE_ADMIN\",\n            \"personalRoutingKey\": \"admin.1.#\",\n            \"admin\": true\n        },\n        \"expiredDate\": \"Fri Jun 29 09:51:46 ICT 2018\",\n        \"token\": \"178cc922532f4ac3bd224467ad1383b7\"\n    },\n    \"extraData\": null,\n    \"successMessage\": null,\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/security/AuthenticationFilter.java",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/company",
    "title": "Create company",
    "name": "Create_company",
    "group": "Company",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "companyName",
            "description": "<p>Name of company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "companyCode",
            "description": "<p>Code of company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "companyLogo",
            "description": "<p>Logo URL of company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "address",
            "description": "<p>Address of company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "billAddress",
            "description": "<p>Bill address of company</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "status",
            "description": "<p>Status of company: 0 = NEW, 1 = NORMAL, 2 = LOCKED, 3 = DELETED</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "telecomApiKey",
            "description": "<p>Telecom API key</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "telecomAppId",
            "description": "<p>Telecom App Id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "telecomPhoneNumber",
            "description": "<p>Telecom phone number</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "timeBookHolder",
            "description": "<p>Time to enable holding booking</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "finishTimeToSellTicket",
            "description": "<p>Time to finish selling ticket</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "pastDaysToSellTicket",
            "description": "<p>Number of past days to enable selling ticket</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phoneNumber",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "isSendSms",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "isSendSmsToCustomer",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "isEditTicketPrice",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Double",
            "optional": true,
            "field": "childrenRatio",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example: ",
          "content": "{\n    \"companyName\":\"An Vui\",\n    \"companyCode\":\"AV\",\n    \"companyLogo\": \"URL example\",\n    \"address\": \"35 Lê Văn Lương\",\n    \"billAddress\": \"35 Lê Văn Lương\",\n    \"status\": 1,\n    \"telecomApiKey\": \"alkldll35436366456546\",\n    \"telecomAppId\": \"394025923532554\",\n    \"telecomPhoneNumber\": \"19001911\",\n    \"timeBookHolder\": 0,\n    \"finishTimeToSellTicket\": 0,\n    \"pastDaysToSellTicket\": 10,\n    \"email\": \"info@anvui.vn\",\n    \"phoneNumber\":\"19007034\",\n    \"isSendSms\": true,\n    \"isSendEmail\": false,\n    \"isSendSmsToCustomer\": true,\n    \"isEditTicketPrice\": true,\n    \"childrenRatio\": 1.0\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"id\": 1,\n        \"companyName\": \"An Vui\",\n        \"companyCode\": \"AV\",\n        \"logo\": null,\n        \"address\": \"35 Lê Văn Lương\",\n        \"billAddress\": \"35 Lê Văn Lương\",\n        \"status\": 1,\n        \"telecomApiKey\": \"alkldll35436366456546\",\n        \"telecomAppId\": \"394025923532554\",\n        \"telecomPhoneNumber\": \"19001911\",\n        \"timeBookHolder\": 0,\n        \"finishTimeToSellTicket\": 0,\n        \"pastDaysToSellTicket\": 10,\n        \"email\": \"info@anvui.vn\",\n        \"phoneNumber\": \"19007034\",\n        \"isSendSms\": true,\n        \"isSendEmail\": false,\n        \"isSendSmsToCustomer\": true,\n        \"isEditTicketPrice\": true,\n        \"childrenRatio\": null,\n        \"createdDate\": 1528368370044\n    },\n    \"extraData\": null,\n    \"successMessage\": \"Company is created successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/CompanyController.java",
    "groupTitle": "Company"
  },
  {
    "type": "get",
    "url": "/company",
    "title": "Get company info",
    "name": "Get_company",
    "group": "Company",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "id",
            "description": "<p>Id of company need to get info</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -i https://localhost:8080/anvui/company?id=1",
        "type": "curl"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"id\": 1,\n        \"companyName\": \"An Vui\",\n        \"companyCode\": \"AV\",\n        \"logo\": null,\n        \"address\": \"35 Lê Văn Lương\",\n        \"billAddress\": \"35 Lê Văn Lương\",\n        \"status\": 1,\n        \"telecomApiKey\": \"alkldll35436366456546\",\n        \"telecomAppId\": \"394025923532554\",\n        \"telecomPhoneNumber\": \"19001911\",\n        \"timeBookHolder\": 0,\n        \"finishTimeToSellTicket\": 0,\n        \"pastDaysToSellTicket\": 10,\n        \"email\": \"info@anvui.vn\",\n        \"phoneNumber\": \"19007034\",\n        \"isSendSms\": true,\n        \"isSendEmail\": false,\n        \"isSendSmsToCustomer\": true,\n        \"isEditTicketPrice\": true,\n        \"childrenRatio\": null,\n        \"createdDate\": 1528368370044\n    },\n    \"extraData\": null,\n    \"successMessage\": \"Company is got successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/CompanyController.java",
    "groupTitle": "Company"
  },
  {
    "type": "put",
    "url": "/company",
    "title": "Lock company",
    "name": "Lock_company",
    "group": "Company",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "id",
            "description": "<p>Id of company need to lock</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -i https://localhost:8080/anvui/company?id=1",
        "type": "curl"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": true,\n    \"extraData\": null,\n    \"successMessage\": \"Company is locked successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/CompanyController.java",
    "groupTitle": "Company"
  },
  {
    "type": "put",
    "url": "/company",
    "title": "Unlock company",
    "name": "Unlock_company",
    "group": "Company",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "id",
            "description": "<p>Id of company need to unlock</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -i https://localhost:8080/anvui/company?id=1",
        "type": "curl"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": true,\n    \"extraData\": null,\n    \"successMessage\": \"Company is unlocked successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/CompanyController.java",
    "groupTitle": "Company"
  },
  {
    "type": "put",
    "url": "/company",
    "title": "Update company",
    "name": "Update_company",
    "group": "Company",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "id",
            "description": "<p>Id of company need to update</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "companyName",
            "description": "<p>Name of company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "companyCode",
            "description": "<p>Code of company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "companyLogo",
            "description": "<p>Logo URL of company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "address",
            "description": "<p>Address of company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "billAddress",
            "description": "<p>Bill address of company</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "status",
            "description": "<p>Status of company: 0 = NEW, 1 = NORMAL, 2 = LOCKED, 3 = DELETED</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "telecomApiKey",
            "description": "<p>Telecom API key</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "telecomAppId",
            "description": "<p>Telecom App Id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "telecomPhoneNumber",
            "description": "<p>Telecom phone number</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "timeBookHolder",
            "description": "<p>Time to enable holding booking</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "finishTimeToSellTicket",
            "description": "<p>Time to finish selling ticket</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "pastDaysToSellTicket",
            "description": "<p>Number of past days to enable selling ticket</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phoneNumber",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "isSendSms",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "isSendSmsToCustomer",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "isEditTicketPrice",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Double",
            "optional": true,
            "field": "childrenRatio",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example: ",
          "content": "{\n    \"id\": 1,\n    \"companyName\":\"An Vui\",\n    \"companyCode\":\"AV\",\n    \"companyLogo\": \"URL example\",\n    \"address\": \"35 Lê Văn Lương\",\n    \"billAddress\": \"35 Lê Văn Lương\",\n    \"status\": 1,\n    \"telecomApiKey\": \"alkldll35436366456546\",\n    \"telecomAppId\": \"394025923532554\",\n    \"telecomPhoneNumber\": \"19001911\",\n    \"timeBookHolder\": 0,\n    \"finishTimeToSellTicket\": 0,\n    \"pastDaysToSellTicket\": 10,\n    \"email\": \"info@anvui.vn\",\n    \"phoneNumber\":\"19007034\",\n    \"isSendSms\": true,\n    \"isSendEmail\": false,\n    \"isSendSmsToCustomer\": true,\n    \"isEditTicketPrice\": true,\n    \"childrenRatio\": 1.0\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"id\": 1,\n        \"companyName\": \"An Vui\",\n        \"companyCode\": \"AV\",\n        \"logo\": null,\n        \"address\": \"35 Lê Văn Lương\",\n        \"billAddress\": \"35 Lê Văn Lương\",\n        \"status\": 1,\n        \"telecomApiKey\": \"alkldll35436366456546\",\n        \"telecomAppId\": \"394025923532554\",\n        \"telecomPhoneNumber\": \"19001911\",\n        \"timeBookHolder\": 0,\n        \"finishTimeToSellTicket\": 0,\n        \"pastDaysToSellTicket\": 10,\n        \"email\": \"info@anvui.vn\",\n        \"phoneNumber\": \"19007034\",\n        \"isSendSms\": true,\n        \"isSendEmail\": false,\n        \"isSendSmsToCustomer\": true,\n        \"isEditTicketPrice\": true,\n        \"childrenRatio\": null,\n        \"createdDate\": 1528368370044\n    },\n    \"extraData\": null,\n    \"successMessage\": \"Company is updated successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/CompanyController.java",
    "groupTitle": "Company"
  },
  {
    "type": "post",
    "url": "/department",
    "title": "Create department",
    "name": "Create_department",
    "group": "Department",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "departmentName",
            "description": "<p>Name of department</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyId",
            "description": "<p>Id of company</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example: ",
          "content": "{ \n    \"departmentName\": \"Ph�ng h�nh ch�nh\",\n    \"companyId\": 1 \n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"id\": 1,\n        \"departmentName\": \"Ph�ng h�nh ch�nh\",\n        \"companyId\": 1,\n        \"createdDate\": 1528425917896\n    },\n    \"extraData\": null,\n    \"successMessage\": \"Department is created successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/DepartmentController.java",
    "groupTitle": "Department"
  },
  {
    "type": "delete",
    "url": "/department",
    "title": "Delete department",
    "name": "Delete_department",
    "group": "Department",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "id",
            "description": "<p>id of department need to delete</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -i https://localhost:8080/anvui/department?id=1",
        "type": "curl"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": true,\n    \"extraData\": null,\n    \"successMessage\": \"Department is deleted successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/DepartmentController.java",
    "groupTitle": "Department"
  },
  {
    "type": "get",
    "url": "/department",
    "title": "Get department info",
    "name": "Get_department",
    "group": "Department",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "id",
            "description": "<p>id of department need to get info</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -i https://localhost:8080/anvui/department?id=1",
        "type": "curl"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"id\": 1,\n        \"departmentName\": \"Ph�ng h�nh ch�nh\",\n        \"companyId\": 1,\n        \"createdDate\": 1528425917896\n    },\n    \"extraData\": null,\n    \"successMessage\": \"Department is got successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/DepartmentController.java",
    "groupTitle": "Department"
  },
  {
    "type": "get",
    "url": "/department",
    "title": "Get departments of company",
    "name": "Get_departments_of_company",
    "group": "Department",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyId",
            "description": "<p>id of company need to get departments</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -i https://localhost:8080/anvui/department/list?companId=1",
        "type": "curl"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": [\n        {\n            \"id\": 1,\n            \"departmentName\": \"Ph�ng h�nh ch�nh\",\n            \"companyId\": 1,\n            \"createdDate\": 1528425918000\n        }\n    ],\n    \"extraData\": null,\n    \"successMessage\": \"List of department are got successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/DepartmentController.java",
    "groupTitle": "Department"
  },
  {
    "type": "put",
    "url": "/department",
    "title": "Update department",
    "name": "Update_department",
    "group": "Department",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "id",
            "description": "<p>Id of department need to update</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "departmentName",
            "description": "<p>Name of department</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyId",
            "description": "<p>Id of company</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example: ",
          "content": "{ \n    \"id\": 1,\n    \"departmentName\": \"Ph�ng h�nh ch�nh\",\n    \"companyId\": 1 \n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"id\": 1,\n        \"departmentName\": \"Ph�ng h�nh ch�nh\",\n        \"companyId\": 1,\n        \"createdDate\": 1528425917896\n    },\n    \"extraData\": null,\n    \"successMessage\": \"Department is updated successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/DepartmentController.java",
    "groupTitle": "Department"
  },
  {
    "type": "put",
    "url": "/registration/user/active",
    "title": "Active user",
    "name": "Active_user",
    "group": "Registration",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyId",
            "description": "<p>Id of company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phoneNumber",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "otpCode",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example: ",
          "content": "{\n    \"companyId\": 1,\n    \"phoneNumber\": \"0912345678\",\n    \"otpCode\": \"235019\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"user\": {\n            \"id\": 4,\n            \"userName\": \"testtest\",\n            \"fullName\": \"Test Test\",\n            \"password\": \"123456\",\n            \"email\": \"test@test.com\",\n            \"phoneNumber\": \"0912345678\",\n            \"stateCode\": 84,\n            \"companyId\": 1,\n            \"dob\": null,\n            \"facebookId\": null,\n            \"googleId\": null,\n            \"createdDate\": 1528445345000,\n            \"gender\": null,\n            \"avatar\": null,\n            \"address\": null,\n            \"lastLogin\": null,\n            \"userType\": 1,\n            \"role\": \"ROLE_ADMIN\",\n            \"personalRoutingKey\": \"admin.4.#\",\n            \"admin\": true\n        },\n        \"accessToken\": {\n            \"id\": 6,\n            \"userId\": 4,\n            \"token\": \"54abb2b5205a438ba86641d2f2ec8253\",\n            \"platformType\": null,\n            \"platformVersion\": null,\n            \"deviceToken\": null,\n            \"deviceId\": null,\n            \"expiredDate\": 1531369238524,\n            \"createdDate\": 1528777238524\n        }\n    },\n    \"extraData\": null,\n    \"successMessage\": \"User is actived successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/RegistrationController.java",
    "groupTitle": "Registration"
  },
  {
    "type": "post",
    "url": "/registration/user",
    "title": "Registration user",
    "name": "Registration_user",
    "group": "Registration",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "userName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "password",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "fullName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phoneNumber",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "stateCode",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "userType",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "staffId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "departmentId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyUserStatus",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "platformType",
            "description": "<p>1 = IOS, 2 = ANDROID, 3 = WEB, 4 = WEB_ADMIN</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "deviceId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "deviceToken",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example: ",
          "content": "{\n    \"userName\": \"testtest\",\n    \"password\": \"123456\",\n    \"fullName\": \"Test Test\",\n    \"phoneNumber\": \"0912345678\",\n    \"stateCode\": 84,\n    \"email\": \"test@test.com\",\n    \"userType\": 1,\n    \"companyId\": 1,\n    \"staffId\": 1,\n    \"departmentId\": 1,\n    \"companyUserStatus\": 1,\n    \"platformType\": 1,\n    \"deviceId\": \"329402390223gkskdls\",\n    \"deviceToken\": \"4-2394450dkjfslf56920-1994595\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"id\": 4,\n        \"userName\": \"testtest\",\n        \"fullName\": \"Test Test\",\n        \"password\": \"123456\",\n        \"salt\": null,\n        \"email\": \"test@test.com\",\n        \"phoneNumber\": \"0912345678\",\n        \"stateCode\": 84,\n        \"companyId\": 1,\n        \"dob\": null,\n        \"facebookId\": null,\n        \"googleId\": null,\n        \"createdDate\": 1528445344606,\n        \"gender\": null,\n        \"avatar\": null,\n        \"district\": null,\n        \"address\": null,\n        \"lastLogin\": null,\n        \"notifyOption\": null,\n        \"userType\": 1,\n        \"userStatus\": null,\n        \"otpCode\": null,\n        \"groupPermissionId\": null,\n        \"educationLevel\": null,\n        \"userReferrerId\": null,\n        \"phoneNumberReferrer\": null,\n        \"role\": \"ROLE_ADMIN\",\n        \"personalRoutingKey\": \"admin.4.#\",\n        \"admin\": true\n    },\n    \"extraData\": null,\n    \"successMessage\": \"User is created successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/RegistrationController.java",
    "groupTitle": "Registration"
  },
  {
    "type": "post",
    "url": "/staff",
    "title": "Create staff",
    "name": "Create_staff",
    "group": "Staff",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "staffName",
            "description": "<p>Name of staff</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyId",
            "description": "<p>Id of company</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example: ",
          "content": "{ \n    \"staffName\": \"Nh�n vi�n h�nh ch�nh\",\n    \"companyId\": 1 \n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"id\": 1,\n        \"staffName\": \"Nh�n vi�n h�nh ch�nh\",\n        \"companyId\": 1,\n        \"createdDate\": 1528425917896\n    },\n    \"extraData\": null,\n    \"successMessage\": \"Staff is created successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/StaffController.java",
    "groupTitle": "Staff"
  },
  {
    "type": "delete",
    "url": "/staff",
    "title": "Delete staff",
    "name": "Delete_staff",
    "group": "Staff",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "id",
            "description": "<p>id of staff need to delete</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -i https://localhost:8080/anvui/staff?id=1",
        "type": "curl"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": true,\n    \"extraData\": null,\n    \"successMessage\": \"Staff is deleted successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/StaffController.java",
    "groupTitle": "Staff"
  },
  {
    "type": "get",
    "url": "/staff",
    "title": "Get staff info",
    "name": "Get_staff",
    "group": "Staff",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "id",
            "description": "<p>id of staff need to get info</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -i https://localhost:8080/anvui/staff?id=1",
        "type": "curl"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"id\": 1,\n        \"staffName\": \"Nh�n vi�n h�nh ch�nh\",\n        \"companyId\": 1,\n        \"createdDate\": 1528425917896\n    },\n    \"extraData\": null,\n    \"successMessage\": \"Staff is got successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/StaffController.java",
    "groupTitle": "Staff"
  },
  {
    "type": "get",
    "url": "/staff",
    "title": "Get staffs of company",
    "name": "Get_staffs_of_company",
    "group": "Staff",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyId",
            "description": "<p>id of company need to get staffs</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -i https://localhost:8080/anvui/staff/list?companId=1",
        "type": "curl"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": [\n        {\n            \"id\": 1,\n            \"staffName\": \"Nh�n vi�n h�nh ch�nh\",\n            \"companyId\": 1,\n            \"createdDate\": 1528425918000\n        }\n    ],\n    \"extraData\": null,\n    \"successMessage\": \"List of staff are got successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/StaffController.java",
    "groupTitle": "Staff"
  },
  {
    "type": "put",
    "url": "/staff",
    "title": "Update staff",
    "name": "Update_staff",
    "group": "Staff",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "id",
            "description": "<p>Id of staff need to update</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "staffName",
            "description": "<p>Name of staff</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyId",
            "description": "<p>Id of company</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example: ",
          "content": "{ \n    \"id\": 1,\n    \"staffName\": \"Nh�n vi�n h�nh ch�nh\",\n    \"companyId\": 1 \n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"id\": 1,\n        \"staffName\": \"Nh�n vi�n h�nh ch�nh\",\n        \"companyId\": 1,\n        \"createdDate\": 1528425917896\n    },\n    \"extraData\": null,\n    \"successMessage\": \"Staff is updated successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/StaffController.java",
    "groupTitle": "Staff"
  },
  {
    "type": "post",
    "url": "/user",
    "title": "Create user",
    "name": "Create_user",
    "group": "User",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "userName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "password",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "fullName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phoneNumber",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "stateCode",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "userType",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "staffId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "departmentId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyUserStatus",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "platformType",
            "description": "<p>1 = IOS, 2 = ANDROID, 3 = WEB, 4 = WEB_ADMIN</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "deviceId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "deviceToken",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example: ",
          "content": "{\n    \"userName\": \"testtest\",\n    \"password\": \"123456\",\n    \"fullName\": \"Test Test\",\n    \"phoneNumber\": \"0912345678\",\n    \"stateCode\": 84,\n    \"email\": \"test@test.com\",\n    \"userType\": 1,\n    \"companyId\": 1,\n    \"staffId\": 1,\n    \"departmentId\": 1,\n    \"companyUserStatus\": 1,\n    \"platformType\": 1,\n    \"deviceId\": \"329402390223gkskdls\",\n    \"deviceToken\": \"4-2394450dkjfslf56920-1994595\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"id\": 4,\n        \"userName\": \"testtest\",\n        \"fullName\": \"Test Test\",\n        \"password\": \"123456\",\n        \"salt\": null,\n        \"email\": \"test@test.com\",\n        \"phoneNumber\": \"0912345678\",\n        \"stateCode\": 84,\n        \"companyId\": 1,\n        \"dob\": null,\n        \"facebookId\": null,\n        \"googleId\": null,\n        \"createdDate\": 1528445344606,\n        \"gender\": null,\n        \"avatar\": null,\n        \"district\": null,\n        \"address\": null,\n        \"lastLogin\": null,\n        \"notifyOption\": null,\n        \"userType\": 1,\n        \"userStatus\": null,\n        \"otpCode\": null,\n        \"groupPermissionId\": null,\n        \"educationLevel\": null,\n        \"userReferrerId\": null,\n        \"phoneNumberReferrer\": null,\n        \"role\": \"ROLE_ADMIN\",\n        \"personalRoutingKey\": \"admin.4.#\",\n        \"admin\": true\n    },\n    \"extraData\": null,\n    \"successMessage\": \"User is created successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/UserController.java",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/user/list",
    "title": "Get list of users",
    "name": "Get_list_of_users",
    "group": "User",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "userName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "fullName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phoneNumber",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "stateCode",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "userType",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "staffId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "departmentId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "gender",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "userStatus",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": true,
            "field": "fromDate",
            "description": "<p>format: yyyy-MM-dd</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": true,
            "field": "toDate",
            "description": "<p>format: yyyy-MM-dd</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "pageIndex",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "pageSize",
            "description": ""
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -i https://localhost:8080/anvui/user/list?companyId=1&fromDate=2018-06-01&toDate=2018-06-30&pageIndex=0&pageSize=10",
        "type": "curl"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"totalRecords\": 3,\n        \"listData\": [\n            {\n                \"id\": 4,\n                \"userName\": \"testtest\",\n                \"fullName\": \"Test Test\",\n                \"password\": \"123456\",\n                \"salt\": null,\n                \"email\": \"test@test.com\",\n                \"phoneNumber\": \"0912345678\",\n                \"stateCode\": 84,\n                \"companyId\": 1,\n                \"dob\": null,\n                \"facebookId\": null,\n                \"googleId\": null,\n                \"createdDate\": 1528445345000,\n                \"gender\": null,\n                \"avatar\": null,\n                \"district\": null,\n                \"address\": null,\n                \"lastLogin\": null,\n                \"notifyOption\": null,\n                \"userType\": 1,\n                \"userStatus\": null,\n                \"otpCode\": null,\n                \"groupPermissionId\": null,\n                \"educationLevel\": null,\n                \"userReferrerId\": null,\n                \"phoneNumberReferrer\": null,\n                \"role\": \"ROLE_ADMIN\",\n                \"personalRoutingKey\": \"admin.4.#\",\n                \"admin\": true\n            }\n        ]\n    },\n    \"extraData\": null,\n    \"successMessage\": \"List of users are got successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/UserController.java",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/user",
    "title": "Get user info",
    "name": "Get_user_info",
    "group": "User",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "userId",
            "description": ""
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -i https://localhost:8080/anvui/user?companyId=1&userId=1",
        "type": "curl"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"user\": {\n            \"id\": 4,\n            \"userName\": \"testtest\",\n            \"fullName\": \"Test Test\",\n            \"password\": \"123456\",\n            \"salt\": null,\n            \"email\": \"test@test.com\",\n            \"phoneNumber\": \"0912345678\",\n            \"stateCode\": 84,\n            \"companyId\": 1,\n            \"dob\": null,\n            \"facebookId\": null,\n            \"googleId\": null,\n            \"createdDate\": 1528445345000,\n            \"gender\": null,\n            \"avatar\": null,\n            \"district\": null,\n            \"address\": null,\n            \"lastLogin\": null,\n            \"notifyOption\": null,\n            \"userType\": 1,\n            \"userStatus\": null,\n            \"otpCode\": null,\n            \"groupPermissionId\": null,\n            \"educationLevel\": null,\n            \"userReferrerId\": null,\n            \"phoneNumberReferrer\": null,\n            \"role\": \"ROLE_ADMIN\",\n            \"personalRoutingKey\": \"admin.4.#\",\n            \"admin\": true\n        },\n        \"company\": {\n            \"id\": 1,\n            \"companyName\": \"An Vui\",\n            \"companyCode\": \"AV\",\n            \"logo\": null,\n            \"address\": \"35 Lê Văn Lương\",\n            \"billAddress\": \"35 Lê Văn Lương\",\n            \"status\": 1,\n            \"telecomApiKey\": \"alkldll35436366456546\",\n            \"telecomAppId\": \"394025923532554\",\n            \"telecomPhoneNumber\": \"19001911\",\n            \"timeBookHolder\": 0,\n            \"finishTimeToSellTicket\": 0,\n            \"pastDaysToSellTicket\": 10,\n            \"email\": \"info@anvui.vn\",\n            \"phoneNumber\": \"19007034\",\n            \"isSendSms\": true,\n            \"isSendEmail\": false,\n            \"isSendSmsToCustomer\": true,\n            \"isEditTicketPrice\": true,\n            \"childrenRatio\": null,\n            \"createdDate\": 1528368370000\n        },\n        \"companyUser\": {\n            \"id\": 1,\n            \"companyId\": 1,\n            \"userId\": 4,\n            \"staffId\": 1,\n            \"departmentId\": 1,\n            \"status\": null,\n            \"createdDate\": 1528445345000\n        },\n        \"staff\": {\n            \"id\": 1,\n            \"staffName\": \"Nhân viên hành chính\",\n            \"companyId\": 1,\n            \"createdDate\": 1528425918000\n        },\n        \"department\": null\n    },\n    \"extraData\": null,\n    \"successMessage\": \"User is got successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/UserController.java",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/user/lock",
    "title": "Lock user",
    "name": "Lock_user",
    "group": "User",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "userId",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example: ",
          "content": "{\n    \"companyId\": 1,\n    \"userId\": 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": true,\n    \"extraData\": null,\n    \"successMessage\": \"User is locked successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/UserController.java",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/user/unlock",
    "title": "Unlock user",
    "name": "Unlock_user",
    "group": "User",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "userId",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example: ",
          "content": "{\n    \"companyId\": 1,\n    \"userId\": 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": true,\n    \"extraData\": null,\n    \"successMessage\": \"User is unlocked successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/UserController.java",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/user",
    "title": "Update user",
    "name": "Update_user",
    "group": "User",
    "version": "0.1.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "AV-Auth-Token",
            "description": "<p>Authentication Token get from Login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example: ",
          "content": "{ \n    \"AV-Auth-Token\": \"b7bef6f9ccfe48f892975b0f229c94d6\" \n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "userName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "password",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "fullName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phoneNumber",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "stateCode",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "userType",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "staffId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "departmentId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "companyUserStatus",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "platformType",
            "description": "<p>1 = IOS, 2 = ANDROID, 3 = WEB, 4 = WEB_ADMIN</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "deviceId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "deviceToken",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example: ",
          "content": "{\n    \"id\": 4,\n    \"userName\": \"testtest\",\n    \"password\": \"123456\",\n    \"fullName\": \"Test Test\",\n    \"phoneNumber\": \"0912345678\",\n    \"stateCode\": 84,\n    \"email\": \"test@test.com\",\n    \"userType\": 1,\n    \"companyId\": 1,\n    \"staffId\": 1,\n    \"departmentId\": 1,\n    \"companyUserStatus\": 1,\n    \"platformType\": 1,\n    \"deviceId\": \"329402390223gkskdls\",\n    \"deviceToken\": \"4-2394450dkjfslf56920-1994595\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{\n    \"error\": null,\n    \"responseData\": {\n        \"id\": 4,\n        \"userName\": \"testtest\",\n        \"fullName\": \"Test Test\",\n        \"password\": \"123456\",\n        \"salt\": null,\n        \"email\": \"test@test.com\",\n        \"phoneNumber\": \"0912345678\",\n        \"stateCode\": 84,\n        \"companyId\": 1,\n        \"dob\": null,\n        \"facebookId\": null,\n        \"googleId\": null,\n        \"createdDate\": 1528445344606,\n        \"gender\": null,\n        \"avatar\": null,\n        \"district\": null,\n        \"address\": null,\n        \"lastLogin\": null,\n        \"notifyOption\": null,\n        \"userType\": 1,\n        \"userStatus\": null,\n        \"otpCode\": null,\n        \"groupPermissionId\": null,\n        \"educationLevel\": null,\n        \"userReferrerId\": null,\n        \"phoneNumberReferrer\": null,\n        \"role\": \"ROLE_ADMIN\",\n        \"personalRoutingKey\": \"admin.4.#\",\n        \"admin\": true\n    },\n    \"extraData\": null,\n    \"successMessage\": \"User is updated successfully.\",\n    \"warningMessage\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apilayer/src/main/java/vn/anvui/api/rest/controller/UserController.java",
    "groupTitle": "User"
  }
] });
