#!/bin/bash
set -x
DATE=`date '+%Y-%m-%d-%H-%M'`
WAR_FILE='pti-'$DATE'.war'
ROOT='root@103.56.158.205'
COMMAND=/root/command.sh
TEMP='/tmp/'
TARGET='/data/tmp/'

if [ $1 = 'test' ]
then
  ROOT='duyanh.tran@35.187.229.128'
  TARGET='/root/data/test/'
  COMMAND=/root/data/test/test.sh
elif [ $1 = 'dev' ]
then
  :
else
  exit 1
fi


(cd bhty && \
gradle build -Pver=$DATE) && \

cd bhty/build/libs/  && \
echo $PWD && \

scp $WAR_FILE $ROOT':'$TEMP && \
ssh $ID_RSA $ROOT "sudo mv $TEMP$WAR_FILE $TARGET && sudo bash $COMMAND $TARGET$WAR_FILE"
